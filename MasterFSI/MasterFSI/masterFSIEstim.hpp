//===========================================================
//                    MasterFSI
//
// Part of MasterFSIContact,             Copyright Inria 2018
//===========================================================
/*!
 \file masterFSIEstim.hpp
 \author Jean-Frederic Gerbeau, Philippe Moireau
 */
#ifndef  _MASTERFSIESTIM_HPP
#define  _MASTERFSIESTIM_HPP
#include  <iostream>
#include  <stdio.h>
#include  <string>
#include  <fstream>

#include "masterFSI.hpp"


#if ESTIMATION == 1


#include  "messageAdmWorkerSEIK.hpp"

/*!
 \class MasterFSI
 \brief Master class
 
 \c MasterFSIEstim is the class handling the FSI algorithm in estimation mode 
 
 \author  Jean-Frederic Gerbeau, Philippe Moireau
 */

class MasterFSIEstim:public MasterFSI{
	//! Pointer to the parameters defined by the data file
	int _protocol_seik;
	int _dimState;
	int _dimParam;
	int _dimObserv;
	int _dimParamFluid;
	int _dimParamStruct;
	int _dimObservFluid;
	int _dimObservStruct;
	int _flagseik_output;
	int _flagseik_loop;
	int _flagseik_msg;
	Vector _state;
	Vector _param;
	Vector _innov;

	double* _paramFluid;
	double* _paramStruct;
	double* _innovFluid;
	double* _innovStruct;

	Vector _lumpedInvCovObserv;
	double* _lumpedInvCovObservFluid;
	double* _lumpedInvCovObservStruct;
	Matrix _invCovParam;
	Matrix _invCovParamFluid;
	Matrix _invCovParamStruct;
	
	MessageAdmWorkerSEIK* _msgAdmSEIK;	

public:
	MasterFSIEstim(ParamFSI* paramDataFile,MessageAdmMasterFSI* msgAdm,OperInterf* operInterf,double time);
	void firstContactWithFluid();	
	void firstContactWithStruct();
	void initializeEstim();
	void iterate(int iter,double dt);
	void mergeFluidStructToState();
	void splitStateToFluidStruct();
	bool stopIterate(int iter);	
	int rank(){return _msgAdmSEIK->rank();};
};
#endif


#endif
