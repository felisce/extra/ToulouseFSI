/*!
 \file main.cpp
 \author Jean-Frederic Gerbeau
 */
#include "paramFSI.hpp"
#include "operInterfDirichletNeumann.hpp"
#include "messageAdmMasterFSI.hpp"
#include "masterFSI.hpp"
#include "masterFSIEstim.hpp"
#include "GetPot.hpp"
//===========================================================
//
//                    MasterFSI
//
//===========================================================
// $Id: main.cpp 167 2010-10-21 13:26:27Z gerbeau $ 
#include "ensight.hpp"


using namespace std;

int main(int argc,char *argv[])
{
	GetPot command_line(argc,argv);
	string data_file_name = command_line.follow("input_master", 2,
													 "-f","--file");
	GetPot data_file(data_file_name.c_str());
	if( command_line.search(2, "-i","--info") ) {
		data_file.print();
		exit(0);
	}
	
	int masterSendInitialState=0;
	int masterRcvInitialState = 0;
	int masterRcvFinalState = 0;
	
	ParamFSI* paramDataFile = new ParamFSI(data_file);
	
	if(paramDataFile->initializationType) masterSendInitialState=1;

	OperInterfDirichletNeumann* operInterf = new OperInterfDirichletNeumann(paramDataFile);
	
	MessageAdmMasterFSI* msgAdm = new MessageAdmMasterFSI(argc,argv,paramDataFile,operInterf,
														  masterSendInitialState,
														  masterRcvInitialState,
														  masterRcvFinalState);
	
	MasterFSI* master;
	
	double init_time;
	
	if(paramDataFile->initializationType != 0) init_time = paramDataFile->initTime;
	else init_time = 0.;
#if ESTIMATION == 1
	if(paramDataFile->protocol==3){
		master = new MasterFSIEstim(paramDataFile,msgAdm,operInterf,init_time);
	} else{
		master = new MasterFSI(paramDataFile,msgAdm,operInterf,init_time);	
	}
#else
	master = new MasterFSI(paramDataFile,msgAdm,operInterf,init_time);	
#endif
	operInterf->setMessageAdm(msgAdm);
	double dt = paramDataFile->timeStep;
	msgAdm->spawnFluidStruct( master->rank() );
	msgAdm->readFSINodesFiles();
	operInterf->initialize();	
	master->initialize();
	
	master->firstContactWithFluid();
	master->firstContactWithStruct();

	master->initializeEstim(); // \todo : remove virtual function in base class, use a dynamic_cast instead
	
	int iter = 0;
	
	do{
		iter++;
		master->iterate(iter,dt);
	}while( !( master->stopIterate(iter) ) );	
	
	master->lastContactWithFluid();
	master->lastContactWithStruct();
	
	msgAdm->finalize();

	cout << "Normal end of MasterFSI 4.0" <<endl;
	return 0;
}
