//===========================================================
//                    MasterFSI
//
// Part of MasterFSIContact,             Copyright Inria 2018
//===========================================================
/*!
 \file fixedPointInterf.hpp
 \author Jean-Frederic Gerbeau
 */

#ifndef _FIXEDPOINTINTERF_HPP
#define _FIXEDPOINTINTERF_HPP

#include "vectormatrix.hpp"
#include "operInterf.hpp"

/*!
 \class FixedPointInterf
 
 Compute the fixed point of an operator T
 
 
 
 */	
class FixedPointInterf{
	int _verbose;
	Vector _x0;
	Vector _fx0;
	Vector _x1;
	Vector _fx1;
	Vector _tmp;
	OperInterf* _oper;
public:
	Vector& x0(){return _x0;};
	Vector& fx0(){return _fx0;};
	Vector& x1(){return _x1;};
	Vector& fx1(){return _fx1;};
//	Vector& x1(){return _oper->structInterfDisp();};
//	Vector& fx1(){return _oper->resultStructInterfDisp();};
	Vector& tmp(){return _tmp;};
  void eval(int flag, double& eps_ext);
	FixedPointInterf(int verbose,OperInterf& oper);
	int solve(double abstol,double reltol,int& maxiter,int algo,double omega);
};
#endif
