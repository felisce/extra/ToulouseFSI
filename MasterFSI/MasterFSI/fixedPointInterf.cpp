//===========================================================
//                    MasterFSI
//
// Part of MasterFSIContact,             Copyright Inria 2018
//===========================================================
/*!
 \file fixedPointInterf.cpp
 \author Jean-Frederic Gerbeau
 */

#include "fixedPointInterf.hpp"
#include "picard.hpp"

FixedPointInterf::FixedPointInterf(int verbose,OperInterf& oper):
_verbose(verbose),_oper(&oper){
	int dim = _oper->nbInterfStructDOF();
	_x0.resize(2*dim);
	_fx0.resize(2*dim);
	_x1.resize(2*dim);
	_fx1.resize(2*dim);
	_tmp.resize(2*dim);
	subrange(_x1,0,dim) = _oper->structInterfDisp();
	subrange(_x1,dim,2*dim) = _oper->structInterfVelo();
}

void FixedPointInterf::eval(int flag, double& eps_ext){ 
	int dim = _oper->nbInterfStructDOF();
	_oper->structInterfDisp() = subrange(_x1,0,dim);
	_oper->structInterfVelo() = subrange(_x1,dim,2*dim);
	_oper->compute(flag, eps_ext); 

	/* After ->compute(..) we put in _x1 the new values of fluid [disp | vel],
	   in order to be capable to evaluate the residual using |x1-fx1| with Aitken
	   for the Robin-Neumann case.  */
	if (eps_ext>0){
	  subrange(_x1,0,dim) = _oper->structInterfDisp();
	  subrange(_x1,dim,2*dim) = _oper->structInterfVelo();
	}
	
	subrange(_fx1,0,dim) = _oper->resultStructInterfDisp();
	subrange(_fx1,dim,2*dim) = _oper->resultStructInterfVelo();
}

int FixedPointInterf::solve(double abstol,double reltol,int& maxiter,int algo,double omega){
  //int cvg = picard(this,_tmp,abstol,reltol,maxiter,1,omega,_verbose);
  int cvg = picard(this,_tmp,abstol,reltol,maxiter,algo,omega,_verbose);
	_oper->structInterfDisp() = _oper->resultStructInterfDisp(); 
	/* If the fixed point has been performed on the displacement, the previous line 
	 is useless (done at the end of the function "picard", but the following one 
	 is necessary to affect the last value of the velocity.
	 It's the contrary if the fixed point is performed on the velocity. That's why I
	 prefer to keep the 2 lines */
	_oper->structInterfVelo() = _oper->resultStructInterfVelo();
	return cvg;
}
