//===========================================================
//                    MasterFSI
//
// Part of MasterFSIContact,             Copyright Inria 2018
//===========================================================
/*!
 \file masterFSI.hpp
 \author Jean-Frederic Gerbeau
 */
#ifndef  _MASTERFSI_HPP
#define  _MASTERFSI_HPP
#include  <iostream>
#include  <stdio.h>
#include  <string>
#include  <fstream>

#include "paramFSI.hpp"
#include "messageAdmMasterFSI.hpp"
#include "operInterf.hpp"
#include "vectormatrix.hpp"
#include "fixedPointInterf.hpp"
#include "newtonInterf.hpp"

/*!
 \class MasterFSI
 \brief Master class
 
 \c MasterFSI is the class handling the FSI algorithm 
 
 \author  Jean-Frederic Gerbeau
 */

class MasterFSI{
protected:
	//! Pointer to the parameters defined by the data file
	ParamFSI* _paramDataFile;
	//! Pointer to the message administrator
	MessageAdmMasterFSI* _msgAdm;
	//! Pointer to the interface operator
	OperInterf* _operInterf;
	//! Total number of fluid nodes (in the volume).
	int _nbFluidNode;
	//! Number of fluid DOF per node (in the volume). 
	int _nbFluidDOFPerNode;
	//! Total number of fluid DOF (in the volume) = _nbFluidNode * _nbFluidDOFPerNode. For the interface DOF see the class \c OperInterf.
	int _nbFluidDOF;
	//! Total number of Windkessel DOF
	int _nbWindkessel;
	//! Total number of conform struct nodes (in the volume). 
	int _nbConfStructNode;
	//! Number of conform struct DOF per node (in the volume). 
	int _nbConfStructDOFPerNode;
	//! Total number of immersed struct DOF (in the volume). For the interface DOF see the class \c OperInterf.
	int _nbConfStructDOF;
	//!Total number of immersed struct nodes (in the volume). 
	int _nbImStructNode;
	//! Number of immersed struct DOF per node (in the volume). 
	int _nbImStructDOFPerNode;
	//! Total number of struct DOF (in the volume). For the interface DOF see the class \c OperInterf.
	int _nbImStructDOF;
	//! Total number of crack struct nodes (in the volume). 
	int _nbCrackStructNode;
	//! Number of crack struct DOF per node (in the volume). 
	int _nbCrackStructDOFPerNode;
	//! Total number of crack struct DOF (in the volume). For the interface DOF see the class \c OperInterf.
	int _nbCrackStructDOF;
	//! Displacement conform struct (in the volume)
	Vector _dispConfStruct;
	//! Velocity conform struct (in the volume)
	Vector _veloConfStruct;
	//! Acceleration conform struct (in the volume)
	Vector _accelConfStruct;
	//! Displacement immersed struct (in the volume)
	Vector _dispImStruct;
	//! Velocity immersed struct (in the volume)
	Vector _veloImStruct;
	//! Acceleration immersed struct (in the volume)
	Vector _accelImStruct;
	//! Displacement crack struct (in the volume)
	Vector _dispCrackStruct;
	//! Velocity crack struct (in the volume)
	Vector _veloCrackStruct;
	//! Accel crack struct (in the volume)
	Vector _accelCrackStruct;
	//! Displacement fluid (in the volume)
	Vector _dispFluid;
	//! Velocity fluid (in the volume)
	Vector _veloFluid;
	//! Windkessel state variables
	Vector _varWindkessel;
	/*!
		Fixed point algorithm
	 */	
	FixedPointInterf* _fixedPoint;
	/*! 
	 Newton algorithm
	 */
	NewtonInterf* _newton;
	/*! 
	 Time
	 */
	double _time;
    int _nbTimeStepReduction;
public:
	MasterFSI(ParamFSI* param,MessageAdmMasterFSI* msgAdm,OperInterf* operInterf,double time);
	virtual ~MasterFSI(){};
	
	//! Initialize the sensitivityState to zero and the sensitivityParam to identity
	void initialize();
	int nbFluidDOF() const {return _nbFluidDOF;};
	int nbStructDOF() const {return _nbConfStructDOF + _nbImStructDOF + _nbCrackStructDOF;};
	//
	virtual void initializeEstim();
	virtual void firstContactWithFluid();	
	virtual void firstContactWithStruct();	
	virtual void lastContactWithFluid();	
	virtual void lastContactWithStruct();	
	//! Iterate: Perform one time iteration of the workers without estimation
	virtual void iterate(int iter,double dt);	
	virtual bool stopIterate(int iter);
	virtual int rank(){return 0;}
};
#endif
