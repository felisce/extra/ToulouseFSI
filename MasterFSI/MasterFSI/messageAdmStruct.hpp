//===========================================================
//                    MasterFSI
//
// Part of MasterFSIContact,             Copyright Inria 2018
//===========================================================
/*!
 \file messageAdmStruct.hpp
 \author Jean-Frederic Gerbeau
 */
#ifndef _MESSAGEADMSTRUCT_HPP
#define _MESSAGEADMSTRUCT_HPP

#include "messageAdmFSI.hpp"


using namespace std;

/*!
 \class MessageAdmStruct
 \brief Messages Administrator for the struct solver
 
 \c MessageAdmStruct is the class managing the messages for the struct solver
 
 \author  Jean-Frederic Gerbeau
 */

class MessageAdmStruct:public MessageAdmFSI
{
#ifdef MSG_PVM
  int _masterId;
#endif
#ifdef MSG_ZMQ
  string _socketTransport; // e.g. tcp
  string _socketAddress; // e.g. 127.0.0.1 (localhost)
  string _socketPort; // e.g. 5556
  zmq::context_t _zmq_context;
  zmq::socket_t _socket;
#endif
public:
#ifdef MSG_PVM
  MessageAdmStruct(int argc, char *argv[],const int& verbose,const int& protocol,int unit);
  int masterId(){return _masterId;}
#endif
#ifdef MSG_ZMQ
  MessageAdmStruct(int argc, char *argv[],const int& verbose,const int& protocol,int unit,string socketTransport,string socketAddress,string socketPort);
#endif
	void receiveInitializationFromMaster();
	void sendInitializationToMaster(int nbNode,int nbDofPerNode);
	void sendStateToMaster(int nbNode,int nbDofPerNode,double* disp,double* vel,double* accel);
	void receiveStateFromMaster(int& nbNode,int& nbDofPerNode,double& time,double* disp,double* vel,double* accel);
	void sendInterfDispVelToMaster(int dim,double* dispStruct,double* veloStruct,double scalar=0.);
	void receiveInterfForceFromMaster(int& status, double& dt,int& dim,double* force);
	
	void sendInitializationEstimToMaster(int dim_param,int dim_observ);
	void receiveStateParamFromMaster(int& dim,double* disp,double* vel,double* accel,int dim_param,double* param,double& dt,
                                     int& flag_loop,int& flag_msg,int& rank);
	void sendStateParamInnovToMaster(int dim,double* disp,double* vel,double* accel,int dim_param,double* param,
									 int dim_innov,double* innov,int flag_output);
	void sendInitStateParamToMaster(int dim,double* disp, double* vel,double* accel, int dim_param, double* param, 
                                    double* invCovParam,int dim_observ,double* lumpedInvCovObserv);
};

#endif
