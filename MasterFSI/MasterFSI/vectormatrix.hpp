//===========================================================
//                    MasterFSI
//
// Part of MasterFSIContact,             Copyright Inria 2018
//===========================================================
/*!
 \file vectormatrix.hpp
 \author Jean-Frederic Gerbeau
 */
#ifndef _VECTOR_MATRIX_HPP
#define _VECTOR_MATRIX_HPP

#include <boost/numeric/ublas/vector.hpp>
#include <boost/numeric/ublas/matrix.hpp>
#include <boost/numeric/ublas/io.hpp>
#include <boost/numeric/ublas/triangular.hpp>
#include <boost/numeric/ublas/matrix_proxy.hpp>
#include <boost/numeric/ublas/vector_proxy.hpp>
#include <boost/numeric/ublas/storage.hpp>
typedef boost::numeric::ublas::vector<double> Vector;
typedef boost::numeric::ublas::vector_range<Vector>  VectorRange;
typedef boost::numeric::ublas::vector<int> VectorInt;
typedef boost::numeric::ublas::triangular_matrix<double, boost::numeric::ublas::lower> LowerTriMatrix;
typedef boost::numeric::ublas::matrix<double> Matrix;
typedef boost::numeric::ublas::matrix_column< Matrix > MatrixColumn;
typedef boost::numeric::ublas::matrix_row< Matrix > MatrixRow;
typedef boost::numeric::ublas::range Range;

#endif
