//===========================================================
//                    MasterFSI
//
// Part of MasterFSIContact,             Copyright Inria 2018
//===========================================================
/*!
 \file utilstring.hpp
 \author Jean-Frederic Gerbeau
 */

#ifndef UTILSTRING_HPP
#define UTILSTRING_HPP

#include <string>
#include <vector>

using namespace std;

/*
 #ifndef DEBUG
#define assert(x)
#else
#define assert(x) \
if (! (x)) \
{ \
cout << "ERROR: Assert " << #x << " failed\n"; \
cout << " on line " << __LINE__  << "\n"; \
cout << " in file " << __FILE__ << "\n";  \
}
#endif
*/
extern char* c_string( string& s);
extern void StringSplit(string str, string delim, vector<string>& results);
extern int CountOccurenceChar(string str,char c);
extern string ReplaceWilcardByInt(string strWildcard,int i,char c);
extern void printArray(int verbose, ostream& fout,string title,double* tab,int dim);
#endif
