//===========================================================
//                    MasterFSI
//
// Part of MasterFSIContact,             Copyright Inria 2018
//===========================================================
/*!
 \file operInterfDirichletNeumann.cpp
 \author Jean-Frederic Gerbeau
 */

#include "operInterfDirichletNeumann.hpp"
#include "messageAdmMasterFSI.hpp"


/*----------------------------------------------------------*/

OperInterfDirichletNeumann::OperInterfDirichletNeumann(ParamFSI* _paramDataFile):
OperInterf(_paramDataFile)
{}

//void OperInterfDirichletNeumann::eval(Vector& SoFvec1,Vector& SoFvec2,const Vector& vec,int flag)
void OperInterfDirichletNeumann::compute(int flag, double& eps_ext) 
{
	//_structInterfDisp and _structInterfVelo must have been updated before calling this function 
	const double& dt= _paramDataFile->timeStep;
	if(flag==1) {
		_nbEval = 0; // new time step
		_nbEvalTangent = 0; // new time step
	}
	if(flag == 0 || flag == 1) _nbEval++;
	if(flag == 2) {
		cout << "Deprecated use of OperInterfDirichletNeumann::compute. Call computeTangent instead " << endl;
		exit(1);
	}
	if(_paramDataFile->verbose>1 && (flag == 0 || flag == 1)) cout << "Eval " << _nbEval  << endl << flush;
	// Interpolation of structure displacement and velocity on the fluid interface  	
	structInterfToFluidInterf(_structInterfDisp,_fluidInterfDisp);	
	structInterfToFluidInterf(_structInterfVelo,_fluidInterfVelo);
	// Send disp and velo to the fluid 
	_msgAdm->sendDispVelToFluid(_fluidInterfDisp.data().begin(),_fluidInterfVelo.data().begin(),dt,flag,_nbInterfFluidDOF);
	
	// Receive force from the fluid
	if( flag != 2 && _paramDataFile->flagInitialStressCorrection){
		_msgAdm->receiveFromFluidWithStressCorrection(_fluidInterfForce.data().begin(),_fluidInterfInitialForce.data().begin(),_nbInterfFluidDOF);				
	}else{
	  if (_paramDataFile->RobinInterfBC==0){
		// Fluid sends the Force only
		_msgAdm->receiveFromFluid(_fluidInterfForce.data().begin(),_nbInterfFluidDOF);
	  } else{
		// Fluid sends the Force, Displacement and Velocity to the MasterFSI
		_msgAdm->receiveForceDispVelFromFluid(_fluidInterfForce.data().begin(),_fluidInterfDisp.data().begin(),_fluidInterfVelo.data().begin(),_nbInterfFluidDOF);
		/* Here we copy the first 3 d.o.f. of fluidDisp,Velo to the arrays structDisp,Velo.
		   The reason is because this allow us to put these values on x1, in Picard.hpp */
		fluidDispVelInterfToStructInterf(_fluidInterfDisp,_structInterfDisp);
		fluidDispVelInterfToStructInterf(_fluidInterfVelo,_structInterfVelo);
	  }
	}
    
	// Interpolation of fluid force on structure interface 
	fluidInterfToStructInterf(_fluidInterfForce,_structInterfForce);
	if(_nbNodeFSIConf){
		// Send force to the conform structure
		_msgAdm->sendToStructConf(_structInterfForce.data().begin(),dt,flag,_nbInterfConfStructDOF);
		// Receive disp and velo from the conform structure 
		_msgAdm->receiveFromStructConf(_resultStructInterfDisp.data().begin(),_resultStructInterfVelo.data().begin(),_nbInterfConfStructDOF);
	}
	if(_nbNodeFSIIm){
		// Send force to the immersed structure 
		_msgAdm->sendToStructIm(_structInterfForce.data().begin(),dt,flag,_nbInterfImStructDOF);
		// Receive disp and velo from the immersed structure 
		_msgAdm->receiveFromStructIm(_resultStructInterfDisp.data().begin(),_resultStructInterfVelo.data().begin(),_nbInterfImStructDOF);
	}
	if(_nbNodeFSICrack){
		// Send force on the 'crack' structure 
		_msgAdm->sendToStructCrack(_structInterfForce.data().begin(),dt,flag,_nbInterfCrackStructDOF);
		// Receive disp and velo from the 'crack' structure 
		_msgAdm->receiveFromStructCrack(_resultStructInterfDisp.data().begin(),_resultStructInterfVelo.data().begin(),_nbInterfCrackStructDOF);
	}
    
    // Convergence criteria (Picard) for Robin-Neumann Interface
	if (_paramDataFile->RobinInterfBC){
	  /* Here one can compute:
		 eps_ext = ||disp_S - disp_F|| + ||vel_S - vel_F||
		 But, we cannot use the Aitken Algorithm, so we prefer to copy fluidInterfDisp|Velo
		 in x1. With this we avoid major modifications in picard.hpp (we should also need
		 to replace residual by eps_ext after the evaluation of ->compute(..) function)
	  */
	  //eps_ext=0;
	  for(int i=0;i<_nbNodeFSIConf+_nbNodeFSIIm+_nbNodeFSICrack;i++){
		/* // COPY in the first three d.o.f.
		for(int icoor=0;icoor<3;icoor++){
		  eps_ext += pow(_fluidInterfDisp[3*i + icoor] - _resultStructInterfDisp[5*i + icoor],2);
		  eps_ext += pow(_fluidInterfVelo[3*i + icoor] - _resultStructInterfVelo[5*i + icoor],2);
		}
		*/
		/* The d.o.f. 4 and 5 are copied to do not mix two different criteria, that of
		   |(ds,vs)_k-(ds,vs)_k-1| and |(ds,vs)-(df,vf)| */
	    // WARNING: Robin-Neumann iterations 	
	    // comment this lines when coupling with Felisce
	    for(int icoor=3;icoor<5;icoor++){
	      _structInterfDisp[5*i + icoor] = _resultStructInterfDisp[5*i + icoor];
	      _structInterfVelo[5*i + icoor] = _resultStructInterfVelo[5*i + icoor];
	    }
	  }
	  //eps_ext = sqrt(eps_ext); // to be a good norm!
	  eps_ext = 1.0;
	}
	else
	  eps_ext = -1.0;
    
	if(_paramDataFile->verbose>1 && (flag == 0 || flag == 1)) cout << "Eval done."  << endl << flush;
}

void OperInterfDirichletNeumann::computeTangent(const Vector& vec){
  	_structInterfDispTangent = subrange(vec,0,_nbInterfStructDOF);
	_structInterfVeloTangent = subrange(vec,_nbInterfStructDOF,2*_nbInterfStructDOF);
	int flag = 2; // to solve the tangent problems
	const double& dt= _paramDataFile->timeStep;
	_nbEvalTangent++ ;
	if(_paramDataFile->verbose>1) cout << "Eval Tangent " << _nbEvalTangent  << endl << flush;
	// Interpolation of structure fields on the fluid interface
	structInterfToFluidInterf( _structInterfDispTangent, _fluidInterfDisp);
	structInterfToFluidInterf( _structInterfVeloTangent, _fluidInterfVelo);
	_msgAdm->sendDispVelToFluid(_fluidInterfDisp.data().begin(),_fluidInterfVelo.data().begin(),dt,flag,_nbInterfFluidDOF);
	// Receive force from the fluid
	_msgAdm->receiveFromFluid(_fluidInterfForce.data().begin(),_nbInterfFluidDOF);
	// Interpolation of fluid force on structure interface 
	fluidInterfToStructInterf(_fluidInterfForce,_structInterfForce);
	if(_nbNodeFSIConf){
		// Send force to the conform structure
		_msgAdm->sendToStructConf(_structInterfForce.data().begin(),dt,flag,_nbInterfConfStructDOF);
		// Receive disp and velo from the conform structure 
		_msgAdm->receiveFromStructConf(_resultStructInterfDispTangent.data().begin(),_resultStructInterfVeloTangent.data().begin(),_nbInterfConfStructDOF);
	}
	if(_nbNodeFSIIm){
		// Send force to the immersed structure 
		_msgAdm->sendToStructIm(_structInterfForce.data().begin(),dt,flag,_nbInterfImStructDOF);
		// Receive disp and velo from the immersed structure 
		_msgAdm->receiveFromStructIm(_resultStructInterfDispTangent.data().begin(),_resultStructInterfVeloTangent.data().begin(),_nbInterfImStructDOF);
	}
	if(_nbNodeFSICrack){
		// Send force on the 'crack' structure 
		_msgAdm->sendToStructCrack(_structInterfForce.data().begin(),dt,flag,_nbInterfCrackStructDOF);
		// Receive disp and velo from the 'crack' structure 
		_msgAdm->receiveFromStructCrack(_resultStructInterfDispTangent.data().begin(),_resultStructInterfVeloTangent.data().begin(),_nbInterfCrackStructDOF);
	}
	if(_paramDataFile->verbose>1) cout << "Eval new Tangent done." << endl << flush;	
}

