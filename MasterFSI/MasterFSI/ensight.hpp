//===========================================================
//                    MasterFSI
//
// Part of MasterFSIContact,             Copyright Inria 2018
//===========================================================
/*!
 \file ensight.hpp
 \author Jean-Frederic Gerbeau
 */

#ifndef _ENSIGHT_H_
#define _ENSIGHT_H_
#include <iostream>
#include <fstream>
#include <vector>
#include <list>
#include <sstream>
#include <cmath>
#include <float.h>

#include "vectormatrix.hpp"

using namespace std;

//=======================================================================================
/*!
 \class EnsightVariable
 
 \brief class to handle a solution (in ensight format).
 
 \author  Jean-Frederic Gerbeau
 */

class EnsightVariable {    
public:   
    enum TypeVar {Unknown,ScalarPerNode,VectorPerNode};
private:
    TypeVar _type;	
	int _time_set;
	int _file_set;
	string _description;
	string _filename;
	string _filename_wildcard;
public:
	EnsightVariable(const string& type,const int& time_set,const int& file_set,
					const string& description, const string& filename);
	string variableFile() const;
	string variableFile(int istep) const;
	string description() const;
};


//=======================================================================================
/*!
 \class EnsightTime
 
 \brief class to handle a time set (in ensight format).
 
 \author  Jean-Frederic Gerbeau
 */

class EnsightTime {    
private:
	int _time_set;
	int _nb_steps;
	int _filename_start_number;
	int _filename_increment;
	vector<double> _time_values;
public:
	EnsightTime(int time_set,int nb_steps,int filename_start_number,int filename_increment);
	double timeValue(int i) const {return _time_values[i];};
	double& timeValue(int i){return _time_values[i];};
	int timeIndex(double time) const;
};


//=======================================================================================
/*!
 \class EnsightCase
 
 \brief class to handle a ensight case
 
 \author  Jean-Frederic Gerbeau
 */

class EnsightCase{
	int _verbose;
	//! Directory where the case file is
	string _directory; 
	//! Case file name (including the postfix '.case')
	string _filename;
	//! Lines of the case file
	vector<string> _line;
	//! Index of the line where the format section starts
	int _sectionFormat;
	//! Index of the line where the geometry section starts
	int _sectionGeometry;
	//! Index of the line where the variable section starts
	int _sectionVariable;
	//! Index of the line where the time section starts
	int _sectionTime;
	//! Time set in the 'model' subsection of Geometry section (-1 if not provided)
	int _model_time_set;
	//! File set int the 'model' subsection of Geometry section (-1 if not provided)
	int _model_file_set;
	//! Optional field in the 'model' subsection of Geometry section (false if not provided)
	bool _change_coords_only;
	//! cstep in change_coords_only item (0 if not provided)
	int  _change_coords_only_cstep;
	//! name of the geometry file (ex crosse.0010.geo)
	string _geo_file;
	//! name of the geometry file with wildcard (ex crosse.****.geo)
	string _geo_file_wildcard;
	//! number of wildcard characters (i.e. character '*')
	int _nb_wildcard;
	//! number of variables
	int _nb_variable;
	//! variable
	vector<EnsightVariable*> _var;
	vector<EnsightTime*> _time;
public:
	EnsightCase(int verbose);
	void read(const string& directory,const string& case_file);
	string geometryFile();
	string geometryFile(int istep);
	EnsightVariable* variable(int ivar);
	EnsightTime* timeSet(int iset);
	int timeIndex(double time) const;
	int variableIndex(string description) const;
	//! dim is the number of nodes for a scalar and 3*nb of nodes for a vector
	void readVariable(int idxvar,int idxtime,double* vec,int dim) const;
	//! dim is the number of nodes for a scalar and 3*nb of nodes for a vector
	void readVariable(string description,double time,double* vec,int dim) const;
};


#endif

