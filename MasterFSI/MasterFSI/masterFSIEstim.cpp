//===========================================================
//                    MasterFSI
//
// Part of MasterFSIContact,             Copyright Inria 2018
//===========================================================
/*!
 \file masterFSIEstim.cpp
 \author Jean-Frederic Gerbeau, Philippe Moireau
 */

#if ESTIMATION == 1

#include "GetPot.hpp"
#include "masterFSI.hpp"
#include <iostream>
#include "ensight.hpp"
#include "utilstring.hpp"
#include "masterFSIEstim.hpp"


/*----------------------------------------------------------*/

MasterFSIEstim::MasterFSIEstim(ParamFSI* paramDataFile,MessageAdmMasterFSI* msgAdm,OperInterf* operInterf,double time):
MasterFSI(paramDataFile,msgAdm,operInterf,time),
_protocol_seik(1),
_dimState(0),
_dimParam(0),
_dimObserv(0),
_dimParamFluid(0),
_dimParamStruct(0),
_dimObservFluid(0),
_dimObservStruct(0)
{
	int argc = 0;
	char** argv=NULL;
	_msgAdmSEIK = new MessageAdmWorkerSEIK(argc,argv,_paramDataFile->verbose,_protocol_seik);
	_msgAdmSEIK->receiveInitialization(); //receive the rank and the protocol
}

void MasterFSIEstim::firstContactWithFluid(){
	if(_paramDataFile->verbose) cout << "MasterFSIEstim::firstContactWithFluid() " << endl;
	MasterFSI::firstContactWithFluid();
	// Protocol 3
	_msgAdm->receiveInitializationEstimFromFluid(_dimParamFluid, _dimObservFluid);
	_dimState += 2*_nbFluidDOF + _nbWindkessel ; // displacement + velocity + Windkessel
	_dimParam += _dimParamFluid;
	_dimObserv += _dimObservFluid;
	//	int flagseik_output = FLAGSEIK_LOOP_OUTPUT;	
}

void MasterFSIEstim::firstContactWithStruct(){
	if(_paramDataFile->verbose) cout << "MasterFSIEstim::firstContactWithStruct() " << endl;
	MasterFSI::firstContactWithStruct();
	// Protocol 3
	_msgAdm->receiveInitializationEstimFromStruct(_dimParamStruct, _dimObservStruct,_msgAdm->structConfId());
	_dimState += 3*nbStructDOF();  // displacement + velocity + acceleration
	_dimParam += _dimParamStruct;
	_dimObserv += _dimObservStruct;
	//	int flagseik_output = FLAGSEIK_LOOP_OUTPUT;
}

void MasterFSIEstim::initializeEstim(){
	_state.resize(_dimState);
	_param.resize(_dimParam);
	_innov.resize(_dimObserv);	
	
	_paramFluid = _param.data().begin();
	_paramStruct = _param.data().begin() + _dimParamFluid;
	_innovFluid = _innov.data().begin();
	_innovStruct = _innov.data().begin() + _dimObservFluid;

	_param.clear();
	_state.clear();
	_innov.clear();

	_msgAdmSEIK->initializeDim(_dimState,_dimParam,_dimObserv);
		
	_lumpedInvCovObserv.resize(_dimObserv);
	_lumpedInvCovObservFluid = _lumpedInvCovObserv.data().begin();
	_lumpedInvCovObservStruct = _lumpedInvCovObservFluid + _dimObservFluid;

	_invCovParam.resize(_dimParam,_dimParam);
	_invCovParamFluid.resize(_dimParamFluid,_dimParamFluid);
	_invCovParamStruct.resize(_dimParamStruct,_dimParamStruct);

	// Protocol 3
	_msgAdm->receiveInitStateParamFromFluid(_dispFluid,_veloFluid,_varWindkessel,_dimParamFluid,_paramFluid,
											_invCovParamFluid.data().begin(),_dimObservFluid,_lumpedInvCovObservFluid);
	_msgAdm->receiveInitStateParamFromStruct(_dispConfStruct,_veloConfStruct,_accelConfStruct,_dimParamStruct,_paramStruct,
											 _invCovParamStruct.data().begin(),_dimObservStruct,_lumpedInvCovObservStruct,_msgAdm->structConfId());
    
	
	//...........
	_invCovParam.clear(); // necessary for the extra diagonal blocks
	//
	for(int i=0;i<_dimParamFluid;i++){
		for(int j=0;j<_dimParamFluid;j++){
			_invCovParam(i,j) = _invCovParamFluid(i,j);
		}
	}
	for(int i=0;i<_dimParamStruct;i++){
		for(int j=0;j<_dimParamStruct;j++){
			_invCovParam(_dimParamFluid+i,_dimParamFluid+j) = _invCovParamStruct(i,j);
		}
	}
	if(_msgAdmSEIK->rank() == 0){
		mergeFluidStructToState();
		_msgAdmSEIK->sendInitStateParam(_dimState,_dimParam,_dimObserv,_state.data().begin(),_param.data().begin(),
										_invCovParam.data().begin(),_lumpedInvCovObserv.data().begin());
	}
	double dt;
	
	_msgAdmSEIK->receiveStateParam(_state.data().begin(),_param.data().begin(),dt,_flagseik_loop,_flagseik_msg);
	splitStateToFluidStruct();
	
	// Protocol 3
	_msgAdm->sendStateParamToFluid(_dispFluid,_veloFluid,_varWindkessel,_paramFluid,_dimParamFluid,dt,_flagseik_loop,_flagseik_msg,_msgAdmSEIK->rank());
	_msgAdm->sendStateParamToStruct(_dispConfStruct,_veloConfStruct,_accelConfStruct,_paramStruct,_dimParamStruct,dt,
                                    _flagseik_loop,_flagseik_msg,_msgAdmSEIK->rank(),
									_msgAdm->structConfId());
}

void MasterFSIEstim::iterate(int iter,double dt){
	int outputfluid,outputstruct;
	cout << "******************* Correction " << iter << " ****************************" << endl << endl;
	// Protocol 3
	_msgAdm->receiveStateParamInnovFromFluid(_dispFluid,_veloFluid,_varWindkessel,_paramFluid,_dimParamFluid,_innovFluid,_dimObservFluid,outputfluid);
	if(_paramDataFile->conformFlag){
		_msgAdm->receiveStateParamInnovFromStruct(_dispConfStruct,_veloConfStruct,_accelConfStruct,_paramStruct,_dimParamStruct,
												  _innovStruct,_dimObservStruct,outputstruct,
												  _msgAdm->structConfId());
	}
	if(outputfluid != outputstruct){
		cout << "\t Postprocessing frequency must be the same in fluid and structure solvers" << endl;
		exit(1);
	}
	if(_paramDataFile->immersedFlag || _paramDataFile->crackFlag) {
		cout << "MasterFSIEstim::iterate not yet implemented for immersed or crack struct" << endl;
		exit(1);
	}
	_flagseik_output = outputfluid;
	
	mergeFluidStructToState();
	
	cout << "FLAGSEIK_MSG = " << _flagseik_msg << endl;
	if(_flagseik_msg == FLAGSEIK_MSG_INNOVATION)
		_msgAdmSEIK->sendStateParamInnov(_state.data().begin(),_param.data().begin(),_innov.data().begin(),_flagseik_output);
	if(_flagseik_msg == FLAGSEIK_MSG_NO_INNOVATION)
		_msgAdmSEIK->sendStateParam(_state.data().begin(),_param.data().begin());
	
	if (_flagseik_output == FLAGSEIK_LOOP_OUTPUT){
		cout << "------------------------------------" << endl;
		cout << "Outputs";
		if(_msgAdmSEIK->rank() == 0){
			_msgAdmSEIK->receiveStateParam(_state.data().begin(),_param.data().begin(),dt,_flagseik_loop,_flagseik_msg);			
			splitStateToFluidStruct();
			// protocol 3
			_msgAdm->sendStateParamToFluid(_dispFluid,_veloFluid,_varWindkessel,_paramFluid,_dimParamFluid,dt,_flagseik_loop,_flagseik_msg,_msgAdmSEIK->rank());
			_msgAdm->sendStateParamToStruct(_dispConfStruct,_veloConfStruct,_accelConfStruct,_paramStruct,_dimParamStruct,dt,_flagseik_loop,_flagseik_msg,
											_msgAdmSEIK->rank(),_msgAdm->structConfId());			
		}
	}
	_msgAdmSEIK->receiveStateParam(_state.data().begin(),_param.data().begin(),dt,_flagseik_loop,_flagseik_msg);
	splitStateToFluidStruct();
	// protocol 3
	_msgAdm->sendStateParamToFluid(_dispFluid,_veloFluid,_varWindkessel,_paramFluid,_dimParamFluid,dt,_flagseik_loop,_flagseik_msg,_msgAdmSEIK->rank());
	_msgAdm->sendStateParamToStruct(_dispConfStruct,_veloConfStruct,_accelConfStruct,_paramStruct,_dimParamStruct,dt,_flagseik_loop,_flagseik_msg,
									_msgAdmSEIK->rank(),_msgAdm->structConfId());			
    
	if(_paramDataFile->conformFlag) _msgAdm->receiveFromStructConf(_operInterf->structInterfDisp().data().begin(),
																   _operInterf->structInterfVelo().data().begin(),
																   _operInterf->nbInterfConfStructDOF());
	if(_paramDataFile->immersedFlag) _msgAdm->receiveFromStructIm(_operInterf->structInterfDisp().data().begin(),
																  _operInterf->structInterfVelo().data().begin(),
																  _operInterf->nbInterfImStructDOF());
	if(_paramDataFile->crackFlag) _msgAdm->receiveFromStructCrack(_operInterf->structInterfDisp().data().begin(),
																  _operInterf->structInterfVelo().data().begin(),
																  _operInterf->nbInterfCrackStructDOF());
	//
	if(_flagseik_loop == FLAGSEIK_LOOP_NEW_TIME_ITER){
		MasterFSI::iterate(iter,dt);
		_msgAdm->sendEndOfTimeStepToFluid();
		_msgAdm->sendEndOfTimeStepToStruct();
	}
	//
}

void MasterFSIEstim::mergeFluidStructToState(){
	for(size_t i=0;i<_dispFluid.size();i++) _state(i) = _dispFluid(i);
	for(size_t i=0;i<_dispFluid.size();i++) _state(i+_dispFluid.size()) = _veloFluid(i);
	for(size_t i=0;i<_dispConfStruct.size();i++) _state(i+2*_dispFluid.size()) = _dispConfStruct(i);
	for(size_t i=0;i<_dispConfStruct.size();i++) _state(i+2*_dispFluid.size()+_dispConfStruct.size()) = _veloConfStruct(i);	
	for(size_t i=0;i<_varWindkessel.size();i++) _state(i+2*_dispFluid.size()+2*_dispConfStruct.size()) = _varWindkessel(i);	
};

void MasterFSIEstim::splitStateToFluidStruct(){
	for(size_t i=0;i<_dispFluid.size();i++) _dispFluid(i) = _state(i);
	for(size_t i=0;i<_dispFluid.size();i++)  _veloFluid(i) = _state(i+_dispFluid.size());
	for(size_t i=0;i<_dispConfStruct.size();i++) _dispConfStruct(i) = _state(i+2*_dispFluid.size());
	for(size_t i=0;i<_dispConfStruct.size();i++) _veloConfStruct(i) = _state(i+2*_dispFluid.size()+_dispConfStruct.size());	
	for(size_t i=0;i<_varWindkessel.size();i++) _varWindkessel(i) = _state(i+2*_dispFluid.size()+2*_dispConfStruct.size());	
};

bool MasterFSIEstim::stopIterate(int iter){
	if(_flagseik_loop == FLAGSEIK_LOOP_STOP){
		if(iter != _paramDataFile->nbTimeIter){
			cout << "*** WARNING : numbers of time steps do not match in masterfsi and masterseik data files" << endl; 
		}
		return true;	
	}
	else{
		return false;	
	} 
}

#endif
