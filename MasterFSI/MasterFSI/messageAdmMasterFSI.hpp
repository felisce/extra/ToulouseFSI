//===========================================================
//                    MasterFSI
//
// Part of MasterFSIContact,             Copyright Inria 2018
//===========================================================
/*!
 \file messageAdmMasterFSI.hpp
 \author Jean-Frederic Gerbeau
 */
#ifndef _MESSAGEADMMASTERFSI_HPP
#define _MESSAGEADMMASTERFSI_HPP

#include "messageAdmFSI.hpp"
#include "paramFSI.hpp"
#include "operInterf.hpp"

using namespace std;

/*!
 \class MessageAdmMasterFSI
 \brief Messages Administrator
 
 \c MessageAdmMasterFSI is the class managing the messages for the master
 
 \author  Jean-Frederic Gerbeau
 
*/

class MessageAdmMasterFSI:public MessageAdmFSI
{
	ParamFSI* _paramDataFile;
	OperInterf* _operInterf;
	int _fluidId;
	int _structConfId;
	int _structImId;
	int _structCrackId;
	string _fluidHost;
	string _structHostConf;
	string _structHostIm;
	string _structHostCrack;
#ifdef MSG_ZMQ
  zmq::context_t _zmq_context;
  zmq::socket_t* _socketFluid;
  vector<zmq::socket_t*> _socketStruct;
#endif
  bool _ipass; // true the first iteration, false after
public:
	MessageAdmMasterFSI(int argc, char *argv[],ParamFSI* param,OperInterf* operInterf,
						int masterSendInitialState,int masterRcvInitialState,int masterRcvFinalState);
	int fluidId(){return _fluidId;}
	int structConfId() {return _structConfId;}
	int structImId() {return _structImId;} 
	int structCrackId() {return _structCrackId;}
	void spawnFluidStruct(int rank); // rank is an id labelling the master (only relevant when several masters are launched in parallel)
	void readFSINodesFiles();
	void sendFSINodesToFluid();
	//
	void sendToFluid(double* vec,double dt,int status,int dim);
	void sendDispVelToFluid(double* dispFluid,double* velFluidInterf,double dt,int status,int dim);
	void receiveFromFluid(double* forceF,int dim);
	void receiveFromFluidWithStressCorrection(double* forceF,double* initForceF,int dim);
	void receiveCoorInterfFromFluid(double* coorInterfF);
	void sendToStructConf(double* forceS,double dt,int status,int dim);
	void receiveFromStructConf(double* dispStruct,double* veloStruct,int dim);
	void sendToStructIm(double* forceS,double dt,int status,int dim);
	void receiveFromStructIm(double* dispStruct,double* veloStruct,int dim);
	void sendToStructCrack(double* forceS,double dt,int status,int dim);
	void receiveFromStructCrack(double* dispStruct,double* veloStruct,int dim);
	void sendToStruct(double* forceS,double dt,int status,int struct_id,int dim);
	void receiveFromStruct(double* dispStruct,double* veloStruct,int struct_id,int dim);
        void receiveFromStructConfFirstContact(double* lumpedMass, double* dispStruct,double* veloStruct,int dim, int lpdmdim, int& hassLumpedMass);
        void sendLumpedMassToFluid(int hasLumpedMass, double* lumpMass, int dim);
	void finalize();
	void sendStopToFluid();
	void sendStopToStruct();
	//
	void sendInitializationToFluid();
	void receiveInitializationFromFluid(int& nbNode,int& nbDofPerNode,int& nbWindkessel);
	void receiveStateFromFluid(int& nbNode,int& nbDofPerNode,Vector& disp,Vector& vel,int& nbWindkessel,Vector& varWindkessel);
	void sendStateToFluid(int nbNode,int nbDofPerNode,double time,Vector& disp,Vector& vel,int nbWindkessel,Vector& varWindkessel);
	//
	void sendInitializationToStruct(int id);
	void receiveInitializationFromStruct(int& nbNode,int& nbDofPerNode,int id);
	void receiveStateFromStruct(int& nbNode,int& nbDofPerNode,Vector& disp,Vector& vel,Vector& accel,int id);
	void sendStateToStruct(int nbNode,int nbDofPerNode,double time,Vector& disp,Vector& vel,Vector& accel,int id);
	//
	void receiveInitializationEstimFromFluid(int& dim_param,int& dim_observ);
	void receiveStateParamInnovFromFluid(Vector& disp,Vector& vel,Vector& varWindkessel,double* pparam,
										 int dim_param,double* pinnov,int dim_innov,int& flag_output);
	void sendStateParamToFluid(Vector& disp,Vector& vel,Vector& varWindkessel,double* pparam,int dim_param,
							   double dt,int flag_loop,int flag_msg,int rank);
	void receiveInitStateParamFromFluid(Vector& disp,Vector& vel,Vector& varWindkessel,int dim_param, double* pparam,
										double* invCovParam,int& dim_observ,double* lumpedInvCovObserv);
	
	//
	void receiveInitializationEstimFromStruct(int& dim_param,int& dim_observ,int id);
	void sendStateParamToStruct(Vector& disp,Vector& vel,Vector& accel,double* pparam,int dim_param,double dt,int flag_loop,int flag_msg,int rank,int id);
	void receiveStateParamInnovFromStruct(Vector& disp,Vector& vel,Vector& accel,double* pparam,int dim_param,double* pinnov,
										  int dim_innov,int& flag_output,int id);
	void receiveInitStateParamFromStruct(Vector& disp,Vector& vel,Vector& accel,int dim_param, double* pparam,
                                         double* invCovParam,int dim_observ,double* lumpedInvCovObserv,int id);
	void sendEndOfTimeStepToFluid();
	void sendEndOfTimeStepToStruct();

    // for the Robin-Neumann convergence criteria
  void receiveForceDispVelFromFluid(double* forceF, double* dispF, double* velF, int dim); 
};

#endif
