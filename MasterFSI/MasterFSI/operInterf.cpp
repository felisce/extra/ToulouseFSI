//===========================================================
//                    MasterFSI
//
// Part of MasterFSIContact,             Copyright Inria 2018
//===========================================================
/*!
 \file operInterf.cpp
 \author Jean-Frederic Gerbeau
 */

#include "operInterf.hpp"
#include <iostream>
#include "utilstring.hpp"

/*----------------------------------------------------------*/

OperInterf::OperInterf(ParamFSI* param):
_paramDataFile(param),
_nbInterfFluidDOF(0),
_nbInterfStructDOF(0),
_hasLumpedMass(0)
{}

void OperInterf::initialize(){
	assert(_nbInterfFluidDOF>0);
	_fluidInterfDisp.resize(_nbInterfFluidDOF);
	_fluidInterfVelo.resize(_nbInterfFluidDOF);
	_fluidInterfForce.resize(_nbInterfFluidDOF);
	_fluidInterfInitialForce.resize(_nbInterfFluidDOF);
	_coorInterfFluid.resize(_nbInterfFluidDOF);
	assert(_nbInterfStructDOF>0);
	_structInterfDisp.resize(_nbInterfStructDOF);
	_structInterfVelo.resize(_nbInterfStructDOF);
	_structInterfLumpMass.resize(_nbInterfStructDOF);
	_structInterfDispTangent.resize(_nbInterfStructDOF);
	_structInterfVeloTangent.resize(_nbInterfStructDOF);
	_resultStructInterfDisp.resize(_nbInterfStructDOF);
	_resultStructInterfVelo.resize(_nbInterfStructDOF);	
	_resultStructInterfDispTangent.resize(_nbInterfStructDOF);
	_resultStructInterfVeloTangent.resize(_nbInterfStructDOF);	
	_structInterfForce.resize(_nbInterfStructDOF);
	_structInterfLumpMassDim = _nbInterfFluidDOF; //3*_nbInterfStructDOF/5;
}

void OperInterf::structInterfToFluidInterf(const Vector& structVec,Vector& fluidVec){
	switch(_paramDataFile->interpolation){
		case 1:
			for(int i=0;i<_nbNodeFSIConf+_nbNodeFSIIm;i++){
				for(int icoor=0;icoor<3;icoor++){
					fluidVec[3*i + icoor] = structVec[3*i+icoor];
				}
			}
			for(int i=0;i<_nbNodeFSICrack;i++){
				for(int icoor=0;icoor<3;icoor++){
					fluidVec[3*(i+_nbNodeFSIConf+_nbNodeFSIIm) + icoor]
					=  structVec[3*(i+_nbNodeFSIConf+_nbNodeFSIIm)+icoor];
					fluidVec[3*(i+_nbNodeFSIConf+_nbNodeFSIIm+_nbNodeFSICrack) + icoor]
					=  structVec[3*(i+_nbNodeFSIConf+_nbNodeFSIIm)+icoor];
				}
			}
			break;
		case 2:
			for(int i=0;i<_nbNodeFSIConf+_nbNodeFSIIm;i++){
				for(int icoor=0;icoor<3;icoor++){
					fluidVec[3*i + icoor] = structVec[5*i+icoor];
				}
			}
			for(int i=0;i<_nbNodeFSICrack;i++){
				for(int icoor=0;icoor<3;icoor++){
					fluidVec[3*(i+_nbNodeFSIConf+_nbNodeFSIIm) + icoor]
					=  structVec[5*(i+_nbNodeFSIConf+_nbNodeFSIIm)+icoor];
					fluidVec[3*(i+_nbNodeFSIConf+_nbNodeFSIIm+_nbNodeFSICrack) + icoor]
					=  structVec[5*(i+_nbNodeFSIConf+_nbNodeFSIIm)+icoor];
				}
			}
			break;
		default:
			cout << "OperInter::structInterfToFluidInterf: interpolation = " << _paramDataFile->interpolation << endl;
			exit(1);
	}
	
}

// ** This function is useful to interpolate the force vector...
void OperInterf::fluidInterfToStructInterf(const Vector& fluidVec,Vector& structVec){
	switch(_paramDataFile->interpolation){
		case 1: /* Identity matching */
			for(int i=0;i<_nbNodeFSIConf+_nbNodeFSIIm;i++){
				for(int icoor=0;icoor<3;icoor++){
					structVec[3*i + icoor] = fluidVec[3*i+icoor];
				}
			}
			/* In the "crack" case the fluid force in two matching nodes are summed 
			   in order to send the force jump to the structure */
			for(int i=0;i<_nbNodeFSICrack;i++){
				for(int icoor=0;icoor<3;icoor++){
					structVec[3*(i+_nbNodeFSIConf+_nbNodeFSIIm) + icoor]
					=  fluidVec[3*(i+_nbNodeFSIConf+_nbNodeFSIIm)+icoor]+
					fluidVec[3*(i+_nbNodeFSIConf+_nbNodeFSIIm+_nbNodeFSICrack)+icoor];
				}
			}
			break;
		case 2: /* MITC shells */
			for(int i=0;i<_nbNodeFSIConf+_nbNodeFSIIm;i++){
				for(int icoor=0;icoor<3;icoor++){
					structVec[5*i + icoor] = fluidVec[3*i+icoor];
				}
			}
			/* In the "crack" case the fluid force in two matching nodes are summed 
			   in order to send the force jump to the structure */
			for(int i=0;i<_nbNodeFSICrack;i++){
				for(int icoor=0;icoor<3;icoor++){
					structVec[5*(i+_nbNodeFSIConf+_nbNodeFSIIm) + icoor]
					=  fluidVec[3*(i+_nbNodeFSIConf+_nbNodeFSIIm)+icoor]+
					fluidVec[3*(i+_nbNodeFSIConf+_nbNodeFSIIm+_nbNodeFSICrack)+icoor];
				}
			}
			break;
		default:
			cout << "OperInter::fluidInterfToStructInterf: interpolation = " << _paramDataFile->interpolation << endl;
			exit(1);   
	}
}

// ** ... while this one is to interpolate displacement and velocity vectors. The main
//        difference yields on the crack surface. This routine is used for Robin Interface BC's
void OperInterf::fluidDispVelInterfToStructInterf(const Vector& fluidVec,Vector& structVec){
	switch(_paramDataFile->interpolation){
		case 1: /* Identity matching */
			for(int i=0;i<_nbNodeFSIConf+_nbNodeFSIIm;i++){
				for(int icoor=0;icoor<3;icoor++){
					structVec[3*i + icoor] = fluidVec[3*i+icoor];
				}
			}
			/* In the "crack" we just copy the set of nodes associated to the first unfolded
			   surface, because in the crack, the displacement (and the velocity) is the same at both sides. */
			for(int i=0;i<_nbNodeFSICrack;i++){
				for(int icoor=0;icoor<3;icoor++){
					structVec[3*(i+_nbNodeFSIConf+_nbNodeFSIIm) + icoor]
					=  fluidVec[3*(i+_nbNodeFSIConf+_nbNodeFSIIm)+icoor];
				}
			}
			break;
		case 2: /* MITC shells */
			for(int i=0;i<_nbNodeFSIConf+_nbNodeFSIIm;i++){
				for(int icoor=0;icoor<3;icoor++){
					structVec[5*i + icoor] = fluidVec[3*i+icoor];
				}
			}
			/* In the "crack" we just copy the set of nodes associated to the first unfolded
			   surface, because in the crack, the displacement (and the velocity) is the same at both sides. */
			for(int i=0;i<_nbNodeFSICrack;i++){
				for(int icoor=0;icoor<3;icoor++){
					structVec[5*(i+_nbNodeFSIConf+_nbNodeFSIIm) + icoor]
					  =  fluidVec[3*(i+_nbNodeFSIConf+_nbNodeFSIIm)+icoor];
				}
			}
			break;
		default:
			cout << "OperInter::fluidInterfToStructInterf: interpolation = " << _paramDataFile->interpolation << endl;
			exit(1);   
	}
}
