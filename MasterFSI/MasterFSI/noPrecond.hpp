//===========================================================
//                    MasterFSI
//
// Part of MasterFSIContact,             Copyright Inria 2018
//===========================================================
/*!
 \file noPrecond.hpp
 \author Jean-Frederic Gerbeau
 */
#ifndef _NOPRECOND_H
#define _NOPRECOND_H
template<typename Vector>
class NoPrecond
{
public:
  inline Vector solve(const Vector &x) const{ return x; }  
  inline Vector trans_solve(const Vector &x) const{ return x; }  
};
#endif
