//===========================================================
//                    MasterFSI
//
// Part of MasterFSIContact,             Copyright Inria 2018
//===========================================================
/*!
 \file messageAdmFluid.cpp
 \author Jean-Frederic Gerbeau
 */
#include <iostream>
#include <fstream>
#include <cstdio>
#include <cstdlib>

#undef MSG_VERBOSE

#include "messageAdmFluid.hpp"
#include "utilstring.hpp"
#include <cassert>
#include <cstdlib>

using namespace std;

#ifdef MSG_PVM
MessageAdmFluid::MessageAdmFluid(int argc, char *argv[],const int& verbose,const int& protocol,int unit):
MessageAdmFSI(argc,argv,verbose,protocol,unit)
#endif
#ifdef MSG_ZMQ
MessageAdmFluid::MessageAdmFluid(int argc, char *argv[],const int& verbose,const int& protocol,int unit,string socketTransport,string socketAddress,string socketPort):
MessageAdmFSI(argc,argv,verbose,protocol,unit),
_zmq_context(1),
_socket(_zmq_context, ZMQ_PAIR),
_socketTransport(socketTransport),
_socketAddress(socketAddress),
_socketPort(socketPort)
#endif
{
#ifdef MSG_PVM
  _masterId = pvm_parent();
  _mytid = pvm_mytid();
  if(_verbose)
  {
    cout << "    My master id is " << _masterId << endl;
    cout << "    My task id " << _mytid << endl;
  }
  if(_masterId==PvmNoParent){
    cout << "I am a poor lonesome job\n";
    exit(1);
  } else{
    if(_masterId==0){
      cout << "PVM is not ok\n";
      exit(1);
    }
  }
#endif
#ifdef MSG_ZMQ
  string socket_endpoint;
  if(_socketPort != "")
    socket_endpoint = _socketTransport + "://" + _socketAddress + ":" + _socketPort;
  else
    socket_endpoint = _socketTransport + "://" + _socketAddress;
  cout << "[ZeroMQ] Connect to " << socket_endpoint << endl;
  _socket.connect(socket_endpoint.c_str());
#endif
}

void MessageAdmFluid::receiveInitializationFromMaster(){
  if(_verbose) cout << "---> Fluid receives initialization message (msg 201)" << endl << flush;
  int rcvprotocol;
  int rcvunit;
#ifdef MSG_PVM
  pvm_recv(_masterId,201);
  pvm_upkint(&rcvprotocol,1,1);
  if( _protocol != rcvprotocol){
    cout << "MessageAdmFluid: protocols are different in the master and in the fluid" << endl << flush;
    exit(1);
  }
  pvm_upkint(&rcvunit,1,1);
  assert(_unit == rcvunit);
  pvm_upkint(&_masterSendInitialState,1,1);
  pvm_upkint(&_masterRcvInitialState,1,1);
  pvm_upkint(&_masterRcvFinalState,1,1);
  pvm_upkint(&_fluidReceivesInterfDisp,1,1);
#endif
#ifdef MSG_ZMQ
  cout << "Error: file " << __FILE__ << " line " << __LINE__ << " -> ZeroMQ not yet implemented" << endl;
#endif
  if(_verbose){
    cout << "\t message protocol = " << _protocol << endl;
    cout << "\t physical unit = " << _unit << endl;
    cout << "\t masterSendInitialState = " << _masterSendInitialState << endl;
    cout << "\t masterRcvInitialState = " << _masterRcvInitialState << endl;
    cout << "\t masterRcvFinalState = " << _masterRcvFinalState << endl;
    cout << "\t fluidReceivesInterfDisp = " << _fluidReceivesInterfDisp<< endl;
  }
}

void MessageAdmFluid::sendInitializationToMaster(int nbNode,int nbDofPerNode,int nbWindkessel){
#ifdef MSG_PVM
  if(_verbose) cout << "<--- Fluid sends initialization message (msg 202)" << endl  << flush;
  pvm_initsend(PvmDataDefault);
  pvm_pkint(&nbNode,1,1);
  pvm_pkint(&nbDofPerNode,1,1);
  pvm_pkint(&nbWindkessel,1,1);
  pvm_send(_masterId,202);
#endif
#ifdef MSG_ZMQ
  cout << "Error: file " << __FILE__ << " line " << __LINE__ << " -> ZeroMQ not yet implemented" << endl;
#endif
  if(_verbose){
    cout << "\t nbNode = " << nbNode << endl;
    cout << "\t nbDofPerNode = " << nbDofPerNode << endl;
    cout << "\t nbWindkessel = " << nbWindkessel << endl;
  }
}

void MessageAdmFluid::sendStateToMaster(int nbNode,int nbDofPerNode,double* disp,double* vel,int nbWindkessel,double* varWindkessel){
  if(_verbose) cout << "<--- Fluid sends state  (msg 210)" << endl  << flush;
  int dim = nbNode*nbDofPerNode;
#ifdef MSG_PVM
  pvm_initsend(PvmDataDefault);
  pvm_pkint(&nbNode,1,1);
  pvm_pkint(&nbDofPerNode,1,1);
  pvm_pkdouble(disp,dim,1);
  pvm_pkdouble(vel,dim,1);
  pvm_pkint(&nbWindkessel,1,1);
  pvm_pkdouble(varWindkessel,nbWindkessel,1);
  pvm_send(_masterId,210);
#endif
#ifdef MSG_ZMQ
  cout << "Error: file " << __FILE__ << " line " << __LINE__ << " -> ZeroMQ not yet implemented" << endl;
#endif
  
  if(_verbose>1){
    cout << "\t nbNode = " << nbNode << endl;
    cout << "\t nbDofPerNode = " << nbDofPerNode << endl;
    cout << "\t nbWindkessel = " << nbWindkessel << endl;
    printArray(_verbose,cout,"\t F->M Disp", disp, dim);
    printArray(_verbose,cout,"\t F->M Vel", vel, dim);
    printArray(_verbose,cout,"\t F->M Windkessel", varWindkessel, nbWindkessel);
  }
}

void MessageAdmFluid::receiveStateFromMaster(int& nbNode,int& nbDofPerNode,double& time,double* disp,double* vel,int& nbWindkessel,double* varWindkessel){
  if(_verbose) cout << "---> Fluid receive state  (msg 220)" << endl  << flush;
  int dim;
#ifdef MSG_PVM
  pvm_recv(_masterId,220);
  pvm_upkint(&nbNode,1,1);
  pvm_upkint(&nbDofPerNode,1,1);
  pvm_upkdouble(&time,1,1);
  dim = nbNode*nbDofPerNode;
  pvm_upkdouble(disp,dim,1);
  pvm_upkdouble(vel,dim,1);
  pvm_upkint(&nbWindkessel,1,1);
  pvm_upkdouble(varWindkessel,nbWindkessel,1);
#endif
#ifdef MSG_ZMQ
  cout << "Error: file " << __FILE__ << " line " << __LINE__ << " -> ZeroMQ not yet implemented" << endl;
#endif
  
  if(_verbose>1){
    cout << "\t nbNode = " << nbNode << endl;
    cout << "\t nbDofPerNode = " << nbDofPerNode << endl;
    cout << "\t nbWindkessel = " << nbWindkessel << endl;
    printArray(_verbose,cout,"\t M->F Disp", disp, dim);
    printArray(_verbose,cout,"\t M->F Vel", vel, dim);
    printArray(_verbose,cout,"\t M->F Windkessel", varWindkessel, nbWindkessel);
  }
}

void MessageAdmFluid::receiveInterfVecFromMaster(int& status, double& dt,int& dim,double* vec){
  if(_verbose){
    if(_fluidReceivesInterfDisp) cout << "---> Fluid receives interface displacement from master (msg 110) \n";
    else cout << "---> Fluid receives interface velocity from master (msg 110) \n";
  }
  int ntdlf;
#ifdef MSG_PVM
  pvm_recv(_masterId,110);
  pvm_upkint(&status,1,1);
  if(status>=0){
    pvm_upkdouble(&dt,1,1);
    pvm_upkint(&ntdlf,1,1);
    assert(ntdlf == dim);
    pvm_upkdouble(vec,dim,1);
  }
#endif
#ifdef MSG_ZMQ
  zmq::message_t msg1;
  _socket.recv(&msg1);
  memcpy(&status,msg1.data(),1*sizeof(int));
  //
  if(status>=0){
    zmq::message_t msg2;
    _socket.recv(&msg2);
    memcpy(&dt,msg2.data(),1*sizeof(double));
    //
    zmq::message_t msg3;
    _socket.recv(&msg3);
    memcpy(&ntdlf,msg3.data(),1*sizeof(int));
    assert(ntdlf == dim);
    //
    zmq::message_t msg4;
    _socket.recv(&msg4);
    memcpy(vec,msg4.data(),dim*sizeof(double));
  }
#endif
  
  if(_verbose>1){
    cout << "\t Status = " << status << endl;
    if(status>=0){
      cout << "\t dt = " << dt << endl;
      cout << "\t dim = " << dim << endl;
      printArray(_verbose,cout,"\t M->F vec", vec, dim);
    }
  }
}

void MessageAdmFluid::receiveInterfDispVelFromMaster(int& status, double& dt,int& dim,double* disp,double* vel){
  if(_verbose){
    cout << "---> Fluid receives interface displacement and velocity from master (msg 110) \n";
  }
  int ntdlf;
#ifdef MSG_PVM
  pvm_recv(_masterId,110);
  pvm_upkint(&status,1,1);
  if(status>=0){
    pvm_upkdouble(&dt,1,1);
    pvm_upkint(&ntdlf,1,1);
    assert(ntdlf == dim);
    pvm_upkdouble(disp,dim,1);
    pvm_upkdouble(vel,dim,1);
  }
#endif
#ifdef MSG_ZMQ
  zmq::message_t msg1;
  _socket.recv(&msg1);
  memcpy(&status,msg1.data(),1*sizeof(int));
  //
  if(status>=0){
    zmq::message_t msg2;
    _socket.recv(&msg2);
    memcpy(&dt,msg2.data(),1*sizeof(double));
    //
    zmq::message_t msg3;
    _socket.recv(&msg3);
    memcpy(&ntdlf,msg3.data(),1*sizeof(int));
    assert(ntdlf == dim);
    //
    zmq::message_t msg4;
    _socket.recv(&msg4);
    memcpy(disp,msg4.data(),dim*sizeof(double));
    //
    zmq::message_t msg5;
    _socket.recv(&msg5);
    memcpy(vel,msg5.data(),dim*sizeof(double));
  }
#endif
  if(_verbose>1){
    cout << "\t Status = " << status << endl;
    if(status>=0){
      cout << "\t dt = " << dt << endl;
      cout << "\t dim = " << dim << endl;
      printArray(_verbose,cout,"\t M->F disp", disp, dim);
      printArray(_verbose,cout,"\t M->F vel", vel, dim);
    }
  }
}

void MessageAdmFluid::sendInterfForceToMaster(int dim,double* force){
  static int ipass = 0;
  double vref=1.;
  if(_verbose) cout << "---> Fluid sends force to master (msg 120) \n";
#ifdef MSG_PVM
  pvm_initsend(PvmDataDefault);
  pvm_pkint(&dim,1,1);
  pvm_pkdouble(force,dim,1);
  if(!ipass){
    pvm_pkdouble(&vref,1,1);
    ipass = 1;
  }
  pvm_send(_masterId,120);
#endif
#ifdef MSG_ZMQ
  zmq::message_t msg1(1*sizeof(int));
  memcpy(msg1.data(),&dim,1*sizeof(int));
  _socket.send(msg1);
  //
  zmq::message_t msg2(dim*sizeof(double));
  memcpy(msg2.data(),force,dim*sizeof(double));
  _socket.send(msg2);
  //
  if(!ipass){
    zmq::message_t msg3(1*sizeof(double));
    memcpy(msg3.data(),&vref,1*sizeof(double));
    _socket.send(msg3);
    ipass = 1;
  }
#endif
  if(_verbose>1){
    printArray(_verbose,cout,"\t F->M force", force, dim);
  }
}

void MessageAdmFluid::sendInterfForceDispVelToMaster(int dim,double* forceF,double* dispF, double* velF){
  // This function is used only for Robin-Neumann Interface Conditions.
  static int ipass = 0;
  double vref=1.;
  if(_verbose) cout << "---> Fluid sends force to master (msg 120: 1 of 2) \n";
#ifdef MSG_PVM
  pvm_initsend(PvmDataDefault);
  // first, the force
  pvm_pkint(&dim,1,1);
  pvm_pkdouble(forceF,dim,1);
  if(_verbose) cout << "---> Fluid sends Disp + Vel to master (msg 120: 2 of 2) \n";
  pvm_pkdouble(dispF,dim,1);
  pvm_pkdouble(velF,dim,1);
  
  if(!ipass){
    pvm_pkdouble(&vref,1,1);
    ipass = 1;
  }
  pvm_send(_masterId,120);
#endif
#ifdef MSG_ZMQ
  zmq::message_t msg1(1*sizeof(int));
  memcpy(msg1.data(),&dim,1*sizeof(int));
  _socket.send(msg1);
  //
  zmq::message_t msg2(dim*sizeof(double));
  memcpy(msg2.data(),forceF,dim*sizeof(double));
  _socket.send(msg2);
  //
  if(_verbose) cout << "---> Fluid sends Disp + Vel to master (msg 120: 2 of 2) \n";
  //
  zmq::message_t msg3(dim*sizeof(double));
  memcpy(msg3.data(),dispF,dim*sizeof(double));
  _socket.send(msg3);
  //
  zmq::message_t msg4(dim*sizeof(double));
  memcpy(msg4.data(),velF,dim*sizeof(double));
  _socket.send(msg4);
  //
  if(!ipass){
    zmq::message_t msg5(1*sizeof(double));
    memcpy(msg5.data(),&vref,1*sizeof(double));
    _socket.send(msg5);
    ipass = 1;
  }
#endif
  
  if(_verbose>1){
    printArray(_verbose,cout,"\t F->M Disp_fluid", dispF, dim);
    printArray(_verbose,cout,"\t F->M Vel_fluid", velF, dim);
  }
}


void MessageAdmFluid::sendInitializationEstimToMaster(int dim_param,int dim_observ){
  if(_verbose) cout << "<--- Fluid sends initialization Estim message (msg 302)" << endl  << flush;
#ifdef MSG_PVM
  pvm_initsend(PvmDataDefault);
  pvm_pkint(&dim_param,1,1);
  pvm_pkint(&dim_observ,1,1);
  pvm_send(_masterId,302);
#endif
#ifdef MSG_ZMQ
  cout << "Error: file " << __FILE__ << " line " << __LINE__ << " -> ZeroMQ not yet implemented" << endl;
#endif
  
  if(_verbose>1){
    cout << "\t dim_param = " << dim_param << endl;
    cout << "\t dim_observ = " << dim_observ << endl;
  }
  
}

void MessageAdmFluid::receiveStateParamFromMaster(int& dim,double* disp,double* vel,int& dimWindkessel,double* varwindkessel,
                                                  int dim_param,double* param,double& dt,
                                                  int& flag_loop,int& flag_msg,int& rank){
  if(_verbose) cout << "---> Fluid receives state and param (msg 320)" << endl  << flush;
#ifdef MSG_PVM
  pvm_recv(_masterId,320);
  pvm_upkint(&dim,1,1);
  pvm_upkdouble(disp,dim,1);
  pvm_upkdouble(vel,dim,1);
  pvm_upkint(&dimWindkessel,1,1);
  pvm_upkdouble(varwindkessel,dimWindkessel,1);
  pvm_upkint(&dim_param,1,1);
  pvm_upkdouble(param,dim_param,1);
  pvm_upkdouble(&dt,1,1);
  pvm_upkint(&flag_loop,1,1);
  pvm_upkint(&flag_msg,1,1);
  pvm_upkint(&rank,1,1);
#endif
#ifdef MSG_ZMQ
  cout << "Error: file " << __FILE__ << " line " << __LINE__ << " -> ZeroMQ not yet implemented" << endl;
#endif
  
  if(_verbose>1){
    cout << "\t dim  = " << dim << endl;
    printArray(_verbose,cout,"\t M->F Disp", disp, dim);
    printArray(_verbose,cout,"\t M->F Vel", vel, dim);
    printArray(_verbose,cout,"\t M->F Windkessel", varwindkessel, dimWindkessel);
    cout << "\t dim param = " << dim_param;
    printArray(_verbose,cout,"\t M->F Param", param, dim_param);
    cout << "\t dt  = " << dt << endl;
    cout << "\t flag_loop  = " << flag_loop << endl << flush;
    cout << "\t flag_msg  = " << flag_msg << endl << flush;
    cout << "\t rank  = " << rank << endl << flush;
  }
}

void MessageAdmFluid::sendStateParamInnovToMaster(int dim,double* disp,double* vel,int dimWindkessel,double *varwindkessel,
                                                  int dim_param,double* param,int dim_innov,double* innov,int flag_output){
  if(_verbose>1) cout << "---> Fluid sends state param and innov to master (msg 310) \n";
#ifdef MSG_PVM
  pvm_initsend(PvmDataDefault);
  pvm_pkdouble(disp,dim,1);
  pvm_pkdouble(vel,dim,1);
  pvm_pkdouble(varwindkessel,dimWindkessel,1);
  pvm_pkdouble(param,dim_param,1);
  pvm_pkdouble(innov,dim_innov,1);
  pvm_pkint(&flag_output,1,1);
  pvm_send(_masterId,310);
#endif
#ifdef MSG_ZMQ
  cout << "Error: file " << __FILE__ << " line " << __LINE__ << " -> ZeroMQ not yet implemented" << endl;
#endif
  
  if(_verbose>1){
    printArray(_verbose,cout,"\t F->M disp", disp, dim);
    printArray(_verbose,cout,"\t F->M vel", vel, dim);
    printArray(_verbose,cout,"\t F->M Windkessel", varwindkessel, dimWindkessel);
    printArray(_verbose,cout,"\t F->M param", param, dim_param);
    printArray(_verbose,cout,"\t F->M innov", innov, dim_innov);
    cout << "\t flag_output = " << flag_output << endl;
  }
}

void MessageAdmFluid::sendInitStateParamToMaster(int dim,double* disp, double* vel, int dimWindkessel, double* varwindkessel,
                                                 int dim_param, double* param, double* invCovParam,int dim_observ,double* lumpedInvCovObserv){
  if(_verbose>1) cout << "---> Fluid sends mean vectors and cov matrices to master (msg 350) \n";
#ifdef MSG_PVM
  pvm_initsend(PvmDataDefault);
  pvm_pkdouble(disp,dim,1);
  pvm_pkdouble(vel,dim,1);
  pvm_pkdouble(varwindkessel,dimWindkessel,1);
  pvm_pkdouble(param,dim_param,1);
  pvm_pkdouble(invCovParam,dim_param*dim_param,1);
  pvm_pkdouble(lumpedInvCovObserv,dim_observ,1);
  pvm_send(_masterId,350);
#endif
#ifdef MSG_ZMQ
  cout << "Error: file " << __FILE__ << " line " << __LINE__ << " -> ZeroMQ not yet implemented" << endl;
#endif
  
  if(_verbose>1){
    printArray(_verbose,cout,"\t F->M disp", invCovParam, dim);
    printArray(_verbose,cout,"\t F->M vel", invCovParam, dim);
    printArray(_verbose,cout,"\t F->M Windkessel", varwindkessel, dimWindkessel);
    printArray(_verbose,cout,"\t F->M param", param, dim_param);
    printArray(_verbose,cout,"\t F->M invCovParam", invCovParam, dim_param*dim_param);
    printArray(_verbose,cout,"\t F->M lumpedInvCovObserv", lumpedInvCovObserv, dim_observ);
  }
  
}


