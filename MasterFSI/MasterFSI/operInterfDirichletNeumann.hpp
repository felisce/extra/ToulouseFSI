//===========================================================
//                    MasterFSI
//
// Part of MasterFSIContact,             Copyright Inria 2018
//===========================================================
/*!
 \file operInterfDirichletNeumann.hpp
 \author Jean-Frederic Gerbeau
 */

#ifndef  _OPERINTERFDirichletNeumann_HPP
#define  _OPERINTERFDirichletNeumann_HPP

#include "operInterf.hpp"

/*!
 \class OperInterfDirichletNeumann
 
 \brief Operator handling a Dirichlet Neumann algorithm
 
 \c OperInterfDirichletNeumann is the class handling the fluid-structure interface exchanges via Dirichlet (Fluid) - Neumann (Structure) iteration
 
 \author  Jean-Frederic Gerbeau
 */



class OperInterfDirichletNeumann: public OperInterf{
public:
	OperInterfDirichletNeumann(ParamFSI* _paramDataFile);
    void compute(int flag, double& eps_ext);
	void computeTangent(const Vector& vec);	
};
#endif
