//===========================================================
//                    MasterFSI
//
// Part of MasterFSIContact,             Copyright Inria 2018
//===========================================================
/*!
 \file messageAdmStruct.cpp
 \author Jean-Frederic Gerbeau
 */
/*!
 \file messageAdmStruct.cpp
 \author Jean-Frederic Gerbeau, Philippe Moireau
 
 \todo In receiveStateFromMaster() and sendStateToMaster() change int nbNode,int nbDofPerNode, by dim to be more consistent with the other functions
 
 \todo Is it useful to send the time ?
 
 */
#include <iostream>
#include <fstream>
#include <cstdio>
#include <cstdlib>


#undef MSG_VERBOSE

#include "messageAdmStruct.hpp"
#include "utilstring.hpp"
#include <cassert>
#include <cstdlib>

using namespace std;

#ifdef MSG_PVM
MessageAdmStruct::MessageAdmStruct(int argc, char *argv[],const int& verbose,const int& protocol,int unit):
MessageAdmFSI(argc,argv,verbose,protocol,unit)
#endif
#ifdef MSG_ZMQ
MessageAdmStruct::MessageAdmStruct(int argc, char *argv[],const int& verbose,const int& protocol,int unit,string socketTransport,string socketAddress,string socketPort):
MessageAdmFSI(argc,argv,verbose,protocol,unit),
_zmq_context(1),
_socket(_zmq_context, ZMQ_PAIR),
_socketTransport(socketTransport),
_socketAddress(socketAddress),
_socketPort(socketPort)
#endif
{
#ifdef MSG_PVM
  _masterId = pvm_parent();
  _mytid = pvm_mytid();
  if(_verbose)
  {
    cout << "    My master id is " << _masterId << endl;
    cout << "    My task id " << _mytid << endl;
  }
  if(_masterId==PvmNoParent){
    cout << "I am a poor lonesome job\n";
    exit(1);
  } else{
    if(_masterId==0){
      cout << "PVM is not ok\n";
      exit(1);
    }
  }
#endif
#ifdef MSG_ZMQ
  string socket_endpoint;
  if(_socketPort != "")
    socket_endpoint = _socketTransport + "://" + _socketAddress + ":" + _socketPort;
  else
    socket_endpoint = _socketTransport + "://" + _socketAddress;
  cout << "[ZeroMQ] Connect to " << socket_endpoint << endl;
  _socket.connect(socket_endpoint.c_str());
#endif
}



void MessageAdmStruct::sendInterfDispVelToMaster(int dim,double* dispStruct,double* veloStruct,double scalar){
  if(_verbose>1) cout << "<--- Struct sends interface solution to master (msg 100) \n";
#ifdef MSG_PVM
  pvm_initsend(PvmDataDefault);
  pvm_pkint(&dim,1,1);
  pvm_pkdouble(dispStruct,dim,1);
  pvm_pkdouble(veloStruct,dim,1);
  switch(_protocol){
    case 1:
      pvm_send(_masterId,100);
      break;
    case 11:
      //pvm_pkdouble(&scalar,1,1);
      pvm_send(_masterId,100);
      break;
    default:
      cout << "Error in file: " << __FILE__ << " line: " << __LINE__ << " -> Unknown protocol" << endl;
      exit(1);
      
  }
#endif
#ifdef MSG_ZMQ
  zmq::message_t msg1(1*sizeof(int));
  memcpy(msg1.data(),&dim,1*sizeof(int));
  _socket.send(msg1);
  //
  zmq::message_t msg2(dim*sizeof(double));
  memcpy(msg2.data(),dispStruct,dim*sizeof(double));
  _socket.send(msg2);
  //
  zmq::message_t msg3(dim*sizeof(double));
  memcpy(msg3.data(),veloStruct,dim*sizeof(double));
  _socket.send(msg3);
  //
  if(_protocol == 11){
    zmq::message_t msg4(1*sizeof(double));
    memcpy(msg4.data(),&scalar,1*sizeof(double));
    _socket.send(msg4);
  }
#endif
  if(_verbose>1){
    printArray(_verbose,cout,"\t S->M Disp", dispStruct, dim);
    printArray(_verbose,cout,"\t S->M Velo", veloStruct, dim);
    if(_protocol == 11) cout << "Scalar = " << scalar << endl;
  }
}

void MessageAdmStruct::receiveInterfForceFromMaster(int& status, double& dt,int& dim,double* force){
  if(_verbose>1) cout << "---> Struct receives interface solution from master (msg 130) \n";
#ifdef MSG_PVM
  pvm_recv(_masterId,130);
  pvm_upkint(&status,1,1);
  if(status>=0){
    pvm_upkdouble(&dt,1,1);
    pvm_upkint(&dim,1,1);
    pvm_upkdouble(force,dim,1);
  }
#endif
#ifdef MSG_ZMQ
  zmq::message_t msg1;
  _socket.recv(&msg1);
  memcpy(&status,msg1.data(),1*sizeof(int));
  //
  if(status>=0){
    zmq::message_t msg2;
    _socket.recv(&msg2);
    memcpy(&dt,msg2.data(),1*sizeof(double));
    //
    zmq::message_t msg3;
    _socket.recv(&msg3);
    memcpy(&dim,msg3.data(),1*sizeof(int));
    //
    zmq::message_t msg4;
    _socket.recv(&msg4);
    memcpy(force,msg4.data(),dim*sizeof(double));
  }
#endif
  
  if(_verbose>1){
    cout << "Status = " << status << endl;
    if(status>=0){
      cout << "dt =" << dt << endl;
      cout << "dim = " << dim << endl;
      printArray(_verbose,cout,"\t M->S Force", force, dim);
    }
  }
}



void MessageAdmStruct::receiveInitializationFromMaster(){
  if(_verbose) cout << "---> Struct receives initialization message (msg 206)" << endl << flush;
  int rcvprotocol;
  int rcvunit;
#ifdef MSG_PVM
  pvm_recv(_masterId,206);
  pvm_upkint(&rcvprotocol,1,1);
  assert(_protocol == rcvprotocol);
  pvm_upkint(&rcvunit,1,1);
  assert(_unit == rcvunit);
  pvm_upkint(&_masterSendInitialState,1,1);
  pvm_upkint(&_masterRcvInitialState,1,1);
  pvm_upkint(&_masterRcvFinalState,1,1);
#endif
#ifdef MSG_ZMQ
  cout << "Error: file " << __FILE__ << " line " << __LINE__ << " -> ZeroMQ not yet implemented" << endl;
#endif
  
  if(_verbose>1){
    cout << "\t message protocol = " << _protocol << endl;
    cout << "\t physical unit = " << _unit << endl;
    cout << "\t masterSendInitialState = " << _masterSendInitialState << endl;
    cout << "\t masterRcvInitialState = " << _masterRcvInitialState << endl;
    cout << "\t masterRcvFinalState = " << _masterRcvFinalState << endl;
  }
}

void MessageAdmStruct::sendInitializationToMaster(int nbNode,int nbDofPerNode){
  if(_verbose) cout << "<--- Struct sends initialization message (msg 207)" << endl  << flush;
#ifdef MSG_PVM
  pvm_initsend(PvmDataDefault);
  pvm_pkint(&nbNode,1,1);
  pvm_pkint(&nbDofPerNode,1,1);
  pvm_send(_masterId,207);
#endif
#ifdef MSG_ZMQ
  cout << "Error: file " << __FILE__ << " line " << __LINE__ << " -> ZeroMQ not yet implemented" << endl;
#endif
  
  if(_verbose>1){
    cout << "\t nbNode = " << nbNode << endl;
    cout << "\t nbDofPerNode = " << nbDofPerNode << endl;
  }
}

void MessageAdmStruct::sendStateToMaster(int nbNode,int nbDofPerNode,double* disp,double* vel,double* accel)
{
  if(_verbose) cout << "<--- Struct sends state  (msg 215)" << endl  << flush;
  int dim = nbNode*nbDofPerNode;
#ifdef MSG_PVM
  pvm_initsend(PvmDataDefault);
  pvm_pkint(&nbNode,1,1);
  pvm_pkint(&nbDofPerNode,1,1);
  pvm_pkdouble(disp,dim,1);
  pvm_pkdouble(vel,dim,1);
  pvm_pkdouble(accel,dim,1);
  pvm_send(_masterId,215);
#endif
#ifdef MSG_ZMQ
  cout << "Error: file " << __FILE__ << " line " << __LINE__ << " -> ZeroMQ not yet implemented" << endl;
#endif
  
  if(_verbose>1){
    cout << "\t nbNode = " << nbNode << endl;
    cout << "\t nbDofPerNode = " << nbDofPerNode << endl;
    printArray(_verbose,cout,"\t S->M Disp", disp, dim);
    printArray(_verbose,cout,"\t S->M Vel", vel, dim);
    printArray(_verbose,cout,"\t S->M Accel", accel, dim);
  }
}

void MessageAdmStruct::receiveStateFromMaster(int& nbNode,int& nbDofPerNode,double& time, double* disp,double* vel,double* accel)
{
  if(_verbose) cout << "---> Struct receive state  (msg 225)" << endl  << flush;
  int dim;
#ifdef MSG_PVM
  pvm_recv(_masterId,225);
  pvm_upkint(&nbNode,1,1);
  pvm_upkint(&nbDofPerNode,1,1);
  pvm_upkdouble(&time,1,1);
  dim = nbNode*nbDofPerNode;
  pvm_upkdouble(disp,dim,1);
  pvm_upkdouble(vel,dim,1);
  pvm_upkdouble(accel,dim,1);
#endif
#ifdef MSG_ZMQ
  cout << "Error: file " << __FILE__ << " line " << __LINE__ << " -> ZeroMQ not yet implemented" << endl;
#endif
  
  if(_verbose>1){
    cout << "\t nbNode = " << nbNode << endl;
    cout << "\t nbDofPerNode = " << nbDofPerNode << endl;
    printArray(_verbose,cout,"\t M->S Disp", disp, dim);
    printArray(_verbose,cout,"\t M->S Vel", vel, dim);
    printArray(_verbose,cout,"\t M->S Accel", accel, dim);
  }
}


void MessageAdmStruct::sendInitializationEstimToMaster(int dim_param,int dim_observ){
  if(_verbose) cout << "<--- Struct sends initialization Estim message (msg 307)" << endl  << flush;
#ifdef MSG_PVM
  pvm_initsend(PvmDataDefault);
  pvm_pkint(&dim_param,1,1);
  pvm_pkint(&dim_observ,1,1);
  pvm_send(_masterId,307);
#endif
#ifdef MSG_ZMQ
  cout << "Error: file " << __FILE__ << " line " << __LINE__ << " -> ZeroMQ not yet implemented" << endl;
#endif
  
  if(_verbose>1){
    cout << "\t dim_param = " << dim_param << endl;
    cout << "\t dim_observ = " << dim_observ << endl;
  }
  
}


void MessageAdmStruct::receiveStateParamFromMaster(int& dim,double* disp,double* vel,double* accel,int dim_param,double* param,double& dt,
                                                   int& flag_loop,int& flag_msg,int& rank)
{
  if(_verbose) cout << "---> Struct receives state and param (msg 325)" << endl  << flush;
#ifdef MSG_PVM
  pvm_recv(_masterId,325);
  pvm_upkint(&dim,1,1);
  pvm_upkdouble(disp,dim,1);
  pvm_upkdouble(vel,dim,1);
  pvm_upkdouble(accel,dim,1);
  pvm_upkint(&dim_param,1,1);
  pvm_upkdouble(param,dim_param,1);
  pvm_upkdouble(&dt,1,1);
  pvm_upkint(&flag_loop,1,1);
  pvm_upkint(&flag_msg,1,1);
  pvm_upkint(&rank,1,1);
#endif
#ifdef MSG_ZMQ
  cout << "Error: file " << __FILE__ << " line " << __LINE__ << " -> ZeroMQ not yet implemented" << endl;
#endif
  
  if(_verbose>1){
    cout << "\t dim  = " << dim << endl;
    printArray(_verbose,cout,"\t M->S Disp", disp, dim);
    printArray(_verbose,cout,"\t M->S Velo", vel, dim);
    printArray(_verbose,cout,"\t M->S Accel", accel, dim);
    cout << "\t dim param = " << dim_param;
    printArray(_verbose,cout,"\t M->S Param", param, dim_param);
    cout << "\t dt  = " << dt << endl;
    cout << "\t flag_loop  = " << flag_loop << endl << flush;
    cout << "\t flag_msg  = " << flag_msg << endl << flush;
    cout << "\t rank  = " << rank << endl << flush;
  }
}

void MessageAdmStruct::sendStateParamInnovToMaster(int dim,double* disp,double* vel,double* accel,int dim_param,double* param,
                                                   int dim_innov,double* innov,int flag_output)
{
  if(_verbose>1) cout << "---> Struct sends state param and innov to master (msg 315) \n";
#ifdef MSG_PVM
  pvm_initsend(PvmDataDefault);
  pvm_pkdouble(disp,dim,1);
  pvm_pkdouble(vel,dim,1);
  pvm_pkdouble(accel,dim,1);
  pvm_pkdouble(param,dim_param,1);
  pvm_pkdouble(innov,dim_innov,1);
  pvm_pkint(&flag_output,1,1);
  pvm_send(_masterId,315);
#endif
#ifdef MSG_ZMQ
  cout << "Error: file " << __FILE__ << " line " << __LINE__ << " -> ZeroMQ not yet implemented" << endl;
#endif
  
  if(_verbose>1){
    printArray(_verbose,cout,"\t S->M disp", disp, dim);
    printArray(_verbose,cout,"\t S->M vel", vel, dim);
    printArray(_verbose,cout,"\t S->M accel", accel, dim);
    printArray(_verbose,cout,"\t S->M param", param, dim_param);
    printArray(_verbose,cout,"\t S->M innov", innov, dim_innov);
    cout << "\t flag_output = " << flag_output << endl;
  }
  
}

void MessageAdmStruct::sendInitStateParamToMaster(int dim,double* disp, double* vel, double* accel, int dim_param, double* param,
                                                  double* invCovParam,int dim_observ,double* lumpedInvCovObserv)
{
  if(_verbose>1) cout << "---> Struct sends mean vectors and cov matrices to master (msg 355) \n";
#ifdef MSG_PVM
  pvm_initsend(PvmDataDefault);
  pvm_pkdouble(disp,dim,1);
  pvm_pkdouble(vel,dim,1);
  pvm_pkdouble(accel,dim,1);
  pvm_pkdouble(param,dim_param,1);
  pvm_pkdouble(invCovParam,dim_param*dim_param,1);
  pvm_pkdouble(lumpedInvCovObserv,dim_observ,1);
  pvm_send(_masterId,355);
#endif
  
#ifdef MSG_ZMQ
  cout << "Error: file " << __FILE__ << " line " << __LINE__ << " -> ZeroMQ not yet implemented" << endl;
#endif
  
  if(_verbose>1){
    printArray(_verbose,cout,"\t S->M disp", disp, dim);
    printArray(_verbose,cout,"\t S->M vel", vel, dim);
    printArray(_verbose,cout,"\t S->M accel", accel, dim);
    printArray(_verbose,cout,"\t S->M param", param, dim_param);
    printArray(_verbose,cout,"\t S->M invCovParam", invCovParam, dim_param*dim_param);
    printArray(_verbose,cout,"\t S->M lumpedInvCovObserv", lumpedInvCovObserv, dim_observ);
  }
}
