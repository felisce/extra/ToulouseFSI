//===========================================================
//                    MasterFSI
//
// Part of MasterFSIContact,             Copyright Inria 2018
//===========================================================
/*!
 \file paramFSI.hpp
 \author Jean-Frederic Gerbeau
 */

#ifndef _PARAMFSI_HPP
#define _PARAMFSI_HPP

#include <string>
#include <iostream>
#include "GetPot.hpp"
#include "vectormatrix.hpp"

using namespace std;

/*!
 \class ParamFSI
 \brief Parameters of the data file
 
 \c ParamFSI is the class to handle the parameters given in the data file
 
 \author  Jean-Frederic Gerbeau
 */

class ParamFSI{
public:
  /*!
   Message Protocol (see details in protocol documentation)
   - \c protocol = 1 : legacy (masterFSI V2.0)
   - \c protocol = 2 : allows global solution initialization
   - \c protocol = 3 : data assimilation driven by masterSEIK
   */
  int protocol;
  /*! Physical unit. Useful to avoid unit mismatch between fluid and structure solvers.
   - \c unit = 1 : CGS
   - \c unit = 2 : SI
   */
  int unit;
  /*! Control the output
   -  \c verbose = 0 : minimal
   -  \c verbose = 1 : regular
   -  \c verbose = 2 : debug
   -  \c verbose = 3 : debug with huge output
   */
  int verbose;
  /*! Number of time iterations */
  int nbTimeIter;
  /*! Maximum number of inner fluid-structure iterations (sub-iterations) performed at each time step */
  int nbInnerIter;
  /*! Time discretization step */
  double timeStep;
  /*! Absolute tolerance for the convergence of the fluid-structure iteration */
  double absoluteTolerance;
  /*! Relative tolerance for the convergence of the fluid-structure iteration */
  double relativeTolerance;
  /*! Relative tolerance for the tangent problem in Newton method (see newton.hpp) */
  double newtonEtaMax;
  /*!
   A reference displacement (used for the convergence test)
   \todo referenceDisplacement is not yet used
   */
  double referenceDisplacement;
  /*!
   Select the coupling algorithm.
   - \c algorithm = 1 : fixed point with a constant relaxation
   - \c algorithm = 2 : fixed point with  Aitken acceleration
   - \c algorithm = 3 : Irons & Tuck implementation of Aitken acceleration
   - \c algorithm = 4 : Newton method
	  
   Remarks:
   
   - In exact arithmetic \c algorithm = 2 or 3 should give the same results.
   - if \c algorithm = 2 or 3 : \c omega is not used
   - if \c algorithm = 4 : Newton is exact or inexact depending on the implementation of the tangent problems in the solvers
   */
  int algorithm;
  /*! The value of fixed relaxation if \c algorithm = 1 */
 	double omega;
  /*!
	  Before the begining of the subiterations, the displacement of the interface can be extrapolated
   - \c prediction = 0 : no extrapolation \f$ d^{n+1}_0 = d^{n} \f$
   - \c prediction = 1 : first order extrapolation \f$ d^{n+1}_0 = d^{n} +  \delta t v^{n} \f$
   - \c prediction = 2 : second order extrapolation \f$ d^{n+1}_0 = d^{n} +  \delta t (3/2 v^{n} - 1/2 v^{n-1}) \f$
   */
  int prediction;
  /*!
   How to extract the displacement from the structure DOF
	  - \c interpolation = 1 : 3D, displacement = are exactly the structure dof
	  - \c interpolation = 2 : MITC, displacements = 3 dof every 5 dof
   */
  int interpolation;
  /*!
	  Path the the fluid solver
   */
  string fluidName;
  /*!
   Absolute path to the conform structure solver (ALE), e.g. structure_conf.sh
   */
  string structureConformName;
  /*!
   Absolute path to the immersed structure solver (Lagrange multiplier) e.g. structure_im.sh
   */
  string structureImmersedName;
  /*!
   Absolute path to the crack structure solver (ALE) e.g. structure_crack.sh
   */
  string structureCrackName;
  /*!
   Name of the test case
   */
  string testCaseName;
  /*!
   Directory for postprocessing
   */
  string postProcDir;
  /*!
   Path to the file providing the id of the fluid nodes belonging to the conform fluid-structure interface
   */
  string fileFSIConform;
  /*!
   Path to the file providing the number of nodes belonging to the immersed interface
   */
  string fileFSIImmersed;
  /*!
   Path to the file providing the id of the fluid nodes belonging to the crack fluid-structure interface
   */
  string fileFSICrack;
  /*!
   - \c conformFlag = 1 if there is a conform structure
   - \c conformFlag = 0 otherwise
   */
  int conformFlag;
  /*!
   - \c immersedFlag = 1 if there is a immersed structure
   - \c immersedFlag = 0 otherwise
   */
  int immersedFlag;
  /*!
   - \c crackFlag = 1 if there is a crack structure
   - \c crackFlag = 0 otherwise
   */
  int crackFlag;
  /*!
   - \c initializationType = 0 : do not send initial state
   - \c initializationType = 1 : send 0 as initial state and initTime
   - \c initializationType = 2 : send a solution read in an ensight file (displacement and velocity for the fluid and the struct)
   */
  int initializationType;
  /*! Directory where the solution files used for the initialization are */
  string initFileDirectory;
  /*! Name of the variable "fluid displacement" in the ensight case file */
  string initFluidDispName;
  /*! "fluid displacement" ensight case file */
  string initFluidDispCaseFile;
  /*! Name of the variable "structure displacement" in the ensight case file */
  string initStructDispName;
  /*! "struct displacement" ensight case file */
  string initStructDispCaseFile;
  /*! Name of the variable "fluid velocity" in the ensight case file */
  string initFluidVelName;
  /*! "fluid velocity" ensight case file */
  string initFluidVelCaseFile;
  /*! "struct vel" ensight case file */
  string initStructVelCaseFile;
  /*! Name of the variable "structure velocity" in the ensight case file */
  string initStructVelName;
  /*! "struct accel" ensight case file */
  string initStructAccelCaseFile;
  /*! Name of the variable "structure acceleration" in the ensight case file */
  string initStructAccelName;
  /*! Initial time of the simulation*/
  double initTime;
  /*! Initial time in the ensight file*/
  double initTimeEnsight;
  /*! The master can send either the interface displacement or the interface velocity to the fluid
   - \c fluidReceivesInterfDisp = 1 : master send interface displacement to the fluid
   - \c fluidReceivesInterfDisp = 0 : master send interface velocity to the fluid
   */
  int fluidReceivesInterfDisp;
  /*!
   If flagInitialStressCorrection = 1 : the first load sent by the fluid is stored and removed from
   all the subsequent loads (including the first one)
   */
  int flagInitialStressCorrection;
  /*!
   If flagInitialStressFromFile = 0 : load removed comes from model computation. If flagInitialStressFromFile = 1 :   load removed comes from file (useful when restart is used)
   */
  int flagInitialStressFromFile;
  /*!
   fileNameInitialStress : name of file where initial stress are stored (tipically postDir/testCaseName_initForceF)
   */
  string fileNameInitialStress;
  /*!
   Number of Windkessel boundary conditions
   */
  int nbWindkesselBC;
  /*!
   Initial Windkessel pressure
   */
  vector<double> initWindkesselPressure;
  /*! Robin Interface Boundary Condition:
   '0' --> residual = |[d,v]_k-[d,v]_k-1| (solid only)
   '1' --> residual = |[d,v]_k(solid) - [d,v]_k(fluid)|
   */
  int RobinInterfBC;
  
  /*!
	  ZeroMQ socket properties for the fluid solver
   */
  string socketTransportFluid; // e.g. tcp
  string socketAddressFluid; // e.g. 127.0.0.1
  string socketPortFluid; // e.g. 5555
  /*!
	  ZeroMQ socket properties for the conform structure solver
   */
  string socketTransportStructConform;// e.g. tcp
  string socketAddressStructConform;// e.g. 127.0.0.1
  string socketPortStructConform;// e.g. 5556
  /*!
   ZeroMQ socket properties for the immersed structure solver
   */
  string socketTransportStructImmersed;// e.g. tcp
  string socketAddressStructImmersed;// e.g. 127.0.0.1
  string socketPortStructImmersed;// e.g. 5557
  /*!
   ZeroMQ socket properties for the crack structure solver
   */
  string socketTransportStructCrack;// e.g. tcp
  string socketAddressStructCrack;// e.g. 127.0.0.1
  string socketPortStructCrack;// e.g. 5558
  
  /*! Constructor */
  ParamFSI(const GetPot& dfile);
};
#endif
