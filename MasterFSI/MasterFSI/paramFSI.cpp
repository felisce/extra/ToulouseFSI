//===========================================================
//                    MasterFSI
//
// Part of MasterFSIContact,             Copyright Inria 2018
//===========================================================
/*!
 \file paramFSI.cpp
 \author Jean-Frederic Gerbeau
 */

#include "paramFSI.hpp"

ParamFSI::ParamFSI(const  GetPot& dfile)
{
  protocol = dfile("protocol",1);
  unit = dfile("unit",1);
  verbose = dfile("verbose",1);
  nbTimeIter = dfile("nbTimeIter",10);
  nbInnerIter = dfile("nbInnerIter",50);
  timeStep = dfile("timeStep",0.1); // time step
  absoluteTolerance = dfile("absoluteTolerance",0.00001);
  relativeTolerance = dfile("relativeTolerance",0.00001);
  newtonEtaMax = dfile("newtonEtaMax",0.001);
  referenceDisplacement = dfile("referenceDisplacement",1.);
  algorithm = dfile("algorithm",2);
  omega = dfile("omega",1.);
  prediction = dfile("prediction",1);
  interpolation = dfile("interpolation",1);
  fluidName = dfile("fluidName","/home/gerbeau/fluid.csh");
  structureConformName = dfile("structureConformName","/home/gerbeau/struct_conf.csh");
  structureImmersedName = dfile("structureImmersedName","/home/gerbeau/struct_im.csh");
  structureCrackName = dfile("structureCrackName","/home/gerbeau/struct_crack.csh");
  testCaseName = dfile("testCaseName","fsi");
  postProcDir = dfile("postProcDir","fsi");
  fileFSIConform = dfile("fileFSIConform","conf_nodes.fsi");
  fileFSIImmersed = dfile("fileFSIImmersed","im_nodes.fsi");
  fileFSICrack = dfile("fileFSICrack","crack_nodes.fsi");
  conformFlag = dfile("conformFlag",1);
  immersedFlag = dfile("immersedFlag",0);
  crackFlag = dfile("crackFlag",0);
  
  initFluidDispName = dfile("initFluidDispName","Displacement");
  initFluidVelName = dfile("initFluidVelName","Velocity");
  initFluidDispCaseFile = dfile("initFluidDispCaseFile","fluid.case");
  initFluidVelCaseFile = dfile("initFluidVelCaseFile","fluid.case");
  
  initializationType = dfile("initializationType",0);
  initFileDirectory = dfile("initFileDirectory","./");
  initStructDispName = dfile("initStructDispName","Displacement");
  initStructVelName = dfile("initStructVelName","Velocity");
  initStructAccelName = dfile("initStructAccelName","Acceleration");
  initStructDispCaseFile = dfile("initStructDispCaseFile","struct.case");
  initStructVelCaseFile = dfile("initStructVelCaseFile","struct.case");
  initStructAccelCaseFile = dfile("initStructAccelCaseFile","struct.case");
  
  initTime = dfile("initTime",0.);
  initTimeEnsight = dfile("initTimeEnsight",0.);
  fluidReceivesInterfDisp = dfile("fluidReceivesInterfDisp",1);
  flagInitialStressCorrection	= dfile("flagInitialStressCorrection",0);
  flagInitialStressFromFile	= dfile("flagInitialStressFromFile",0);
  fileNameInitialStress	= dfile("fileNameInitialStress","fluidCase_initForceF");
  
  RobinInterfBC = dfile("RobinInterfBC",0);
  
  nbWindkesselBC = dfile("nbWindkesselBC",0);
  if(nbWindkesselBC){
    initWindkesselPressure.resize(nbWindkesselBC);
  }
  for(int i=0;i<nbWindkesselBC;i++){
    initWindkesselPressure[i] = dfile("initWindkesselPressure",0.,i);
  }
  
  socketTransportFluid = dfile("socketTransportFluid","tcp");
  socketAddressFluid = dfile("socketAddressFluid","127.0.0.1");
  socketPortFluid = dfile("socketPortFluid","5555");
  
  socketTransportStructConform = dfile("socketTransportStructConform","tcp");
  socketAddressStructConform = dfile("socketAddressStructConform","127.0.0.1");
  socketPortStructConform = dfile("socketPortStructConform","5556");
  
  socketTransportStructImmersed = dfile("socketTransportStructImmersed","tcp");
  socketAddressStructImmersed = dfile("socketAddressStructImmersed","127.0.0.1");
  socketPortStructImmersed = dfile("socketPortStructImmersed","5557");
  
  socketTransportStructCrack = dfile("socketTransportStructCrack","tcp");
  socketAddressStructCrack = dfile("socketAddressStructCrack","127.0.0.1");
  socketPortStructCrack = dfile("socketPortStructCrack","5558");
  
  cout << "----------------------------------------" << endl;
  cout << "MasterFSI V5.0 " << endl;
  int res_sys = system("date");
  cout << "Read data file" << endl;
  if(verbose>2) dfile.print();
  vector<string> ufos;
  ufos = dfile.unidentified_variables(59,
                                      "unit",
                                      "protocol",
                                      "verbose",
                                      "nbTimeIter",
                                      "nbInnerIter",
                                      "timeStep",
                                      "absoluteTolerance",
                                      "relativeTolerance",
                                      "newtonEtaMax",
                                      "referenceDisplacement",
                                      "algorithm",
                                      "omega",
                                      "prediction",
                                      "interpolation",
                                      "fluidName",
                                      "structureConformName",
                                      "structureImmersedName",
                                      "structureCrackName",
                                      "testCaseName",
                                      "postProcDir",
                                      "fileFSIConform",
                                      "fileFSIImmersed",
                                      "fileFSICrack",
                                      "conformFlag",
                                      "immersedFlag",
                                      "crackFlag",
                                      "initializationType",
                                      "initFileDirectory",
                                      "initFluidDispName","initFluidVelName","initFluidDispCaseFile","initFluidVelCaseFile",
                                      "initStructDispName","initStructVelName","initStructAccelName","initStructDispCaseFile","initStructVelCaseFile","initStructAccelCaseFile",
                                      "initTime","initTimeEnsight",
                                      "fluidReceivesInterfDisp",
                                      "flagInitialStressCorrection","flagInitialStressFromFile","fileNameInitialStress",
                                      "nbWindkesselBC",
                                      "initWindkesselPressure",
                                      "RobinInterfBC",
                                      "socketTransportFluid","socketAddressFluid","socketPortFluid",
                                      "socketTransportStructConform","socketAddressStructConform","socketPortStructConform",
                                      "socketTransportStructImmersed","socketAddressStructImmersed","socketPortStructImmersed",
                                      "socketTransportStructCrack","socketAddressStructCrack","socketPortStructCrack"
                                      );
  
  if(ufos.size()){
    cout << "MasterFSI V4.0: Incorrect datafile:" << endl;
    cout << "       The following variables are unknown:" << endl;
    for(vector<string>::const_iterator it = ufos.begin(); it != ufos.end();it++){
      cout << "      " << *it << endl;
    }
    exit(1);
  }
  if(verbose){
    cout << "............ CUT & PASTE data file (begin) ............ " << endl;
    cout << "# -*- getpot -*-" << endl;
    cout << "#----------------------------------------------" << endl;
    cout << "#     MasterFSI V5.0 data file " << endl;
    cout << "#----------------------------------------------" << endl;
    cout << endl;
    cout << "testCaseName = " << testCaseName << endl;
    cout << "postProcDir = " << postProcDir << endl;
    cout << "unit = " << unit << " # 1: CGS, 2: SI" << endl;
    cout << "protocol = " << protocol << endl;
    cout << "verbose = " << verbose << endl;
    cout << "fluidReceivesInterfDisp = " << fluidReceivesInterfDisp << " #  0: fluid rcv velo, 1: fluid rcv disp"<< endl;
    cout << "interpolation = " << interpolation << " # 1: three DOF, 2: five DOF per struct nodes" <<  endl;
    cout << "#----------------------------------------------" << endl;
    cout << "#   Time iterations   " << endl;
    cout << "#----------------------------------------------" << endl;
    cout << "nbTimeIter = " << nbTimeIter << endl;
    cout << "timeStep = " << timeStep << endl;
    cout << "#----------------------------------------------" << endl;
    cout << "#   Coupling algorithm   " << endl;
    cout << "#----------------------------------------------" << endl;
    cout << "algorithm = " << algorithm << " # 1: fixed point (omega), 2: Aitken, 4: Newton" << endl;
    cout << "prediction = " << prediction << " # 0: no prediction, 1: first order, 2: second order " << endl;
    cout << "omega = " << omega << " # only for algorithm 1" << endl;
    cout << "RobinInterfBC = " << RobinInterfBC << " # 0: residual = |X_k-X_{k-1}|(solid), 1: residual = |X_k(solid)-X_k(fluid)|, with X=(disp,vel) and k the actual inner iteration." << endl;
    cout << "#----------------------------------------------" << endl;
    cout << "#   Stopping test   " << endl;
    cout << "#----------------------------------------------" << endl;
    cout << "nbInnerIter = " << nbInnerIter << endl;
    cout << "absoluteTolerance = " << absoluteTolerance << endl;
    cout << "relativeTolerance = " << relativeTolerance << endl;
    cout << "newtonEtaMax = " << newtonEtaMax << " # if negative: fixed value, else : relative to nonlinear residual"
    << endl;
    cout << "referenceDisplacement = " << referenceDisplacement << endl;
    cout << "#----------------------------------------------" << endl;
    cout << "#   ALE, Immersed interface, Crack interface   " << endl;
    cout << "#----------------------------------------------" << endl;
    cout << "conformFlag = " << conformFlag << endl;
    cout << "immersedFlag = " << immersedFlag << endl;
    cout << "crackFlag = " << crackFlag << endl;
    cout << "fluidName = " << fluidName << endl;
    cout << "structureConformName = " << structureConformName << endl;
    cout << "structureImmersedName = " << structureImmersedName << endl;
    cout << "structureCrackName = " << structureCrackName << endl;
    cout << "fileFSIConform = " << fileFSIConform << endl;
    cout << "fileFSIImmersed = " << fileFSIImmersed << endl;
    cout << "fileFSICrack = " << fileFSICrack << endl;
    cout << "socketTransportFluid = " << socketTransportFluid << endl;
    cout << "socketAddressFluid = " << socketAddressFluid << endl;
    cout << "socketPortFluid = " << socketPortFluid << endl;
    cout << "socketTransportStructConform = " << socketTransportStructConform << endl;
    cout << "socketAddressStructConform = " << socketAddressStructConform << endl;
    cout << "socketPortStructConform = " << socketPortStructConform << endl;
    cout << "socketTransportStructImmersed = " << socketTransportStructImmersed << endl;
    cout << "socketAddressStructImmersed = " << socketAddressStructImmersed << endl;
    cout << "socketPortStructImmersed = " << socketPortStructImmersed << endl;
    cout << "socketTransportStructCrack = " << socketTransportStructCrack << endl;
    cout << "socketAddressStructCrack = " << socketAddressStructCrack << endl;
    cout << "socketPortStructCrack = " << socketPortStructCrack << endl;
    
    cout << "#----------------------------------------------" << endl;
    cout << "#     Initialization " << endl;
    cout << "#----------------------------------------------" << endl;
    cout << "initializationType = " << initializationType << " # 0: send nothing, 1: send 0, 2: send a sol read on the files , 3: send solution read on the files (fluid) and 0 to structure , 4: static, ie, read displacements of fluid and solid from files and set velocities to zero (take care of setting DispFluid and DispStruct compatible for getting VelFluidMesh=0), 5: send zero with exception of the windkessel" << endl;
    cout << "initTime = " << initTime << " # initial time of the simulation " <<  endl;
    cout << "initTimeEnsight = "<< initTimeEnsight << " # time of the initial data in the ensight file " << initTimeEnsight<< endl;
    cout << "initFileDirectory = " << initFileDirectory << endl;
    cout << "initFluidDispName = " << initFluidDispName<< endl;
    cout << "initFluidDispCaseFile = " << initFluidDispCaseFile << endl;
    cout << "initFluidVelName = " << initFluidVelName << endl;
    cout << "initFluidVelCaseFile = " << initFluidVelCaseFile << endl;
    cout << "initStructDispName = " << initStructDispName << endl;
    cout << "initStructDispCaseFile = " << initStructDispCaseFile<< endl;
    cout << "initStructVelName = " << initStructVelName << endl;
    cout << "initStructVelCaseFile = " << initStructVelCaseFile<< endl;
    cout << "initStructAccelName = " << initStructAccelName << endl;
    cout << "initStructAccelCaseFile = " << initStructAccelCaseFile << endl;
    cout << "flagInitialStressCorrection = " << flagInitialStressCorrection << " # 0: do nothing, 1: correct with initial stress" << endl;
    cout << "flagInitialStressFromFile = " << flagInitialStressFromFile << " # 0: correct stress from computation, 1: correct from file" << endl;
    cout << "fileNameInitialStress = " << fileNameInitialStress << " #  name of file with stress" << endl;
    cout << "nbWindkesselBC = " << nbWindkesselBC << endl;
    cout <<  "initWindkesselPressure = '";
    for(int i=0;i<nbWindkesselBC;i++) cout << initWindkesselPressure[i] << " ";
    cout << "'" << endl;
    
    cout << "............ CUT & PASTE data file (end  ) ............ " << endl;
  }
}
