//===========================================================
//                    MasterFSI
//
// Part of MasterFSIContact,             Copyright Inria 2018
//===========================================================
/*!
 \file utilstring.cpp
 \author Jean-Frederic Gerbeau
 */

#include "utilstring.hpp"
#include <iostream>
#include <fstream>
#include <vector>
#include <sstream>
#include <stdlib.h>

using namespace std;

char* c_string( string& s)
{
	char* p = new char[s.length()+1];
	s.copy(p,string::npos);
	p[s.length()] = 0;
	return p;
}

/*!
 Splits the string str by any char appears in delim, not including this char. 
 For instance, the call SplitString( "abcdefghabcd", "ade", v);
 will put in v the strings "bc" "fgh" "bc".
 */
void StringSplit(string str, string delim, vector<string>& results)
{
	results.resize(0);
	size_t cutAt;
	while( (cutAt = str.find_first_of(delim)) != str.npos )
	{
		if(cutAt > 0)
		{
			results.push_back(str.substr(0,cutAt));
		}
		str = str.substr(cutAt+1);
	}
	if(str.length() > 0)
	{
		results.push_back(str);
	}
}

/*! Count the occurence of char 'c' in string str */
int CountOccurenceChar(string str,char c){
	int count=0;
	char* ch_ptr = c_string(str);
	while(*ch_ptr != '\0'){
		if (*ch_ptr == c) ++count;
		++ch_ptr;
	}	
	return count;
}

/*! Replace a wilcard 'c' by the value of an integer  
 Example: ReplaceWilcardByInt("name.****.vct",12,'*')
 returns name.0012.vct
 */
string ReplaceWilcardByInt(string strWildcard,int i,char c){
	string strInt=strWildcard;
	unsigned int dim = CountOccurenceChar(strWildcard, c);
	ostringstream index;
	index << i;
	if(index.str().size()>dim){
		cout << "Integer too large for the dim of wildcard" << endl;
		exit(1);		
	}
	string zero="";
	for(unsigned int i=0;i<dim-index.str().size();i++) zero += "0";
	string num = zero + index.str();
	int start = strInt.find(c);
	strInt.replace(start,dim,num);
	return strInt;
}

/*! Print an array of double in a output stream */
void printArray(int verbose, ostream& fout,string title,double* tab,int dim){
	if ((verbose > 2) || (dim <= 36)) {
		int n=0;
		fout << title << ", [size = " << dim << "]" << endl;
		for(int i=0;i<dim;i++){
			if ( n == 0 ) fout << "\t";
			fout.setf(ios_base::scientific);	
			fout.precision(5);	
			fout.width(12);
			fout << tab[i];
			n++;
			if ( n == 6 ) {
				fout << std::endl;
				n=0;
			}
		}
		fout << endl;
	} else {
		int n=0;
		fout << title << ", [size = " << dim << "]" << endl;
		for(int i=0;i<18;i++){
			if ( n == 0 ) fout << "\t";
			fout.setf(ios_base::scientific);	
			fout.precision(5);	
			fout.width(12);
			fout << tab[i];
			n++;
			if ( n == 6 ) {
				fout << std::endl;
				n=0;
			}
		}
		fout << "\t ((...) - vector of high dimension - (...))" <<endl;
        for(int i=18;i>0;i--){
            if ( n == 0 ) fout << "\t";
            fout.setf(ios_base::scientific);
            fout.precision(5);
            fout.width(12);
            fout << tab[dim-i];
            n++;
            if ( n == 6 ) {
                fout << std::endl;
                n=0;
            }
        }    
        fout << endl;	
	}
		
}
