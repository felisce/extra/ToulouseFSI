//===========================================================
//                    MasterFSI
//
// Part of MasterFSIContact,             Copyright Inria 2018
//===========================================================
/*!
 \file masterFSI.cpp
 \author Jean-Frederic Gerbeau
 */

#include "GetPot.hpp"
#include "masterFSI.hpp"
#include <iostream>
#include "ensight.hpp"
#include "utilstring.hpp"

/*----------------------------------------------------------*/

MasterFSI::MasterFSI(ParamFSI* param,MessageAdmMasterFSI* msgAdm,OperInterf* operInterf,double time):
_paramDataFile(param),
_msgAdm(msgAdm),
_operInterf(operInterf),
_nbFluidNode(0),
_nbFluidDOFPerNode(0),
_nbFluidDOF(0),
_nbConfStructNode(0),
_nbConfStructDOFPerNode(0),
_nbConfStructDOF(0),
_nbImStructNode(0),
_nbImStructDOFPerNode(0),
_nbImStructDOF(0),
_nbCrackStructNode(0),
_nbCrackStructDOFPerNode(0),
_nbCrackStructDOF(0),
_time(time),
_nbTimeStepReduction(0)
{}

void MasterFSI::initialize()
{
  int dim = _operInterf->nbInterfStructDOF();
  assert(dim>0);
  //
  _fixedPoint = NULL;
  _newton = NULL;
  //
  if(_paramDataFile->algorithm==1 || _paramDataFile->algorithm==2 || _paramDataFile->algorithm==3)
    _fixedPoint = new FixedPointInterf(_paramDataFile->verbose,*_operInterf);
	 if(_paramDataFile->algorithm==4)
     _newton = new NewtonInterf(_paramDataFile->verbose,*_operInterf);
  //
}
//
void MasterFSI::firstContactWithFluid(){
  switch (_paramDataFile->protocol) {
    case 1:
    {
      //
      // Protocol 1
      //
      _msgAdm->sendFSINodesToFluid();
      _msgAdm->receiveCoorInterfFromFluid(_operInterf->coorInterfFluid().data().begin());
      break;
    }
    case 2:case 3:{
      //
      // Protocol 2 & 3
      //
      _msgAdm->sendInitializationToFluid();
      if(_msgAdm->masterRcvInitialState()){
        _msgAdm->receiveStateFromFluid(_nbFluidNode,_nbFluidDOFPerNode,_dispFluid,_veloFluid,_nbWindkessel,_varWindkessel);
        _nbFluidDOF = _nbFluidNode * _nbFluidDOFPerNode;
      }else{
        _msgAdm->receiveInitializationFromFluid(_nbFluidNode,_nbFluidDOFPerNode,_nbWindkessel);
        _nbFluidDOF = _nbFluidNode * _nbFluidDOFPerNode;
        _dispFluid.resize(_nbFluidDOF);
        _veloFluid.resize(_nbFluidDOF);
        _varWindkessel.resize(_nbWindkessel);
      }
      switch(_paramDataFile->initializationType){
        case 0: case 1:
          _dispFluid.clear();
          _veloFluid.clear();
          _varWindkessel.clear();
          break;
        case 2:{
          EnsightCase enscase(_paramDataFile->verbose);
          enscase.read(_paramDataFile->initFileDirectory,_paramDataFile->initFluidDispCaseFile);
          enscase.readVariable(_paramDataFile->initFluidDispName,_paramDataFile->initTimeEnsight,_dispFluid.data().begin(),_nbFluidDOF);
          enscase.read(_paramDataFile->initFileDirectory,_paramDataFile->initFluidVelCaseFile);
          enscase.readVariable(_paramDataFile->initFluidVelName,_paramDataFile->initTimeEnsight,_veloFluid.data().begin(),_nbFluidDOF);
          // Initial Windkessel pressure read from the master data file
          assert(_nbWindkessel == _paramDataFile->nbWindkesselBC);
          for(int i=0;i<_nbWindkessel;i++) _varWindkessel[i] = _paramDataFile->initWindkesselPressure[i];
          break;
        }
          
        case 3:{
          EnsightCase enscase(_paramDataFile->verbose);
          _dispFluid.clear();
          enscase.read(_paramDataFile->initFileDirectory,_paramDataFile->initFluidVelCaseFile);
          enscase.readVariable(_paramDataFile->initFluidVelName,_paramDataFile->initTimeEnsight,_veloFluid.data().begin(),_nbFluidDOF);
          // Initial Windkessel pressure read from the master data file
          assert(_nbWindkessel == _paramDataFile->nbWindkesselBC);
          for(int i=0;i<_nbWindkessel;i++) _varWindkessel[i] = _paramDataFile->initWindkesselPressure[i];
          break;
          
        }
        case 4:{
          EnsightCase enscase(_paramDataFile->verbose);
          enscase.read(_paramDataFile->initFileDirectory,_paramDataFile->initFluidDispCaseFile);
          enscase.readVariable(_paramDataFile->initFluidDispName,_paramDataFile->initTimeEnsight,_dispFluid.data().begin(),_nbFluidDOF);
          _veloFluid.clear();
          // Initial Windkessel pressure read from the master data file
          assert(_nbWindkessel == _paramDataFile->nbWindkesselBC);
          for(int i=0;i<_nbWindkessel;i++) _varWindkessel[i] = _paramDataFile->initWindkesselPressure[i];
          break;
        }
        case 5:{
          _dispFluid.clear();
          _veloFluid.clear();
          assert(_nbWindkessel == _paramDataFile->nbWindkesselBC);
          for(int i=0;i<_nbWindkessel;i++) _varWindkessel[i] = _paramDataFile->initWindkesselPressure[i];
          break;
        }
        default:{
          cout << "MasterFSI::firstContactWithFluid() : unknown initialization type" << endl;
          exit(1);
        }
      }
      
      
      if(_msgAdm->masterSendInitialState()){
        _msgAdm->sendStateToFluid(_nbFluidNode, _nbFluidDOFPerNode, _time, _dispFluid, _veloFluid, _nbWindkessel, _varWindkessel);
      }
      _msgAdm->sendFSINodesToFluid();
    }
    case 6:
    {
      _msgAdm->sendFSINodesToFluid();
      _msgAdm->sendLumpedMassToFluid(_operInterf->hasLumpedMass(),_operInterf->structInterfLumpMass().data().begin(),_operInterf->structInterfLumpMassDim());
      break;
    }
    default:
      break;
  }
}
//
void MasterFSI::firstContactWithStruct(){
  switch (_paramDataFile->protocol) {
    case 1:
    case 6:
    {
      //
      // Protocol 1
      //
      if(_paramDataFile->conformFlag) {
        if ( _paramDataFile->protocol == 1 )
          _msgAdm->receiveFromStructConf(_operInterf->structInterfDisp().data().begin(),
                                         _operInterf->structInterfVelo().data().begin(),_operInterf->nbInterfConfStructDOF());
        else
          _msgAdm->receiveFromStructConfFirstContact(_operInterf->structInterfLumpMass().data().begin(),
                                                     _operInterf->structInterfDisp().data().begin(),
                                                     _operInterf->structInterfVelo().data().begin(),
                                                     _operInterf->nbInterfConfStructDOF(),
                                                     _operInterf->structInterfLumpMassDim(),
                                                     _operInterf->hasLumpedMass());
      }
      if(_paramDataFile->immersedFlag) _msgAdm->receiveFromStructIm(_operInterf->structInterfDisp().data().begin(),
                                                                    _operInterf->structInterfVelo().data().begin(), _operInterf->nbInterfImStructDOF());
      if(_paramDataFile->crackFlag) _msgAdm->receiveFromStructCrack(_operInterf->structInterfDisp().data().begin(),
                                                                    _operInterf->structInterfVelo().data().begin(),_operInterf->nbInterfCrackStructDOF());
      break;
    }
    case 2: case 3:{
      //
      // Protocol 2 & 3
      //
      if(_paramDataFile->conformFlag) _msgAdm->sendInitializationToStruct(_msgAdm->structConfId());
      if(_paramDataFile->immersedFlag) _msgAdm->sendInitializationToStruct(_msgAdm->structImId());
      if(_paramDataFile->crackFlag) _msgAdm->sendInitializationToStruct(_msgAdm->structCrackId());
      //
      if(_msgAdm->masterRcvInitialState()){
        if(_paramDataFile->conformFlag) _msgAdm->receiveStateFromStruct(_nbConfStructNode,_nbConfStructDOFPerNode,_dispConfStruct,_veloConfStruct,_accelConfStruct,_msgAdm->structConfId());
        _nbConfStructDOF = _nbConfStructNode * _nbConfStructDOFPerNode;
        if(_paramDataFile->immersedFlag) _msgAdm->receiveStateFromStruct(_nbImStructNode,_nbImStructDOFPerNode,_dispImStruct,_veloImStruct,_accelImStruct,_msgAdm->structImId());
        _nbImStructDOF = _nbImStructNode * _nbImStructDOFPerNode;
        if(_paramDataFile->crackFlag) _msgAdm->receiveStateFromStruct(_nbCrackStructNode,_nbCrackStructDOFPerNode,_dispCrackStruct,_veloCrackStruct,_accelCrackStruct,_msgAdm->structCrackId());
        _nbCrackStructDOF = _nbCrackStructNode * _nbCrackStructDOFPerNode;
      }else{
        if(_paramDataFile->conformFlag) _msgAdm->receiveInitializationFromStruct(_nbConfStructNode,_nbConfStructDOFPerNode,_msgAdm->structConfId());
        _nbConfStructDOF = _nbConfStructNode * _nbConfStructDOFPerNode;
        _dispConfStruct.resize(_nbConfStructDOF);
        _veloConfStruct.resize(_nbConfStructDOF);
        _accelConfStruct.resize(_nbConfStructDOF);
        if(_paramDataFile->immersedFlag) _msgAdm->receiveInitializationFromStruct(_nbImStructNode,_nbImStructDOFPerNode,_msgAdm->structImId());
        _nbImStructDOF = _nbImStructNode * _nbImStructDOFPerNode;
        _dispImStruct.resize(_nbImStructDOF);
        _veloImStruct.resize(_nbImStructDOF);
        _accelImStruct.resize(_nbImStructDOF);
        if(_paramDataFile->crackFlag) _msgAdm->receiveInitializationFromStruct(_nbCrackStructNode,_nbCrackStructDOFPerNode,_msgAdm->structCrackId());
        _nbCrackStructDOF = _nbCrackStructNode * _nbCrackStructDOFPerNode;
        _dispCrackStruct.resize(_nbCrackStructDOF);
        _veloCrackStruct.resize(_nbCrackStructDOF);
        _accelCrackStruct.resize(_nbCrackStructDOF);
      }
      switch(_paramDataFile->initializationType){
        case 0:case 1: case 3: case 5:
          if(_paramDataFile->conformFlag){
            _dispConfStruct.clear();
            _veloConfStruct.clear();
            _accelConfStruct.clear();
          }
          if(_paramDataFile->immersedFlag){
            _dispImStruct.clear();
            _veloImStruct.clear();
            _accelImStruct.clear();
          }
          if(_paramDataFile->crackFlag){
            _dispCrackStruct.clear();
            _veloCrackStruct.clear();
            _accelCrackStruct.clear();
          }
          break;
        case 2:{
          if(_paramDataFile->immersedFlag || _paramDataFile->crackFlag){
            cout << "Initialization by file for immersed or crack structure not yet implemented" << endl;
            exit(1);
          }
          EnsightCase enscase(_paramDataFile->verbose);
          enscase.read(_paramDataFile->initFileDirectory,_paramDataFile->initStructDispCaseFile);
          enscase.readVariable(_paramDataFile->initStructDispName,_paramDataFile->initTimeEnsight,_dispConfStruct.data().begin(),_nbConfStructDOF);
          enscase.read(_paramDataFile->initFileDirectory,_paramDataFile->initStructVelCaseFile);
          enscase.readVariable(_paramDataFile->initStructVelName,_paramDataFile->initTimeEnsight,_veloConfStruct.data().begin(),_nbConfStructDOF);
          enscase.read(_paramDataFile->initFileDirectory,_paramDataFile->initStructAccelCaseFile);
          enscase.readVariable(_paramDataFile->initStructAccelName,_paramDataFile->initTimeEnsight,_accelConfStruct.data().begin(),_nbConfStructDOF);
          break;
        }
          
        case 4:{
          if(_paramDataFile->immersedFlag || _paramDataFile->crackFlag){
            cout << "Initialization by file for immersed or crack structure not yet implemented" << endl;
            exit(1);
          }
          EnsightCase enscase(_paramDataFile->verbose);
          enscase.read(_paramDataFile->initFileDirectory,_paramDataFile->initStructDispCaseFile);
          enscase.readVariable(_paramDataFile->initStructDispName,_paramDataFile->initTimeEnsight,_dispConfStruct.data().begin(),_nbConfStructDOF);
          _veloConfStruct.clear();
          _accelConfStruct.clear();
          break;
        }
          
        default:{
          cout << "MasterFSI::firstContactWithStruct() : unknown initialization type" << endl;
          exit(1);
        }
          
          
          
      }
      if(_msgAdm->masterSendInitialState()){
        if(_paramDataFile->conformFlag)
          _msgAdm->sendStateToStruct(_nbConfStructNode, _nbConfStructDOFPerNode, _time, _dispConfStruct,_veloConfStruct,_accelConfStruct,_msgAdm->structConfId());
        if(_paramDataFile->immersedFlag)
          _msgAdm->sendStateToStruct(_nbImStructNode,_nbImStructDOFPerNode, _time, _dispImStruct,_veloImStruct,_accelImStruct,_msgAdm->structImId());
        if(_paramDataFile->crackFlag)
          _msgAdm->sendStateToStruct(_nbCrackStructNode,_nbCrackStructDOFPerNode, _time, _dispCrackStruct,_veloCrackStruct,_accelCrackStruct,_msgAdm->structCrackId());
      }
      if(_paramDataFile->conformFlag) _msgAdm->receiveFromStructConf(_operInterf->structInterfDisp().data().begin(),_operInterf->structInterfVelo().data().begin(),_operInterf->nbInterfConfStructDOF());
      if(_paramDataFile->immersedFlag) _msgAdm->receiveFromStructIm(_operInterf->structInterfDisp().data().begin(),
                                                                    _operInterf->structInterfVelo().data().begin(), _operInterf->nbInterfImStructDOF());
      if(_paramDataFile->crackFlag) _msgAdm->receiveFromStructCrack(_operInterf->structInterfDisp().data().begin(),
                                                                    _operInterf->structInterfVelo().data().begin(),_operInterf->nbInterfCrackStructDOF());
      break;
    }
    default:
      cout<< "Protocol ???" << endl;
      exit(1);
      break;
  }
}




void MasterFSI::lastContactWithFluid(){
  switch (_paramDataFile->protocol) {
    case 1:
    case 6:
    {
      //
      // Protocol 1
      //
      _msgAdm->sendStopToFluid();
      break;
    }
    case 2:case 3:{
      //
      // Protocol 2 & 3
      //
      _msgAdm->sendStopToFluid();
      if(_msgAdm->masterRcvFinalState()){
        int rcvnbflnode;
        int rcvnbfldof;
        int rcvnbwind;
        _msgAdm->receiveStateFromFluid(rcvnbflnode,rcvnbfldof,_dispFluid,_veloFluid,rcvnbwind,_varWindkessel);
        assert(rcvnbwind == _nbWindkessel);
        assert(rcvnbflnode == _nbFluidNode);
        assert(rcvnbfldof == _nbFluidDOFPerNode);
      }
    }
    default:
      break;
  }
}

void MasterFSI::lastContactWithStruct(){
  switch (_paramDataFile->protocol) {
    case 1:
    case 6:
    {
      //
      // Protocol 1
      //
      _msgAdm->sendStopToStruct();
      break;
    }
    case 2:case 3:{
      //
      // Protocol 2 & 3
      //
      _msgAdm->sendStopToStruct();
      if(_msgAdm->masterRcvFinalState()){
        int rcvnbnode;
        int rcvnbdof;
        _msgAdm->receiveStateFromStruct(rcvnbnode,rcvnbdof,_dispConfStruct,_veloConfStruct,_accelConfStruct,_msgAdm->structConfId());
        assert(rcvnbnode == _nbConfStructNode);
        assert(rcvnbdof == _nbConfStructDOFPerNode);
      }
      break;
    }
    default:
      break;
  }
}




void MasterFSI::iterate(int iter,double dt)
{
  _time += dt;
  if(_paramDataFile->verbose){
    cout << "===================================================" << endl;
    cout << "MasterFSI Iteration " << iter << ", time = " << _time << endl;
    int res_sys = system("date");
    cout << "===================================================" << endl;
  }
  
  int maxiter = _paramDataFile->nbInnerIter;
  int cvg=1;
  /*
   Initial guess : extrapolation
   */
  if(_paramDataFile->fluidReceivesInterfDisp){
    // prediction only if the fluid receives a displacement, i.e. if _vec1StructInterf = disp, and _vec2StructInterf = velo
    switch (_paramDataFile->prediction) {
      case 0:
        // no prediction
        break;
      case 1:
        // first order prediction
        cout << "First order prediction not yet implemented in this version of masterfsi" << endl << flush;
        exit(1);
        break;
      case 2:
        // second order prediction
        cout << "Second order prediction not yet implemented in this version of masterfsi" << endl << flush;
        exit(1);
        break;
      default:
        cout << "MasterFSI::iterate: unknown prediction " << endl;
        exit(1);
        break;
    }
  }
  /*
   Resolution of the time step
   */
  switch (_paramDataFile->algorithm) {
    case 1:case 2: case 3:
      cvg = _fixedPoint->solve(_paramDataFile->absoluteTolerance,_paramDataFile->relativeTolerance,
                               maxiter,_paramDataFile->algorithm-1,_paramDataFile->omega);
      break;
    case 4:
      cvg = _newton->solve(_paramDataFile->absoluteTolerance,_paramDataFile->relativeTolerance,
                           maxiter,0,_paramDataFile->newtonEtaMax);
      break;
    case 5: // Robin-Neumann explicit coupling schemes
      double eps_ext;
      _operInterf->compute(1,eps_ext);
      _operInterf->structInterfDisp() = _operInterf->resultStructInterfDisp();
      _operInterf->structInterfVelo() = _operInterf->resultStructInterfVelo();
      maxiter = 0;
      cvg = 0;
      break;
    default:
      cout << "MasterFSI::iterate : unknown algorithm" << endl;
      break;
  }
  if(cvg == 1) {
    cout << "WARNING : MasterFSI::iterate : Inners iterations failed\n";
    
    cout << "EXPERIMENTAL : the timestep is divided by 2" << endl;
    // Newton convergence failed: we reduce the time step and increment nbTimeStepReduction
    dt = dt/2.;
    _nbTimeStepReduction++;
    if(_nbTimeStepReduction>5){
      // Too many time step reductions : we stop 
      _msgAdm->sendStopToFluid();
      _msgAdm->sendStopToStruct();  
      exit(1);  
      
    }
    
  }  else {
    if(_nbTimeStepReduction){
      // Newton convergence succeeded, but the time step was reduced : we increase the time step, and decrement nbTimeStepReduction 
      dt = dt*2.;
      _nbTimeStepReduction--;
    }
    if(_paramDataFile->verbose) cout << "Number of inner iterations       : " << maxiter << endl;
    if(_paramDataFile->verbose) cout << "Number of fluid-structure evaluations : " << _operInterf->nbEval() << endl;
    if(_paramDataFile->verbose) cout << "Number of tangent fluid-structure evaluations : " << _operInterf->nbEvalTangent() << endl;
  }
}

void MasterFSI::initializeEstim(){
  //	cout << "Do Nothing" << endl;
}

bool MasterFSI::stopIterate(int iter){
  if(iter == _paramDataFile->nbTimeIter) return true;
  else return false;
}
