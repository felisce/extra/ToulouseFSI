//===========================================================
//                    MasterFSI
//
// Part of MasterFSIContact,             Copyright Inria 2018
//===========================================================
/*!
 \file messageAdmFSI.hpp
 \author Jean-Frederic Gerbeau
 */
#ifndef _MESSAGEADMFSI_HPP
#define _MESSAGEADMFSI_HPP

#define HOSTNAME "localhost"

#ifdef MSG_PVM
#include  <pvm3.h>
#endif

#ifdef MSG_ZMQ
#include <zmq.hpp>
#endif

#include  <string>

#define FLAGFSI_LOOP_STOP -1
#define FLAGFSI_LOOP_REDO_TIME_ITER 0
#define FLAGFSI_LOOP_NEW_TIME_ITER 1
#define FLAGFSI_LOOP_TANGENT_OPER 2

#define UNIT_CGS 1
#define UNIT_SI 2

using namespace std;



/*!
 \class MessageAdmFSI
 \brief Messages Administrator
 
 \c MessageAdmFSI is the class handling the messages between the master and the workers
 
 \author Jean-Frederic Gerbeau
 */

class MessageAdmFSI{
protected:
  int _verbose;
  int _protocol;
  int _unit;
  int _masterSendInitialState;
  int _masterRcvInitialState;
  int _masterRcvFinalState;
  int _fluidReceivesInterfDisp;
#ifdef MSG_PVM
  int _mytid;
#endif
public:
  MessageAdmFSI(int argc, char *argv[],const int& verbose,const int& protocol,int unit);
  int masterSendInitialState(){return _masterSendInitialState;}
  int masterRcvInitialState(){return _masterRcvInitialState;}
  int masterRcvFinalState(){return _masterRcvFinalState;}
  int fluidReceivesInterfDisp(){return _fluidReceivesInterfDisp;}
  void setFluidReceivesInterfDisp(int flag){_fluidReceivesInterfDisp=flag;}
  void setVerbose(int newVerbose){_verbose = newVerbose;}
  int getVerbose(){return _verbose;}
  void setProtocol(int newProtocol){_verbose = newProtocol;}
  int getProtocol(){return _protocol;}
};

char* c_string( string& s);

#endif
