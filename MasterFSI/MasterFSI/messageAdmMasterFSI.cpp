//===========================================================
//                    MasterFSI
//
// Part of MasterFSIContact,             Copyright Inria 2018
//===========================================================
/*!
 \file messageAdmMasterFSI.cpp
 \author Jean-Frederic Gerbeau
 */
#include <iostream>
#include <fstream>
#undef MSG_VERBOSE

#include "messageAdmMasterFSI.hpp"
#include "utilstring.hpp"

using namespace std;

MessageAdmMasterFSI::MessageAdmMasterFSI(int argc, char *argv[],ParamFSI* param,OperInterf* operInterf,
                                         int masterSendInitialState,int masterRcvInitialState,int masterRcvFinalState):
MessageAdmFSI(argc,argv, param->verbose, param->protocol, param->unit),
_paramDataFile(param),_operInterf(operInterf),_ipass(true)
#ifdef MSG_ZMQ
,_zmq_context(1)
#endif
{
#ifdef MSG_PVM
  _fluidHost="localhost";
  _structHostConf="localhost";
  _structHostIm="localhost";
  _structHostCrack="localhost";
  _mytid = pvm_mytid();
#endif
#ifdef MSG_ZMQ
  _fluidId = 0;
  _socketFluid = new zmq::socket_t(_zmq_context, ZMQ_PAIR);
  if(_paramDataFile->socketPortFluid != "")
    _fluidHost = _paramDataFile->socketTransportFluid + "://"+_paramDataFile->socketAddressFluid+":"+_paramDataFile->socketPortFluid;
  else
    _fluidHost = _paramDataFile->socketTransportFluid + "://"+_paramDataFile->socketAddressFluid;
  //
  _socketStruct.resize(3);
  _structConfId = 0;
  _socketStruct[_structConfId] = new zmq::socket_t(_zmq_context, ZMQ_PAIR);
  if(_paramDataFile->socketPortStructConform != "")
    _structHostConf = _paramDataFile->socketTransportStructConform + "://"+_paramDataFile->socketAddressStructConform+":"+_paramDataFile->socketPortStructConform;
  else
    _structHostConf = _paramDataFile->socketTransportStructConform + "://"+_paramDataFile->socketAddressStructConform;
  //
  _structImId = 1;
  _socketStruct[_structImId] = new zmq::socket_t(_zmq_context, ZMQ_PAIR);
  if(_paramDataFile->socketPortStructImmersed != "")
    _structHostIm = _paramDataFile->socketTransportStructImmersed + "://"+_paramDataFile->socketAddressStructImmersed+":"+_paramDataFile->socketPortStructImmersed;
  else
    _structHostIm = _paramDataFile->socketTransportStructImmersed + "://"+_paramDataFile->socketAddressStructImmersed;
  //
  _structCrackId = 2;
  _socketStruct[_structCrackId] = new zmq::socket_t(_zmq_context, ZMQ_PAIR);
  if(_paramDataFile->socketPortStructCrack != "")
    _structHostCrack = _paramDataFile->socketTransportStructCrack + "://"+_paramDataFile->socketAddressStructCrack+":"+_paramDataFile->socketPortStructCrack;
  else
    _structHostCrack = _paramDataFile->socketTransportStructCrack + "://"+_paramDataFile->socketAddressStructCrack;
#endif
  _masterSendInitialState = masterSendInitialState;
  _masterRcvInitialState = masterRcvInitialState;
  _masterRcvFinalState = masterRcvFinalState;
  _fluidReceivesInterfDisp = _paramDataFile->fluidReceivesInterfDisp;
  
  if(param->verbose){
#ifdef MSG_PVM
    cout << "Task id of the MasterFSI  : " << _mytid << endl;
    cout << "\nThe workers are:\n";
    cout << "\t" << _paramDataFile->fluidName << " on " << _fluidHost << endl;
    if(_paramDataFile->conformFlag) cout << "\t" << _paramDataFile->structureConformName  << " (conformal) on " << _structHostConf << endl<< endl;
    if(_paramDataFile->immersedFlag) cout << "\t" << _paramDataFile->structureImmersedName  << " (immersed) on " << _structHostIm << endl<< endl;
    if(_paramDataFile->crackFlag) cout << "\t" << _paramDataFile->structureCrackName  << " (crack) on " << _structHostCrack << endl<< endl;
#endif
    cout << "    masterSendInitialState = " << _masterSendInitialState << endl;
    cout << "    masterRcvInitialState = " << _masterRcvInitialState << endl;
    cout << "    masterRcvFinalState = " << _masterRcvFinalState << endl;
  }
}

void MessageAdmMasterFSI::spawnFluidStruct(int rank){
  int     flag = 0;
  int  ntask_fluid=1,ntask_struct=1;
  
  char* args[]={(char*)"000",(char*)'\0'};
  char strnum[4];
  if (rank < 10)
    sprintf(strnum,"00%d",rank);
  else if (rank < 100)
    sprintf(strnum,"0%d",rank);
  else
    sprintf(strnum,"%d",rank);
  args[0] = strnum;
#ifdef MSG_PVM
  //---------------
  int info = pvm_spawn(c_string(_paramDataFile->fluidName),args, flag, c_string(_fluidHost), ntask_fluid, &_fluidId);
  if(_paramDataFile->verbose) cout << "[PVM] spawn fluid... ";
  if(info && _paramDataFile->verbose) cout << "the fluid task id is " << _fluidId << endl;
  if(!info){
    cout << "cannot execute " << _paramDataFile->fluidName << " !\n";
    exit(1);
  }
  //---------------
  if(_paramDataFile->conformFlag){
    info = pvm_spawn(c_string(_paramDataFile->structureConformName),args, flag,
                     c_string(_structHostConf),ntask_struct, &_structConfId);
    if(_paramDataFile->verbose) cout << "[PVM] spawn structure (conformal)... ";
    if(info && _paramDataFile->verbose) cout <<"the structure task id is " << _structConfId << endl;
    if(!info){
      cout << "cannot execute " << _paramDataFile->structureConformName << " !\n";
      exit(1);
    }
  }
  if(_paramDataFile->immersedFlag){
    info = pvm_spawn(c_string(_paramDataFile->structureImmersedName),args, flag,
                     c_string(_structHostIm),ntask_struct, &_structImId);
    if(_paramDataFile->verbose) cout << "[PVM] spawn structure (immersed)... ";
    if(info && _paramDataFile->verbose) cout << "the structure task id is " << _structImId << endl;
    else{
      cerr << "cannot execute " << _paramDataFile->structureImmersedName << " !\n";
      exit(1);
    }
  }
  if(_paramDataFile->crackFlag){
    info = pvm_spawn(c_string(_paramDataFile->structureCrackName),args, flag,
                     c_string(_structHostCrack),ntask_struct, &_structCrackId);
    cout << "[PVM] spawn structure... ";
    if(info) cout << "the structure task id is " << _structCrackId << endl;
    if(!info){
      cout << "cannot execute " << _paramDataFile->structureCrackName << " !\n";
      exit(1);
    }
  }
#endif
#ifdef MSG_ZMQ
  cout << "[ZeroMQ] binds Fluid to " << _fluidHost << endl;
  _socketFluid->bind(_fluidHost.c_str());
  if(_paramDataFile->conformFlag){
    cout << "[ZeroMQ] binds Structure (conform) to " << _structHostConf << endl;
    _socketStruct[0]->bind(_structHostConf.c_str());
  }
  if(_paramDataFile->immersedFlag){
    cout << "[ZeroMQ] binds Structure (immersed) to " << _structHostIm << endl;
    _socketStruct[1]->bind(_structHostIm.c_str());
  }
  if(_paramDataFile->crackFlag){
    cout << "[ZeroMQ] binds Structure (crack) to " << _structHostCrack << endl;
    _socketStruct[2]->bind(_structHostCrack.c_str());
  }
#endif
}

void MessageAdmMasterFSI::readFSINodesFiles(){
  
  VectorInt fnifsconf;
  VectorInt fnifscrack;
  //
  // This function initialiazes all the following quantities (operInterf)
  //
  int& nbNodeFSIConf=_operInterf->nbNodeFSIConf();
  int& nbNodeFSIIm=_operInterf->nbNodeFSIIm();
  int& nbNodeFSICrack=_operInterf->nbNodeFSICrack();
  int& nbNodeFSI=_operInterf->nbNodeFSI();
  VectorInt& nodeFSI=_operInterf->nodeFSI();
  int& nbInterfFluidDOF=_operInterf->nbInterfFluidDOF();
  int& nbInterfConfFluidDOF=_operInterf->nbInterfConfFluidDOF();
  int& nbInterfImFluidDOF=_operInterf->nbInterfImFluidDOF();
  int& nbInterfCrackFluidDOF=_operInterf->nbInterfCrackFluidDOF();
  int& nbInterfStructDOF=_operInterf->nbInterfStructDOF();
  int& nbInterfConfStructDOF=_operInterf->nbInterfConfStructDOF();
  int& nbInterfImStructDOF=_operInterf->nbInterfImStructDOF();
  int& nbInterfCrackStructDOF=_operInterf->nbInterfCrackStructDOF();
  
  //=========== Conformal =============
  //
  if(_paramDataFile->conformFlag){
    if(_paramDataFile->verbose) cout << "Read the conformal fsi nodes on " << _paramDataFile->fileFSIConform << endl;
    ifstream fileFSIConform(_paramDataFile->fileFSIConform.c_str());
    if(!fileFSIConform){
      cout << "I cannot find the fileFSIConf: " << _paramDataFile->fileFSIConform << endl;
      exit(1);
    }
    fileFSIConform >> nbNodeFSIConf;
    fnifsconf.resize(nbNodeFSIConf);
    for(int i=0;i<nbNodeFSIConf;i++){
      fileFSIConform >> fnifsconf[i];
    }
    fileFSIConform.close();
  } else {
    nbNodeFSIConf=0;
  }
  //
  //
  // =========== Im =============
  //
  if(_paramDataFile->immersedFlag){
    if(_paramDataFile->verbose) cout << "Read the number of immersed fsi nodes on " << _paramDataFile->fileFSIImmersed << endl;
    ifstream fileFSIIm(_paramDataFile->fileFSIImmersed.c_str());
    if(!fileFSIIm){
      cout << "I cannot find the fileFSIIm: " << _paramDataFile->fileFSIImmersed << endl;
      exit(1);
    }
    fileFSIIm >> nbNodeFSIIm;
    fileFSIIm.close();
  } else {
    nbNodeFSIIm=0;
  }
  //
  // ===========   Crack   =============
  //
  if(_paramDataFile->crackFlag){
    if(_paramDataFile->verbose) cout << "Read the 'crack' fsi nodes on " << _paramDataFile->fileFSICrack << endl;
    ifstream fileFSICrack(_paramDataFile->fileFSICrack.c_str());
    if(!fileFSICrack){
      cout << "I cannot find the fileFSICrack: " << _paramDataFile->fileFSICrack << endl;
      exit(1);
    }
    fileFSICrack >> nbNodeFSICrack;
    fnifscrack.resize(2*nbNodeFSICrack);
    for(int i=0;i<nbNodeFSICrack;i++){
      fileFSICrack >> fnifscrack[i];
    }
    fileFSICrack.close();
    
    int tmpint;
    string crackFlag2_name = _paramDataFile->fileFSICrack + "2";
    if(_paramDataFile->verbose) cout << "Crack surface : we read the other matching fluid points on " << crackFlag2_name << endl;
    ifstream fileFSICrack2(crackFlag2_name.c_str());
    fileFSICrack2 >> tmpint;
    if (tmpint != nbNodeFSICrack){
      cout << nbNodeFSICrack << endl;
      cout << tmpint << endl;
      cout << "MessageAdmMasterFSI::readFSINodesFiles():  nonmatching number of nodes on the two faces of the 'crack' structure"
      << endl;
      exit(1);
    }
    for(int i=0;i<nbNodeFSICrack;i++){
      fileFSICrack2 >> fnifscrack[nbNodeFSICrack+i];
      //    cout << fnifs[nbNodeFSI+i] << endl;
    }
    fileFSICrack2.close();
  } else {
    nbNodeFSICrack=0;
  }
  //
  nbNodeFSI = nbNodeFSIConf+nbNodeFSICrack*2;
  
  nodeFSI.resize(nbNodeFSI);
  for(int i=0;i<nbNodeFSIConf;i++){
    nodeFSI[i] = fnifsconf[i];
  }
  for(int i=0;i<nbNodeFSICrack;i++){
    nodeFSI[i+nbNodeFSIConf] = fnifscrack[i];
  }
  for(int i=0;i<nbNodeFSICrack;i++){
    nodeFSI[i+nbNodeFSICrack+nbNodeFSIConf] = fnifscrack[nbNodeFSICrack+i];
  }
  //
  //=================================
  nbInterfConfFluidDOF = 3 * nbNodeFSIConf;
  nbInterfImFluidDOF = 3 * nbNodeFSIIm;
  nbInterfCrackFluidDOF = 3 * 2 * nbNodeFSICrack;
  switch(_paramDataFile->interpolation){
    case 1:
      nbInterfConfStructDOF = 3 * nbNodeFSIConf;
      nbInterfImStructDOF = 3 * nbNodeFSIIm;
      nbInterfCrackStructDOF = 3 * nbNodeFSICrack;
      break;
    case 2:
      nbInterfConfStructDOF = 5 * nbNodeFSIConf;
      nbInterfImStructDOF = 5 * nbNodeFSIIm;
      nbInterfCrackStructDOF = 5 * nbNodeFSICrack;
      break;
    default:
      cout << " MessageAdmMasterFSI::readFSINodesFiles : interpol = " << _paramDataFile->interpolation << " ???" << endl;
      exit(1);
  }
  
  if(_paramDataFile->verbose){
    cout << "   Number of conformal FSI nodes = " << nbNodeFSIConf << endl;
    cout << "   Number of immersed FSI nodes  = " << nbNodeFSIIm << endl;
    cout << "   Number of crack FSI nodes     = 2 x " << nbNodeFSICrack << endl;
  }
  
  nbInterfFluidDOF = nbInterfConfFluidDOF +  nbInterfImFluidDOF +  nbInterfCrackFluidDOF;
  nbInterfStructDOF = nbInterfConfStructDOF +  nbInterfImStructDOF +  nbInterfCrackStructDOF;
}






//===========================================================================================================
void MessageAdmMasterFSI::sendFSINodesToFluid(){
  if(_paramDataFile->verbose)  cout << "---> Master send FSI nodes to fluid \n";
#ifdef MSG_PVM
  pvm_initsend(PvmDataDefault);
  pvm_pkint(&_operInterf->nbNodeFSIConf(),1,1);
  pvm_pkint(&_operInterf->nbNodeFSIIm(),1,1);
  pvm_pkint(&_operInterf->nbNodeFSICrack(),1,1);
  pvm_pkint(&_operInterf->nbInterfConfFluidDOF(),1,1);
  pvm_pkint(&_operInterf->nbInterfImFluidDOF(),1,1);
  pvm_pkint(&_operInterf->nbInterfCrackFluidDOF(),1,1);
  pvm_pkint(_operInterf->nodeFSI().data().begin(),_operInterf->nbNodeFSI(),1);
  pvm_send(_fluidId,140);
#endif
#ifdef MSG_ZMQ
  zmq::message_t msg1(1*sizeof(int));
  memcpy(msg1.data(),&_operInterf->nbNodeFSIConf(),1*sizeof(int));
  _socketFluid->send(msg1);
  
  zmq::message_t msg2(1*sizeof(int));
  memcpy(msg2.data(),&_operInterf->nbNodeFSIIm(),1*sizeof(int));
  _socketFluid->send(msg2);
  
  zmq::message_t msg3(1*sizeof(int));
  memcpy(msg3.data(),&_operInterf->nbNodeFSICrack(),1*sizeof(int));
  _socketFluid->send(msg3);
  
  zmq::message_t msg4(1*sizeof(int));
  memcpy(msg4.data(),&_operInterf->nbInterfConfFluidDOF(),1*sizeof(int));
  _socketFluid->send(msg4);
  
  zmq::message_t msg5(1*sizeof(int));
  memcpy(msg5.data(),&_operInterf->nbInterfImFluidDOF(),1*sizeof(int));
  _socketFluid->send(msg5);
  
  zmq::message_t msg6(1*sizeof(int));
  memcpy(msg6.data(),&_operInterf->nbInterfCrackFluidDOF(),1*sizeof(int));
  _socketFluid->send(msg6);
  
  zmq::message_t msg7(_operInterf->nbNodeFSI()*sizeof(int));
  memcpy(msg7.data(),_operInterf->nodeFSI().data().begin(),_operInterf->nbNodeFSI()*sizeof(int));
  _socketFluid->send(msg7);
#endif
		
}

void MessageAdmMasterFSI::sendLumpedMassToFluid(int hasLumpedMass, double* lumpMass, int dim){
#ifdef MSG_PVM
  pvm_initsend(PvmDataDefault);
  pvm_pkint(&hasLumpedMass,1,1);
  pvm_pkint(&dim,1,1);
  pvm_pkdouble(lumpMass,dim,1);
  pvm_send(_fluidId,99);
#endif
#ifdef MSG_ZMQ
  zmq::message_t msg1(1*sizeof(int));
  memcpy(msg1.data(),&hasLumpedMass,1*sizeof(int));
  _socketFluid->send(msg1);
  
  zmq::message_t msg2(1*sizeof(int));
  memcpy(msg2.data(),&dim,1*sizeof(int));
  _socketFluid->send(msg2);
  
  zmq::message_t msg3(dim*sizeof(double));
  memcpy(msg3.data(),lumpMass,dim*sizeof(double));
  _socketFluid->send(msg3);
#endif
  
}

void MessageAdmMasterFSI::sendToFluid(double* vec,double dt,int status,int dim){
  /* send to the fluid
   - the status
   - the time step
   - the displacement or the velocity
   */
  if(_paramDataFile->verbose>1)  cout << "---> Master sends to fluid\n";
  
#ifdef MSG_PVM
  pvm_initsend(PvmDataDefault);
  pvm_pkint(&status,1,1);
  if(status != -1){
    pvm_pkdouble(&dt,1,1);
    pvm_pkint(&dim,1,1);
    pvm_pkdouble(vec,dim,1);
  }
  pvm_send(_fluidId,110);
#endif
#ifdef MSG_ZMQ
  zmq::message_t msg1(1*sizeof(int));
  memcpy(msg1.data(),&status,1*sizeof(int));
  _socketFluid->send(msg1);
  if(status != -1){
    zmq::message_t msg2(1*sizeof(double));
    memcpy(msg2.data(),&dt,1*sizeof(double));
    _socketFluid->send(msg2);
    
    zmq::message_t msg3(1*sizeof(int));
    memcpy(msg3.data(),&dim,1*sizeof(int));
    _socketFluid->send(msg3);
    
    zmq::message_t msg4(dim*sizeof(double));
    memcpy(msg4.data(),vec,dim*sizeof(double));
    _socketFluid->send(msg4);
  }
#endif
  if(_paramDataFile->verbose>1){
    cout << "\t status : " << status << endl;
    if(status != -1){
      cout << "\t dt: " << dt << endl;
      printArray(_paramDataFile->verbose,cout,"\t M->F Vec", vec, dim);
    }
  }
  
}

void MessageAdmMasterFSI::sendDispVelToFluid(double* dispFluid,double* velFluid,double dt,int status,int dim){
  /* send to the fluid
   - the status
   - the time step
   - the displacement of the interface
   - the velocity of the interface
   */
  if(_paramDataFile->verbose>1)  cout << "---> Master sends velocity and displacement to fluid\n";
#ifdef MSG_PVM
  pvm_initsend(PvmDataDefault);
  pvm_pkint(&status,1,1);
  if(status != -1){
    pvm_pkdouble(&dt,1,1);
    pvm_pkint(&dim,1,1);
    pvm_pkdouble(dispFluid,dim,1);
    pvm_pkdouble(velFluid,dim,1);
  }
  pvm_send(_fluidId,110);
#endif
#ifdef MSG_ZMQ
  zmq::message_t msg1(1*sizeof(int));
  memcpy(msg1.data(),&status,1*sizeof(int));
  _socketFluid->send(msg1);
  if(status != -1){
    zmq::message_t msg2(1*sizeof(double));
    memcpy(msg2.data(),&dt,1*sizeof(double));
    _socketFluid->send(msg2);
    
    zmq::message_t msg3(1*sizeof(int));
    memcpy(msg3.data(),&dim,1*sizeof(int));
    _socketFluid->send(msg3);
    
    zmq::message_t msg4(dim*sizeof(double));
    memcpy(msg4.data(),dispFluid,dim*sizeof(double));
    _socketFluid->send(msg4);
    
    zmq::message_t msg5(dim*sizeof(double));
    memcpy(msg5.data(),velFluid,dim*sizeof(double));
    _socketFluid->send(msg5);
  }
#endif
  if(_paramDataFile->verbose>1){
    cout << "\t status : " << status << endl;
    if(status != -1){
      cout << "\t dt: " << dt << endl;
      printArray(_paramDataFile->verbose,cout,"\t M->F Disp", dispFluid, dim);
      printArray(_paramDataFile->verbose,cout,"\t M->F Vel", velFluid, dim);
    }
  }
  
}

void MessageAdmMasterFSI::receiveFromFluid(double* forceF,int dim){
  if(_paramDataFile->verbose>1) cout << "<--- Master receives from fluid\n";
  int nt;
  double vref;
#ifdef MSG_PVM
  pvm_recv(_fluidId,120);
  pvm_upkint(&nt,1,1);
  assert(nt==dim);
  pvm_upkdouble(forceF,dim,1);
  if(_ipass){
    pvm_upkdouble(&vref,1,1);
    if(_paramDataFile->verbose>2) cout << "\t vref=" << vref << endl;
    _ipass = false;
  }
#endif
#ifdef MSG_ZMQ
  zmq::message_t msg1;
  _socketFluid->recv(&msg1);
  memcpy(&nt,msg1.data(),1*sizeof(int));
  //
  assert(nt==dim);
  //
  zmq::message_t msg2;
  _socketFluid->recv(&msg2);
  memcpy(forceF,msg2.data(),dim*sizeof(double));
  //
  if(_ipass){
    zmq::message_t msg3;
    _socketFluid->recv(&msg3);
    memcpy(&vref,msg3.data(),1*sizeof(double));
    if(_paramDataFile->verbose>2) cout << "\t vref=" << vref << endl;
    _ipass = false;
  }
#endif
  if(_paramDataFile->verbose>1){
    printArray(_paramDataFile->verbose,cout,"\t F->M Vec", forceF, dim);
  }
  
}

void MessageAdmMasterFSI::receiveForceDispVelFromFluid(double* forceF, double* dispF, double* velF,int dim){
  // This function is used only for Robin-Neumann Interface Conditions.
  if(_paramDataFile->verbose>1) cout << "<--- Master receives from fluid\n";
  int nt;
  double vref;
#ifdef MSG_PVM
  pvm_recv(_fluidId,120);
  pvm_upkint(&nt,1,1);
  assert(nt==dim);
  pvm_upkdouble(forceF,dim,1);
  pvm_upkdouble(dispF,dim,1);
  pvm_upkdouble(velF,dim,1);
  // WARNING: Robin-Neumann iterations: comment the above three
  // lines and uncoment the following when coupling witch felisce
  //pvm_upkdouble(velF,dim,1);
  //pvm_upkdouble(forceF,dim,1);
  if(_ipass){
    pvm_upkdouble(&vref,1,1);
    if(_paramDataFile->verbose>2) cout << "\t vref=" << vref << endl;
    _ipass = false;
  }
#endif
#ifdef MSG_ZMQ
  zmq::message_t msg1;
  _socketFluid->recv(&msg1);
  memcpy(&nt,msg1.data(),1*sizeof(int));
  //
  assert(nt==dim);
  //
  zmq::message_t msg2;
  _socketFluid->recv(&msg2);
  memcpy(forceF,msg2.data(),dim*sizeof(double));
  //
  zmq::message_t msg3;
  _socketFluid->recv(&msg3);
  memcpy(dispF,msg3.data(),dim*sizeof(double));
  //
  zmq::message_t msg4;
  _socketFluid->recv(&msg4);
  memcpy(velF,msg4.data(),dim*sizeof(double));
  //
  // WARNING: Robin-Neumann iterations: comment the above three
  // messages and uncoment the following when coupling witch felisce
  /*
   zmq::message_t msg2;
   _socketFluid->recv(&msg2);
   memcpy(velF,msg2.data(),dim*sizeof(double));
   //
   zmq::message_t msg3;
   _socketFluid->recv(&msg3);
   memcpy(forceF,msg3.data(),dim*sizeof(double));
   */
  if(_ipass){
    zmq::message_t msg5;
    _socketFluid->recv(&msg5);
    memcpy(&vref,msg5.data(),1*sizeof(double));
    if(_paramDataFile->verbose>2) cout << "\t vref=" << vref << endl;
    _ipass = false;
  }
#endif
  if(_paramDataFile->verbose>1){
    printArray(_paramDataFile->verbose,cout,"\t F->M Force_F", forceF, dim);
    //printArray(_paramDataFile->verbose,cout,"\t F->M Disp_F ", dispF, dim);
    printArray(_paramDataFile->verbose,cout,"\t F->M Vel_F  ", velF, dim);
  }
}


void MessageAdmMasterFSI::receiveFromFluidWithStressCorrection(double* forceF,double* initForceF,int dim){
  if(_paramDataFile->verbose>1) cout << "<--- Master receives from fluid (with initial stress correction)\n";
  int nt;
  double vref;
#ifdef MSG_PVM
  pvm_recv(_fluidId,120);
  pvm_upkint(&nt,1,1);
  assert(nt==dim);
  pvm_upkdouble(forceF,dim,1);
#endif
#ifdef MSG_ZMQ
  zmq::message_t msg1;
  _socketFluid->recv(&msg1);
  memcpy(&nt,msg1.data(),1*sizeof(int));
  //
  assert(nt==dim);
  //
  zmq::message_t msg2;
  _socketFluid->recv(&msg2);
  memcpy(forceF,msg2.data(),dim*sizeof(double));
#endif
  if(_ipass){
#ifdef MSG_PVM
    pvm_upkdouble(&vref,1,1);
#endif
#ifdef MSG_ZMQ
    zmq::message_t msg3;
    _socketFluid->recv(&msg3);
    memcpy(&vref,msg3.data(),1*sizeof(double));
#endif
    if(_paramDataFile->verbose>2){
      cout << "\t vref=" << vref << endl;
      cout << "\t Store the initial stress" << endl;
    }
    
    if (_paramDataFile->flagInitialStressFromFile){ // read initial stress from file
      
      string initForceF_file_name = _paramDataFile->fileNameInitialStress;
      ifstream initForceF_fileRead;
      initForceF_fileRead.open((initForceF_file_name.c_str()));
      
      cout << "\t Reading initial stresses from file: " << _paramDataFile->fileNameInitialStress  << endl;
      if (!initForceF_fileRead.good()){
        cout << "\t Error! File " << _paramDataFile->fileNameInitialStress  << " does not exist!" << endl;
        exit(1);
      }
      
      
      for(int i=0;i<dim;i++) { initForceF_fileRead >> initForceF[i];} // cout << "\t initForceF["<< i<< "]: " << initForceF[i]  << endl;  }
      initForceF_fileRead.close();
      
    }else
		  {
        for(int i=0;i<dim;i++)initForceF[i] = forceF[i];  // take initial stress from computation
      }
    _ipass = false;
    
    // Write file with initial stress
    string initForceF_file_name = _paramDataFile->postProcDir+"/"+_paramDataFile->testCaseName+"_initForceF";
    ofstream initForceF_file;
    initForceF_file.open((initForceF_file_name.c_str()));
    for(int i=0;i<dim;i++) initForceF_file << initForceF[i] << endl;
    initForceF_file.close();
    
  }
  //
  for(int i=0;i<dim;i++)  forceF[i] = forceF[i] - initForceF[i];
  //
  if(_paramDataFile->verbose>1){
    printArray(_paramDataFile->verbose,cout,"\t F->M Vec (corrected)", forceF, dim);
  }
}

void MessageAdmMasterFSI::sendToStruct(double* forceS,double dt,int status,int struct_id,int dim){
  if(_paramDataFile->verbose>1) cout << "---> Master sends to structure\n";
#ifdef MSG_PVM
  pvm_initsend(PvmDataDefault);
  pvm_pkint(&status,1,1);
  if(status != -1){
    pvm_pkdouble(&dt,1,1);
    pvm_pkint(&dim,1,1);
    pvm_pkdouble(forceS,dim,1);
  }
  pvm_send(struct_id,130);
#endif
#ifdef MSG_ZMQ
  zmq::message_t msg1(1*sizeof(int));
  memcpy(msg1.data(),&status,1*sizeof(int));
  _socketStruct[struct_id]->send(msg1);
  if(status != -1){
    zmq::message_t msg2(1*sizeof(double));
    memcpy(msg2.data(),&dt,1*sizeof(double));
    _socketStruct[struct_id]->send(msg2);
    
    zmq::message_t msg3(1*sizeof(int));
    memcpy(msg3.data(),&dim,1*sizeof(int));
    _socketStruct[struct_id]->send(msg3);
    
    zmq::message_t msg4(dim*sizeof(double));
    memcpy(msg4.data(),forceS,dim*sizeof(double));
    _socketStruct[struct_id]->send(msg4);
  }
#endif
  if(_paramDataFile->verbose>1){
    cout << "\t status : " << status << endl;
    if(status != -1){
      cout << "\t dt: " << dt << endl;
      printArray(_paramDataFile->verbose,cout,"\t M->S Force", forceS, dim);
    }
  }
}

void MessageAdmMasterFSI::receiveFromStruct(double* dispStruct,double* veloStruct,int struct_id,int dim){
  if(_paramDataFile->verbose>1) cout << "<--- Master receives from structure\n";
  int nt;
#ifdef MSG_PVM
  pvm_recv(struct_id,100);
  pvm_upkint(&nt,1,1);
  assert(nt==dim);
  int info;
  info = pvm_upkdouble(dispStruct,dim,1);
  info = pvm_upkdouble(veloStruct,dim,1);
#endif
#ifdef MSG_ZMQ
  zmq::message_t msg1;
  _socketStruct[struct_id]->recv(&msg1);
  memcpy(&nt,msg1.data(),1*sizeof(int));
  //
  assert(nt==dim);
  //
  zmq::message_t msg2;
  _socketStruct[struct_id]->recv(&msg2);
  memcpy(dispStruct,msg2.data(),dim*sizeof(double));
  //
  zmq::message_t msg3;
  _socketStruct[struct_id]->recv(&msg3);
  memcpy(veloStruct,msg3.data(),dim*sizeof(double));
#endif
  if(_paramDataFile->verbose>1){
    printArray(_paramDataFile->verbose,cout,"\t S->M Disp", dispStruct, dim);
    printArray(_paramDataFile->verbose,cout,"\t S->M Velo", veloStruct, dim);
  }
}

void MessageAdmMasterFSI::sendStopToFluid(){
  if(_paramDataFile->verbose) cout << "---> Master sends 'stop' to the fluid" << endl << flush;
  int status = -1;
#ifdef MSG_PVM
  pvm_initsend(PvmDataDefault);
  pvm_pkint(&status,1,1);
  pvm_send(_fluidId,110);
#endif
#ifdef MSG_ZMQ
  zmq::message_t msg1(1*sizeof(int));
  memcpy(msg1.data(),&status,1*sizeof(int));
  _socketFluid->send(msg1);
#endif
}

void MessageAdmMasterFSI::sendStopToStruct(){
  if(_paramDataFile->verbose) cout << "---> Master sends 'stop' to the struct" << endl << flush;
  int status = -1;
#ifdef MSG_PVM
  pvm_initsend(PvmDataDefault);
  pvm_pkint(&status,1,1);
  if(_operInterf->nbInterfConfFluidDOF()) pvm_send(_structConfId,130);
  if(_operInterf->nbInterfImFluidDOF()) pvm_send(_structImId,130);
  if(_operInterf->nbInterfCrackFluidDOF()) pvm_send(_structCrackId,130);
#endif
#ifdef MSG_ZMQ
  if(_paramDataFile->conformFlag){
    zmq::message_t msg1(1*sizeof(int));
    memcpy(msg1.data(),&status,1*sizeof(int));
    _socketStruct[_structConfId]->send(msg1);
  }
  if(_paramDataFile->immersedFlag){
    zmq::message_t msg2(1*sizeof(int));
    memcpy(msg2.data(),&status,1*sizeof(int));
    _socketStruct[_structImId]->send(msg2);
  }
  if(_paramDataFile->crackFlag){
    zmq::message_t msg3(1*sizeof(int));
    memcpy(msg3.data(),&status,1*sizeof(int));
    _socketStruct[_structCrackId]->send(msg3);
  }
#endif
}

void MessageAdmMasterFSI::receiveCoorInterfFromFluid(double* coorInterfF){
  int nt;
#ifdef MSG_PVM
  pvm_recv(_fluidId,125);
  pvm_upkint(&nt,1,1);
  assert(nt == _operInterf->nbInterfConfFluidDOF());
  pvm_upkdouble(coorInterfF,nt,1);
#endif
#ifdef MSG_ZMQ
  zmq::message_t msg1;
  _socketFluid->recv(&msg1);
  memcpy(&nt,msg1.data(),1*sizeof(int));
  //
  assert(nt == _operInterf->nbInterfConfFluidDOF());
  //
  zmq::message_t msg2;
  _socketFluid->recv(&msg2);
  memcpy(coorInterfF,msg2.data(),nt*sizeof(double));
#endif
  
  if(_paramDataFile->verbose>2){
    cout << "\t coor: " << endl;
    cout.setf(ios_base::scientific);
    cout.precision(5);
    cout.width(12);
    for(int i=0;i<_operInterf->nbNodeFSI();i++){
      cout << coorInterfF[3*i+0] << " " << coorInterfF[3*i+1] << " " << coorInterfF[3*i+2] << endl;
    }
    cout << endl;
  }
}

void MessageAdmMasterFSI::sendToStructConf(double* forceS,double dt,int status,int dim){
  sendToStruct(forceS,dt,status,_structConfId,dim);
}

void MessageAdmMasterFSI::receiveFromStructConf(double* dispStruct,double* veloStruct,int dim){
  receiveFromStruct(dispStruct,veloStruct,_structConfId,dim);
}

void MessageAdmMasterFSI::receiveFromStructConfFirstContact(double* lumpedMass, double* dispStruct,double* veloStruct,int dim, int lpdmdim, int& hasLumpedMass){
  //receiveFromStruct(dispStruct,veloStruct,_structConfId,dim);
  int struct_id = _structConfId;
  if(_paramDataFile->verbose>1) cout << "<--- Master receives from structure" << endl;
  int nt, info, lpdmdimr, thickStruct;
#ifdef MSG_PVM
  pvm_recv(struct_id,100);
  pvm_upkint(&thickStruct,1,1);
  hasLumpedMass = 0;
  if ( (_paramDataFile->algorithm == 5) && ( thickStruct != 0 ) )
    hasLumpedMass = 1;
  pvm_upkint(&lpdmdimr,1,1);
  
  cout <<  " lpdmdim , lpdmdimr " << lpdmdim << "  " << lpdmdimr << endl;
  
  assert(lpdmdim== lpdmdimr);
  info = pvm_upkdouble(lumpedMass,lpdmdim,1);
  
  if(_paramDataFile->verbose>1)
    printArray(10,cout,"\t S->M Lumped Mass", lumpedMass, lpdmdim);
  
  pvm_upkint(&nt,1,1);
  
  cout <<  nt << "  "  << dim << endl;
  
  assert(nt==dim);
  
  info = pvm_upkdouble(dispStruct,dim,1);
  info = pvm_upkdouble(veloStruct,dim,1);
#endif
#ifdef MSG_ZMQ
  //
  zmq::message_t msg1;
  _socketStruct[struct_id]->recv(&msg1);
  memcpy(&thickStruct,msg1.data(),1*sizeof(int));
  
  hasLumpedMass = 0;
  if ( (_paramDataFile->algorithm == 5) && ( thickStruct != 0 ) )
    hasLumpedMass = 1;
  //
  zmq::message_t msg2;
  _socketStruct[struct_id]->recv(&msg2);
  memcpy(&lpdmdimr,msg2.data(),1*sizeof(int));
  
  cout <<  " lpdmdim , lpdmdimr " << lpdmdim << "  " << lpdmdimr << endl;
  
  assert(lpdmdim== lpdmdimr);
  
  //
  zmq::message_t msg3;
  _socketStruct[struct_id]->recv(&msg3);
  memcpy(lumpedMass,msg3.data(),lpdmdim*sizeof(double));
  
  if(_paramDataFile->verbose>1)
    printArray(10,cout,"\t S->M Lumped Mass", lumpedMass, lpdmdim);
  //
  zmq::message_t msg4;
  _socketStruct[struct_id]->recv(&msg4);
  memcpy(&nt,msg4.data(),1*sizeof(int));
  
  cout <<  nt << "  "  << dim << endl;
  
  assert(nt==dim);
  //
  zmq::message_t msg5;
  _socketStruct[struct_id]->recv(&msg5);
  memcpy(dispStruct,msg5.data(),dim*sizeof(double));
  //
  zmq::message_t msg6;
  _socketStruct[struct_id]->recv(&msg6);
  memcpy(veloStruct,msg6.data(),dim*sizeof(double));
#endif
  
  if(_paramDataFile->verbose>1){
    printArray(_paramDataFile->verbose,cout,"\t S->M Disp", dispStruct, dim);
    printArray(_paramDataFile->verbose,cout,"\t S->M Velo", veloStruct, dim);
  }
}




void MessageAdmMasterFSI::sendToStructIm(double* forceS,double dt,int status,int dim){
  int offset = _operInterf->nbInterfConfStructDOF();
  //	assert(offset);
  sendToStruct(forceS+offset,dt,status,_structImId,dim);
}

void MessageAdmMasterFSI::receiveFromStructIm(double* dispStruct,double* veloStruct,int dim){
  int offset = _operInterf->nbInterfConfStructDOF();
  //	assert(offset);
  receiveFromStruct(dispStruct+offset,veloStruct+offset,_structImId,dim);
}

void MessageAdmMasterFSI::sendToStructCrack(double* forceS,double dt,int status,int dim){
  int offset = _operInterf->nbInterfConfStructDOF() + _operInterf->nbInterfImStructDOF();
  //	assert(offset);
  sendToStruct(forceS+offset,dt,status,_structCrackId,dim);
}

void MessageAdmMasterFSI::receiveFromStructCrack(double* dispStruct,double* veloStruct,int dim){
  int offset = _operInterf->nbInterfConfStructDOF() + _operInterf->nbInterfImStructDOF();
  // assert(offset);
  receiveFromStruct(dispStruct+offset,veloStruct+offset,_structCrackId,dim);
}

void MessageAdmMasterFSI::finalize(){
#ifdef MSG_PVM
  pvm_exit();
#endif
  
}

//======= Protocol 2 (fluid)

void MessageAdmMasterFSI::sendInitializationToFluid(){
#ifdef MSG_PVM
  if(_verbose)  cout << "---> Master sends initialization to fluid (msg 201)\n";
  pvm_initsend(PvmDataDefault);
  pvm_pkint(&_protocol,1,1);
  pvm_pkint(&_unit,1,1);
  pvm_pkint(&_masterSendInitialState,1,1);
  pvm_pkint(&_masterRcvInitialState,1,1);
  pvm_pkint(&_masterRcvFinalState,1,1);
  pvm_pkint(&_fluidReceivesInterfDisp,1,1);
  pvm_send(_fluidId,201);
#endif
#ifdef MSG_ZMQ
  cout << "Error: file " << __FILE__ << " line " << __LINE__ << " -> ZeroMQ not yet implemented" << endl;
#endif
  
  
  if(_verbose>1) {
    cout << "\t masterSendInitialState=" << _masterSendInitialState << endl;
    cout << "\t masterRcvInitialState=" << _masterRcvInitialState << endl;
    cout << "\t masterRcvFinalState=" << _masterRcvFinalState << endl;
    cout << "\t fluidReceivesInterfDisp=" << _fluidReceivesInterfDisp << endl;
  }
}

void MessageAdmMasterFSI::receiveInitializationFromFluid(int& nbNode,int& nbDofPerNode,int& nbWindkessel){
  if(_paramDataFile->verbose)  cout << "---> Master receives initialization from fluid (msg 202)\n";
#ifdef MSG_PVM
  pvm_recv(_fluidId,202);
  pvm_upkint(&nbNode,1,1);
  pvm_upkint(&nbDofPerNode,1,1);
  pvm_upkint(&nbWindkessel,1,1);
#endif
#ifdef MSG_ZMQ
  cout << "Error: file " << __FILE__ << " line " << __LINE__ << " -> ZeroMQ not yet implemented" << endl;
#endif
  
  
  if(_verbose>1){
    cout << "\t nbNode = " << nbNode << endl;
    cout << "\t nbDofPerNode = " << nbDofPerNode << endl;
    cout << "\t nbWindkessel = " << nbWindkessel << endl;
  }
}

void MessageAdmMasterFSI::receiveStateFromFluid(int& nbNode,int& nbDofPerNode,Vector& disp,Vector& vel,int& nbWindkessel,Vector& varWindkessel){
  if(_paramDataFile->verbose)  cout << "---> Master receives state from fluid (msg 210)\n";
  double *pdisp = disp.data().begin();
  double *pvel = vel.data().begin();
  double *pvarwindkessel = varWindkessel.data().begin();
  int dim;
#ifdef PVM_MSG
  pvm_recv(_fluidId,210);
  pvm_upkint(&nbNode,1,1);
  pvm_upkint(&nbDofPerNode,1,1);
  dim = nbNode*nbDofPerNode;
  disp.resize(dim);
  vel.resize(dim);
  pvm_upkdouble(pdisp,dim,1);
  pvm_upkdouble(pvel,dim,1);
  pvm_upkint(&nbWindkessel,1,1);
  varWindkessel.resize(nbWindkessel);
  pvm_upkdouble(pvarwindkessel,nbWindkessel,1);
#endif
#ifdef MSG_ZMQ
  cout << "Error: file " << __FILE__ << " line " << __LINE__ << " -> ZeroMQ not yet implemented" << endl;
#endif
  
  
  if(_verbose>1){
    cout << "\t nbNode = " << nbNode << endl;
    cout << "\t nbDofPerNode = " << nbDofPerNode << endl;
    cout << "\t nbWindkessel = " << nbWindkessel << endl;
    printArray(_verbose,cout,"\t F->M Disp", pdisp, dim);
    printArray(_verbose,cout,"\t F->M Velo", pvel, dim);
    printArray(_verbose,cout,"\t F->M Windkessel", pvarwindkessel, nbWindkessel);
  }
}

void MessageAdmMasterFSI::sendStateToFluid(int nbNode,int nbDofPerNode,double time,Vector& disp,Vector& vel,int nbWindkessel,Vector& varWindkessel){
  if(_paramDataFile->verbose)  cout << "---> Master send state to fluid (msg 220)\n";
  double *pdisp = disp.data().begin();
  double *pvel = vel.data().begin();
  double *pvarwindkessel = varWindkessel.data().begin();
  int dim;
#ifdef MSG_PVM
  pvm_initsend(PvmDataDefault);
  pvm_pkint(&nbNode,1,1);
  pvm_pkint(&nbDofPerNode,1,1);
  pvm_pkdouble(&time,1,1);
  dim = nbNode*nbDofPerNode;
  pvm_pkdouble(pdisp,dim,1);
  pvm_pkdouble(pvel,dim,1);
  pvm_pkint(&nbWindkessel,1,1);
  pvm_pkdouble(pvarwindkessel,nbWindkessel,1);
  pvm_send(_fluidId,220);
#endif
#ifdef MSG_ZMQ
  cout << "Error: file " << __FILE__ << " line " << __LINE__ << " -> ZeroMQ not yet implemented" << endl;
#endif
  
  if(_verbose>1){
    cout << "\t nbNode = " << nbNode << endl;
    cout << "\t nbDofPerNode = " << nbDofPerNode << endl;
    cout << "\t nbWindkessel = " << nbWindkessel << endl;
    printArray(_verbose,cout,"\t M->F Disp", pdisp, dim);
    printArray(_verbose,cout,"\t M->F Velo", pvel, dim);
    printArray(_verbose,cout,"\t F->M Windkessel", pvarwindkessel, nbWindkessel);
  }
}

//======= Protocol 2 (struct)

void MessageAdmMasterFSI::sendInitializationToStruct(int id){
  if(_verbose)  cout << "---> Master sends initialization to struct (msg 206)\n";
#ifdef MSG_PVM
  pvm_initsend(PvmDataDefault);
  pvm_pkint(&_protocol,1,1);
  pvm_pkint(&_unit,1,1);
  pvm_pkint(&_masterSendInitialState,1,1);
  pvm_pkint(&_masterRcvInitialState,1,1);
  pvm_pkint(&_masterRcvFinalState,1,1);
  pvm_send(id,206);
#endif
#ifdef MSG_ZMQ
  cout << "Error: file " << __FILE__ << " line " << __LINE__ << " -> ZeroMQ not yet implemented" << endl;
#endif
  
  
  if(_verbose>1) {
    cout << "\t masterSendInitialState =" << _masterSendInitialState << endl;
    cout << "\t masterRcvInitialState =" << _masterRcvInitialState << endl;
    cout << "\t masterRcvFinalState =" << _masterRcvFinalState << endl;
  }
}

void MessageAdmMasterFSI::receiveInitializationFromStruct(int& nbNode,int& nbDofPerNode,int id){
  if(_paramDataFile->verbose)  cout << "---> Master receives initialization from struct (msg 207)\n";
#ifdef MSG_PVM
  pvm_recv(id,207);
  pvm_upkint(&nbNode,1,1);
  pvm_upkint(&nbDofPerNode,1,1);
#endif
#ifdef MSG_ZMQ
  cout << "Error: file " << __FILE__ << " line " << __LINE__ << " -> ZeroMQ not yet implemented" << endl;
#endif
  
  
  if(_verbose){
    cout << "\t nbNode = " << nbNode << endl;
    cout << "\t nbDofPerNode = " << nbDofPerNode << endl;
  }
}


void MessageAdmMasterFSI::receiveStateFromStruct(int& nbNode,int& nbDofPerNode,Vector& disp,Vector& vel,Vector& accel,int id)
{
  if(_paramDataFile->verbose)  cout << "---> Master receives state from struct (msg 215)\n";
  double *pdisp = disp.data().begin();
  double *pvel = vel.data().begin();
  double *paccel = accel.data().begin();
  int dim;
#ifdef MSG_PVM
  pvm_recv(id,215);
  pvm_upkint(&nbNode,1,1);
  pvm_upkint(&nbDofPerNode,1,1);
  dim = nbNode*nbDofPerNode;
  disp.resize(dim); // \todo : le mettre ailleurs ???
  vel.resize(dim); //  le mettre ailleurs ???
  pvm_upkdouble(pdisp,dim,1);
  pvm_upkdouble(pvel,dim,1);
  pvm_upkdouble(paccel,dim,1);
#endif
#ifdef MSG_ZMQ
  cout << "Error: file " << __FILE__ << " line " << __LINE__ << " -> ZeroMQ not yet implemented" << endl;
#endif
  
  if(_verbose>1){
    cout << "\t nbNode = " << nbNode << endl;
    cout << "\t nbDofPerNode = " << nbDofPerNode << endl;
    printArray(_verbose,cout,"\t S->M Disp", pdisp, dim);
    printArray(_verbose,cout,"\t S->M Velo", pvel, dim);
    printArray(_verbose,cout,"\t S->M Accel", paccel, dim);
  }
}

void MessageAdmMasterFSI::sendStateToStruct(int nbNode,int nbDofPerNode,double time, Vector& disp,Vector& vel,Vector& accel,int id)
{
  if(_paramDataFile->verbose)  cout << "---> Master send state to struct (msg 225)\n";
  double *pdisp = disp.data().begin();
  double *pvel = vel.data().begin();
  double *paccel = accel.data().begin();
#ifdef MSG_PVM
  pvm_initsend(PvmDataDefault);
  pvm_pkint(&nbNode,1,1);
  pvm_pkint(&nbDofPerNode,1,1);
  pvm_pkdouble(&time,1,1);
  int dim = nbNode*nbDofPerNode;
  pvm_pkdouble(disp.data().begin(),dim,1);
  pvm_pkdouble(vel.data().begin(),dim,1);
  pvm_pkdouble(accel.data().begin(),dim,1);
  if(_verbose>1){
    cout << "\t nbNode = " << nbNode << endl;
    cout << "\t nbDofPerNode = " << nbDofPerNode << endl;
    printArray(_verbose,cout,"\t M->S Disp", pdisp, dim);
    printArray(_verbose,cout,"\t M->S Velo", pvel, dim);
    printArray(_verbose,cout,"\t M->S Accel", paccel, dim);
  }
  pvm_send(id,225);
#endif
#ifdef MSG_ZMQ
  cout << "Error: file " << __FILE__ << " line " << __LINE__ << " -> ZeroMQ not yet implemented" << endl;
#endif
  
}

//======= Protocol 3 (fluid)

void MessageAdmMasterFSI::receiveInitializationEstimFromFluid(int& dim_param,int& dim_observ){
  if(_paramDataFile->verbose)  cout << "---> Master receives initialization Estim from fluid (msg 302)\n";
#ifdef MSG_PVM
  pvm_recv(_fluidId,302);
  pvm_upkint(&dim_param,1,1);
  pvm_upkint(&dim_observ,1,1);
#endif
#ifdef MSG_ZMQ
  cout << "Error: file " << __FILE__ << " line " << __LINE__ << " -> ZeroMQ not yet implemented" << endl;
#endif
  
  
  if(_verbose){
    cout << "\t dim_param = " << dim_param << endl;
    cout << "\t dim_observ = " << dim_observ << endl;
  }
}

void MessageAdmMasterFSI::receiveStateParamInnovFromFluid(Vector& disp,Vector& vel,Vector& varWindkessel,
                                                          double* pparam,int dim_param,double* pinnov,int dim_innov,int& flag_output){
  if(_paramDataFile->verbose)  cout << "---> Master receives state, param and innov from fluid (msg 310)\n";
  double *pdisp = disp.data().begin();
  double *pvel = vel.data().begin();
  double *pvarwindkessel = varWindkessel.data().begin();
#ifdef MSG_PVM
  pvm_recv(_fluidId,310);
  pvm_upkdouble(pdisp,disp.size(),1);
  pvm_upkdouble(pvel,vel.size(),1);
  pvm_upkdouble(pvarwindkessel,varWindkessel.size(),1);
  pvm_upkdouble(pparam,dim_param,1);
  pvm_upkdouble(pinnov,dim_innov,1);
  pvm_upkint(&flag_output,1,1);
#endif
#ifdef MSG_ZMQ
  cout << "Error: file " << __FILE__ << " line " << __LINE__ << " -> ZeroMQ not yet implemented" << endl;
#endif
  
  
  if(_verbose>1){
    printArray(_verbose,cout,"\t F->M Disp", pdisp, disp.size());
    printArray(_verbose,cout,"\t F->M Velo", pvel, vel.size());
    printArray(_verbose,cout,"\t F->M Windkessel", pvarwindkessel, varWindkessel.size());
    printArray(_verbose,cout,"\t F->M Param", pparam, dim_param);
    printArray(_verbose,cout,"\t F->M Innov", pinnov, dim_innov);
    cout << "\t flag_output = " << flag_output << endl;
  }
  if ((_verbose) && (flag_output == 2)){ // FLAGSEIK_LOOP_OUTPUT = 2
    cout << "\t !! Worker request mean values for outputs" << endl;
  }
}

void MessageAdmMasterFSI::sendStateParamToFluid(Vector& disp,Vector& vel,Vector& varWindkessel,double* pparam,int dim_param,
                                                double dt,int flag_loop,int flag_msg,int rank){
  if(_paramDataFile->verbose)  cout << "---> Master send state and param to fluid (msg 320)\n";
  double *pdisp = disp.data().begin();
  double *pvel = vel.data().begin();
  double *pvarwindkessel = varWindkessel.data().begin();
#ifdef MSG_PVM
  pvm_initsend(PvmDataDefault);
  int dim = disp.size();
  pvm_pkint(&dim, 1, 1);
  pvm_pkdouble(disp.data().begin(),disp.size(),1);
  pvm_pkdouble(vel.data().begin(),vel.size(),1);
  int dimWindkessel = varWindkessel.size();
  pvm_pkint(&dimWindkessel,1,1);
  pvm_pkdouble(varWindkessel.data().begin(),varWindkessel.size(),1);
  pvm_pkint(&dim_param, 1, 1);
  pvm_pkdouble(pparam,dim_param,1);
  pvm_pkdouble(&dt,1,1);
  pvm_pkint(&flag_loop,1,1);
  pvm_pkint(&flag_msg,1,1);
  pvm_pkint(&rank,1,1);
  if(_verbose>1){
    printArray(_verbose,cout,"\t M->F Disp", pdisp, disp.size());
    printArray(_verbose,cout,"\t M->F Velo", pvel, vel.size());
    printArray(_verbose,cout,"\t M->F Windkessel", pvarwindkessel, varWindkessel.size());
    printArray(_verbose,cout,"\t M->F Param", pparam, dim_param);
    cout << "\t dt  = " << dt << endl;
    cout << "\t flag_loop  = " << flag_loop << endl;
    cout << "\t flag_msg  = " << flag_msg << endl << flush;
    cout << "\t rank  = " << rank << endl << flush;
  }
  pvm_send(_fluidId,320);
#endif
#ifdef MSG_ZMQ
  cout << "Error: file " << __FILE__ << " line " << __LINE__ << " -> ZeroMQ not yet implemented" << endl;
#endif
  
  
}

void MessageAdmMasterFSI::receiveInitStateParamFromFluid(Vector& disp,Vector& vel,Vector& varWindkessel,int dim_param, double* pparam,
                                                         double* invCovParam,int& dim_observ,double* lumpedInvCovObserv){
  if(_paramDataFile->verbose) cout << "---> MessageAdmMasterFSI:: Receive mean vectors and covariance matrix from fluid (msg 350)" << endl;
  double *pdisp = disp.data().begin();
  double *pvel = vel.data().begin();
  double *pvarwindkessel = varWindkessel.data().begin();
#ifdef MSG_PVM
  pvm_recv(_fluidId,350);
  pvm_upkdouble(pdisp,disp.size(),1);
  pvm_upkdouble(pvel,vel.size(),1);
  pvm_upkdouble(pvarwindkessel,varWindkessel.size(),1);
  pvm_upkdouble(pparam,dim_param,1);
  pvm_upkdouble(invCovParam,dim_param*dim_param,1);
  pvm_upkdouble(lumpedInvCovObserv,dim_observ,1);
#endif
#ifdef MSG_ZMQ
  cout << "Error: file " << __FILE__ << " line " << __LINE__ << " -> ZeroMQ not yet implemented" << endl;
#endif
  
  
  if(_paramDataFile->verbose>1){
    printArray(_paramDataFile->verbose,cout,"\t F->M Disp", pdisp, disp.size());
    printArray(_paramDataFile->verbose,cout,"\t F->M Velo", pvel, vel.size());
    printArray(_paramDataFile->verbose,cout,"\t F->M Windkessel", pvarwindkessel, varWindkessel.size());
    printArray(_paramDataFile->verbose,cout,"\t F->M Param", pparam, dim_param);
    printArray(_paramDataFile->verbose,cout,"\t F->M invCovParam ", invCovParam, dim_param*dim_param);
    printArray(_paramDataFile->verbose,cout,"\t F->M lumpedInvCovObserv ", lumpedInvCovObserv, dim_observ);
  }
}

void MessageAdmMasterFSI::sendEndOfTimeStepToFluid(){
  if(_paramDataFile->verbose) cout << "---> Master sends 'end of time step' to the fluid" << endl;
  int status = -2;
#ifdef MSG_PVM
  pvm_initsend(PvmDataDefault);
  pvm_pkint(&status,1,1);
  pvm_send(_fluidId,110);
#endif
#ifdef MSG_ZMQ
  cout << "Error: file " << __FILE__ << " line " << __LINE__ << " -> ZeroMQ not yet implemented" << endl;
#endif
  
  
}


//======= Protocol 3 (struct)

void MessageAdmMasterFSI::receiveInitializationEstimFromStruct(int& dim_param,int& dim_observ,int id){
  if(_paramDataFile->verbose)  cout << "---> Master receives initialization Estim from struct (msg 307)\n";
#ifdef MSG_PVM
  pvm_recv(id,307);
  pvm_upkint(&dim_param,1,1);
  pvm_upkint(&dim_observ,1,1);
#endif
#ifdef MSG_ZMQ
  cout << "Error: file " << __FILE__ << " line " << __LINE__ << " -> ZeroMQ not yet implemented" << endl;
#endif
  
  
  if(_verbose){
    cout << "\t dim_param = " << dim_param << endl;
    cout << "\t dim_observ = " << dim_observ << endl;
  }
}

void MessageAdmMasterFSI::receiveStateParamInnovFromStruct(Vector& disp,Vector& vel,Vector& accel,double* pparam,int dim_param,double* pinnov,
                                                           int dim_innov,int& flag_output,int id)
{
  if(_paramDataFile->verbose)  cout << "---> Master receives state, param and innov from struct (msg 315)\n";
  double *pdisp = disp.data().begin();
  double *pvel = vel.data().begin();
  double *paccel = accel.data().begin();
#ifdef MSG_PVM
  pvm_recv(id,315);
  pvm_upkdouble(pdisp,disp.size(),1);
  pvm_upkdouble(pvel,vel.size(),1);
  pvm_upkdouble(paccel,accel.size(),1);
  pvm_upkdouble(pparam,dim_param,1);
  pvm_upkdouble(pinnov,dim_innov,1);
  pvm_upkint(&flag_output,1,1);
#endif
#ifdef MSG_ZMQ
  cout << "Error: file " << __FILE__ << " line " << __LINE__ << " -> ZeroMQ not yet implemented" << endl;
#endif
  
  if(_verbose>1){
    printArray(_verbose,cout,"\t S->M Disp", pdisp, disp.size());
    printArray(_verbose,cout,"\t S->M Velo", pvel, vel.size());
    printArray(_verbose,cout,"\t S->M Accel", paccel, accel.size());
    printArray(_verbose,cout,"\t S->M Param", pparam, dim_param);
    printArray(_verbose,cout,"\t S->M Innov", pinnov, dim_innov);
    cout << "\t flag_output = " << flag_output << endl;
  }
  if ((_verbose) && (flag_output == 2)){ // FLAGSEIK_LOOP_OUTPUT = 2
    cout << "\t !! Worker request mean values for outputs" << endl;
  }
}

void MessageAdmMasterFSI::sendStateParamToStruct(Vector& disp,Vector& vel,Vector& accel,double* pparam,int dim_param,
                                                 double dt,int flag_loop,int flag_msg,int rank,int id)
{
  if(_paramDataFile->verbose)  cout << "---> Master send state and param to struct (msg 325)\n";
  double *pdisp = disp.data().begin();
  double *pvel = vel.data().begin();
  double *paccel = accel.data().begin();
  int dim_disp = disp.size();
#ifdef MSG_PVM
  pvm_initsend(PvmDataDefault);
  pvm_pkint(&dim_disp,1,1);
  pvm_pkdouble(disp.data().begin(),disp.size(),1);
  pvm_pkdouble(vel.data().begin(),vel.size(),1);
  pvm_pkdouble(accel.data().begin(),accel.size(),1);
  pvm_pkint(&dim_param,1,1);
  pvm_pkdouble(pparam,dim_param,1);
  pvm_pkdouble(&dt,1,1);
  pvm_pkint(&flag_loop,1,1);
  pvm_pkint(&flag_msg,1,1);
  pvm_pkint(&rank,1,1);
  if(_verbose>1){
    printArray(_verbose,cout,"\t M->S Disp", pdisp, disp.size());
    printArray(_verbose,cout,"\t M->S Velo", pvel, vel.size());
    printArray(_verbose,cout,"\t M->S Accel", paccel, accel.size());
    printArray(_verbose,cout,"\t M->S Param", pparam, dim_param);
    cout << "\t dt  = " << dt << endl;
    cout << "\t flag_loop  = " << flag_loop << endl;
    cout << "\t flag_msg  = " << flag_msg << endl << flush;
    cout << "\t rank  = " << rank << endl << flush;
  }
  pvm_send(id,325);
#endif
#ifdef MSG_ZMQ
  cout << "Error: file " << __FILE__ << " line " << __LINE__ << " -> ZeroMQ not yet implemented" << endl;
#endif
  
  
}

void MessageAdmMasterFSI::receiveInitStateParamFromStruct(Vector& disp,Vector& vel,Vector& accel,int dim_param, double* pparam,
                                                          double* invCovParam,int dim_observ,double* lumpedInvCovObserv,int id)
{
  if(_paramDataFile->verbose) cout << "---> MessageAdmMasterFSI:: Receive mean vectors and covariance matrix from struct (msg 355)" << endl;
  double *pdisp = disp.data().begin();
  double *pvel = vel.data().begin();
  double *paccel = accel.data().begin();
#ifdef MSG_PVM
  pvm_recv(id,355);
  pvm_upkdouble(pdisp,disp.size(),1);
  pvm_upkdouble(pvel,vel.size(),1);
  pvm_upkdouble(paccel,accel.size(),1);
  pvm_upkdouble(pparam,dim_param,1);
  pvm_upkdouble(invCovParam,dim_param*dim_param,1);
  pvm_upkdouble(lumpedInvCovObserv,dim_observ,1);
#endif
#ifdef MSG_ZMQ
  cout << "Error: file " << __FILE__ << " line " << __LINE__ << " -> ZeroMQ not yet implemented" << endl;
#endif
  
  if(_paramDataFile->verbose>1){
    printArray(_verbose,cout,"\t M<-S Disp", pdisp, disp.size());
    printArray(_verbose,cout,"\t M<-S Velo", pvel, vel.size());
    printArray(_verbose,cout,"\t M<-S Accel", paccel, accel.size());
    printArray(_verbose,cout,"\t M<-S Param", pparam, dim_param);
    printArray(_verbose,cout,"\t M<-S invCovParam ", invCovParam, dim_param*dim_param);
    printArray(_verbose,cout,"\t M<-S lumpedInvCovObserv ", lumpedInvCovObserv, dim_observ);
  }
}

void MessageAdmMasterFSI::sendEndOfTimeStepToStruct(){
  if(_paramDataFile->verbose) cout << "---> Master sends 'end of time step' to the struct" << endl;
  int status = -2;
#ifdef MSG_PVM
  pvm_initsend(PvmDataDefault);
  pvm_pkint(&status,1,1);
  if(_operInterf->nbInterfConfFluidDOF()) pvm_send(_structConfId,130);
  if(_operInterf->nbInterfImFluidDOF()) pvm_send(_structImId,130);
  if(_operInterf->nbInterfCrackFluidDOF()) pvm_send(_structCrackId,130);
#endif
#ifdef MSG_ZMQ
  cout << "Error: file " << __FILE__ << " line " << __LINE__ << " -> ZeroMQ not yet implemented" << endl;
#endif
  
}


