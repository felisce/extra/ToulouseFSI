//===========================================================
//                    MasterFSI
//
// Part of MasterFSIContact,             Copyright Inria 2018
//===========================================================
/*!
 \file operInterf.hpp
 \author Jean-Frederic Gerbeau
 */
#ifndef  _OPERINTERF_HPP
#define  _OPERINTERF_HPP
#include  <iostream>
#include  <stdio.h>
#include  <string>
#include  <fstream>
#include "paramFSI.hpp"
class MessageAdmMasterFSI;

/*!
 \class OperInterf
 
 \brief Operator handling the interface exchanges
 
 \c OperInterf is the class handling the fluid-structure interface exchanges. 
 The interface operator typically takes as input the displacement
 and the velocity of the structure interface (_structInterfDisp and _structInterfVelo), 
 send them to the fluid, receive the force, send it to the solid and then get the structure displacement 
 and velocity (_resultStructInterfDisp and _resultStructInterfVelo)
 
 \todo Non matching grids are not supported
 \todo This class is too "Dirichlet-Neumann oriented". At the level of the parent class, the generality could be improved. 
 
 \author  Jean-Frederic Gerbeau
 */


class OperInterf{
protected:
	//! Pointer to the parameters defined by the data file
	ParamFSI* _paramDataFile;
	//! Pointer to the message administrator
	MessageAdmMasterFSI* _msgAdm;
	//! Number of FSI conform nodes. Initialized in MessageAdmMasterFSI::readFSINodesFiles()
    int _nbNodeFSIConf;
	//! Number of FSI immersed nodes. Initialized in MessageAdmMasterFSI::readFSINodesFiles()
    int _nbNodeFSIIm;
	//! Number of FSI crack nodes. Initialized in MessageAdmMasterFSI::readFSINodesFiles()
    int _nbNodeFSICrack;
	//! Total number of FSI nodes = nbNodeFSIConf + 2*nbNodeCrack (dimension of _nodeFSI). Initialized in MessageAdmMasterFSI::readFSINodesFiles()
	int _nbNodeFSI;
	//! Nodes of FSI interface. Initialized in MessageAdmMasterFSI::readFSINodesFiles()
	VectorInt _nodeFSI;
	//! Number of interface fluid DOF  = _nbInterfConfFluidDOF +  _nbInterfImFluidDOF +  _nbInterfCrackFluidDOF. Initialized in MessageAdmMasterFSI::readFSINodesFiles()
	int _nbInterfFluidDOF;
	//! Number of conform interface fluid DOF. Initialized in MessageAdmMasterFSI::readFSINodesFiles()
	int _nbInterfConfFluidDOF;
	//! Number of immersed interface fluid DOF. Initialized in MessageAdmMasterFSI::readFSINodesFiles()
	int _nbInterfImFluidDOF;
	//! Number of crack interface fluid DOF. Initialized in MessageAdmMasterFSI::readFSINodesFiles()
	int _nbInterfCrackFluidDOF;
	//! Number of interface struct DOF _nbInterfConfStructDOF +  _nbInterfImStructDOF +  _nbInterfCrackStructDOF. Initialized in MessageAdmMasterFSI::readFSINodesFiles()
	int _nbInterfStructDOF;
	//! Number of conform interface struct DOF. Initialized in MessageAdmMasterFSI::readFSINodesFiles()
	int _nbInterfConfStructDOF;
	//! Number of immersed interface struct DOF. Initialized in MessageAdmMasterFSI::readFSINodesFiles()
	int _nbInterfImStructDOF;
	//! Number of crack interface struct DOF. Initialized in MessageAdmMasterFSI::readFSINodesFiles()
	int _nbInterfCrackStructDOF;
	//! Fluid coordinates on the interface
	Vector _coorInterfFluid;
	//! Structure interface displacement (input)
	Vector _structInterfDisp;
	//! Structure interface velocity (input)
	Vector _structInterfVelo;
        //! Structure interface lumped mass (input)
	Vector _structInterfLumpMass;
	//! Structure interface displacement (input for tangent operator)
	Vector _structInterfDispTangent;
	//! Structure interface velocity (input for tangent opertor)
	Vector _structInterfVeloTangent;
	//! Interf displacement resulting from the composition of the Structure and Fluid operators (output) 
	Vector _resultStructInterfDisp;
	//! Velocity displacement resulting from the composition of the Structure and Fluid operators (output)
	Vector _resultStructInterfVelo;
	//! Interf displacement resulting from the derivative of the composition of the Structure and Fluid operators (output for tangent pb) 
	Vector _resultStructInterfDispTangent;
	//! Velocity displacement resulting from the derivative of the the composition of the Structure and Fluid operators (output for tangent pb)
	Vector _resultStructInterfVeloTangent;
	//! Fluid interface displacement (internal use)
	Vector _fluidInterfDisp;
	//! Fluid interface velocity (internal use)
	Vector _fluidInterfVelo;
	//! Force on fluid interface (internal use)
	Vector _fluidInterfForce;
	//! Force on structure interface (internal use)
	Vector _structInterfForce;	
	//! Initial force on fluid interface (for stress correction if flagInitialStressCorrection=1) 
	Vector _fluidInterfInitialForce;
	//! Number of operator evaluation (1 evaluation = 1 fluid + 1 structure)
	int _nbEval;
	//! Number of tangent operator evaluation (1 evaluation = 1 tangent fluid + 1 tangent structure)
	int _nbEvalTangent;

        //! =1 if lumped mass needed
        int _hasLumpedMass;
        int _structInterfLumpMassDim;

public:
	OperInterf(ParamFSI* param);
	//! Set the message admnistrator pointer
	void setMessageAdm(MessageAdmMasterFSI* msgAdm){_msgAdm = msgAdm;}
	//! Number of FSI conform nodes
    int& nbNodeFSIConf(){return _nbNodeFSIConf;}
	//! Number of FSI immersed nodes
    int& nbNodeFSIIm(){return _nbNodeFSIIm;}
	//! Number of FSI crack nodes
	int& nbNodeFSICrack() {return _nbNodeFSICrack;}
	//! Total number of FSI nodes = nbNodeFSIConf + 2*nbNodeCrack (dimension of _nodeFSI)
	int& nbNodeFSI(){return _nbNodeFSI;}
	//! Nodes of FSI interface
	Vector& coorInterfFluid(){return _coorInterfFluid;}
	//! Structure interface displacement
	Vector& structInterfDisp(){return _structInterfDisp;}
	//! Structure interface velocity
	Vector& structInterfVelo(){ return _structInterfVelo;}
        Vector& fluidInterfVelo(){ return _fluidInterfVelo;}
        //! Structure interface lumped mass
	Vector& structInterfLumpMass(){ return _structInterfLumpMass;}
        int& hasLumpedMass(){return _hasLumpedMass;}
        //! Structure interface displacement (tangent)
	Vector& structInterfDispTangent(){return _structInterfDispTangent;}
	//! Structure interface velocity (tangent)
	Vector& structInterfVeloTangent(){ return _structInterfVeloTangent;}
	Vector& resultStructInterfDisp(){return _resultStructInterfDisp;}
	Vector& resultStructInterfVelo(){return _resultStructInterfVelo;}
	Vector& resultStructInterfDispTangent(){return _resultStructInterfDispTangent;}
	Vector& resultStructInterfVeloTangent(){return _resultStructInterfVeloTangent;}
	VectorInt& nodeFSI(){return _nodeFSI;}
	//! Number of interface fluid DOF  = _nbInterfConfFluidDOF +  _nbInterfImFluidDOF +  _nbInterfCrackFluidDOF
	int& nbInterfFluidDOF() { return _nbInterfFluidDOF;}
	//! Number of conform interface fluid DOF
	int& nbInterfConfFluidDOF() { return _nbInterfConfFluidDOF;}
	//! Number of immersed interface fluid DOF
	int& nbInterfImFluidDOF() { return _nbInterfImFluidDOF;}
	//! Number of crack interface fluid DOF
	int& nbInterfCrackFluidDOF() { return _nbInterfCrackFluidDOF;}
	//! Number of interface struct DOF _nbInterfConfStructDOF +  _nbInterfImStructDOF +  _nbInterfCrackStructDOF
	int& nbInterfStructDOF() { return _nbInterfStructDOF;}
	//! Number of conform interface struct DOF
	int& nbInterfConfStructDOF() { return _nbInterfConfStructDOF;}
	//! Number of immersed interface struct DOF
	int& nbInterfImStructDOF() { return _nbInterfImStructDOF;}
	//! Number of crack interface struct DOF
	int& nbInterfCrackStructDOF() { return _nbInterfCrackStructDOF;}
	//! Number of fluid-structure evaluation
	int& nbEval() { return _nbEval;}
	//! Number of tangent fluid-structure evaluation
	int& nbEvalTangent() { return _nbEvalTangent;}
        int structInterfLumpMassDim(){return 	_structInterfLumpMassDim;}
  	//! Resize the vectors _fluidInterfDisp, _structInterfForce, etc. \warning Be sure that _nbInterFluidDOF and _nbInterfStructDOF are defined before (calling MessageAdmMasterFSI::readFSINodesFiles()) 
	void initialize();
	//! Operator fluid interface to structure interface (Force only)
	void fluidInterfToStructInterf(const Vector& fluidVec,Vector& structVec);
    //! Operator fluid interface to structure interface (Disp and Vel): used for Robin Interface BC's
  void fluidDispVelInterfToStructInterf(const Vector& fluidVec,Vector& structVec);
	//! Operator structure interface to fluid interface 
	void structInterfToFluidInterf(const Vector& structVec,Vector& fluidVec);
	//! Structure + Fluid (full operator if flag = 0 or 1, tangent operator if flag = 2)
    virtual void compute(int flag, double& eps_ext) = 0;
	virtual void computeTangent(const Vector& vec) = 0;
	virtual ~OperInterf() {};
};
#endif
