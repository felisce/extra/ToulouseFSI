//===========================================================
//                    MasterFSI
//
// Part of MasterFSIContact,             Copyright Inria 2018
//===========================================================
/*!
 \file messageAdmFSI.cpp
 \author Jean-Frederic Gerbeau
 */
#include <iostream>
#include <fstream>

#include "messageAdmFSI.hpp"
#include "utilstring.hpp"

using namespace std;

MessageAdmFSI::MessageAdmFSI(int argc, char *argv[],const int& verbose,const int& protocol,int unit):
_verbose(verbose),
_protocol(protocol),
_unit(unit),
_masterSendInitialState(0),
_masterRcvInitialState(0),
_masterRcvFinalState(0),
_fluidReceivesInterfDisp(1)
{
	if(_verbose){
		cout << "*** MessageAdmFSI:" << endl;
		cout << "\t verbose  = " << _verbose << endl;
		cout << "\t protocol = " << _protocol << endl;
		cout << "\t unit = " << _unit << endl;
	}
}


