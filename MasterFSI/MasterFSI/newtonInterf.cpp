//===========================================================
//                    MasterFSI
//
// Part of MasterFSIContact,             Copyright Inria 2018
//===========================================================
/*!
 \file newtonInterf.cpp
 \author Jean-Frederic Gerbeau
 */

#include "newtonInterf.hpp"
#include "newton.hpp"
#include "noPrecond.hpp"
#include "gmres.hpp"


NewtonInterf::NewtonInterf(int verbose,OperInterf& oper):
_verbose(verbose),_oper(&oper){
	_dim = _oper->nbInterfStructDOF();
	_step.resize(2*_dim);
    _step.clear();
	_residual.resize(2*_dim);
    _residual.clear();
	_sol.resize(2*_dim);
    _sol.clear();
	_prod_y.resize(2*_dim);
    _prod_y.clear();    
}

int NewtonInterf::solve(double abstol,double reltol,int& maxiter,int algo,double eta_max){
	subrange(_sol,0,_dim) = _oper->structInterfDisp();
	subrange(_sol,_dim,2*_dim) = _oper->structInterfVelo();
	int cvg = newton(this,_step,abstol, reltol, maxiter,eta_max,algo,_verbose);
	/* In the two following lines, we use the special structure of the nonlinear pb:
	 R(y) = y  - F o S(y) 
	 To evaluate the last residual, the quantity y - F o S (y) has been computed,
	 and F o S (y) is a priori a better approximation of the solution than y itself (y being the last
	 Newton iteration).
	 Thus, we define as the last iteration not y but rather F o S(y)*/
	_oper->structInterfDisp() = _oper->resultStructInterfDisp(); 
	_oper->structInterfVelo() = _oper->resultStructInterfVelo();	
    return cvg;
}

void NewtonInterf::solveJac(double& linear_rel_tol){
	int maxit = 50;
	int restart = 50;
	Matrix H(restart+1, restart);
	H.clear();
	NoPrecond<Vector> Prec;
	if (_verbose>1) cout << "    GMRES (relative tolerance required " << linear_rel_tol << ")" << endl ;
	_residual *= -1;
	int result= GMRES(*this, _step,_residual, Prec, H, restart, maxit,linear_rel_tol);
	//
	if(result){
		cout << "NewtonInterf::solveJac Warning: convergence failed in GMRES" << endl;
	}
	if (_verbose>1) cout << ".... GMRES cvg in " << maxit << " iterations with relative tolerance = " << linear_rel_tol <<endl;	
}

void NewtonInterf::evalResidual(int iter){
	int status = 0;
	double eps_ext; // here is a dummy variable, used for eps_ext=|ds-df|+|vf-vs|<tol convergence criteria
	if(iter == 0) status = 1;
	if (_verbose>1){
		cout << "*** Residual computation g(x_" << iter <<" )";
		if (status) cout << " [new time step] ";
		cout << endl;
	}
	_oper->structInterfDisp() = subrange(_sol,0,_dim);
	_oper->structInterfVelo() = subrange(_sol,_dim,2*_dim);
	_oper->compute(status,eps_ext);
	subrange(_residual,0,_dim) = _oper->resultStructInterfDisp() - _oper->structInterfDisp() ;
	subrange(_residual,_dim,2*_dim) = _oper->resultStructInterfVelo() - _oper->structInterfVelo() ;	
}

Vector NewtonInterf::prod(Vector& y){
	_oper->computeTangent(y);
	subrange(_prod_y,0,_dim) = _oper->resultStructInterfDispTangent() - _oper->structInterfDispTangent();
	subrange(_prod_y,_dim,2*_dim) = _oper->resultStructInterfVeloTangent()- _oper->structInterfVeloTangent();
	return _prod_y;
}

void NewtonInterf::updateJac(int iter){
	// Intentionally blank function
}
