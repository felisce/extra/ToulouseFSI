//===========================================================
//                    MasterFSI
//
// Part of MasterFSIContact,             Copyright Inria 2018
//===========================================================
/*!
 \file ensight.cpp
 \author Jean-Frederic Gerbeau
 */
#include "ensight.hpp"
#include <iostream>
#include <fstream>

#include "utilstring.hpp"
#undef DEBUGVAR


EnsightTime::EnsightTime(int time_set,int nb_steps,int filename_start_number,int filename_increment):
_time_set(time_set),
_nb_steps(nb_steps),
_filename_start_number(filename_start_number),
_filename_increment(filename_increment)
{
	_time_values.resize(_nb_steps);
}

int EnsightTime::timeIndex(double time) const{
	for(int i=0;i<_nb_steps;i++){
		if( fabs(time - _time_values[i]) <= 10*DBL_EPSILON*( fabs(time)+fabs(_time_values[i]) )){
			return (i+_filename_start_number);	
		}
	}
	cout << "EnsightTime::timeIndex : time " << time << " is not in the time set" << endl;
	exit(1);
}

//=================================================================================================
EnsightVariable::EnsightVariable(const string& type,const int& time_set,const int& file_set,
								 const string& description, const string& filename):
_type(Unknown),
_time_set(time_set),
_file_set(file_set),
_description(description),
_filename(filename)
{
	cout << "EnsightVariable::  " << _description << " on " << _filename << endl;
	if(CountOccurenceChar(filename,'*')){
		_filename_wildcard = filename;	
	}
	else {
		_filename_wildcard = "";
		_filename = filename;
	}
	if(type == "scalar per node") _type = ScalarPerNode;
	if(type == "vector per node") _type = VectorPerNode;
	if(_type == Unknown){
		cout << type << endl;
		cout << "EnsightVariable:: Unknown type of variable" << endl;
		exit(1);
	}
#ifdef DEBUGVAR
	cout << "type: " << type << endl;
	cout << "time set: " << time_set << endl;
	cout << "file set: " << file_set << endl;
	cout << "description: " << description << endl;
	cout << "filename: " << filename << endl;
#endif
}

string EnsightVariable::variableFile() const{
	if(_filename_wildcard!=""){
		cout << "Variable file name contains wildcard characters " << _filename_wildcard << endl;
		cout << "Use EnsightVariable::variableFile(int i) to specify the time step" << endl;
		exit(1);
	}
	return _filename;
}

string EnsightVariable::variableFile(int i) const{
	if(_filename_wildcard=="") return _filename;
	else return ReplaceWilcardByInt(_filename_wildcard, i, '*');
}

string EnsightVariable::description() const{
	return _description;
}



//==============================================================================
EnsightCase::EnsightCase(int verbose):_verbose(verbose)
{}

EnsightVariable* EnsightCase::variable(int ivar) {
	return _var[ivar];
}

EnsightTime* EnsightCase::timeSet(int iset) {
	return _time[iset];
}


void EnsightCase::read(const string& directory,const string& case_file){
	_sectionFormat=-1;
	_sectionGeometry=-1;
	_sectionVariable=-1;
	_sectionTime=-1;
	_line.resize(0);
	_directory = directory;
	_filename = case_file;
	string casefilename = _directory+"/"+case_file;
	ifstream casefile(casefilename.c_str(), ios_base::in);
	if(_verbose) cout << "EnsightCase::read case file " << casefilename << endl;
	if( !casefile){
		cout << "Cannot open " << casefilename << endl;
		exit(1);
	}
	size_t found;
	string dummy;
	int i=0;
	while ( getline( casefile, dummy ) ){
		_line.push_back(dummy);
		found=_line[i].find("FORMAT");
		if(found!=string::npos) _sectionFormat=i;
		found=_line[i].find("GEOMETRY");
		if(found!=string::npos) _sectionGeometry=i;
		found=_line[i].find("VARIABLE");
		if(found!=string::npos) _sectionVariable=i;
		found=_line[i].find("TIME");
		if(found!=string::npos) _sectionTime=i;
		i++;
	}
	casefile.close();
	
	if(_sectionFormat==-1){
		cout << "Error reading ensight case file: FORMAT section is required" << endl;
		exit(1);
	}
	if(_sectionGeometry==-1){
		cout << "Error reading ensight case file: GEOMETRY section is required" << endl;
		exit(1);
	}
	if(_sectionVariable==-1){
		cout << "Warning: case file contains no VARIABLE section" << endl;
	}
	if(_sectionTime==-1){
		cout << "Warning: case file contains no TIME section" << endl;
	}
#undef DEBUGFORMAT
#ifdef DEBUGFORMAT
	cout << "format : " << _sectionFormat << endl;
	cout << "geometry : " << _sectionGeometry<< endl;
	cout << "variable : " << _sectionVariable << endl;
	cout << "time : " << _sectionTime << endl;
#endif
	
	//====================
	// Read FORMAT section
	//====================
	/*
	 This section is required; it specifies the data type.
	 FORMAT
	 type: ensight gold
	 */
	
	//======================
	// Read GEOMETRY section
	//======================
	/*
	 This is a required section. It describes the geometry of the data. Furthermore it can describe periodic match file if used, 
	 a boundary file if used, rigid body file if used, and vector glyph file if used.
	 GEOMETRY
	 model:      [ts] [fs]      filename          [change_coords_only [cstep]]
	 measured:   [ts] [fs]      filename          [change_coords_only]
	 match:                     filename
	 boundary:                  filename
	 rigid_body:                filename
	 Vector_glyphs:             filename
	 
	 where:
	 ts = a time set number as specified in TIME section, ( optional ).
	 fs = a corresponding file set number; specified in the FILE section.
	 If an fs number is specified, then a ts is required. 
	 filename = The declared filename.
     - Wildcard symbols, * , are NOT USED for model or measured files that are static, ( or if written in the Single File Format). 
	 Furthermore, wildcard symbols are not used for match, boundary, or rigid_body filenames.
     - Wildcard symbols ARE USED for model or measured changing geometry written to multiple files.
     - Model filenames with structured block continuation options will contain % wildcards.
	 change_coords_only = This option specifies that the changing geometry is for coordinates only; else the connectivity will 
	 also assumed to be changing and need to be specified as well.
	 cstep = Used with change_coords_only option for the zero-based time step containing the connectivity, ( Optional ). 
	 If all time steps have connectivity, it can be omitted. If used, the remaining time steps no not need to repeat the connectivity.	 
	 */
	vector<string> contents;
	StringSplit(_line[_sectionGeometry+1]," \t",contents);// delimiter is either a space or a tab
	bool has_a_cstep=false;
	if(contents[0] == "model:"){
		_change_coords_only = false;
		_change_coords_only_cstep = 0;
		//		for(vector<string>::iterator it = contents.begin(); it!=contents.end(); ++it){
		int dim = contents.size();
		for(int i=0;i<dim;i++){
			found= contents[i].find("change_coords_only");
			if(found!=string::npos){
				_change_coords_only = true;
				if(i==dim-2){
					has_a_cstep = true;
					_change_coords_only_cstep = atoi(contents[dim-1].c_str());	
				}
			}
		}
		if(_change_coords_only){
			// change_coords_only
			if(has_a_cstep){
				// cstep
				if(dim == 4){
					_model_time_set = 0;
					_model_file_set = 0;
					_geo_file = contents[1];
				}
				if(dim == 5){
					_model_time_set = atoi(contents[1].c_str());
					_model_file_set = 0;
					_geo_file = contents[2];
				}
				if(dim == 6){
					_model_time_set = atoi(contents[1].c_str());
					_model_file_set = atoi(contents[2].c_str());
					_geo_file = contents[3];
				}
			}else{
				// no cstep
				if(dim == 3){
					_model_time_set = 0;
					_model_file_set = 0;
					_geo_file = contents[1];
				}
				if(dim == 4){
					_model_time_set = atoi(contents[1].c_str());
					_model_file_set = 0;
					_geo_file = contents[2];
				}
				if(dim == 5){
					_model_time_set = atoi(contents[1].c_str());
					_model_file_set = atoi(contents[2].c_str());
					_geo_file = contents[3];
				}
			}
		}else{
			// no change_coords_only
			if(dim == 2){
				_model_time_set = 0;
				_model_file_set = 0;	
				_geo_file = contents[1];
			}
			if(dim == 3){
				_model_time_set = atoi(contents[1].c_str());
				_model_file_set = 0;
				_geo_file = contents[2];
			}
			if(dim == 4){
				_model_time_set = atoi(contents[1].c_str());
				_model_file_set = atoi(contents[2].c_str());
				_geo_file = contents[3];
			}
		}
	}else{
		cout << contents[0] << " not yet implemented" << endl;
		exit(1);
	}
	_nb_wildcard = CountOccurenceChar(_geo_file,'*');
	if(_nb_wildcard) _geo_file_wildcard = _geo_file;
	else _geo_file_wildcard = "";
	if(CountOccurenceChar(_geo_file,'%')){
		cout << "Wildcard % not yet implemented " << endl;
		exit(1);
	}
	if(_geo_file_wildcard != "") _geo_file = ReplaceWilcardByInt(_geo_file_wildcard, 198, '*');
	
#undef DEBUGGEO
#ifdef DEBUGGEO
	cout << "GEOMETRY" << endl;
	cout << "\t model:" << endl;
	cout << "\t model_time_set = " << _model_time_set << endl;
	cout << "\t mode_file_set = " << _model_file_set << endl;
	cout << "\t geo_file = " << _geo_file << endl;
	cout << "\t geo_file_wildcard = " << _geo_file_wildcard << endl;
	cout << "\t change_coords_only = " << _change_coords_only << endl;
	cout << "\t change_coords_only_cstep = " << _change_coords_only_cstep << endl;
#endif
	//======================
	// Read VARIABLE section
	//======================
	/*
	 This section of the ASCII case file is optional. If declared, it specifies the filenames and variables of the data. 
	 The EnSight Gold Casefile Format allows for 300 variables to be declared.
	 Here's an example
	 
	 VARIABLE
	 constant per case:           [ts]              description const_value(s)
	 constant per case file:      [ts]              description cvfilename
	 scalar per node:             [ts] [fs]         description filename
	 vector per node:             [ts] [fs]         description filename
	 tensor symm per node:        [ts] [fs]         description filename
	 tensor asym per node:        [ts] [fs]         description filename
	 scalar per element:          [ts] [fs]         description filename
	 vector per element:          [ts] [fs]         description filename
	 tensor symm per element:     [ts] [fs]         description filename
	 tensor asym per element:     [ts] [fs]         description filename
	 scalar per measured node:    [ts] [fs]         description filename
	 vector per measured node:    [ts] [fs]         description filename
	 complex scalar per node:     [ts] [fs]         description Re_fn Im_fn freq
	 complex vector per node:     [ts] [fs]         description Re_fn Im_fn freq
	 complex scalar per element:  [ts] [fs]         description Re_fn Im_fn freq
	 complex vector per element:  [ts] [fs]         description Re_fn Im_fn freq
	 
	 
	 where:
	 ts = The corresponding time set number ( or index ) as specified in the TIME section.
	 fs = The corresponding file set number ( Or index ) as specified in the FILE section.
	 description = This will be the variable name presented by the GUI.
	 const_value(s) = If constants change over time then ns in TIME section below; else ts for constant,
	 cvfilename = The constant values filename, that contains the actual constant values.
	 Re_fn = The real values filename, that contains the real parts of a complex variable.
	 Im_fn = The imaginary values filename, that contains the imaginary parts of a complex variable.
	 freq = The corresponding harmonic frequency of the complex variable. UNDEFINED may be used as an acceptable 
	 value if it's undefined.
	 Notes:
	 As many variable lines as needed may be used.
	 Variable descriptions have the following restrictions:
	 - 19 characters 
	 - duplicates not allowed 
	 - leading or trailing whitespace is disguarded 
	 - must not start with a digit 
	 - must not contain any of the following reserved characters 
	 ( [ + @ ! * $ ) ] - space # ^ /
	 */	
	//	vector<string> contents;
	_nb_variable = _sectionTime - _sectionVariable - 1;
	_var.resize(_nb_variable);
	string typevar,restofline,description,filename;
	int time_set,file_set;
	for(int i=0;i<_nb_variable;i++){
		found = _line[_sectionVariable+1+i].find(':');
		typevar = _line[_sectionVariable+1+i].substr(0,found);
		restofline = _line[_sectionVariable+1+i].substr(found+1,_line[i].size()-found-1);
		StringSplit(restofline," \t",contents);// delimiter is either a space or a tab
		if(contents.size() == 4){
			time_set = atoi(c_string(contents[0]));
			file_set = atoi(c_string(contents[1]));
			description = contents[2];
			filename = contents[3];
		}
		if(contents.size() == 3){
			time_set = atoi(c_string(contents[0]));
			file_set = -1;
			description = contents[1];
			filename = contents[2];
		}
		if(contents.size() == 2){
			time_set = -1;
			file_set = -1;
			description = contents[0];
			filename = contents[1];
		}
		_var[i] = new EnsightVariable(typevar,time_set,file_set,description,filename);
	}
	//======================
	// Read TIME section
	//======================
	/*	 
	 This is an optional section for steady state cases. For transient cases, it is required. It describes time set information.
	 Below are three example usages:
	 
	 TIME
	 time set:               ts [description]
	 number of steps:        ns
	 filename start number:  fs
	 filename increment:     fi
	 time values:            time_1 time_2 .... time_ns
	 TIME
	 time set:               ts [description]
	 number of steps:        ns
	 filename numbers:       fn
	 time values:            time_1 time_2 .... time_ns
	 TIME
	 time set:               ts [description]
	 number of steps:        ns
	 filename numbers file:  fnfilename
	 time values file:       tvfilename
	 
	 where:
	 ts = timeset number. This is the number referenced in the GEOMETRY and VARIABLE sections. description = optional timeset 
	 description which will be shown in userinterface.
	 ns = number of transient steps
	 fs = the number to replace the “*” wildcards in the filenames, for the first step
	 fi = the increment to fs for subsequent steps
	 time = the actual time values for each step, each of which must be separated by a white space and which may continue on 
	 the next line if needed
	 fn = a list of numbers or indices, to replace the “*” wildcards in the filenames.
	 fnfilename = name of file containing ns filename numbers (fn).
	 tvfilename = name of file containing the time values(time_1 ... time_ns).
	 */
	
	//!\todo Several time sets
	
	_time.resize(1);
	int ilgn,nb_steps, filename_start_number,filename_increment;
	//
	ilgn = _sectionTime+1;
	found = _line[ilgn].find(':');
	typevar = _line[ilgn].substr(0,found);
	restofline = _line[ilgn].substr(found+1,_line[ilgn].size()-found-1);
	StringSplit(restofline," \t",contents);// delimiter is either a space or a tab
	if(typevar == "time set")
		time_set = atoi(contents[0].c_str());
	else{
		cout << "Read: " << typevar << endl;
		cout << "Expected: time set" << endl;
		exit(1);
	}
	ilgn = _sectionTime+2;	
	found = _line[ilgn].find(':');
	typevar = _line[ilgn].substr(0,found);
	restofline = _line[ilgn].substr(found+1,_line[ilgn].size()-found-1);
	StringSplit(restofline," \t",contents);// delimiter is either a space or a tab
	if(typevar == "number of steps")
		nb_steps = atoi(contents[0].c_str());
	else{
		cout << "Read: " << typevar << endl;
		cout << "Expected: number of steps " << endl;
		exit(1);
	}
	ilgn = _sectionTime+3;
	found = _line[ilgn].find(':');
	typevar = _line[ilgn].substr(0,found);
	restofline = _line[ilgn].substr(found+1,_line[ilgn].size()-found-1);
	StringSplit(restofline," \t",contents);// delimiter is either a space or a tab
	if(typevar == "filename start number")
		filename_start_number = atoi(contents[0].c_str());
	else{
		cout << "Read: " << typevar << endl;
		cout << "Expected: file name start number" << endl;
		exit(1);
	}
	ilgn = _sectionTime+4;
	found = _line[ilgn].find(':');
	typevar = _line[ilgn].substr(0,found);
	restofline = _line[ilgn].substr(found+1,_line[ilgn].size()-found-1);
	StringSplit(restofline," \t",contents);// delimiter is either a space or a tab
	if(typevar == "filename increment")
		filename_increment = atoi(contents[0].c_str());
	else{
		cout << "Read: " << typevar << endl;
		cout << "Expected: file name increment" << endl;
		exit(1);
	}
	//
	_time[0] = new EnsightTime(time_set,nb_steps,filename_start_number,filename_increment);
	//
	ilgn = _sectionTime+5;
	found = _line[ilgn].find(':');
	typevar = _line[ilgn].substr(0,found);
	restofline = _line[ilgn].substr(found+1,_line[ilgn].size()-found-1);
	StringSplit(restofline," \t",contents);// delimiter is either a space or a tab
	if(typevar == "time values"){
		stringstream buftime;
		for(unsigned int iline = _sectionTime+6;iline< _line.size();iline++){
			buftime << _line[iline];
		}
		//	cout	<< buftime.str()<< endl;
		for(int i=0;i<nb_steps;i++){
			buftime >> _time[0]->timeValue(i);
		}
	}
	else{
		cout << "Read: " << typevar << endl;
		cout << "Expected: time values" << endl;
		exit(1);
	}
	
	//======================
	// Read FILE section
	//======================
	/*
	 This section is optional. It's used for multiple time step data written with the single-file option. This section 
	 will specify the number of time steps for each file, for each data entity ( meaning for each geometry and each 
	 variable, model and/or measured).
	 If the operation system imposed a maximum file size limit, and the data exceeds this limit, then a data entity 
	 will use as many continuation files as needed. Each file set corresponds to one and only one time set, but a time 
	 set may be referenced by many file sets.
	 Usage:
	 FILE
	 file set:                    fs
	 filename index:              fi # Note: only used when data continues in other files
	 number of steps:             ns
	 
	 where:
	 fs = file set number. This is the same number referenced in the GEOMETRY and VARIABLE sections.
	 ns = number of transient steps.
	 fi = file index number in the file name ( This replaces "*" in the filenames ).
	 Material Section
	 */
	
	//======================
	// Read MATERIAL section
	//======================
	/*
	 Material Section
	 
	 This is an optional section. It provides information for the material interface part case. Currently, as of version 8.2, EnSight supports a single material set.
	 Usage:
	 MATERIAL
	 material set number:          ms [description]
	 material id count:            nm
	 material id numbers:          matno_1 matno_2 ... matno_nm
	 material id names:            matdesc_1 mat_2 ... mat_nm
	 material id per element:      [ts] [fs] filename
	 material mixed ids:           [ts] [fs] filename
	 material mixed values:        [ts] [fs] filename
	 # optional species parameters
	 species id count:             ns
	 species id numbers:           spno_1 spno_2 … spno_ns
	 species id names:             spdesc_1 spdesc_2 … spdesc_ns
	 species per material counts:  spm_1 spm_2 … spm_nm
	 species per material lists:   matno_1_sp_1 matno_1_sp_2 … matno_1_sp_spm_1
	 matno_2_sp_1 matno_2_sp_2 … matno_2_sp_spm_2
	 …
	 matno_nm_sp_1 matno_nm_sp2 … matno_nm_sp_spm_nm
	 (Note: above concatenated lists do not have to be on
	 separate lines.)
	 
	 species element values:       [ts] [fs] sp_filename
	 
	 where:
	 ts = The time set number, the same as declared in the TIME section. Only required for transient materials.
	 fs = The file set number, the same as declared in the FILE section. ( Note: if fs is specified, then ts becomes mandatory vs optional.
	 ms = The material set number, ( although at this time only one material set is supported ).
	 nm = The number of materials for this set.
	 matno = The material number used from the material and mixed-material id file. There is a total of nm of these. Non-positive materials are all grouped together into a null-material.
	 matdesc = The GUI's material description or the nm matno above.
	 filename = The material filename.
	 ns = The number of species for the set.
	 spno = The "species per material list:" specification. There should be ns of these positive integers.
	 spdesc = The GUI description corresponding to the ns spno's.
	 spm = The "number of species per material number" (matno); an entry of "0" is valid if no species exist for a given material
	 matno_#_sp = Specie id number ( spno) list member for this material number id ( matno ). If no species for this material, then proceed to next material that has species.
	 sp_filename = The filename of the appropriate "species element values:" file.
	 */
	
	//======================
	// Read BLOCK_CONTINUATION section
	//======================
	/*
	 Block Continuation Section
	 
	 This is an optional section, used for grouping structured blocks together. The purpose of this is to allow the capability to be able to read N number of files using M number of cluster notes, where N > M. The filenames of the geometry and variables must comtain "%" wildcards if this option is used.
	 
	 Usage:
	 
	 BLOCK_CONTINUATION
	 number of sets:                 ns
	 filename start number:          fs
	 filename increment:             fi
	 
	 where:
	 ns = The number of contiguous partioned structured files.
	 fs = the number to replace the "%" wildcard in the geometry and variable filename, for the first set.
	 fi = the increment to fs for subsequent sets.
	 
	 
	 */				
}


string EnsightCase::geometryFile(){
	if(_geo_file_wildcard!=""){
		cout << "Geometry file contains wildcard characters " << _geo_file_wildcard << endl;
		cout << "Use EnsightCase::geometryFile(int i) to specify the time step" << endl;
		exit(1);
	}
	return _geo_file;
}

string EnsightCase::geometryFile(int i){
	if(_geo_file_wildcard=="") return _geo_file;
	else return ReplaceWilcardByInt(_geo_file_wildcard, i, '*');
}

int EnsightCase::variableIndex(string description) const{
	for(unsigned int i=0;i<_var.size();i++){
		if(_var[i]->description()==description) return i;
	}
	cout <<"Variable " << description << " is not in the case file" << endl;
	exit(1);
}

int EnsightCase::timeIndex(double time) const{
	return _time[0]->timeIndex(time);
}


void EnsightCase::readVariable(int idxvar,int idxtime,double* vec,int dim) const{
	string filename = _directory + "/" + _var[idxvar]->variableFile(idxtime);
	if(_verbose) cout << "EnsightCase::readVariable File " << filename << endl;
	ifstream solfile( filename.c_str(), ios_base::in); 
	char line[1024];
	if ( !solfile ){
		cout << "Ensight:: ERROR: Cannot open file "+filename<<endl;
		exit(1);
	}
	solfile.getline(line,1024);
	int i=0;
	while(solfile >> vec[i]){
		i++;	
	}
	if( i!= dim){
		cout << "Something is wrong during the reading of variable #" << idxvar << " in " << filename << endl;
		cout << dim << " expected values " << endl;
		cout << i << " read values" << endl;
		exit(1);
	}	
}


void EnsightCase::readVariable(string description,double time,double* vec,int dim) const{
	int idxtime = timeIndex(time);
	int idxvar = variableIndex(description);
	if(_verbose) cout << "EnsightCase::readVariable " << description << " at time " << time << endl;
	readVariable(idxvar,idxtime,vec,dim);
}


