//===========================================================
//                    MasterFSI
//
// Part of MasterFSIContact,             Copyright Inria 2018
//===========================================================
/*!
 \file picard.hpp
 \author Jean-Frederic Gerbeau
 */

#ifndef PICARD_H_INCLUDED
#define PICARD_H_INCLUDED
#include <iostream>

/*! 
 \file picard.hpp
 \author Jean-Frederic Gerbeau
 
 */


using namespace std;


/*!

 
 Fixed point algorithms x = f(x) with relaxation
 
 \param oper: must have 5 vectors (accessible through the methods x0(), fx0(), x1(), fx1(), tmp())
 and a function eval(int status) which gives to fx1 the value of f(x1)
 and status indicate whether it is the first iterate (status=1) or not (status=0).
 The initial guess is in x1.
 
 \param method
 - \c method = 0 : constant relaxation factor given by omega
 (if omega=1 : stantard fixed point without relaxation)
 - \c method = 1 : heuristic extension of Aitken formula (omega not used)
 - \c method = 2 : Aitken formula due to Irons & Tuck (omega not used)
 (in exact arithmetic 1 and 2 coincide)
 
 The last iterate is copied in x1
  
 \author Jean-Frederic Gerbeau
 */
template <typename Vector,typename Oper>
int
picard(Oper* oper,Vector& tmp, // only for the type Vector (the initial value of tmp does not matter)
       double abstol,double reltol,int& maxit,int method,double omega,
	   int verbose)
{
    double eps_ext; // convergence criteria: eps_ext = ||df-ds|| + ||vf-vs|| < tol
	double xxnorm;
	double mu = 0;
	int iter = 1;
	Vector& x0 = oper->x0();
	Vector& fx0 = oper->fx0();
	Vector& x1 = oper->x1();
	Vector& fx1 = oper->fx1();
	oper->eval(1,eps_ext); // new time step
	x0 = x1; 
	fx0 = fx1;
	//
	x1 = fx0;
	
	double normRes = norm_2(fx0 - x0);
	
	double stop_tol = abstol + reltol*normRes;
	string methodName;
	switch (method){
		case 0:
			methodName = "constant relaxation";
			break;
		case 1:
			methodName = "Aitken";
			break;
		case 2:
			methodName = "Irons & Tuck";
			break;
		default:
			cout << "Unknown Picard method\n";
			exit(1);
	}
	if(verbose){
		cout << "---------------------------------------------------------------"
		<< endl;
		cout << "**** Picard 1 : residual=" << normRes << ", stoping tolerance = "
		<< stop_tol << endl;
	}
	if(normRes <= stop_tol){
		// the algorithm has converged in 1 iteration without relaxation
		if(verbose) cout << "--- Picard : convergence in 1 iteration\n\n";
		maxit = iter;
		return 0;    
	}
	// One iteration was not enough : a second iteration is necessary
	iter++;
	oper->eval(0,eps_ext); // new inner step
	normRes = norm_2(x1-fx1);

	/*
	 if the above normRes is small enough, the algorithm
     has converged in 2 iterations without any relaxation. Otherwise,
     we enter in the loop with relaxation
	 */
	while(normRes > stop_tol && iter < maxit){
		// computation of the relaxation parameter omega
		switch(method){
			case 1:
				tmp = x1 - fx1 - x0 + fx0;
				omega = inner_prod(x1 - x0,tmp);
				xxnorm = inner_prod(tmp,tmp);
				if(xxnorm != 0) omega = omega / xxnorm;
				else omega = 1.;
				break;
			case 2:
				tmp = x0 - fx0 - x1 + fx1;
				xxnorm = inner_prod(tmp,tmp);
				if(xxnorm != 0)
					mu = mu + (mu - 1) * inner_prod( x1-fx1 ,tmp ) / xxnorm;
				omega =  1 - mu;
				break;
		}

		if(verbose){
		  cout <<  "**** Picard " << iter  << " (" << methodName
			   << ") " << "omega=" << omega;
		  if (eps_ext>=0){
			cout << ", norm=||(df,vf)-(ds,vs)||";
		  }
		  cout <<", residual=" << normRes << endl;
		}
		if(fabs(omega)<1e-4){
			omega = 0.0001;
			cout << "!!! Picard: Omega too small. Arbitrary correction : omega = "
			<< omega << endl;
		}
		iter++;
		// save the old iterate
		fx0 = fx1;
		x0 = x1;
		x1 =  ( 1 - omega ) * x1   +   omega * fx1;
 		// evaluate the function on the new iterate
		oper->eval(0,eps_ext); // new inner step
		normRes = norm_2(fx1 - x1);
	}

	if(normRes>stop_tol){
		cout << "!!! Picard: convergence fails" << endl;
		maxit = iter;
		return 1;
	}
	if(verbose) cout << "Picard: convergence in " << iter << " iterations\n\n";
	maxit = iter;
	x1 = fx1;
	return 0;
}
#endif

