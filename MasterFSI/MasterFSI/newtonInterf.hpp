//===========================================================
//                    MasterFSI
//
// Part of MasterFSIContact,             Copyright Inria 2018
//===========================================================
/*!
 \file newtonInterf.hpp
 \author Jean-Frederic Gerbeau
 */

#ifndef _NEWTONINTERF_HPP
#define _NEWTONINTERF_HPP

#include "vectormatrix.hpp"
#include "operInterf.hpp"

/*!
 \class NewtonInterf
 
 Solve T = 0 with a Newton algorithm
 */	
class NewtonInterf{
	int _verbose;
	size_t _dim;
	OperInterf* _oper;
	int _fluidReceivesInterfDisp;
	Vector _sol;
	Vector _step;
	Vector _residual;
	Vector _prod_y;
public:
	NewtonInterf(int verbose,OperInterf& oper);
	int solve(double abstol,double reltol,int& maxiter,int algo,double eta_max);
	Vector& step(){return _step;}
	Vector& residual(){return _residual;}
	Vector& sol(){return _sol;}
	void solveJac(double& linear_rel_tol);
	void evalResidual(int iter);
	void updateJac(int iter);
	Vector prod(Vector& y);	
};
#endif
