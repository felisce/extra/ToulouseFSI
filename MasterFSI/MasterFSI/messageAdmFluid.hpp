//===========================================================
//                    MasterFSI
//
// Part of MasterFSIContact,             Copyright Inria 2018
//===========================================================
/*!
 \file messageAdmFluid.hpp
 \author Jean-Frederic Gerbeau
 */
#ifndef _MESSAGEADMFLUID_HPP
#define _MESSAGEADMFLUID_HPP

#include "messageAdmFSI.hpp"


using namespace std;

/*!
 \class MessageAdmFluid
 \brief Messages Administrator for the fluid solver
 
 \c MessageAdmFluid is the class managing the messages for the fluid solver
 
 \author  Jean-Frederic Gerbeau
 */

class MessageAdmFluid:public MessageAdmFSI
{
#ifdef MSG_PVM
	int _masterId;
#endif
#ifdef MSG_ZMQ
  string _socketTransport; // e.g. tcp
  string _socketAddress; // e.g. 127.0.0.1 (localhost)
  string _socketPort; // e.g. 5555
  zmq::context_t _zmq_context;
  zmq::socket_t _socket;
#endif
public:
#ifdef MSG_PVM
  MessageAdmFluid(int argc, char *argv[],const int& verbose,const int& protocol,int unit);
  int masterId(){return _masterId;}
#endif
#ifdef MSG_ZMQ
  MessageAdmFluid(int argc, char *argv[],const int& verbose,const int& protocol,int unit,string socketTransport,string socketAddress,string socketPort);
#endif
  void receiveInitializationFromMaster();
	void sendInitializationToMaster(int nbNode,int nbDofPerNode,int nbWindkessel);
	void sendStateToMaster(int nbNode,int nbDofPerNode,double* disp,double* vel,int nbWindkessel,double* varWindkessel);
	void receiveStateFromMaster(int& nbNode,int& nbDofPerNode,double& time,double* disp,double* vel,int& nbWindkessel,double* varWindkessel);
	//	
	void receiveInterfVecFromMaster(int& status, double& dt,int& dim,double* vec);
	void receiveInterfDispVelFromMaster(int& status, double& dt,int& dim,double* disp,double* vel);
	void sendInterfForceToMaster(int dim,double* force);
	// 
	void sendInitializationEstimToMaster(int dim_param,int dim_observ);
	void receiveStateParamFromMaster(int& dim,double* disp,double* vel,int& dimWindkessel,double* varwindkessel,
									 int dim_param,double* param,double& dt,int& flag_loop,int& flag_msg,int& rank);
	void sendStateParamInnovToMaster(int dim,double* disp,double* vel,int dimWindkessel,double *varwindkessel,
									 int dim_param,double* param,int dim_innov,double* innov,int flag_output);
	void sendInitStateParamToMaster(int dim,double* disp, double* vel, int dimWindkessel, double* varwindkessel, 
									int dim_param, double* param, double* invCovParam,int dim_observ,double* lumpedInvCovObserv);
  void sendInterfForceDispVelToMaster(int dim,double* forceF,double* dispF, double* velF); // for Robin-Neumann convergence criteria
};

#endif
