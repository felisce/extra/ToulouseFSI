#ifndef _PARAM_H
#define _PARAM_H
#include <string>
using namespace std;

class Param
{
public:
  double A0[3]; // A0, A1 : the two extremal points on the valve
  double A1[3];
  double B0[3]; //B0, B1 : Initialcondition (compressed initially)
  double B1[3]; 
  double K; 
  double M;
  double g;
  int testCase; 
  int freqPost;
  int protocol;
  int verbose;
  string postProcDir;// post processing directory
  string testCaseName; // test case name
  string socket_transport;
  string socket_address;
  string socket_port;
  Param(const string& data_file_name);
};
#endif
