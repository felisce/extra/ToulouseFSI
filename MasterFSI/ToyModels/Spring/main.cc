#include <iostream>
#include "GetPot.h"
#include "Spring.h"

int main(int argc,char** argv)
{
/*  GetPot command_line(argc,argv);
  const char* data_file_name = command_line.follow("data", 2, "-f","--file");*/
  string data_file_name;
  if(argc==1) data_file_name = "data";
  else data_file_name = argv[2];
  Spring spr(data_file_name);
  spr.run();
  cout << "Solution Complete" << endl;
  return 0;
}
