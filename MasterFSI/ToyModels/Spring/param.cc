#include "param.h"
#include "GetPot.h"
#include <iostream>

using namespace std;

Param::Param(const string& data_file_name)
{
  GetPot dfile(data_file_name.c_str());
  
  A0[0] =  dfile("values/A0",0.,0);
  A0[1] =  dfile("values/A0",1.,1);
  A0[2] =  dfile("values/A0",2.,2);
  A1[0] =  dfile("values/A1",0.,0);
  A1[1] =  dfile("values/A1",1.,1);
  A1[2] =  dfile("values/A1",2.,2);
  B0[0] =  dfile("values/B0",0.,0);
  B0[1] =  dfile("values/B0",1.,1);
  B0[2] =  dfile("values/B0",2.,2);
  B1[0] =  dfile("values/B1",0.,0);
  B1[1] =  dfile("values/B1",1.,1);
  B1[2] =  dfile("values/B1",2.,2);
  K =  dfile("values/K",1.);
  M = dfile("values/M",1.);

  //----------------------------------------------------------------------
  testCase = dfile("numerics/testCase",1);
  protocol = dfile("numerics/protocol",1);
  socket_transport = dfile("numerics/socket_transport","tcp");
  socket_address = dfile("numerics/socket_address","127.0.0.1");
  socket_port = dfile("numerics/socket_port","5555");
  verbose = dfile("numerics/verbose",1);
  freqPost = dfile("postprocessing/freq_post",1);
  postProcDir = dfile("postprocessing/dir_post","./");
  testCaseName = dfile("postprocessing/test_case_name","struct");
  cout << "----------------------------------------" << endl;
  cout << "Spring: data file" << endl;
  dfile.print();
  /*
    vector<string> ufos;
    ufos = dfile.unidentified_variables(17,"numerics/nb_points","numerics/nb_time_step","numerics/dt",
    "numerics/beta","numerics/gamma","physics/test_case","physics/rho_w",
    "physics/length","physics/x_start","physics/thick","physics/young",
    "physics/timoshenko","physics/poisson","physics/viscoelastic",
    "physics/radius","postprocessing/freq_post","postprocessing/dir_post","postprocessing/test_case_name");
    if(ufos.size()){
    cout << "Incorrect datafile:" << endl;
    cout << "    The following variables are unknown:" << endl;
    for(vector<string>::const_iterator it = ufos.begin(); it != ufos.end();it++){
    cout << "      " << *it << endl;
    }
    exit(1);
    }
  */
  cout << "----------------------------------------" << endl;
}

