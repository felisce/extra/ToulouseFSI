#include "param.h"
#include <string>

#ifdef MSG_PVM
#include "pvm3.h"
#endif

#ifdef MSG_ZMQ
#include "zmq.hpp"
#endif

using namespace std;

class Spring:
  public Param
{
  double* coor;
  double* coorInit;
  double* disp;
  double* disp_old;
  double* vel;
  double* vel_old;
  double* forceMaster;
  
  string caseFileName;
  string geoFileName;
  int compteur;

  int fsiStatus;
  int nbPoints;
  double angularMomentum;
  double energy;
  double time;
  double dt;
#ifdef MSG_PVM
  int masterId;
#endif
#ifdef MSG_ZMQ
  zmq::context_t zmq_context;
  zmq::socket_t socket;
#endif

public:
  Spring(const string& menu_name);
  void postCoorMTV(string filename,bool flag) const;
  void postPtMTV(string filename,bool flag) const;
  void writeCaseFile() const;
  void writeGeoFile() const;
  void writeDisplacementFile(int iter) const;
  void writeRotationFile(int iter) const;
  void updateCaseFile(double time, int& compteur) const;
  void updateNumberOfSteps(int iter) const;


  void run();
  void sdToMasterFSI();
  void rvFromMasterFSI();
};
