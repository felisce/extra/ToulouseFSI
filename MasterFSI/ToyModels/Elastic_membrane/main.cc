#include <iostream>
#include "GetPot.h"
#include "Membrane.h"

int main(int argc,char** argv)
{
  string data_file_name;
  if(argc==1) data_file_name = "data";
  else data_file_name = argv[2];
  Membrane pb(data_file_name);
  pb.solve();
  cout << "Normal end of Membrane" << endl;
  return 0;
}
