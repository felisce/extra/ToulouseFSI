//written for fixed leading edge and zero displacment gradient at the free end 
// rho_s*thickness*(del_2_u/del_t_2) - E*(del_2_u/del_x_2) = F
#include "Membrane.h"
#include <iostream>
#include <fstream>
#include <sstream>
#include <math.h>
#include <cstdlib>
#include <iomanip>
#include <boost/numeric/ublas/matrix.hpp>
#include <boost/numeric/ublas/lu.hpp>
#include <boost/numeric/ublas/vector.hpp>
#include <boost/numeric/ublas/triangular.hpp>

const int MSG_VERBOSE=1;
using namespace std;

using namespace boost::numeric::ublas;

namespace patch
{
  template < typename T > std::string to_string( const T& n )
  {
    std::ostringstream stm ;
    stm << n ;
    return stm.str() ;
  }
}

Membrane::Membrane(const string& menu_name):
#ifdef MSG_ZMQ
Param(menu_name),time(0.),zmq_context(1),socket(zmq_context, ZMQ_PAIR)
#else
Param(menu_name),time(0.)
#endif
{
  coor = new double[3*nbPoints];
  coorInit = new double[3*nbPoints];
  disp = new double[3*nbPoints];
  disp_old = new double[3*nbPoints];
  vel_old = new double[3*nbPoints];
  vel = new double[3*nbPoints];
  forceMaster = new double[3*nbPoints];
  contactForce = new double[3*nbPoints];
  externalForce = new double[3*nbPoints];
  energy = 0.;
  f = 0.; //internal force
  f = 1000.; //internal force for test "Elastic_string"
  L = sqrt(A1[0]*A1[0] + A1[1]*A1[1]);  //assuming first point is 0,0
   
  for(int i=0;i<nbPoints;i++){
    for(int j=0;j<3;j++){
	  coorInit[3*i + j] = A0[j] + i* (A1[j]-A0[j])/(nbPoints-1);
      coor[3*i + j] = coorInit[3*i + j];
      if (j==1) coor[3*i + j] = B0[1] + (B1[1]-B0[1])*(coor[3*i]-B0[0])/(B1[0]-B0[0]);
      //if (j==1) coor[3*i + 1] = -coorInit[3*i] * 0.2;
      disp[3*i + j] = coor[3*i + j] - coorInit[3*i + j];
      disp_old[3*i + j] = 0.;
      vel_old[3*i + j] = 0.;
      vel[3*i + j] = 0.;
      forceMaster[3*i + j] = 0.;
      contactForce[3*i + j] = 0.;
      externalForce[3*i + j] = 0.;
    }
  }
  
  
  if(verbose>1)
  {
	cout << "Default coordinates" << endl;
	for( int i=0; i<nbPoints; i++)
	{
	  for( int j=0; j<3;j++)
	  {
		  cout << coorInit[3*i + j] << " ";
	  }
	  cout<< endl;
	}
  
	cout << "initial condition" << endl;
	for( int i=0; i<nbPoints; i++)
	{
	  for( int j=0; j<3;j++)
	  {
		  cout << coor[3*i + j] << " ";
	  }
	  cout<< endl;
	}
  
	cout << "displacement" << endl;
	for( int i=0; i<nbPoints; i++)
	{
	  for( int j=0; j<3;j++)
	  {
		  cout << disp[3*i + j] << " ";
	  }
	  cout<< endl;
	}
  }
  
  //-------
  // Case file
  //-------
  caseFileName = postProcDir + "/solid.case";
  geoFileName = postProcDir + "/solid.geo";
  compteur = 0;
  writeCaseFile();
  writeGeoFile();
  
  //-------
  // Valve
  //-------
  fsiStatus = 1;
   
#ifdef MSG_PVM
  masterId = pvm_parent();
  if(masterId==PvmNoParent){
    cerr << "I am a poor lonesome job\n";
    exit(1);
  } else
    if(masterId==0){
      cerr << "PVM is not ok\n";
      exit(1);
    }
  cout << "I am the slave " << pvm_mytid()<< endl;
  cout << "My master is " << masterId << endl;
#endif
  
#ifdef MSG_ZMQ
  string socket_endpoint;
  if(socket_port != "")
    socket_endpoint = socket_transport + "://" + socket_address + ":" + socket_port;
  else
    socket_endpoint = socket_transport + "://" + socket_address;
  cout << "[ZeroMQ] Connect to " << socket_endpoint << endl;
  socket.connect(socket_endpoint.c_str());
#endif

   
  sdToMasterFSI();
}

void Membrane::solve()
{
	
  bool flagpost=false;
  int iter=0,istep=1;
  
  updateNumberOfSteps(iter);
  do{
#if defined(MSG_PVM) || defined(MSG_ZMQ)
    rvFromMasterFSI();
#endif
    if(fsiStatus == -1) break;
    if(fsiStatus == 1) {
      writeDisplacementFile(iter, time);
      writeRotationFile(iter, time);
      updateCaseFile(time, compteur);
      updateNumberOfSteps(iter);
      //writefile();
      
      // time iteration
      iter++;
      time += dt;
      cout<<"\n============================================================\n";
      cout << " Valve Iteration : " << iter << ", Time : " << time << endl;
      cout << "============================================================\n";
      istep = 1;
      for(int i=0; i<nbPoints; i++)
      {
			disp_old[3*i + 1] = disp[3*i + 1];
			vel_old[3*i + 1] = vel[3*i + 1];  
	   }
    }
    if(fsiStatus == 0){
      // inner iteration
      istep ++;
    }
    if(fsiStatus == 0 || fsiStatus == 1){
      cout << "\n*** inner iteration : " << istep << endl;
	double h = (A1[0]-A0[0])/(nbPoints-1);  //uniform mesh
	
	matrix<double> K(nbPoints, nbPoints);
	matrix<double> M(nbPoints, nbPoints);
	boost::numeric::ublas::vector<double> force(nbPoints);
	
	//global stiffness matrix and mass matrix
	for(int i=0; i<nbPoints; i++)
	{
		for(int j=0; j<nbPoints; j++)
		{
			if ((i==0 && j==0) || (i==(nbPoints-1) && j==(nbPoints-1)))
			{
				K(i,j) = 1.;
				M(i,j) = 1./3;
			}
			else if (i==j)
			{
				K(i,j) = 2.;
				M(i,j) = 2./3;
			}
			else if (j==(i-1) || j==(i+1))
			{
				K(i,j) = -1.;
				M(i,j) = 1./6;
			}
			else
			{
				K(i,j) = 0.;
				M(i,j) = 0.;
			}
		
			K(i,j) *= E/h;
			M(i,j) *= rho*thickness*h/dt;
					
		}
		if ( i==0 || i==(nbPoints-1) )
			force(i) = 0.5*f*h*1; // f is the internal force
		else
			force(i) = 0.5*f*h*2; // f is the internal force
		force(i) += forceMaster[3*i + 1];
	}

	//rhs
	boost::numeric::ublas::vector<double> rhs(nbPoints);
	boost::numeric::ublas::vector<double> temp(nbPoints);
	rhs = force * dt;
	for(int i=0; i<nbPoints; i++)
	{
		temp(i) = disp_old[3*i + 1];
	}
	rhs += prod(M,temp);
	for(int i=0; i<nbPoints; i++)
	{
		temp(i) = vel_old[3*i + 1];
	}
	rhs += prod(M,temp) * dt;
	
	//lhs
	matrix<double> lhs(nbPoints,nbPoints);
	lhs = M + (K*dt);
	
	//incorporation of boundary condition: u(x=0,t)=0
	lhs(0,0) = 1.;
	for(int i=1; i<nbPoints; i++)
	{
		lhs(0,i) = 0.;
		lhs(i,0) = 0.;
	}
	rhs(0) = 0.;
	
	/*
	//incorporation of boundary condition: u(x=L,t)=0
	lhs((nbPoints-1),(nbPoints-1)) = 1.;
	for(int i=0; i<nbPoints-1; i++)
	{
		lhs((nbPoints-1),i) = 0.;
		lhs(i,(nbPoints-1)) = 0.;
	}
	rhs(nbPoints-1) = 0.;
	*/
	
	//solution
	permutation_matrix<double> pm(nbPoints);
	int res = lu_factorize(lhs,pm);
	if(res != 0) cerr << "LU factorization failed" << endl;
	identity_matrix<double> inv(nbPoints);
	matrix<double> inverse = inv;
	lu_substitute(lhs,pm,inverse);
	temp = prod(inverse,rhs);
	  
	for(int i=0; i<nbPoints; i++)
	{
		disp[3*i + 1] = temp(i);
		vel[3*i + 1] = (disp[3*i + 1] - disp_old[3*i + 1])/dt;
		coor[3*i + 1] = coorInit[3*i + 1] + disp[3*i + 1];
	}
	
	//energy calc
	lhs = M + (K*dt);
	
	//incorporation of boundary condition: u(x=0,t)=0
	//lhs(0,0) = 1.; 
	//lhs(0,1) = 0.;
	//lhs(1,0) = 0.;


  lhs(0,0) = 1.;
  for(int i=1; i<nbPoints; i++)
  {
    lhs(0,i) = 0.;
    lhs(i,0) = 0.;
  }

	//////////////////////////////////
	for(int i=0; i<nbPoints; i++)
	{
		rhs(i) -= contactForce[3*i + 1]*dt;
	}
	///////////////////////////////////

  rhs(0) = 0.;  

	boost::numeric::ublas::vector<double> temp2(nbPoints);	
	
  energy = 0.5*inner_prod(temp,prod(lhs,temp));
  energy -= inner_prod(temp,rhs);
  energy /= dt;
  }

#if defined(MSG_PVM) || defined(MSG_ZMQ)
    sdToMasterFSI();
#endif
  }
#if defined(MSG_PVM) || defined(MSG_ZMQ)
  while (1); // the master decides
#else
  while(iter <= nbTimeSteps);
#endif
}

void Membrane::rvFromMasterFSI()
{
  cout << "<--- Structure receives from master\n";
  int ntdls;
#ifdef MSG_PVM
  pvm_recv(masterId,130);
  pvm_upkint(&fsiStatus,1,1);
  if(fsiStatus!=-1){
    pvm_upkdouble(&dt,1,1); 
    pvm_upkint(&ntdls,1,1);
    if(ntdls != 3*nbPoints){
      cout << "rvFromMasterFSI: fatal error : " << ntdls
      << " != " << 3*nbPoints << endl;
      exit(1);
    }
    if(protocol == 1)	pvm_upkdouble(forceMaster,3*nbPoints,1);
    else if(protocol == 11 ) 
    {
		pvm_upkdouble(externalForce,3*nbPoints,1);
		pvm_upkdouble(contactForce,3*nbPoints,1);
		for(int i=0;i<nbPoints;i++){
		  for(int j=0;j<3;j++){
			  forceMaster[3*i + j] = contactForce[3*i + j] + externalForce[3*i + j];
			}
		}
	}
	else
		cout << "Unknown protocol" << endl;
    cout << "force received = " << forceMaster[3*(nbPoints-1)+1] << endl;
  }
#endif
#ifdef MSG_ZMQ
  zmq::message_t msg1;
  socket.recv(&msg1);
  memcpy(&fsiStatus,msg1.data(),1*sizeof(int));
  //
  if(fsiStatus!=-1){
    zmq::message_t msg2;
    socket.recv(&msg2);
    memcpy(&dt,msg2.data(),1*sizeof(double));
    //
    zmq::message_t msg3;
    socket.recv(&msg3);
    memcpy(&ntdls,msg3.data(),1*sizeof(int));
    //
    if(ntdls != 3*nbPoints){
      cout << "rvFromMasterFSI: fatal error : " << ntdls
      << " != " << 3*nbPoints << endl;
      exit(1);
    }
    zmq::message_t msg4;
    socket.recv(&msg4);
    memcpy(forceMaster,msg4.data(),3*nbPoints*sizeof(double));
  }
#endif

  if(fsiStatus == 2){
    cout << "fsiStatus = 2 not yet implemented\n" << flush;
    exit(1);
  } 
  
if(verbose>1)
{
  cout << "\t status: " << fsiStatus << endl;
  if(fsiStatus!=-1){
    cout << "\t dt=" << dt << endl;
    cout << "\t force: ";
    cout << endl;
  }
 }
}


void Membrane::sdToMasterFSI()
{
  int ntdls = 3*nbPoints; 
  cout << "ntdls=" << ntdls << endl;
  cout << "---> Valve sends to master " << endl;
  if(fsiStatus == 2){
    cout << "fsiStatus = 2 not yet implemented" << endl << flush;
    exit(1);
  } else {
  }
if(verbose>1) {
  cout <<"\t disp: ";
  for(int i=0;i<nbPoints;i++)
    cout << disp[3*i] << " " << disp[3*i+1] << " " << disp[3*i+2] << endl;
  cout << endl;
  cout <<"\t velo: ";
  for(int i=0;i<nbPoints;i++) cout << vel[3*i  ] << " "
    << vel[3*i+1] << " "
    << vel[3*i+2] << endl;
}
	
	switch(protocol){
    case 1:
	{
#ifdef MSG_PVM
      pvm_initsend(PvmDataDefault);
      pvm_pkint(&ntdls,1,1);
      pvm_pkdouble(disp,ntdls,1);
      pvm_pkdouble(vel,ntdls,1);
      pvm_send(masterId,100);
#endif
#ifdef MSG_ZMQ
	  zmq::message_t msg1(1*sizeof(int));
      memcpy(msg1.data(),&ntdls,1*sizeof(int));
      socket.send(msg1);
      //
      zmq::message_t msg2(ntdls*sizeof(double));
      memcpy(msg2.data(),disp,ntdls*sizeof(double));
      socket.send(msg2);
      //
      zmq::message_t msg3(ntdls*sizeof(double));
      memcpy(msg3.data(),vel,ntdls*sizeof(double));
      socket.send(msg3);
#endif
		cout << "protocol 1 finished" << endl;
      break;
  }
    case 11:
	{
		cout << "protocol 11" << endl;
#ifdef MSG_PVM
      pvm_initsend(PvmDataDefault);
      pvm_pkint(&ntdls,1,1);
      pvm_pkdouble(disp,ntdls,1);
      pvm_pkdouble(vel,ntdls,1);
      pvm_pkdouble(&energy,1,1);
      pvm_send(masterId,1100);
#endif
#ifdef MSG_ZMQ
      zmq::message_t msg1(1*sizeof(int));
      memcpy(msg1.data(),&ntdls,1*sizeof(int));
      socket.send(msg1);
      //
      zmq::message_t msg2(ntdls*sizeof(double));
      memcpy(msg2.data(),disp,ntdls*sizeof(double));
      socket.send(msg2);
      //
      zmq::message_t msg3(ntdls*sizeof(double));
      memcpy(msg3.data(),vel,ntdls*sizeof(double));
      socket.send(msg3);
      //
      zmq::message_t msg4(1*sizeof(double));
      memcpy(msg4.data(),&energy,1*sizeof(double));
      socket.send(msg4);
#endif
      cout << "Energy sent: " << energy << endl; 
      break;
  }
    default:
      cout << "Error in " << __FILE__ << " line " << __LINE__ << ": unknown protocol " << protocol << endl;
      exit(1);
      cout << "switch finished" << endl;
  }
  cout << "sent" << endl;
}


void Membrane::writeCaseFile() const {
  
  ofstream f;
  
  f.open(caseFileName.c_str());
  f << "FORMAT" << endl;
  f << "type: ensight" << endl;
  f << "GEOMETRY" << endl;
  f << "model: 1 solid.geo" << endl;
  f << "VARIABLE" << endl;
  f << "vector per node: 1 displacement displacement.*****.vct" << endl;
  f << "scalar per node: 1 rotation rotation.*****.scl" << endl;
  f << "TIME" << endl;
  f << "time set: 1" << endl;
  f << "number of steps:               " << endl;
  f << "filename start number: 0" << endl;
  f << "filename increment: 1" << endl;
  f << "time values:" << endl;
  
  f.close();
  
}

void Membrane::writeGeoFile() const {
  
  ofstream f;
  
  f.open(geoFileName.c_str());
  f << "Geometry file" << endl;
  f << "Geometry file" << endl;
  f << "node id assign" << endl;
  f << "element id assign" << endl;
  f << "coordinates" << endl;
  f << "      " << nbPoints << endl;
  
  f.precision(5);
  
  for (int i = 0; i < nbPoints; ++i) {
    for (int j = 0; j < 3; ++j) {
      if (coorInit[3*i + j] >= 0) {
        f << scientific << " " << coorInit[3*i + j];
      }
      else {
        f << scientific << coorInit[3*i + j];
      }
    }
    
    f << endl;
    
  }
  
  f << "part       1" << endl;
  f << "domain" << endl;
  f << "bar2" << endl; 
  f << "      " << nbPoints-1 << endl;
  
  for (int i = 1; i <= nbPoints-1; ++i) {
    
    f << "      ";
    
    if(i < 10) {
      f << " " << i;
    }
    else {
      f << i;
    }
    
    f << "      ";
    
    if(i < 9) {
      f << " " << i+1;
    }
    else {
      f << i+1;
    }
    
    f << endl;
  }
  
  f << "part       2" << endl;
  f << "fixedPoint" << endl;
  f << "point" << endl;
  f << "       1" << endl;
  f << "       1" << endl;  
  f << "part       3" << endl;
  f << "freePoint" << endl;
  f << "point" << endl;
  f << "       1" << endl;
  
  if (nbPoints < 10) {
    f << "       " << nbPoints << endl;
  }
  else {
    f << "      " << nbPoints << endl;
  }
  
  f.close();
  
}


void Membrane::writeDisplacementFile(int iter, double time) const {
  
  ofstream f;
  string displacementFile = postProcDir + "/";
  
  if(iter < 10) {
    displacementFile += "displacement.0000" + patch::to_string(iter) + ".vct";
  }
  else if(iter < 100) {
    displacementFile += "displacement.000" + patch::to_string(iter) + ".vct";
  }
  else if(iter < 1000) {
    displacementFile += "displacement.00" + patch::to_string(iter) + ".vct";
  }
  else if(iter < 10000) {
    displacementFile += "displacement.0" + patch::to_string(iter) + ".vct";
  }
  else{
    displacementFile += "displacement." + patch::to_string(iter) + ".vct";
  }
  
  f.open(displacementFile.c_str());
  
  f << "Vector per node" << endl;
  
  int compteur = 0;
  
  f.precision(5);
  
  for (int i = 0; i < nbPoints; ++i) {
    for (int j = 0; j < 3; ++j) {
      if(disp[3*i+j] >= 0) {
        f << scientific << " " << disp[3*i+j];
      }
      else {
        f << scientific << disp[3*i+j];
      }
    }
    
    ++compteur;
    
    if (compteur == 2) {
      f << endl;
      compteur = 0;
    }
    else {
      
    }
    
  }
  
  f.close();
  
}

void Membrane::writeRotationFile(int iter, double time) const {
  
  ofstream f;
  string rotationFile = postProcDir + "/";
  
  if(iter < 10) {
    rotationFile += "rotation.0000" + patch::to_string(iter) + ".scl";
  }
  else if(iter < 100) {
    rotationFile += "rotation.000" + patch::to_string(iter) + ".scl";
  }
  else if(iter < 1000) {
    rotationFile += "rotation.00" + patch::to_string(iter) + ".scl";
  }
  else if(iter < 10000) {
    rotationFile += "rotation.0" + patch::to_string(iter) + ".scl";
  }
  else{
    rotationFile += "rotation." + patch::to_string(iter) + ".scl";
  }
  
  f.open(rotationFile.c_str());
  
  f << "Scalar per node" << endl;
  
  int compteur = 0;
  double rotation = 0.;
  
  f.precision(5);
  
  for (int i = 0; i < nbPoints; ++i) {   
    if (rotation >= 0.) {
      f << scientific << " " << rotation;
    }
    else {
      f << scientific << rotation;
    }
    
    ++compteur;
    
    if (compteur == 6) {
      f << endl;
      compteur = 0;
    }
    else {
      
    }
    
  }
  
  f.close();
  
}


void Membrane::updateCaseFile(double time, int& compteur) const {
  
  ofstream f;
  f.open(caseFileName.c_str(), ios::app);
  
  f << fixed;
  f << setprecision(3);
  
  f << "        " << time;
  ++compteur;
  
  if(compteur == 5) {
    f << endl;
    compteur = 0;
  }
  
  f.close();
  
}

void Membrane::updateNumberOfSteps(int iter) const {
  
  ofstream f;
  f.open(caseFileName.c_str(), ios::out|ios::in);
  
  f.seekp(194); // position into the file
  
  f << iter; // write a string of size 4
  
  f.close();
  
}


void Membrane::writefile()
{
	ofstream file1;
	string name = postProcDir + "/../FRAMESOLID/F_";
    string fileend = ".dat";
    string result;
    char numstr[21];
    sprintf(numstr, "%lf", time);
    result = name + numstr+fileend;
    file1.open(result.c_str());

    file1<<"VARIABLES = \"X\"\n\"Y\"\n\"Z\"\n\"d_x\"\n\"d_y\"\n\"d_z\"\n"<<endl;
    file1<<"ZONE T=\"Grp 0.1\" N="<<(nbPoints*3)<<" E="<<(nbPoints-1)<<" F=FEBLOCK ET=TRIANGLE\n"<<endl;
        
    for(int i=0;i<nbPoints;i++)
    {
		file1<<coor[3*i]<<endl;
	}
		
	for(int i=0;i<nbPoints;i++)
    {
		file1<<coor[3*i + 1]<<endl;
	}
	
	for(int i=0;i<nbPoints;i++)
    {
		file1<<coor[3*i + 2]<<endl;
	}
	
	for(int i=0;i<nbPoints;i++)
    {
		file1<<disp[3*i]<<endl;
	}
		
	for(int i=0;i<nbPoints;i++)
    {
		file1<<disp[3*i + 1]<<endl;
	}
	
	for(int i=0;i<nbPoints;i++)
    {
		file1<<disp[3*i + 2]<<endl;
	}
        
    for(int i=0;i<(nbPoints-1);i++)
    {
		file1<<(i+1)<<"\t"<<(i+2)<<"\t"<<1<<endl;
	}
        
    file1.close();
}

