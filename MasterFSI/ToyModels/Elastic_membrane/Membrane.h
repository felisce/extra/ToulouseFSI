#include "param.h"
#include <string>

#ifdef MSG_PVM
#include "pvm3.h"
#endif

#ifdef MSG_ZMQ
#include "zmq.hpp"
#endif

using namespace std;

class Membrane:
  public Param
{
  double* coor;
  double* coorInit;
  double* disp;
  double* disp_old;
  double* vel_old;
  double* vel;
  double* forceMaster;
  double* contactForce;
  double* externalForce;
  double f;
  
  string caseFileName;
  string geoFileName;
  int compteur;

  int fsiStatus;
  double energy;
  double time;
  double dt;
  double L;
#ifdef MSG_PVM
  int masterId;
#endif
#ifdef MSG_ZMQ
  zmq::context_t zmq_context;
  zmq::socket_t socket;
#endif

public:
  Membrane(const string& menu_name);
  void writeCaseFile() const;
  void writeGeoFile() const;
  void writeDisplacementFile(int iter, double time) const;
  void writeRotationFile(int iter, double time) const;
  void updateCaseFile(double time, int& compteur) const;
  void updateNumberOfSteps(int iter) const;
  void writefile();

  void solve();
  void sdToMasterFSI();
  void rvFromMasterFSI();
};
