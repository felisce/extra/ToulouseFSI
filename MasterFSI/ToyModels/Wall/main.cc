#include <iostream>
#include "GetPot.h"
#include "Wall.h"

int main(int argc,char** argv)
{
/*  GetPot command_line(argc,argv);
  const char* data_file_name = command_line.follow("data", 2, "-f","--file");*/
  string data_file_name;
  if(argc==1) data_file_name = "data";
  else data_file_name = argv[2];
  Wall pb(data_file_name);
  pb.run();
  cout << "Normal end of wall" << endl;
  return 0;
}
