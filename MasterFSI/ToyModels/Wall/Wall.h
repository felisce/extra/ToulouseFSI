#include "param.h"
#include <string>

#ifdef MSG_PVM
#include "pvm3.h"
#endif

#ifdef MSG_ZMQ
#include "zmq.hpp"
#endif

using namespace std;

class Wall:
  public Param
{
  double* coor;
  double* disp;
  double* velo;
  double* forceMaster;
  double energy;

  string caseFileName;
  string geoFileName;
  int compteur;

  int fsiStatus;
  double time;
#ifdef MSG_PVM
  int masterId;
#endif
#ifdef MSG_ZMQ
  zmq::context_t zmq_context;
  zmq::socket_t socket;
#endif

public:
  Wall(const string& menu_name);
  void writeCaseFile() const;
  void writeGeoFile() const;
  void writeDisplacementFile(int iter, double time) const;
  void writeRotationFile(int iter, double time) const;
  void updateCaseFile(double time, int& compteur) const;
  void updateNumberOfSteps(int iter) const;


  void run();
  void sdToMasterFSI();
  void rvFromMasterFSI();
};
