#include "param.h"
#include "GetPot.h"
#include <iostream>

using namespace std;

Param::Param(const string& data_file_name)
{
  GetPot dfile(data_file_name.c_str());

  A0[0] =  dfile("physics/A0",0.,0);
  A0[1] =  dfile("physics/A0",1.,1);
  A0[2] =  dfile("physics/A0",2.,2);
  A1[0] =  dfile("physics/A1",0.,0);
  A1[1] =  dfile("physics/A1",1.,1);
  A1[2] =  dfile("physics/A1",2.,2);
  C0[0] =  dfile("physics/C0",0.,0);
  C0[1] =  dfile("physics/C0",1.,1);
  C0[2] =  dfile("physics/C0",2.,2);
  Jaxis =  dfile("physics/Jaxis",1.);
  stiffness =  dfile("physics/stiffness",0.);
  theta_min =  dfile("physics/theta_min",5.);
  theta_max =  dfile("physics/theta_max",85.);
  //----------------------------------------------------------------------
  testCase = dfile("numerics/testCase",1);
  nbPoints = dfile("numerics/nbPoints",10);
  nbTimeSteps = dfile("numerics/nbTimeSteps",10);
  dt = dfile("numerics/dt",0.1);
  protocol = dfile("numerics/protocol",1);
  socket_transport = dfile("numerics/socket_transport","tcp");
  socket_address = dfile("numerics/socket_address","127.0.0.1");
  socket_port = dfile("numerics/socket_port","5555");
  freqPost = dfile("postprocessing/freq_post",1);
  postProcDir = dfile("postprocessing/dir_post","./");
  testCaseName = dfile("postprocessing/test_case_name","struct");
  cout << "----------------------------------------" << endl;
  cout << "Wall: data file" << endl;
  dfile.print();
  /*
    vector<string> ufos;
    ufos = dfile.unidentified_variables(17,"numerics/nb_points","numerics/nb_time_step","numerics/dt",
    "numerics/beta","numerics/gamma","physics/test_case","physics/rho_w",
    "physics/length","physics/x_start","physics/thick","physics/young",
    "physics/timoshenko","physics/poisson","physics/viscoelastic",
    "physics/radius","postprocessing/freq_post","postprocessing/dir_post","postprocessing/test_case_name");
    if(ufos.size()){
    cout << "Incorrect datafile:" << endl;
    cout << "    The following variables are unknown:" << endl;
    for(vector<string>::const_iterator it = ufos.begin(); it != ufos.end();it++){
    cout << "      " << *it << endl;
    }
    exit(1);
    }
  */
  cout << "----------------------------------------" << endl;
}

