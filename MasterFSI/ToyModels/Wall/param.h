#ifndef _PARAM_H
#define _PARAM_H
#include <string>
using namespace std;

class Param
{
public:
  double A0[3]; // A0, A1 : the two extremal points on the valve
  double A1[3];
  double C0[3]; // C0 : the fixed point (not yet implemented)
  double stiffness; // stiffness (not yet implemented)
  double theta_min; // angle in degree (0 = 0x axis, 90 = 0y axis)
  double theta_max; // angle in degree (0 = 0x axis, 90 = 0y axis)
  // Remark: theta_max is by default the initial value of theta (??)
  int testCase; 
  int nbPoints;
  double dt;
  double Jaxis;
  int nbTimeSteps;
  int freqPost;
  int protocol;
  string postProcDir;// post processing directory
  string testCaseName; // test case name
  string socket_transport;
  string socket_address;
  string socket_port;
  Param(const string& data_file_name);
};
#endif
