#include "Wall.h"
#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <cstdlib>

const int MSG_VERBOSE=1;
using namespace std;

namespace patch
{
  template < typename T > std::string to_string( const T& n )
  {
    std::ostringstream stm ;
    stm << n ;
    return stm.str() ;
  }
}

Wall::Wall(const string& menu_name):
#ifdef MSG_ZMQ
Param(menu_name),time(0.),zmq_context(1),socket(zmq_context, ZMQ_PAIR)
#else
Param(menu_name),time(0.)
#endif
{
  energy = 0.0;
  
  //------
  // Mesh
  //------
  coor = new double[3*nbPoints];
  disp = new double[3*nbPoints];
  velo = new double[3*nbPoints];
  forceMaster = new double[3*nbPoints];
  
  for(int i=0;i<nbPoints;i++){
    for(int j=0;j<3;j++){
	  coor[3*i + j] = A0[j] + i* (A1[j]-A0[j])/(nbPoints-1); 	
      disp[3*i + j] = 0.;
      velo[3*i + j] = 0.;
    }
  }
  //-------
  // Case file
  //-------
  
  caseFileName = postProcDir + "/solid.case";
  geoFileName = postProcDir + "/solid.geo";
  compteur = 0;
  writeCaseFile();
  writeGeoFile();
  
  //-------
  // Valve
  //-------
  fsiStatus = 1;
  
#ifdef MSG_PVM
  masterId = pvm_parent();
  if(masterId==PvmNoParent){
    cerr << "I am a poor lonesome job\n";
    exit(1);
  } else
    if(masterId==0){
      cerr << "PVM is not ok\n";
      exit(1);
    }
  cout << "I am the slave " << pvm_mytid()<< endl;
  cout << "My master is " << masterId << endl;
#endif
  
#ifdef MSG_ZMQ
  string socket_endpoint;
  if(socket_port != "")
    socket_endpoint = socket_transport + "://" + socket_address + ":" + socket_port;
  else
    socket_endpoint = socket_transport + "://" + socket_address;
  cout << "[ZeroMQ] Connect to " << socket_endpoint << endl;
  socket.connect(socket_endpoint.c_str());
#endif
  
  sdToMasterFSI();
  
}

void Wall::run()
{
	int iter=0,istep=1;
    updateNumberOfSteps(iter);
    do{
#if defined(MSG_PVM) || defined(MSG_ZMQ)
    rvFromMasterFSI();
#endif
    if(fsiStatus == 1) {
      writeDisplacementFile(iter, time);
      writeRotationFile(iter, time);
      updateCaseFile(time, compteur);
      updateNumberOfSteps(iter);
      // time iteration
      iter++;
      time += dt;
      cout<<"\n============================================================\n";
      cout << " Valve Iteration : " << iter << ", Time : " << time << endl;
      cout << "============================================================\n";
      istep = 1;
    }
    if(fsiStatus == 0){
      // inner iteration
      istep ++;
    }
    if(fsiStatus == 0 || fsiStatus == 1){
      cout << "\n*** inner iteration : " << istep << endl;      
    }
#if defined(MSG_PVM) || defined(MSG_ZMQ)
    sdToMasterFSI();
#endif
  }
#if defined(MSG_PVM) || defined(MSG_ZMQ)
  while (1); // the master decides
#else
  while(iter <= nbTimeSteps);
#endif
}


void Wall::writeCaseFile() const {
  
  ofstream f;
  f.open(caseFileName.c_str());
  f << "FORMAT" << endl;
  f << "type: ensight" << endl;
  f << "GEOMETRY" << endl;
  f << "model: 1 solid.geo" << endl;
  f << "VARIABLE" << endl;
  f << "vector per node: 1 displacement displacement.*****.vct" << endl;
  f << "scalar per node: 1 rotation rotation.*****.scl" << endl;
  f << "TIME" << endl;
  f << "time set: 1" << endl;
  f << "number of steps:               " << endl;
  f << "filename start number: 0" << endl;
  f << "filename increment: 1" << endl;
  f << "time values:" << endl;
  
  f.close();
  
}

void Wall::writeGeoFile() const {
  
  ofstream f;
  
  f.open(geoFileName.c_str());
  f << "Geometry file" << endl;
  f << "Geometry file" << endl;
  f << "node id assign" << endl;
  f << "element id assign" << endl;
  f << "coordinates" << endl;
  f << "      " << nbPoints << endl;
  
  f.precision(5);
  
  for (int i = 0; i < nbPoints; ++i) {
    for (int j = 0; j < 3; ++j) {
      if (coor[3*i + j] >= 0) {
        f << scientific << " " << coor[3*i + j];
      }
      else {
        f << scientific << coor[3*i + j];
      }
    }
    f << endl;
  }
  
  f << "part       1" << endl;
  f << "domain" << endl;
  f << "bar2" << endl;
  f << "      " << nbPoints-1 << endl;
  
  for (int i = 1; i <= nbPoints-1; ++i) {
    f << "      ";
    if(i < 10) {
      f << " " << i;
    }
    else {
      f << i;
    }
    f << "      ";
    if(i < 9) {
      f << " " << i+1;
    }
    else {
      f << i+1;
    }
    f << endl;
  }
  f << "part       2" << endl;
  f << "fixedPoint" << endl;
  f << "point" << endl;
  f << "       1" << endl;
  f << "       1" << endl;
  f << "part       3" << endl;
  f << "freePoint" << endl;
  f << "point" << endl;
  f << "       1" << endl;
  if (nbPoints < 10) {
    f << "       " << nbPoints << endl;
  }
  else {
    f << "      " << nbPoints << endl;
  }
  f.close();
}

void Wall::writeDisplacementFile(int iter, double time) const {
  
  ofstream f;
  string displacementFile = postProcDir + "/";
  
  if(iter < 10) {
    displacementFile += "displacement.0000" + patch::to_string(iter) + ".vct";
  }
  else if(iter < 100) {
    displacementFile += "displacement.000" + patch::to_string(iter) + ".vct";
  }
  else if(iter < 1000) {
    displacementFile += "displacement.00" + patch::to_string(iter) + ".vct";
  }
  else if(iter < 10000) {
    displacementFile += "displacement.0" + patch::to_string(iter) + ".vct";
  }
  else{
    displacementFile += "displacement." + patch::to_string(iter) + ".vct";
  }
  
  f.open(displacementFile.c_str());
  
  f << "Vector per node" << endl;
  
  int compteur = 0;
  
  f.precision(5);
  
  for (int i = 0; i < nbPoints; ++i) {
    for (int j = 0; j < 3; ++j) {
      if(disp[3*i+j] >= 0) {
        f << scientific << " " << disp[3*i+j];
      }
      else {
        f << scientific << disp[3*i+j];
      }
    }
    
    ++compteur;
    
    if (compteur == 2) {
      f << endl;
      compteur = 0;
    }
    else {
      
    }
    
  }
  
  f.close();
  
}

void Wall::writeRotationFile(int iter, double time) const {
  
  ofstream f;
  string rotationFile = postProcDir + "/";
  
  if(iter < 10) {
    rotationFile += "rotation.0000" + patch::to_string(iter) + ".scl";
  }
  else if(iter < 100) {
    rotationFile += "rotation.000" + patch::to_string(iter) + ".scl";
  }
  else if(iter < 1000) {
    rotationFile += "rotation.00" + patch::to_string(iter) + ".scl";
  }
  else if(iter < 10000) {
    rotationFile += "rotation.0" + patch::to_string(iter) + ".scl";
  }
  else{
    rotationFile += "rotation." + patch::to_string(iter) + ".scl";
  }
  
  f.open(rotationFile.c_str());
  
  f << "Scalar per node" << endl;
  
  int compteur = 0;
  double rotation = 0.;
  
  f.precision(5);
  
  for (int i = 0; i < nbPoints; ++i) {
    
    if (rotation >= 0.) {
      f << scientific << " " << rotation;
    }
    else {
      f << scientific << rotation;
    }
    
    ++compteur;
    
    if (compteur == 6) {
      f << endl;
      compteur = 0;
    }
    else {
      
    }
    
  }
  
  f.close();
  
}


void Wall::updateCaseFile(double time, int& compteur) const {
  
  ofstream f;
  f.open(caseFileName.c_str(), ios::app);
  
  f << fixed;
  f << setprecision(3);
  
  f << "        " << time;
  ++compteur;
  
  if(compteur == 5) {
    f << endl;
    compteur = 0;
  }
  
  f.close();
  
}

void Wall::updateNumberOfSteps(int iter) const {
  
  ofstream f;
  f.open(caseFileName.c_str(), ios::out|ios::in);
  
  f.seekp(194); // position into the file
  
  f << iter; // write a string of size 4
  
  f.close();
  
}



void Wall::sdToMasterFSI()
{
  int ntdls = 3*nbPoints;
  cout << "---> Valve sends to master " << endl;
  if(fsiStatus == 2){
    cout << "fsiStatus = 2 not yet implemented" << endl << flush;
    exit(1);
  } 
  
  switch(protocol){
    case 1:
    {
#ifdef MSG_PVM
	  cout << "case1" << endl;
      pvm_initsend(PvmDataDefault);
      cout << "pvm initsend" << endl;
      pvm_pkint(&ntdls,1,1);
      cout << "ntdls" << endl;
      pvm_pkdouble(disp,ntdls,1);
      cout << "disp" << endl;
      pvm_pkdouble(velo,ntdls,1);
      cout << "velo" << endl;
      pvm_send(masterId,100);
#endif
#ifdef MSG_ZMQ
      zmq::message_t msg1(1*sizeof(int));
      memcpy(msg1.data(),&ntdls,1*sizeof(int));
      socket.send(msg1);
      //
      zmq::message_t msg2(ntdls*sizeof(double));
      memcpy(msg2.data(),disp,ntdls*sizeof(double));
      socket.send(msg2);
      //
      zmq::message_t msg3(ntdls*sizeof(double));
      memcpy(msg3.data(),velo,ntdls*sizeof(double));
      socket.send(msg3);
#endif
      break;
    }
    case 11:
    {
#ifdef MSG_PVM
      pvm_initsend(PvmDataDefault);
      pvm_pkint(&ntdls,1,1);
      pvm_pkdouble(disp,ntdls,1);
      pvm_pkdouble(velo,ntdls,1);
      pvm_pkdouble(&energy,1,1);
      pvm_send(masterId,1100);
#endif
#ifdef MSG_ZMQ
      zmq::message_t msg1(1*sizeof(int));
      memcpy(msg1.data(),&ntdls,1*sizeof(int));
      socket.send(msg1);
      //
      zmq::message_t msg2(ntdls*sizeof(double));
      memcpy(msg2.data(),disp,ntdls*sizeof(double));
      socket.send(msg2);
      //
      zmq::message_t msg3(ntdls*sizeof(double));
      memcpy(msg3.data(),velo,ntdls*sizeof(double));
      socket.send(msg3);
      //
      zmq::message_t msg4(1*sizeof(double));
      memcpy(msg4.data(),&energy,1*sizeof(double));
      socket.send(msg4);
#endif
      break;
    }
    default:
      cout << "Error in " << __FILE__ << " line " << __LINE__ << ": unknown protocol " << protocol << endl;
      exit(1);
  }
}

void Wall::rvFromMasterFSI()
{
  cout << "<--- Structure receives from master\n";
  int ntdls;
#ifdef MSG_PVM
  pvm_recv(masterId,130);
  pvm_upkint(&fsiStatus,1,1);
  if(fsiStatus!=-1){
    pvm_upkdouble(&dt,1,1);
    pvm_upkint(&ntdls,1,1);
    if(ntdls != 3*nbPoints){
      cout << "rvFromMasterFSI: fatal error : " << ntdls
      << " != " << 3*nbPoints << endl;
      exit(1);
    }
    pvm_upkdouble(forceMaster,3*nbPoints,1);
  }
#endif
#ifdef MSG_ZMQ
  zmq::message_t msg1;
  socket.recv(&msg1);
  memcpy(&fsiStatus,msg1.data(),1*sizeof(int));
  //
  if(fsiStatus!=-1){
    zmq::message_t msg2;
    socket.recv(&msg2);
    memcpy(&dt,msg2.data(),1*sizeof(double));
    //
    zmq::message_t msg3;
    socket.recv(&msg3);
    memcpy(&ntdls,msg3.data(),1*sizeof(int));
    //
    if(ntdls != 3*nbPoints){
      cout << "rvFromMasterFSI: fatal error : " << ntdls
      << " != " << 3*nbPoints << endl;
      exit(1);
    }
    zmq::message_t msg4;
    socket.recv(&msg4);
    memcpy(forceMaster,msg4.data(),3*nbPoints*sizeof(double));
  }
#endif
  if(fsiStatus == 2){
    cout << "fsiStatus = 2 not yet implemented\n" << flush;
    exit(1);
  } 
  
  if(MSG_VERBOSE){
    cout << "\t status: " << fsiStatus << endl;
    if(fsiStatus!=-1){
      cout << "\t dt=" << dt << endl;
      cout << "\t force: ";
      cout << endl;
    }
  }
}
