#include <iostream>
#include "GetPot.h"
#include "rigidValve.h"

int main(int argc,char** argv)
{
/*  GetPot command_line(argc,argv);
  const char* data_file_name = command_line.follow("data", 2, "-f","--file");*/
  string data_file_name;
  if(argc==1) data_file_name = "data";
  else data_file_name = argv[2];
  RigidValve pb(data_file_name);
  pb.run();
  cout << "Normal end of RigidValve" << endl;
  return 0;
}
