#ifndef RIGIDVALVE
#define RIGIDVALVE
#include "param.h"
#include <string>

#ifdef MSG_PVM
#include "pvm3.h"
#endif

#ifdef MSG_ZMQ
#include "zmq.hpp"
#endif

using namespace std;

class RigidValve:
  public Param
{
  double length;
  double* coor;
  double* coorInit;
  double* disp;
  double* velo;
  double* forceMaster; // forceMaster is the total force if protocol = 1, and only the external force if protocol = 11
  double* contactForce; // only used for protocol = 11

  string caseFileName;
  string geoFileName;
  int compteur;

  double theta;
  double thetap;
  double theta_n;
  double thetap_n;
  int fsiStatus;
  double angularMomentum;
  double angularMomentumContact;
  double energy;
  double time;
#ifdef MSG_PVM
  int masterId;
#endif
#ifdef MSG_ZMQ
  zmq::context_t zmq_context;
  zmq::socket_t socket;
#endif

public:
  RigidValve(const string& menu_name);
  void writeCaseFile() const;
  void writeGeoFile() const;
  void writeDisplacementFile(int iter, double time, double theta) const;
  void writeRotationFile(int iter, double time, double theta) const;
  void updateCaseFile(double time, int& compteur) const;
  void updateNumberOfSteps(int iter) const;


  void run();
  void updateCoor();
  void sdToMasterFSI();
  void rvFromMasterFSI();
};

#endif
