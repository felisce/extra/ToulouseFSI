#include "rigidValve.h"
#include <iostream>
#include <fstream>
#include <sstream>
#include <math.h>
#include <iomanip>
#include <cstdlib>

const int MSG_VERBOSE=1;
//const double epsilon=1e-6;

using namespace std;

namespace patch
{
  template < typename T > std::string to_string( const T& n )
  {
    std::ostringstream stm ;
    stm << n ;
    return stm.str() ;
  }
}

RigidValve::RigidValve(const string& menu_name):
#ifdef MSG_ZMQ
Param(menu_name),time(0.),zmq_context(1),socket(zmq_context, ZMQ_PAIR)
#else
Param(menu_name),time(0.)
#endif
{
  //------
  // Mesh
  //------
  length = sqrt( (A1[0]-A0[0])*(A1[0]-A0[0]) +
                (A1[1]-A0[1])*(A1[1]-A0[1]) +
                (A1[2]-A0[2])*(A1[2]-A0[2]) );
  cout << "length = " << length << endl;
  coor = new double[3*nbPoints];
  coorInit = new double[3*nbPoints];
  disp = new double[3*nbPoints];
  velo = new double[3*nbPoints];
  forceMaster = new double[3*nbPoints];
  contactForce = new double[3*nbPoints];

  for(int i=0;i<nbPoints;i++){
    for(int j=0;j<3;j++){
      coorInit[3*i + j] = A0[j] + i* (A1[j]-A0[j])/(nbPoints-1);
      coor[3*i + j] = coorInit[3*i + j];
      disp[3*i + j] = 0.;
      velo[3*i + j] = 0.;
    }
  }
  //-------
  // Case file
  //-------
  
  caseFileName = postProcDir + "/solid.case";
  geoFileName = postProcDir + "/solid.geo";
  compteur = 0;
  writeCaseFile();
  writeGeoFile();
  
  //-------
  // Valve
  //-------
  fsiStatus = 1;
  
#ifdef MSG_PVM
  masterId = pvm_parent();
  if(masterId==PvmNoParent){
    cerr << "I am a poor lonesome job\n";
    exit(1);
  } else
    if(masterId==0){
      cerr << "PVM is not ok\n";
      exit(1);
    }
  cout << "I am the slave " << pvm_mytid()<< endl;
  cout << "My master is " << masterId << endl;
#endif
  
#ifdef MSG_ZMQ
  string socket_endpoint;
  if(socket_port != "")
    socket_endpoint = socket_transport + "://" + socket_address + ":" + socket_port;
  else
    socket_endpoint = socket_transport + "://" + socket_address;
  cout << "[ZeroMQ] Connect to " << socket_endpoint << endl;
  socket.connect(socket_endpoint.c_str());
#endif
  
  sdToMasterFSI();
  
}

void RigidValve::run()
{
  bool flagpost=false;
  int iter=0,istep=1;
  double theta_init = atan((A1[1]-A0[1])/(A1[0]-A0[0]));
  cout << "maxTheta = Theta_init = " << theta_init << endl;
  static double thetaold = theta_init;
  theta= theta_init;
  thetap=0;
  updateNumberOfSteps(iter);
  do{
#if defined(MSG_PVM) || defined(MSG_ZMQ)
    rvFromMasterFSI();
#else
    angularMomentum   = 3.; //test
#endif
    if(fsiStatus == -1) break;
    if(fsiStatus == 1) {
      writeDisplacementFile(iter, time, theta);
      writeRotationFile(iter, time, theta);
      updateCaseFile(time, compteur);
      updateNumberOfSteps(iter);
      // time iteration
      iter++;
      time += dt;
      cout<<"\n============================================================\n";
      cout << " Valve Iteration : " << iter << ", Time : " << time << endl;
      cout << "============================================================\n";
      istep = 1;
      theta_n  = theta;
      thetap_n = thetap;
      thetaold = theta;
    }
    if(fsiStatus == 0){
      // inner iteration
      istep ++;
    }
    if(fsiStatus == 0 || fsiStatus == 1){
      cout << "\n*** inner iteration : " << istep << endl;
      // time integration
      /*
       thetap = thetap_n*Jaxis/(Jaxis + dt*visc) + dt/(Jaxis + dt*visc)*(angularMomentum+angularMomentumContact);
       theta  = theta_n + dt/2*( 1 + Jaxis/(Jaxis+visc*dt))*thetap_n + dt*dt/(2*(Jaxis+dt*visc))*(angularMomentum+angularMomentumContact);
      */
      thetap = thetap_n*Jaxis/(Jaxis + dt*visc) + dt/(Jaxis + dt*visc)*angularMomentum;
      theta  = theta_n + dt/2*( 1 + Jaxis/(Jaxis+visc*dt))*thetap_n + dt*dt/(2*(Jaxis+dt*visc))*angularMomentum;
      // valve blocking to simulate a stenotic valve.
      if ( (theta <= (M_PI*theta_min)/180.) && (thetap <= 0.) ){
        theta = thetaold;
        thetap = 0.;
      }
      if ( (theta >= (M_PI*theta_max)/180.) && (thetap >= 0.) ){
        theta = thetaold;
        thetap = 0.;
      }
      updateCoor();
      flagpost=true;
    }
#if defined(MSG_PVM) || defined(MSG_ZMQ)
    sdToMasterFSI();
#endif
  }
#if defined(MSG_PVM) || defined(MSG_ZMQ)
  while (1); // the master decides
#else
  while(iter <= nbTimeSteps);
#endif
}

void RigidValve::updateCoor()
{
  double c[2];
  double cPi = 4.0*atan(1.0);
  c[0] = cos(theta);
  c[1] = sin(theta);
  // coordinates
  for(int i=0;i<nbPoints-1;i++){ // tip point excluded
    for(int j=0;j<2;j++){
      /*
       coor[3*i + j] = A0[j] + i * length/(nbPoints-1)*c[j];
       disp[3*i + j] = coor[3*i+j] - coorInit[3*i+j];
       */
      coor[3*i + j] = A0[j] + i * length/(nbPoints-1)*c[j] + contactForce[3*i+j];
      disp[3*i + j] = coor[3*i+j] - coorInit[3*i+j];
    }
    velo[3*i + 0] = -thetap * (coor[3*i+1] - A0[1]) ; // TO BE CORRECTED !
    velo[3*i + 1] =  thetap * (coor[3*i+0] - A0[0]) ;
  }
  // tip point
  for(int j=0;j<2;j++){
    coor[3*(nbPoints-1) + j] = A0[j] + length*c[j] + contactForce[3*(nbPoints-1)+j];
    disp[3*(nbPoints-1) + j] = coor[3*(nbPoints-1)+j] - coorInit[3*(nbPoints-1)+j];
  }
  velo[3*(nbPoints-1) + 0] = -thetap * (coor[3*(nbPoints-1)+1] - A0[1]) ; // TO BE CORRECTED !
  velo[3*(nbPoints-1) + 1] =  thetap * (coor[3*(nbPoints-1)+0] - A0[0]) ;
  // --
  
  energy = 0;
  double x,y;
  for(int i=0;i<nbPoints;i++){
    x = coor[3*i + 0];
    y = coor[3*i + 1];
    energy -= 0.5 * ( x*x + y*y);
    //    if(i==nbPoints-1) energy += 0.5/epsilon * ( (x-A0[0])*(x-A0[0]) +  (y-A0[1])*(y-A0[1]) - length*length );
  }
  x=coor[3*(nbPoints-1) + 0] - coor[3*0 + 0];
  y=coor[3*(nbPoints-1) + 1] - coor[3*0 + 1];
  double true_length = sqrt( x*x + y*y );

  cout << "theta   = " << theta << endl;
  cout << "thetap  = " << thetap << endl;
  cout << "true length  = " << true_length << endl;
  cout << "energy  = " << energy << endl;
}

void RigidValve::writeCaseFile() const {
  
  ofstream f;
  f.open(caseFileName.c_str());
  f << "FORMAT" << endl;
  f << "type: ensight" << endl;
  f << "GEOMETRY" << endl;
  f << "model: 1 solid.geo" << endl;
  f << "VARIABLE" << endl;
  f << "vector per node: 1 displacement displacement.*****.vct" << endl;
  f << "scalar per node: 1 rotation rotation.*****.scl" << endl;
  f << "TIME" << endl;
  f << "time set: 1" << endl;
  f << "number of steps:               " << endl;
  f << "filename start number: 0" << endl;
  f << "filename increment: 1" << endl;
  f << "time values:" << endl;
  
  f.close();
  
}

void RigidValve::writeGeoFile() const {
  
  ofstream f;
  
  f.open(geoFileName.c_str());
  f << "Geometry file" << endl;
  f << "Geometry file" << endl;
  f << "node id assign" << endl;
  f << "element id assign" << endl;
  f << "coordinates" << endl;
  f << "      " << nbPoints << endl;
  
  f.precision(5);
  
  for (int i = 0; i < nbPoints; ++i) {
    for (int j = 0; j < 3; ++j) {
      if (coor[3*i + j] >= 0) {
        f << scientific << " " << coor[3*i + j];
      }
      else {
        f << scientific << coor[3*i + j];
      }
    }
    f << endl;
  }
  
  f << "part       1" << endl;
  f << "domain" << endl;
  f << "bar2" << endl;
  f << "      " << nbPoints-1 << endl;
  
  for (int i = 1; i <= nbPoints-1; ++i) {
    f << "      ";
    if(i < 10) {
      f << " " << i;
    }
    else {
      f << i;
    }
    f << "      ";
    if(i < 9) {
      f << " " << i+1;
    }
    else {
      f << i+1;
    }
    f << endl;
  }
  f << "part       2" << endl;
  f << "fixedPoint" << endl;
  f << "point" << endl;
  f << "       1" << endl;
  f << "       1" << endl;
  f << "part       3" << endl;
  f << "freePoint" << endl;
  f << "point" << endl;
  f << "       1" << endl;
  if (nbPoints < 10) {
    f << "       " << nbPoints << endl;
  }
  else {
    f << "      " << nbPoints << endl;
  }
  f.close();
}

void RigidValve::writeDisplacementFile(int iter, double time, double theta) const {
  
  ofstream f;
  string displacementFile = postProcDir + "/";
  
  if(iter < 10) {
    displacementFile += "displacement.0000" + patch::to_string(iter) + ".vct";
  }
  else if(iter < 100) {
    displacementFile += "displacement.000" + patch::to_string(iter) + ".vct";
  }
  else if(iter < 1000) {
    displacementFile += "displacement.00" + patch::to_string(iter) + ".vct";
  }
  else if(iter < 10000) {
    displacementFile += "displacement.0" + patch::to_string(iter) + ".vct";
  }
  else{
    displacementFile += "displacement." + patch::to_string(iter) + ".vct";
  }
  
  f.open(displacementFile.c_str());
  
  f << "Vector per node" << endl;
  
  int compteur = 0;
  
  f.precision(5);
  
  for (int i = 0; i < nbPoints; ++i) {
    for (int j = 0; j < 3; ++j) {
      if(disp[3*i+j] >= 0) {
        f << scientific << " " << disp[3*i+j];
      }
      else {
        f << scientific << disp[3*i+j];
      }
    }
    
    ++compteur;
    
    if (compteur == 2) {
      f << endl;
      compteur = 0;
    }
    else {
      
    }
    
  }
  
  f.close();
  
}

void RigidValve::writeRotationFile(int iter, double time, double theta) const {
  
  ofstream f;
  string rotationFile = postProcDir + "/";
  
  if(iter < 10) {
    rotationFile += "rotation.0000" + patch::to_string(iter) + ".scl";
  }
  else if(iter < 100) {
    rotationFile += "rotation.000" + patch::to_string(iter) + ".scl";
  }
  else if(iter < 1000) {
    rotationFile += "rotation.00" + patch::to_string(iter) + ".scl";
  }
  else if(iter < 10000) {
    rotationFile += "rotation.0" + patch::to_string(iter) + ".scl";
  }
  else{
    rotationFile += "rotation." + patch::to_string(iter) + ".scl";
  }
  
  f.open(rotationFile.c_str());
  
  f << "Scalar per node" << endl;
  
  int compteur = 0;
  double theta_init = atan((A1[1]-A0[1])/(A1[0]-A0[0]));
  double rotation = 0.;
  
  f.precision(5);
  
  for (int i = 0; i < nbPoints; ++i) {
    
    rotation = theta - theta_init;
    
    if (rotation >= 0.) {
      f << scientific << " " << rotation;
    }
    else {
      f << scientific << rotation;
    }
    
    ++compteur;
    
    if (compteur == 6) {
      f << endl;
      compteur = 0;
    }
    else {
      
    }
    
  }
  
  f.close();
  
}


void RigidValve::updateCaseFile(double time, int& compteur) const {
  
  ofstream f;
  f.open(caseFileName.c_str(), ios::app);
  
  f << fixed;
  f << setprecision(3);
  
  f << "        " << time;
  ++compteur;
  
  if(compteur == 5) {
    f << endl;
    compteur = 0;
  }
  
  f.close();
  
}

void RigidValve::updateNumberOfSteps(int iter) const {
  
  ofstream f;
  f.open(caseFileName.c_str(), ios::out|ios::in);
  
  f.seekp(194); // position into the file
  
  f << iter; // write a string of size 4
  
  f.close();
  
}



void RigidValve::sdToMasterFSI()
{
  int ntdls = 3*nbPoints;
  cout << "---> Valve sends to master " << endl;
  if(fsiStatus == 2){
    cout << "fsiStatus = 2 not yet implemented" << endl << flush;
    exit(1);
  } else {
  }
  if(MSG_VERBOSE){
    cout <<"\t disp: ";
    for(int i=0;i<nbPoints;i++)
      cout << disp[3*i] << " " << disp[3*i+1] << " " << disp[3*i+2] << endl;
    cout << endl;
    cout <<"\t velo: ";
    for(int i=0;i<nbPoints;i++) cout << velo[3*i  ] << " "
      << velo[3*i+1] << " "
      << velo[3*i+2] << endl;
    cout << endl;
  }
#ifdef MSG_PVM
  pvm_initsend(PvmDataDefault);
  pvm_pkint(&ntdls,1,1);
  pvm_pkdouble(disp,ntdls,1);
  pvm_pkdouble(velo,ntdls,1);
  if(protocol==1) pvm_send(masterId,100);
  if(protocol==11){
    pvm_pkdouble(&energy,1,1);
    pvm_send(masterId,1100);
  }
#endif
#ifdef MSG_ZMQ
  zmq::message_t msg1(1*sizeof(int));
  memcpy(msg1.data(),&ntdls,1*sizeof(int));
  socket.send(msg1);
  //
  zmq::message_t msg2(ntdls*sizeof(double));
  memcpy(msg2.data(),disp,ntdls*sizeof(double));
  socket.send(msg2);
  //
  zmq::message_t msg3(ntdls*sizeof(double));
  memcpy(msg3.data(),velo,ntdls*sizeof(double));
  socket.send(msg3);
  //
  if(protocol==11){
    zmq::message_t msg4(1*sizeof(double));
    memcpy(msg4.data(),&energy,1*sizeof(double));
    socket.send(msg4);
  }
#endif
}

void RigidValve::rvFromMasterFSI()
{
  cout << "<--- Structure receives from master\n";
  int ntdls;
#ifdef MSG_PVM
  pvm_recv(masterId,130);
  pvm_upkint(&fsiStatus,1,1);
  if(fsiStatus!=-1){
    pvm_upkdouble(&dt,1,1);
    pvm_upkint(&ntdls,1,1);
    if(ntdls != 3*nbPoints){
      cout << "rvFromMasterFSI: fatal error : " << ntdls
      << " != " << 3*nbPoints << endl;
      exit(1);
    }
    pvm_upkdouble(forceMaster,3*nbPoints,1);
    if(protocol == 11 ) pvm_upkdouble(contactForce,3*nbPoints,1);
  }
#endif
#ifdef MSG_ZMQ
  zmq::message_t msg1;
  socket.recv(&msg1);
  memcpy(&fsiStatus,msg1.data(),1*sizeof(int));
  //
  if(fsiStatus!=-1){
    zmq::message_t msg2;
    socket.recv(&msg2);
    memcpy(&dt,msg2.data(),1*sizeof(double));
    //
    zmq::message_t msg3;
    socket.recv(&msg3);
    memcpy(&ntdls,msg3.data(),1*sizeof(int));
    //
    if(ntdls != 3*nbPoints){
      cout << "rvFromMasterFSI: fatal error : " << ntdls
      << " != " << 3*nbPoints << endl;
      exit(1);
    }
    zmq::message_t msg4;
    socket.recv(&msg4);
    memcpy(forceMaster,msg4.data(),3*nbPoints*sizeof(double));
    if(protocol == 11 ){
      zmq::message_t msg5;
      socket.recv(&msg5);
      memcpy(contactForce,msg5.data(),3*nbPoints*sizeof(double));
    }
  }
#endif
  if(fsiStatus == 2){
    cout << "fsiStatus = 2 not yet implemented\n" << flush;
    exit(1);
  } else {
    angularMomentum=0.;
    angularMomentumContact=0.;
    for(int i=0;i<nbPoints;i++){
      angularMomentum += (coor[3*i+0] - A0[0]) * forceMaster[3*i+1]
      - (coor[3*i+1] - A0[1]) * forceMaster[3*i+0];
      if(protocol==11){
        angularMomentumContact += (coor[3*i+0] - A0[0]) * contactForce[3*i+1]
        - (coor[3*i+1] - A0[1]) * contactForce[3*i+0];
      }
    }
    if(protocol==11){
      cout << "Angular momentum external =" << angularMomentum << endl;
      cout << "Angular momentum contact =" << angularMomentumContact << endl;
    } else cout << "Total Angular momentum =" << angularMomentum << endl;
  }
  
  if(MSG_VERBOSE){
    cout << "\t status: " << fsiStatus << endl;
    if(fsiStatus!=-1){
      cout << "\t dt=" << dt << endl;
      cout << "\t force: ";
      for(int i=0;i<nbPoints;i++)
        cout << forceMaster[3*i] << " " << forceMaster[3*i+1] << " " << forceMaster[3*i+2] << endl;
      cout << endl;
    }
  }
}
