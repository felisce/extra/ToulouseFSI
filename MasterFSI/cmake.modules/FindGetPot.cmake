# Tool to parse the command line and configuration files.  Powerful command line and configuration file parsing for C++, Python, Ruby and Java (others to come). This tool provides many features, such as separate treatment for options, variables, and flags, unrecognized object detection, prefixes and much more

# Auxiliar definition of GETPOT_INCLUDE_DIR if GETPOT_ROOT_DIR is defined
IF(NOT DEFINED GETPOT_INCLUDE_DIR)
    SET(GETPOT_INCLUDE_DIR "${CMAKE_CURRENT_SOURCE_DIR}/external/getpot-cpp")
    MESSAGE(STATUS "Setting GETPOT_INCLUDE_DIR as ${CMAKE_CURRENT_SOURCE_DIR}/external/getpot-cpp")
ENDIF(NOT DEFINED GETPOT_INCLUDE_DIR)

FIND_PATH(GETPOT_INCLUDE_DIR GetPot)
IF(NOT GETPOT_INCLUDE_DIR)
    MESSAGE(FATAL_ERROR "GetPot not found. Please define manually GETPOT_INCLUDE_DIR or GETPOT_ROOT_DIR")
ENDIF(NOT GETPOT_INCLUDE_DIR)
