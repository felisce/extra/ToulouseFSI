# Finding and including BOOST library (version should not matter anymore)
IF(DEFINED ENV{BOOST_ROOT})
    SET(BOOST_ROOT $ENV{BOOST_ROOT})
ENDIF(DEFINED ENV{BOOST_ROOT})

IF(BOOST_ROOT_DIR)
    FIND_PATH(BOOST_INCLUDE_DIR boost/numeric/ublas/vector.hpp
      "${BOOST_ROOT_DIR}"
    )
    IF(NOT BOOST_INCLUDE_DIR)
        MESSAGE(FATAL_ERROR "Finding Boost failed, please try to set properly the var BOOST_ROOT_DIR")
    ELSE(NOT BOOST_INCLUDE_DIR)
        SET(BOOST_LIBRARIES "")
        SET(BOOST_FOUND TRUE)
    ENDIF(NOT BOOST_INCLUDE_DIR)
ELSE(BOOST_ROOT_DIR)
    IF (BOOST_INCLUDE_DIR AND BOOST_LIBRARIES)
        SET(BOOST_FOUND TRUE)
    ELSE (BOOST_INCLUDE_DIR AND BOOST_LIBRARIES)
        MESSAGE(STATUS "Boost not found manually. Trying again with CMake macro search.")

        FIND_PACKAGE(Boost)

        IF(BOOST_FOUND)
            SET(BOOST_USE_MULTITHREADED ON)
            SET(BOOST_REALPATH ON)

            IF (Boost_INCLUDE_DIR)
                SET(BOOST_INCLUDE_DIR ${Boost_INCLUDE_DIR})
            ELSE (Boost_INCLUDE_DIR)
                FIND_PATH(BOOST_INCLUDE_DIR boost/numeric/ublas/vector.hpp
                  "${OS_INCLUDE_DIRECTORIES}"
                )
            ENDIF (Boost_INCLUDE_DIR)
        ELSE(BOOST_FOUND)
            FIND_PATH(BOOST_INCLUDE_DIR boost/numeric/ublas/vector.hpp
            "${OS_INCLUDE_DIRECTORIES}"
            )
            IF(NOT BOOST_INCLUDE_DIR)
                MESSAGE(FATAL_ERROR "Finding Boost failed, please try to set properly the var BOOST_ROOT_DIR")
            ELSE(NOT BOOST_INCLUDE_DIR)
                SET( BOOST_LIBRARIES "")
                SET(MPI_FOUND ON)
            ENDIF(NOT BOOST_INCLUDE_DIR)
        ENDIF(BOOST_FOUND)
    ENDIF (BOOST_INCLUDE_DIR AND BOOST_LIBRARIES)
ENDIF(BOOST_ROOT_DIR)

INCLUDE_DIRECTORIES(SYSTEM ${BOOST_INCLUDE_DIR})
MESSAGE(STATUS "Boost found. Includes found at ${BOOST_INCLUDE_DIR} and libraries at ${BOOST_LIBRARIES}")
