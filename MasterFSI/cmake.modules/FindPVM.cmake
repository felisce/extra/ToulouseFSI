#  PVM (Parallel Virtual Machine) is a software system that enables a collection of heterogeneous computers to be used as a coherent and flexible concurrent computational resource. The individual computers may be shared- or local-memory multiprocessors, vector supercomputers, specialized graphics engines, or scalar workstations, that may be interconnected by a variety of networks, such as ethernet, FDDI, etc. PVM support software executes on each machine in a user-configurable pool, and presents a unified, general, and powerful computational environment of concurrent applications. User programs written in C or Fortran are provided access to PVM through the use of calls to PVM library routines for functions such as process initiation, message transmission and reception, and synchronization via barriers or rendezvous. Users may optionally control the execution location of specific application components. The PVM system transparently handles message routing, data conversion for incompatible architectures, and other tasks that are necessary for operation in a heterogeneous, network environment.
#
# PVM is particularly effective for heterogeneous applications that exploit specific strengths of individual machines on a network. As a loosely coupled concurrent supercomputer environment, PVM is a viable scientific computing platform. The PVM system has been used for applications such as molecular dynamics simulations, superconductivity studies, distributed fractal computations, matrix algorithms, and in the classroom as the basis for teaching concurrent computing.
#
# See https://www.netlib.org/pvm3/

# If LIBPVM_ROOT_DIR is defined but LIBPVM_DIR is defined we define here
IF(NOT DEFINED LIBPVM_ROOT_DIR)
    IF(DEFINED LIBPVM_DIR)
        MESSAGE(STATUS "Setting LIBPVM_ROOT_DIR as ${LIBPVM_DIR}")
        SET(LIBPVM_ROOT_DIR ${LIBPVM_DIR})
    ENDIF(DEFINED LIBPVM_DIR)
ENDIF(NOT DEFINED LIBPVM_ROOT_DIR)

IF (DEFINED PVM_INCLUDE)
    SET(LIBPVM_INCLUDE_DIR ${PVM_INCLUDE})
ELSE (DEFINED PVM_INCLUDE)
    FIND_PATH(LIBPVM_INCLUDE_DIR pvm3.h
        ${OS_INCLUDE_DIRECTORIES}
        "${LIBPVM_ROOT_DIR}/include"
    )
ENDIF (DEFINED PVM_INCLUDE)

IF (DEFINED PVM_LIBRARY)
    SET(LIBPVM_LIBRARY ${PVM_LIBRARY})
ELSE (DEFINED PVM_LIBRARY)
    FIND_LIBRARY(LIBPVM_LIBRARY pvm3
        ${OS_LIB_DIRECTORIES}
        "${LIBPVM_ROOT_DIR}/lib"
    )
ENDIF (DEFINED PVM_LIBRARY)

IF(LIBPVM_INCLUDE_DIR AND LIBPVM_LIBRARY)
    SET( LIBPVM_LIBRARIES ${LIBPVM_LIBRARY})
    SET( LIBPVM_FOUND ON )
    MESSAGE(STATUS "LIBPVM found. Includes found at ${LIBPVM_INCLUDE_DIR} and libraries at ${LIBPVM_LIBRARIES}")
ELSE(LIBPVM_INCLUDE_DIR AND LIBPVM_LIBRARY)
    MESSAGE(FATAL_ERROR "Finding LIBPVM failed, please try to set the var LIBPVM_ROOT_DIR")
ENDIF(LIBPVM_INCLUDE_DIR AND LIBPVM_LIBRARY)
