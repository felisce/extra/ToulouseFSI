# Set CMAKE_PREFIX_PATH
SET(CMAKE_PREFIX_PATH ${PVM_DIR} ${ZEROMQ_DIR})

# Some OS specific definitions
IF(${OS_NAME} MATCHES "Linux")
    SET(OS_INCLUDE_DIRECTORIES
        "/usr/local/include"
        "/usr/include"
        "$ENV{INCLUDE}"
    )
    SET(OS_LIB_DIRECTORIES
        "/usr/local/lib"
        "/usr/lib"
        "/usr/lib/x86_64-linux-gnu"
    )
    IF (DEFINED ENV{LD_LIBRARY_PATH})
        STRING(REPLACE ":" ";" LOCAL_LIBRARIES_DIRECTORIES $ENV{LD_LIBRARY_PATH})
        SET(OS_LIB_DIRECTORIES ${OS_LIB_DIRECTORIES} ${LOCAL_LIBRARIES_DIRECTORIES})
    ENDIF (DEFINED ENV{LD_LIBRARY_PATH})
ELSEIF(${OS_NAME} MATCHES "Darwin")
    SET(OS_INCLUDE_DIRECTORIES
        "/usr/local/include"
        "/usr/include"
    )
    SET(OS_LIB_DIRECTORIES
        "/usr/local/lib"
        "/usr/lib"
        "/usr/local/lib/gcc/10"
        "/usr/local/lib/gcc/9"
        "/usr/local/lib/gcc/8"
        "/usr/local/lib/gcc/7"
        # Add more if necessary
    )
ENDIF(${OS_NAME} MATCHES "Linux")

## Check getpot #TODO: Remove from code and make as an external dependency
#INCLUDE(FindGetPot)

# Check boost
INCLUDE(FindBoostLibrary)

# Check NLopt
IF(NOT DEFINED USE_NLOPT)
    SET(USE_NLOPT OFF)
ENDIF(NOT DEFINED USE_NLOPT)
IF(${USE_NLOPT})
    INCLUDE(FindNLopt)
ENDIF(${USE_NLOPT})

# Check PVM if required
IF(${MSG} STREQUAL "PVM")
    INCLUDE(FindPVM)
    # Add definitions
    ADD_DEFINITIONS( -DMSG_PVM )
ELSEIF(${MSG} STREQUAL "ZMQ")
    INCLUDE(FindZeroMQ)
    # Add definitions
    ADD_DEFINITIONS( -DMSG_ZMQ )
ELSE(${MSG} STREQUAL "PVM")
    MESSAGE(FATAL_ERROR "At least ZeroMQ or PVM must be defined")
ENDIF(${MSG} STREQUAL "PVM")
