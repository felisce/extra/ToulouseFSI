# NLopt is a library for nonlinear local and global optimization, for functions with and without gradient information. It is designed as a simple, unified interface and packaging of several free/open-source nonlinear optimization libraries.
# https://nlopt.readthedocs.io/en/latest/

# If NLOPT_ROOT_DIR is defined but NLOPT_DIR is defined we define here
IF(NOT DEFINED NLOPT_ROOT_DIR)
    IF(DEFINED NLOPT_DIR)
        MESSAGE(STATUS "Setting NLOPT_ROOT_DIR as ${NLOPT_DIR}")
        SET(NLOPT_ROOT_DIR ${NLOPT_DIR})
    ENDIF(DEFINED NLOPT_DIR)
ENDIF(NOT DEFINED NLOPT_ROOT_DIR)

FIND_PATH(NLOPT_INCLUDE_DIR nlopt.h
    ${OS_INCLUDE_DIRECTORIES}
    "${NLOPT_ROOT_DIR}/include"
)

FIND_LIBRARY(NLOPT_LIBRARY nlopt
    ${OS_LIB_DIRECTORIES}
    "${NLOPT_ROOT_DIR}/lib"
)

IF(NLOPT_INCLUDE_DIR AND NLOPT_LIBRARY)
    SET( NLOPT_LIBRARIES ${NLOPT_LIBRARY})
    SET( NLOPT_FOUND ON )
    MESSAGE(STATUS "NLOPT found. Includes found at ${NLOPT_INCLUDE_DIR} and libraries at ${NLOPT_LIBRARIES}")
ELSE(NLOPT_INCLUDE_DIR AND NLOPT_LIBRARY)
    MESSAGE(FATAL_ERROR "Finding NLOPT failed, please try to set the var NLOPT_ROOT_DIR")
ENDIF(NLOPT_INCLUDE_DIR AND NLOPT_LIBRARY)
