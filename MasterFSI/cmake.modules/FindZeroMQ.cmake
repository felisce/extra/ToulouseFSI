# ZeroMQ (also spelled ØMQ, 0MQ or ZMQ) is a high-performance asynchronous messaging library, aimed at use in distributed or concurrent applications. It provides a message queue, but unlike message-oriented middleware, a ZeroMQ system can run without a dedicated message broker. The library's API is designed to resemble Berkeley sockets.
#
# ZeroMQ is developed by a large community of contributors, founded by iMatix, which holds the domain name and trademarks. There are third-party bindings for many popular programming languages.

# If ZEROMQ_ROOT_DIR is defined but ZEROMQ_DIR is defined we define here
IF(NOT DEFINED ZEROMQ_ROOT_DIR)
    IF(DEFINED ZEROMQ_DIR)
        MESSAGE(STATUS "Setting ZEROMQ_ROOT_DIR as ${ZEROMQ_DIR}")
        SET(ZEROMQ_ROOT_DIR ${ZEROMQ_DIR})
    ENDIF(DEFINED ZEROMQ_DIR)
ENDIF(NOT DEFINED ZEROMQ_ROOT_DIR)

FIND_PATH(ZEROMQ_INCLUDE_DIR zmq.h
    ${OS_INCLUDE_DIRECTORIES}
    "${ZEROMQ_ROOT_DIR}/include"
)

FIND_LIBRARY(ZEROMQ_LIBRARY zmq
    ${OS_LIB_DIRECTORIES}
    "${ZEROMQ_ROOT_DIR}/lib"
)

IF(ZEROMQ_INCLUDE_DIR AND ZEROMQ_LIBRARY)
    SET( ZEROMQ_LIBRARIES ${ZEROMQ_LIBRARY})
    SET( ZEROMQ_FOUND ON )
    MESSAGE(STATUS "ZEROMQ found. Includes found at ${ZEROMQ_INCLUDE_DIR} and libraries at ${ZEROMQ_LIBRARIES}")
ELSE(ZEROMQ_INCLUDE_DIR AND ZEROMQ_LIBRARY)
    MESSAGE(FATAL_ERROR "Finding ZEROMQ failed, please try to set the var ZEROMQ_ROOT_DIR")
ENDIF(ZEROMQ_INCLUDE_DIR AND ZEROMQ_LIBRARY)
