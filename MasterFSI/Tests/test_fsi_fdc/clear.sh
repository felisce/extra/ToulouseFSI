#!/bin/bash
# Clear the folder and the result files

rm -rf ./PostS/
rm -rf ./PostF/ 
rm -rf output_master
rm -rf output_fluid
rm -rf output_struct
rm -rf output_multistruct
mkdir ./PostS/
mkdir ./PostF/
