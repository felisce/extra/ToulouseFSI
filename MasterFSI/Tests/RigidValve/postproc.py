#!/usr/bin/env python3

import sys,string
import numpy as np
import matplotlib.pyplot as plt

if(len(sys.argv)==1):
    time_start = 0
elif(len(sys.argv)==2):
    time_start = float(sys.argv[1])
else:
  print("usage: 'postproc.py' to start from iteration 0, or 'postproc.py n' to start from iteration n")
  sys.exit()
 
file_name = "mean_forces_0.dat"
sol = np.loadtxt(file_name,comments='#')
time = sol[:,0]
contact_x = sol[:,1]
contact_y = sol[:,2]
contact_z = sol[:,3]
external_x = sol[:,4]
external_y = sol[:,5]
external_z = sol[:,6]
tip_x = sol[:,7]
tip_y = sol[:,8]
tip_z = sol[:,9]

for i in range(0,len(time)):
    if time[i]>= time_start:
        n_start = i
        break

fig1, (ax1, ax2, ax3, ax4) = plt.subplots(4, 1)

ax1.plot(time[n_start:],contact_x[n_start:],"b",label='contact x')
ax1.plot(time[n_start:],contact_y[n_start:],"r",label='contact y')
ax1.legend()
ax2.plot(time[n_start:],external_x[n_start:],"b",label='external x')
ax2.plot(time[n_start:],external_y[n_start:],"r",label='external y')
ax2.legend()
ax3.plot(time[n_start:],tip_x[n_start:],"b",label='tip x')
ax3.legend()
ax4.plot(time[n_start:],tip_y[n_start:],"r",label='tip y')
ax4.legend()

plt.show()
