#!/usr/bin/env python3
import numpy as np

L = 2./np.sqrt(3.)
n = 22
delta = L/n
theta = np.pi/4.

print("MeshVersionFormatted 1")
print("Dimension")
print("3")
print("Vertices")
print(n+1)
for i in range(0,n+1):
    x = 2 + i * delta * np.cos(theta)
    y = 0 + i * delta * np.sin(theta)
    print((str)(x)+'\t'+(str)(y)+'\t'+'0.000000\t 1')
print("Edges")
print(n)
for i in range(0,n):
    print(str(i+1)+'\t' + str(i+2) + ' 1')
print('END')
 