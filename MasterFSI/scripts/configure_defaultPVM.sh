#!/bin/sh

# # Ensuring that submodules are up-to-date
# git submodule sync --recursive
# git submodule update --init --recursive
# git submodule update --remote

# Set variables
export MASTER_FSICONTACT_SOURCE="${MASTER_FSICONTACT_SOURCE:-"$( cd "$(dirname "$0")" ; pwd -P )"/..}"
export MASTER_FSICONTACT_BUILD="${MASTER_FSICONTACT_SOURCE}/build"

# Set basic configuration
# export MASTER_FSICONTACT_BUILD_TYPE=${MASTER_FSICONTACT_BUILD_TYPE:-"FullDebug"}
export MASTER_FSICONTACT_BUILD_TYPE=${MASTER_FSICONTACT_BUILD_TYPE:-"Release"}

# Add custom definitions
export CUSTOM_DEFINITIONS=""

# Set compiler
if [ -z "$CC" ] ;then
    export CC=gcc
    export CXX=g++
#     export CC=clang
#     export CXX=clang++
fi

# # Compiler flags
# if [ "$CC" = gcc ] ; then
#     export MASTER_FSICONTACT_CMAKE_CXX_FLAGS="${MASTER_FSICONTACT_CMAKE_CXX_FLAGS} -Wignored-qualifiers -Werror=ignored-qualifiers -Werror=suggest-override -Werror=unused-variable -Werror=misleading-indentation -Werror=return-type -Werror=sign-compare -Werror=unused-but-set-variable -Werror=unused-local-typedefs -Werror=reorder -Werror=maybe-uninitialized -Werror=comment -Werror=parentheses"
# else
#     export MASTER_FSICONTACT_CMAKE_CXX_FLAGS="${MASTER_FSICONTACT_CMAKE_CXX_FLAGS} -Werror=delete-non-abstract-non-virtual-dtor -Werror=extra-tokens -Werror=defaulted-function-deleted -Werror=potentially-evaluated-expression -Werror=instantiation-after-specialization -Werror=undefined-var-template -Werror=unused-lambda-capture -Werror=unused-const-variable -Werror=parentheses -Werror=pessimizing-move -Werror=logical-op-parentheses"
# fi

# Auxiliar sets
if [ -z "$MSG" ] ;then
    export MSG="PVM"
fi
if [ -z "$USE_NLOPT" ] ;then
    export USE_NLOPT=OFF
fi

# Clean
clear
rm -rf "${MASTER_FSICONTACT_BUILD}/${MASTER_FSICONTACT_BUILD_TYPE}/cmake_install.cmake"
rm -rf "${MASTER_FSICONTACT_BUILD}/${MASTER_FSICONTACT_BUILD_TYPE}/CMakeCache.txt"
rm -rf "${MASTER_FSICONTACT_BUILD}/${MASTER_FSICONTACT_BUILD_TYPE}/CMakeFiles"

# Configure
cmake --no-warn-unused-cli ..                                                                       \
-H"${MASTER_FSICONTACT_SOURCE}"                                                                     \
-B"${MASTER_FSICONTACT_BUILD}/${MASTER_FSICONTACT_BUILD_TYPE}"                                      \
-DCMAKE_BUILD_TYPE="${MASTER_FSICONTACT_BUILD_TYPE}"                                                \
-DCMAKE_CXX_FLAGS="${MASTER_FSICONTACT_CMAKE_CXX_FLAGS}"                                            \
-DCMAKE_UNITY_BUILD=ON                                                                              \
-DBUILD_PROGRAM="MasterFSI"                                                                         \
-DMSG=${MSG}                                                                                        \
-DUSE_NLOPT=${USE_NLOPT}                                                                            \
-DNLOPT_ROOT_DIR="$HOME/local/"                                                                     \

# Compile with Unity
cmake --build "${MASTER_FSICONTACT_BUILD}/${MASTER_FSICONTACT_BUILD_TYPE}" --target install -- -j$(nproc)
