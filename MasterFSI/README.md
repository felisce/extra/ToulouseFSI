MasterFSIContact
=================

[[_TOC_]]

# Introduction

![](docs/Logos/master_fsi_logo.png)

<p align="left">
    ![https://gitlab.inria.fr/gerbeau/MasterFSIContact/-/blob/master/LICENSE](https://img.shields.io/badge/licence-PRIVATE-blue?style=flat-square)
    ![pipeline status](https://gitlab.inria.fr/gerbeau/MasterFSIContact/badges/master/pipeline.svg?style=flat-square)
</p>

# Code structure

**MasterFSIContact** contains the following pieces of code:

## MasterFSI:

A master code to handle fluid-structure interaction algorithms, and manage multi-solids contact.

## MultiContact:

A master code to handle solid-solid and solid-wall contact

## Tests:

Some simple models

## ToyModels:

Toy structural models

# Install

In order to compile and install, create a `build` folder and copy there the `configure.sh` script from `scripts` folder. Then configure to your needs and:

~~~sh
sh configure.sh
~~~

# License

*MasterFSIContact* is licensed under private license. See the [license](LICENSE).
