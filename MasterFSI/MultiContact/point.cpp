//===========================================================
//                    MultiContact
//
// Part of MasterFSIContact,             Copyright Inria 2018
//===========================================================
/*!
 \author Jean-Frederic Gerbeau
 */
#include "point.hpp"

void Point::init(const size_t& id,const R3& coor0,const size_t& marker)
{
  set_point(coor0);
  _id = id;
  _marker = marker;
}


ostream& operator <<(ostream& c,const Point& pt)
{
  c << "Point " << pt._id << " : " ;
  c << (R3)pt << " (" << pt._marker << ")";
  return c;
}
