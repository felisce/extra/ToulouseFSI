//===========================================================
//                    MultiContact
//
// Part of MasterFSIContact,             Copyright Inria 2018
//===========================================================
/*!
 \author Jean-Frederic Gerbeau
 */
#ifndef UTIL_GEOMETRIE_H
#define  UTIL_GEOMETRIE_H


void generatePlaneMesh(int Ni,int Nj,
                       double x0,double y0,
                       double x1,double y1,
                       double z0,double z1,
                       int RefNode,
                       int RefQuad,
                       int RefEdges);


int isInside(int npol, double* xp, double* yp, double x, double y);

void xprod(double [3], double [3], double [3]);
void xprod(R3 &, R3 &, double [3]);

void barycentricCoords2D(int npol, double* xp, double*yp, double x, double y,
                         double *lambda1, double *lambda2, int *secondNode);
void barycentricCoords3D(int npol, double* xp, double* yp, double* zp, double x, double y, double z, double*,double*,double*);

double prjptedge(double [3], double [2][3], double [3]);
double normvec(double [3]);

double trinorm(double [3][3], double [3]);
double prptplane(double [3], double [3], double [3], double [3]);
double prptplane2(double pt[3], double orig[3], double nrm[3], double *sol1, double *sol2, double *sol3);
double voltetra(double [3][3]);
double htetra(double tetra[3][3]);

void bndbox(int npol, double tetra[4][3], double bbox[6]);
int ptinbox(double pt[3], double box[6], double eps);
int ptinvertex(double pt[3], double ver[3], double eps);
int ptinedge(double pt[3], double ver[2][3], double eps);
int ptintri(double pt[3], double tri[3][3], double eps);
int ptclosetri(double pt1,double pt2, double pt3, double tri[3][3], double eps, double *dplane, double *solu1, double *solu2, double *solu3);
int isInside3D(int npol, double* xp, double* yp, double* zp, double x, double y, double z, double eps);
int isClose3D(int npol, double* xp, double* yp, double* zp, double x, double y, double z, double eps);



void generatePlaneMesh(int Ni,int Nj,
                       double x0,double y0,
                       double x1,double y1,
                       double z0,double z1,
                       int RefNode,
                       int RefQuad,
                       int RefEdges) {
  // Will display the .mesh format file of a plane containing the point (x0,y0,z0), (x0,y1,z0), (x1,y1,z1)
  // with Ni * Ni nodes.
  
  double dx = (x1 - x0) / (Ni - 1.);
  double dz = (z1 - z0) / (Ni - 1);
  double dy = (y1 - y0) / (Nj - 1);
  cout << "############ CUT AND PASTE THE MESH ############" << endl;
  cout << "MeshVersionFormatted 1" << endl;
  cout << endl;
  cout << "Dimension" << endl;
  cout << "3" << endl;
  cout << endl;
  cout << "Vertices " << endl;
  cout << Ni * Nj << endl;
  for (int j=0 ; j<=Nj-1 ; j++)
    for (int i = 0; i <= Ni-1 ; i++)
      cout << x0 + i * dx << " " << y0 + j * dy  << " " << z0 + i * dz << " " << RefNode << endl;
  
  cout << endl; cout << endl;   cout << endl;
  cout << "Quadrilaterals " << endl;
  cout << (Ni-1) * (Nj-1) << endl;
  for (int j=0 ; j<=Nj-2 ; j++)
    for (int i = 0; i <= Ni-2 ; i++)
      cout << j * Ni + i + 1 << " "
      << j * Ni+ i + 2 << " "
      << (j+1)*Ni + i + 2 << " "
      << (j+1)*Ni + i +1 << " " << RefQuad << endl;
  
  cout << endl;   cout << endl; cout << endl;
  cout << "Edges " << endl;
  cout << Nj * (Ni-1) +  (Nj-1) * Ni << endl;
  for (int j=0 ; j<=Nj-1 ; j++)
    for (int i = 1; i <= Ni-1 ; i++)
      cout << j*Ni + i  << " "
      << j*Ni + i + 1<< " " << RefEdges << endl;
  for (int j=0 ; j<=Nj-2 ; j++)
    for (int i = 1; i <= Ni ; i++)
      cout << j*Ni + i  << " "
      << (j+1)*Ni + i << " " << RefEdges << endl;
  exit(1);
}


/* Projection of a point on an edge, returns de distance between the original point and the proj */

double prjptedge(double pt[3], double line[2][3], double pres[3])
{
  int i;
  double dp, u[3], dis, vec[3],dpvec;
  
  dpvec=0;
  for(i=0;i<3;i++){
    vec[i]=line[1][i]-line[0][i];
    dpvec+=vec[i]*vec[i];
  }
  
  /* Compute the scalar product of the projected vector and the edge's vector */
  
  for(i=0;i<3;i++)
    u[i] = pt[i] - line[0][i];
  
  dp = 0;
  for(i=0;i<3;i++)
  {
    vec[i]= vec[i]/sqrt(dpvec);
    dp += u[i]*vec[i] ;
  }
  
  for(i=0;i<3;i++)
    pres[i] = line[0][i] + dp * vec[i];
  
  dis=0;
  for(i=0;i<3;i++)
    dis += (pt[i]-pres[i])*(pt[i]-pres[i]);
  return(sqrt(dis));
}

/* Given a vector return the normal vector */

double normvec(double v[3])
{
  int i;
  double dprd = 0;
  
  for(i=0;i<3;i++)
    dprd += v[i] * v[i];
  
  if((dprd = sqrt(dprd)))
    for(i=0;i<3;i++)
      v[i] /= dprd;
  
  return(dprd);
}


/* Evaluate the cross product */

void xprod(double v1[3], double v2[3], double sol[3])
{
  sol[0] = v1[1] * v2[2] - v1[2] * v2[1];
  sol[1] = v1[2] * v2[0] - v1[0] * v2[2];
  sol[2] = v1[0] * v2[1] - v1[1] * v2[0];
}

void xprod(R3 & v1, R3 & v2, double sol[3])
{
  sol[0] = v1[1] * v2[2] - v1[2] * v2[1];
  sol[1] = v1[2] * v2[0] - v1[0] * v2[2];
  sol[2] = v1[0] * v2[1] - v1[1] * v2[0];
}

/* Evaluate a triangle's normal vector - to be used in prptplan*/

double trinorm(double tri[3][3], double nv[3])
{
  int i;
  double v1[3], v2[3];
  
  for(i=0;i<3;i++)
  {
    v1[i] = tri[1][i]-tri[0][i];
    v2[i] = tri[2][i]-tri[0][i];
  }
  
  xprod(v1, v2, nv);
  
  return(normvec(nv));
}

/* Normal projection of a point on a plane and returns the distance */

double prptplane(double pt[3], double orig[3], double nrm[3], double sol[3])
{
  int i;
  double dp, u[3];
  
  dp=0;
  
  for(i=0;i<3;i++){
    u[i] = pt[i] - orig[i];
    dp += u[i]*nrm[i];
  }
  
  for(i=0;i<3;i++)
    sol[i] = pt[i] - dp * nrm[i];
  
  return(dp);
}

/* another better prptplane that gives as answer three doubles as the proj pt*/

double prptplane2(double pt[3], double orig[3], double nrm[3], double *sol1, double *sol2, double *sol3)
{
  int i;
  double dp, u[3],sol[3];
  /* 	cout<<"*******************************************"<<endl; */
  /* 	cout<<" pt = "<< pt[0] << " " <<pt[1]<< " "<<pt[2]<<endl; */
  /* 	cout<<" orig   = "<< orig[0] << " " <<orig[1]<< " "<<orig[2]<<endl; */
  /* 	cout<<" nrm    = "<< nrm[0]<< " "<<nrm[1]<< " "<<nrm[2]<<endl; */
  /* 	cout<<"*******************************************"<<endl; */
  dp=0.;
  
  for(i=0;i<3;i++){
    u[i] = pt[i] - orig[i];
    dp += u[i] * nrm[i];
  }
  //	cout<<"dp before "<< dp<<endl;
  
  for(i=0;i<3;i++)
    sol[i] = pt[i] - dp * nrm[i];
  *sol1=sol[0];*sol2=sol[1];*sol3=sol[2];
  /* 	cout<<"     sol = "<< *sol1<<" "<<*sol2<<" "<<*sol3<<endl; */
  /* 	cout << "dp = " << dp <<endl; */
  
  return(dp);
}



double voltetra(double tetra[3][3])
{
  double c[9];
  
  /* Create the linear system, each column is a vector */
  
  c[0] = tetra[1][0] - tetra[0][0];
  c[1] = tetra[2][0] - tetra[0][0];
  c[2] = tetra[3][0] - tetra[0][0];
  
  c[3] = tetra[1][1] - tetra[0][1];
  c[4] = tetra[2][1] - tetra[0][1];
  c[5] = tetra[3][1] - tetra[0][1];
  
  c[6] = tetra[1][2] - tetra[0][2];
  c[7] = tetra[2][2] - tetra[0][2];
  c[8] = tetra[3][2] - tetra[0][2];
  
  /* Return the determinant of the matrix */
  
  return((c[0] * (c[4]*c[8] - c[5]*c[7]) + c[1] * (c[5]*c[6] - c[3]*c[8]) + c[2] * (c[3]*c[7] - c[4]*c[6]))/6.);
}

double htetra(double tetra[3][3])
{
  double aux=0;
  double h=0;
  double hprov=0;
  for(int j=0;j<4;j++)
  {
    for(int k=j+1;j<4;j++)
    {
      aux=0;
      for(int i=0;i<3;i++)
      {
        aux+=(tetra[j][i]-tetra[k][i])*(tetra[j][i]-tetra[k][i]);
      }
      hprov=sqrt(aux);
      if(hprov>h) h=hprov;
    }
  }  
  return(h);
}

#endif
