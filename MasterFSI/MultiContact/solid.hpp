//===========================================================
//                    MultiContact
//
// Part of MasterFSIContact,             Copyright Inria 2018
//===========================================================
/*!
 \author Jean-Frederic Gerbeau
 */
#ifndef _SOLID_HPP_
#define _SOLID_HPP_

#include <iostream>
#include <string>
#include "edge.hpp"
#include "contact_element.hpp"

using namespace std;

/*
 A  solid is defined by its current position, stored in _point.
 At t=0, its initial position is stored in _ref_point
 which will serve as the reference position.
 Of course, it is assumed that the mesh provided in mesh_file to initialize the
 solid is indeed the initial configuration of the solid in the solid solver.
 A displacement sent by the solid solver is defined with respect to this reference
 position: _point[i] = _ref_point[i] + disp[i].
 */
class Solid
{
  size_t _dim;
  size_t _nbcoor;
  size_t _id;
  string _solver_name;
  size_t _nb_dof_per_point;// typically: 3, shell: 5
  int _verbose;
  size_t _nb_point;
  size_t _nb_edge;
  size_t _nb_point_state; // nb of points of the complete structure, whereas _nb_point is the number of point at the interface
  // _nb_point_state is only useful to transfer the whole solution (for a restart for example)
  Point* _point; // current configuration
  Point* _ref_point; // reference configuration (t=0)
  Edge* _edge;
  
  size_t _nb_ctc_elt;     // total number of CTC_ELT of type CTCELT
  size_t _nb_sub_ctc_elt; // total number of CTC_ELT of type SUBCTCELT
  size_t _nb_ctc_pt;      // total number of CTC_ELT of type POINTELT
  
  size_t   _nb_contact_ref;
  size_t * _contact_ref;  // (mesh file) references that must be taken into account for contact analysis for the current solid.
  
  vector<size_t> _offset_CTC_ELT[3];
  // contact elements of reference contact_ref(i) are stored
  // from _CTC_ELT[typelt][_offset_CTC_ELT[typelt][i]]
  
  vector<size_t> _nb_CTC_ELT[3];
  // _nb_CTC_ELT[typelt][ref] gives the number of elements of type
  // typelt with mesh reference contact_ref[ref]
  // nb_CTC_ELT[typelt][ref] = _offset_CTC_ELT[typelt][ref+1] - _offset_CTC_ELT[typelt][ref]
  
  ContactElement * _ctc_elt;
  ContactElement * _sub_ctc_elt;
  ContactElement * _ctc_pt;
  ContactElement * _CTC_ELT[3]; // _CTC_ELT[0] is _ctc_elt, _CTC_ELT[1] is _sub_ctc_elt, _CTC_ELT[2] is _ctc_pt
  
  int _contact_algo;
  int _contact_sub_algo;
  
  
  double _dx; // space step on [0,_L]
  double *_dx_ctc; // space step on [0,_L]
  double _L; // length in the reference configuration
  bool _use_msg;
#ifdef MSG_PVM
  int _pvm_id;
#endif
public:
  // The memory for the forces and displacement is shared with the MultiContact
  double* total_force; // pointer on an array in MultiContact
  double* external_force;// pointer on an array  in MultiContact
  double* contact_force;// pointer on an array  in MultiContact
  double* disp;// pointer on an array  in MultiContact
  double* velo;// pointer on an array  in MultiContact
  Solid(){};
  void init(const size_t &nbcoor,size_t id, const string & solver_name,
            size_t nbdof,
            const string & mesh_file,
            int verbose, int &contact_algo, int &contact_sub_algo,
            size_t  &nb_contact_ref , size_t * contact_ref);
  
  void compact_mesh(const size_t &nbcoor, size_t id,const string& solver_name,size_t nbdof,const string& mesh_file, int verbose,   int &contact_algo, int &contact_sub_algo, int& contact_element_ref);
  friend ostream& operator <<(ostream& c,const Solid& s);
  inline const double& L() const{return _L;};
  inline const double& dx() const{return _dx;};
  inline const double& dx(size_t i) const{return _dx_ctc[i];};
  void dx_init(int contact_algo) ;
  inline const size_t& nbPoint() const{return _nb_point;};
  inline const size_t& nbPointState() const{return _nb_point_state;};
  inline void setNbPointState(int nb){_nb_point_state=nb;};
  inline const size_t& nbEdge() const{return _nb_edge;};
  inline const size_t& nbDofPerPoint() const {return _nb_dof_per_point;}
  
  inline const size_t& nbCtcRef()             const {return _nb_contact_ref;}
  inline const size_t& nbCtcElt(size_t i)     const {return (_nb_CTC_ELT[CTCELT]   [i]);}
  inline const size_t& nbSubCtcElt(size_t i)  const {return (_nb_CTC_ELT[SUBCTCELT][i]);}
  inline const size_t& nbCtcPt(size_t i)      const {return (_nb_CTC_ELT[POINTELT] [i]);}
  
  inline double meas() const;
  inline const   Point& operator()  (size_t i) const{return _point[i];};
  inline const   Point& operator()  (size_t ref,size_t typelt,size_t ni, size_t i) const{return _point[_CTC_ELT[typelt][i + _offset_CTC_ELT[typelt][ref]].get_idnode(ni)];};
  
  inline const   size_t& dim()  const{return _dim;};
  inline const ContactElement  & get_ctc_elt(size_t ref,size_t typelt , size_t j) const{return _CTC_ELT[typelt][j + _offset_CTC_ELT[typelt][ref]];};
#ifdef MSG_PVM
  inline int& pvmId() { return _pvm_id;};
  inline const int& pvmId() const { return _pvm_id;}
#endif
  inline size_t& id() { return _id;};
  const string& solverName() const{return _solver_name;} ;
  const bool& useMSG() const{return _use_msg;};
  void save(const string& filename, const size_t &ref) const;
  void applyDisplacement(double* disp, const double & coef = 1);
  void setExternalForce(const double& fx,const double& fy,const double& fz,const double& time);
  void meanForces(double& mean_contact_x, double& mean_contact_y, double& mean_contact_z,
                  double& mean_external_x, double& mean_external_y, double& mean_external_z);
};
#endif
