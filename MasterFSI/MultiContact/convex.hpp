//===========================================================
//                    MultiContact
//
// Part of MasterFSIContact,             Copyright Inria 2018
//===========================================================
/*!
 \author Jean-Frederic Gerbeau
 */
#ifndef _CONVEX_HPP_
#define _CONVEX_HPP_

#include <vector>
#include <iostream>
#include "vecteur.hpp"
#include "solid.hpp"

using namespace std;

class Convex
{
  double _gap;            // This gap is enforced between a solid phi and a solid psi
  bool _admissible;
  double _omega0;
  double _omega_ctcelt;
  double _omega_subctcelt;
  
  //  matrice<R3>* _theta;    // Array of normals of a face (3D) or an edge (2D) of solid psi in the direction of a node i of solid phi
  //  matrice<R3>* _thetaSub; // Array of "normals" of an edge j on psi in the direction of an edge i on phi
  //                             (colinear to the vector of the two points minimizing the distance between edge i and j)
  matrice<R3>* _THETA[2]; // _THETA[0] = _theta , THETA[1] = _thetaSub;
  Solid* _phi;
  Solid* _psi;
  size_t _phi_ref;
  size_t _psi_ref; // A convex is related to the contact between (phi,phi_ref) and (psi,psi_ref)
  
  //  matrice<double>* _p[3]; // Contact Lagrange multipliers between a node on solid phi and a contact elt (face in 3D, edge in 2D) on solid psi
  //matrice<double>* _pSub[4]; // Contact Lagrange multipliers (3D only) between a Ctc Elt of 2 nodes on solid phi and a Ctc Elt of 2 nodes on solid psi
  matrice<double>* _P[2][4];   // _P[0] is _p , _P[1] is _pSub
  matrice<double>* _Pold[2][4];   // _P[0] is _p , _P[1] is _pSub
  matrice<double>* _Ptilde[2][4];   // _P[0] is _p , _P[1] is _pSub
  matrice<double>* _Poldold[2][4];   // _P[0] is _p , _P[1] is _pSub
  matrice<double>* _Ptildeold[2][4];   // _P[0] is _p , _P[1] is _pSub
  
  matrice<double>* _P0[2][4];
  matrice<double>* _lambda[2][4];
  matrice<double>* _lambda0[2][4];
  matrice<double>* _omega[2][4];
  
  matrice<double>* _diff[2][4];
  
  vector<double>* _sum[2][4];
  vector<double>* _tau[2][4];
  vector<double>* _tau0[2][4];
  
  double _dx;
  double meas; //measure, it could be _dx or _dx*_dx depending on the size of the problem
  int _verbose;
public:
  Convex(){};
  void init(Solid& phi,const size_t & phiRef, Solid& psi,const size_t & psiRef, const double& gap,const int &verbose);
  void init_(const size_t & type_elt,const int & NI,const int & NJ,const int & imax,const int & jmax);
  friend ostream& operator<<(ostream& c,const Convex& cvx);
  double residual() const;
  //double residual_acceleratedUzawa() const;
  double residual_(const size_t & typelt0,const size_t & typelt,const int & NI,const int & NJ,const int & imax,const int & jmax) const;
  //double residual_acceleratedUzawa_(const size_t & typelt0,const size_t & typelt,const int & NI,const int & NJ,const int & imax,const int & jmax) const; // need to be implemented
  
  void descent(double const& step, const bool & aitken, const bool & activate_aitken);
  void descent_acceleratedUzawa(double const& step, const bool & aitken, const bool & activate_aitken);
  void descent_(const bool & aitken,const size_t & typelt0,const size_t & typelt,const int & NI,const int & NJ,const int & imax,const int & jmax, const double & step);
  void descent_acceleratedUzawa_(const bool & aitken,const size_t & typelt0,const size_t & typelt,const int & NI,const int & NJ,const int & imax,const int & jmax, const double & step);
  
  void relaxation(const size_t & typelt,const int & NI,const int & NJ,const int & imax,const int & jmax);
  
  void addContactForce() const;
  void addContactForce_(const size_t & typelt0,const size_t & typelt,const int & NI,const int & NJ,const int & imax,const int & jmax) const;
  double update();
  double update_(const size_t & typelt0,const size_t & typelt,const int & NI,const int & NJ,const int & imax,const int & jmax);
  
  
  
  bool admissibleUpdate();
  void admissibleUpdate_(size_t typelt0, size_t typelt,int NI, int NJ,int imax, int jmax);
  inline double dx() const { return _dx;};
  
  double projectNodeOnEdge(
                           const Point &M,
                           const Point & P0,
                           const Point & P1,
                           const R3 & P01,
                           R3 & Proj);
  void projectNodeOnEdge(
                         const Point &M,
                         const Point & P0,
                         const Point & P1,
                         const R3 & P01,
                         R3 & Proj,
                         R3 &nVec);
  
  
  void unitVector( const R3  & vec , R3 & nvec);  // double &nx, double &ny, double &nz);
  double inZone(bool Dim2,const Point & M, const Point & P0, const Point & P1, const Point & P2);
  void projectionUnitVector(
                            bool Dim2,
                            const Point & M,
                            const Point & P0, const Point & P1, const Point & P2,
                            R3 & nvec);
  void projectionUnitVectorSub(
                               const Point & A0, 
                               const Point & A1, 
                               const Point & B0,
                               const Point & B1,
                               R3 & nvec);
                               
  int _xToLambda(vector<double> x, int iterator); 
  double corr_dual_energy();                            
  int grad_dual_energy(vector<double> &grad, int count);
  
};
#endif
