//===========================================================
//                    MultiContact
//
// Part of MasterFSIContact,             Copyright Inria 2018
//===========================================================
/*!
 \author Jean-Frederic Gerbeau
 */
#include "linearConstraint.hpp"

LinearConstraint::LinearConstraint(Solid& phi,size_t ref,int nb_coor,int icoor):
_phi( &phi),_nb_node(0),_nb_coor(nb_coor),_verbose(1),_last_algebraic_residual(0) // CKT VERSION PERSO WITH LAST ALGEBRAIC RESIDUAL
{
  // here node references are used to identify the node of the solid that will be used for the linear constraint @@@M.A.
  
  for(size_t i=0;i<phi.nbPoint();i++){
    if( phi(i).marker() == ref ){
      _node.push_back(i);
      _nb_node++;
    }
  }
  //
  if(_verbose) cout << "Nb of constraint nodes : " << _nb_node << endl;
  
  _coef = new double[_nb_coor*_nb_node];// Forme Lineaire de type barycentrage//
  for(size_t i=0;i<_nb_node*_nb_coor;i++) _coef[i] = 0.;
  for(size_t i=0;i<_nb_node;i++) _coef[icoor*_nb_node + i] = 1./_nb_node;
  
  cout << "LinearConstraint::LinearConstraint() on solid #" << _phi->id() << endl;
  if(_verbose) { cout << "_nbcoor :" << _nb_coor << endl;
    cout << "icoor :"<< icoor << endl;
    cout << "nodes :"<< endl; }
  for(size_t i=0;i<_nb_node;i++) cout  << _node[i] << " ";
  cout << endl;
  if(_verbose > 1 ) cout << "coef :"<< endl;
  for(size_t j=0;j<_nb_node;j++){
    for(size_t i=0;i<_nb_coor;i++){
      cout << _coef[i*_nb_node + j] << " ";
    }
    cout << endl;
  }
  _p0=0.;
  _p1=0.;
}

void LinearConstraint::addForce() const
{
  cout << "LinearConstraint::addForce()" << endl;
  Solid& phi = *_phi;
  size_t inod;
  size_t phidof = phi.nbDofPerPoint();
  
  if(_verbose) cout << "Nb of ddl per noeud = phidof = " << phidof << endl;
  
  for(size_t i=0;i<_nb_node;i++){
    inod = _node[i];
    for(size_t icoor=0;icoor<_nb_coor;icoor++){
      phi.total_force[phidof*inod + icoor] += - _p0 * _coef[icoor*_nb_node + i];
      if (_verbose>1)
        if (_verbose > 1) cout << "@@@ linear force (inode,icoor) = " << i << " " << icoor << " " << - _p0 * _coef[icoor*_nb_node + i] << endl;
    }
  }
}

void LinearConstraint::addForceTG() const
{
  cout << "LinearConstraint::addForceTG()" << endl;
  Solid& phi = *_phi;
  size_t inod;
  size_t phidof = phi.nbDofPerPoint();
  
  if(_verbose) cout << "Nb of ddf per noeud = phidof = " << phidof << endl;
  
  for(size_t i=0;i<_nb_node;i++){
    inod = _node[i];
    for(size_t icoor=0;icoor<_nb_coor;icoor++){
      phi.total_force[phidof*inod + icoor] += - _p1 * _coef[icoor*_nb_node + i];
      if (_verbose>1)
        if (_verbose > 1) cout << "@@@ linear force (inode,icoor) = " << i << " " << icoor << " " << - _p0 * _coef[icoor*_nb_node + i] << endl;
    }
  }
}

double LinearConstraint::residual()
{
  cout << "LinearConstraint::residual()" << endl;
  Solid& phi = *_phi;
  size_t inod;
  size_t phidof = phi.nbDofPerPoint();
  
  double g=0.;
  
  for(size_t i=0;i<_nb_node;i++){
    inod = _node[i];
    for(size_t icoor=0;icoor<_nb_coor;icoor++){
      g += _coef[icoor*_nb_node + i] * phi.disp[phidof*inod+icoor];
      if (_verbose>1)
        cout << "@@@ residual contribution (i,icoor) " << i << " " << icoor << " " << _coef[icoor*_nb_node + i] * phi.disp[phidof*inod+icoor] << endl;
    }
  }
  if(_verbose) cout << "linear residual g  = "<< fabs(g) << endl;
  _last_algebraic_residual = g;
  return fabs(g);
}



void LinearConstraint::descent(double const& step)
{
  //  cout << "LinearConstraint::descent() step = " << step  << endl;
  Solid& phi = *_phi;
  size_t inod;
  size_t phidof = phi.nbDofPerPoint();
  double g=0;
  
  for(size_t i=0;i<_nb_node;i++){
    inod = _node[i];
    for(size_t icoor=0;icoor<_nb_coor;icoor++){
      g += _coef[icoor*_nb_node + i] * phi.disp[phidof*inod+icoor];  //ckt a modifier avec disp//
      if (_verbose>1)
        cout << "@@@ Desc au noeud " << i << " dans la dir " << icoor << " de " <<  _coef[icoor*_nb_node + i] * phi.disp[phidof*inod+icoor] << endl;
    }
  }
  _p0 += step*g;
  if (_verbose>1)
    cout << "@@@ Lagrange multiplier associated to the centering : " << _p0 << endl;
  
}


void LinearConstraint::descentTG(double const& step)
{
  cout << "LinearConstraint::descentTG() step = " << step  << endl;
  Solid& phi = *_phi;
  size_t inod;
  size_t phidof = phi.nbDofPerPoint();
  double g=0;
  
  for(size_t i=0;i<_nb_node;i++){
    inod = _node[i];
    for(size_t icoor=0;icoor<_nb_coor;icoor++){
      g += _coef[icoor*_nb_node + i] * phi.disp[phidof*inod+icoor];  //ckt a modifier avec disp//
      if (_verbose>1)
        cout << "@@@ Desc au noeud " << i << " dans la dir " << icoor << " de " <<  _coef[icoor*_nb_node + i] * phi.disp[phidof*inod+icoor] << endl;
    }
  }
  _p1 += step*g;
  if (_verbose>1)
    cout << "@@@ Lagrange multiplier associated to centering (tg problem) : " << _p1 << endl;
}
