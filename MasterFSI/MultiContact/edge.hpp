//===========================================================
//                    MultiContact
//
// Part of MasterFSIContact,             Copyright Inria 2018
//===========================================================
/*!
 \author Jean-Frederic Gerbeau
 */
#ifndef _EDGE_HPP_
#define _EDGE_HPP_

#include <iostream>
#include "point.hpp"

using namespace std;

class Edge
{
  Point* _point1;
  Point* _point2;
  int _id;
  size_t _marker;
public:
  Edge(){};
  void init(const size_t& id,Point& pt1,Point& pt2,const size_t& marker);
  
  inline double meas() {
    return euclidNorm(*_point2 - *_point1);};
  
  inline const Point & pt1() {
    return *_point1;};
  
  inline const Point & pt2() {
    return *_point2;};
  
  inline const size_t & marker() {
    return _marker;};
  
  friend ostream& operator <<(ostream& c,const Edge& ed);
};
#endif
