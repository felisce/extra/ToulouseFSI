//===========================================================
//                    MultiContact
//
// Part of MasterFSIContact,             Copyright Inria 2018
//===========================================================
/*!
 \author Jean-Frederic Gerbeau
 */
#ifndef _MULTI_CONTACT_HPP_
#define _MULTI_CONTACT_HPP_


#ifdef MSG_PVM
#include <pvm3.h>
#endif

#ifdef MSG_ZMQ
#include <zmq.hpp>
#endif

#include <vector>
#include "convex.hpp"
#include "simpleConvex.hpp"
#include "solid.hpp"
#include "dataFile.hpp"
#include "attachConstraint.hpp"
#include "linearConstraint.hpp"
#include <nlopt.hpp>

using namespace std;

class MultiContact:
public DataFile
{
#ifdef MSG_ZMQ
  zmq::context_t _zmq_context;
  vector<zmq::socket_t*> _socket;
#endif
  vector<Solid> _solid;
  size_t _nb_point;
  size_t _nb_dof; // typically 3*nb_point (but maybe different in presence of shells)
  int _nbatt; // total number of attach given by the sum of the number of attach references
  vector<size_t> _offset;
  double _time;
  double* _disp;
  double* _disp_old;
  double* _disp_oldold;
  double* _disp_tilde;
  double* _disp_tildeold;
  double* _velo;
  double* _dualEnergy;
  // The memory for the forces is shared with the solids
  double* _total_force; // sum of _external_force and _contact_force
  double* _external_force;
  double* _contact_force;
  //
  size_t _nb_point_state; // whole structure (for restart)
  size_t _nb_dof_state; // whole structure (for restart)
  double* _disp_state; // whole structure displacement (for restart)
  double* _vel_state; // whole structure velocity (for restart)
  double* _acc_state; // whole structure velocity (for restart)
  vector<size_t> _offset_state;
  //
  // PVM stuff
  int _pvm_id;
  int _master_id;
  int _fsi_status;
  int _msg_verbose;
  int _inner_iter;
  bool _admissible;
  bool _activate_aitken_uzawa;
  bool _activate_aitken_uzawa_contact;
  // For Contact algorithm 1
  vector<SimpleConvex> _simple_convex;
  // For Contact algorithm 2
  vector<Convex> _convex;
  // For attach constraints
  vector<AttachConstraint*> _attach;
  // For linear constraints
  vector<LinearConstraint*> _linear_constraint;
  int _nb_linear_constraint;
  //
  vector<double> _post_time_list; //! postprocessing
  double optimal_descent_step();
  
  //messageAdministrator Stuff
  MessageAdmStruct* msgAdmStruct; // for the communication with MasterFSI  (this code is seen as a structure from MasterFSI)
  ///////for nlopt//////////
  int counter_nlopt;
  int size_of_lambda;
  void nlopt_optim();
  double correct_dual_energy();
  vector<double> gradient_dual_energy();
  void xToLambda(vector<double> x);
public:
  //  MultiContact(int contact_algo,double epsilon,int nbsolid,char* argv[],int verbose);
  MultiContact(const string& datafile);
  void writeMeanForces();
  double residual() const;
  double residual_contact() const;
  void setExternalForce();
  void addConstraintForce() const;
  const Solid& solid(size_t i){return _solid[i];};
  void solveSolid();
  const int& fsiSatus() const{return _fsi_status;};
  int& fsiStatus(){return _fsi_status;};
  bool admissibleUpdate();
  void descent(const double& step,const bool& aitken_uzawa=0);
  void descent_contact(const double& step,const bool& aitken_uzawa=0);
  double update();
  void initForce() const;
  /*
   void GeneratePlaneMesh(  int Ni = 10, int Nj = 10,
   double x0 = 0., double y0 = 0.,
   double x1 = 1., double y1 = 1.,
   double z0 = 0., double z1 = 1.,
   int RefNode = 0,int RefQuad = 1, int RefEdges = 1);
   */
  void solveWithoutContact(); // algo = 0
  void solveSimpleContact(); // algo = 1
  void solveMultiContact(); // algo = 2
  void solveStructure();  // algo = 3
  void newTimeStep();
  void postProcessing(const size_t& iter);
  void set_field_old(double * field , double * field_old);
  void relaxation_conv();
  //
  void spawnStruct();
  void firstContactWithStruct();
  void sendInitializationToStruct(int i);
  void sendStateToStruct(int nbNode,int nbDofPerNode,double time, double* disp,double* vel,int i);
  void receiveInitializationFromStruct(int& nbNode,int& nbDofPerNode,int i);
  void sdToMasterFSI();
  void sendStop();
  void rvFromMasterFSI();
  void sdStopToStruct(size_t i);
  void sdToStruct(size_t i);
  void rvFromStruct(size_t i);
  int _masterSendInitialState;
  int _masterRcvInitialState;
  int _masterRcvFinalState;
  
  ////for nlopt/////
   double myvfunc(const vector<double> &x, vector<double> &grad);

};

#endif
