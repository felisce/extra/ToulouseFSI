//===========================================================
//                    MultiContact
//
// Part of MasterFSIContact,             Copyright Inria 2018
//===========================================================
/*!
 \author Jean-Frederic Gerbeau
 */
#include "edge.hpp"

void Edge::init(const size_t& id,Point& pt1,Point& pt2,const size_t& marker){
  _id = id;
  _point1 = &pt1;
  _point2 =&pt2;
  _marker = marker;
}

ostream& operator <<(ostream& c,const Edge& ed)
{
  c << "Edge " << ed._id << " (" << ed._marker << ")" << " : " << endl;
  c << "     " << *ed._point1 << endl;
  c << "     " << *ed._point2;
  return c;
}
