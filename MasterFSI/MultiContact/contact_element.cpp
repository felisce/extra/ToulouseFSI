//===========================================================
//                    MultiContact
//
// Part of MasterFSIContact,             Copyright Inria 2018
//===========================================================
/*!
 \author Jean-Frederic Gerbeau
 */
#include "contact_element.hpp"

void ContactElement::init(const int& id,Point &pt1,const int& marker){ // Point
  _id = id;
  _points[0] = &pt1;
  _points[1] = &pt1;    //NULL;
  _points[2] = &pt1;    //NULL;
  _marker = marker;
}

void ContactElement::init(const int& id,Point &pt1,Point& pt2,const int& marker){ // Edge
  _id = id;
  _points[0] = &pt1;
  _points[1] = &pt2;
  _points[2] = &pt1;    //NULL;
  _marker = marker;
}

void ContactElement::init(const int& id,Point &pt1,Point& pt2,Point &pt3 , const int& marker){ // Triangle
  _id = id;
  _points[0] = &pt1;
  _points[1] =&pt2;
  _points[2] = &pt3;
  _marker = marker;
}

ostream& operator <<(ostream& c,const ContactElement & ed)
{
  c << "Contact Element " << ed._id << " (" << ed._marker << ")" << " : " << endl;
  c << "     " << *ed._points[0] << endl;
  c << "     " << *ed._points[1] << endl;
  c << "     " << *ed._points[2] << endl;
  
  return c;
}
