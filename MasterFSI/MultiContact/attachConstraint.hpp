//===========================================================
//                    MultiContact
//
// Part of MasterFSIContact,             Copyright Inria 2018
//===========================================================
/*!
 \author Jean-Frederic Gerbeau
 */
#ifndef _ATTACHCONSTRAINT_HPP_
#define _ATTACHCONSTRAINT_HPP_

#include <vector>
#include <iostream>
#include "vecteur.hpp"
#include "solid.hpp"

using namespace std;

class AttachConstraint
{
  Solid* _phi;
  double _attach_coor[3];
  int _nb_coor;
  int _nb_node;
  double _omega0;
  
  int* _node;
  double* _dist0;
  double* _dist;
  double* _p0;
  double* _p0old;
  double* _p0oldold;
  double* _p0tilde;
  double* _p0tildeold;
  int _verbose;
  
public:
  AttachConstraint(Solid& phi,double Ox,double Oy,double Oz,const int & nb_coor, const size_t & ref,const double &  dist, const bool & init_dist, const int & verbose);
  void addForce() const;
  double residual() const;
  void descent  (const double & step, const bool & aitken, const bool & activate_aitken );
  void relaxation();
  
};
#endif
