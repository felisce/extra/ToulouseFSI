//===========================================================
//                    MultiContact
//
// Part of MasterFSIContact,             Copyright Inria 2018
//===========================================================
/*!
 \author Jean-Frederic Gerbeau
 */
#include "convex.hpp"
#include "multiContact.hpp"
#include "dataFile.hpp"
#include <string>

using namespace std;

int main(int argc,char* argv[])
{
  string datafile;
  if(argc==1) datafile = "data";
  else datafile = argv[1];
  MultiContact multi(datafile);
  size_t iter=0;
  do{
    iter++;
    if(multi.slaveMode()){
      multi.rvFromMasterFSI();
      if(multi.fsiStatus() == 1) multi.postProcessing(iter);
    } else {
      
      multi.newTimeStep();
      multi.postProcessing(iter);
      //multi.setExternalForce();
    }
    
    
    // if(multi.fsiStatus() == 2)
    //   multi.solveStructure();
    // else{
    switch(multi.contactAlgo()){
      case 0:
        multi.solveWithoutContact();
        break;
      case 1:
        multi.solveSimpleContact();
        break;
      case 2:
        multi.solveMultiContact();
        break;
      default:
        cout << "main.cpp: Unknown contact algorithm " << multi.contactAlgo() <<  endl;
        exit(1);
    }
    
    //multi.writeMeanForces();

    
    //}
    if( multi.slaveMode() ) multi.sdToMasterFSI();
    else if( iter == multi.maxIter() ) multi.fsiStatus() = -1;
  }while (multi.fsiStatus() != -1);
  multi.sendStop();
  cout << "Normal end of MultiStruct" << endl;
  exit(0);
}


