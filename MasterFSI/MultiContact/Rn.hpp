#ifndef _Rn_HPP_
#define _Rn_HPP_

#include <iostream>
#include <vector>
#include <cmath>
#include "RNM.hpp"
using namespace std;


typedef KN<double> Rn;

class R3:
public Rn
{
public:
  R3(): Rn(3,0.){};
  R3(const R3& coor0):Rn(coor0){};
};


ostream &operator<<(ostream &out,const R3 &a);

inline const double euclidNorm(const R3& vec) {
  double result(0);
  result=(vec,vec);
  return sqrt(result);
}


/*
 inline const  R3   operator- (const R3 & vec2) {
 R3 result;
 for(size_t i=0; i<vec2.size();i++) result[i]=-vec2.at(i);
 return result;
 }
 inline const  R3   operator/ (const R3 & vec1,const double & val) {
 R3 result;
 for(size_t i=0; i<vec1.size();i++) result[i]=vec1.at(i)/val;
 return result;
 }
 
 inline const  R3   operator* (const R3 & vec1,const double & val) {
 R3 result;
 for(size_t i=0; i<vec1.size();i++) result[i]=vec1.at(i)*val;
 return result;
 }
 inline const  R3   operator* (const double & val,const R3 & vec1) {
 R3 result;
 for(size_t i=0; i<vec1.size();i++) result[i]=vec1.at(i)*val;
 return result;
 }
 */
#endif
