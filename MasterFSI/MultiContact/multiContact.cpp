//===========================================================
//                    MultiContact
//
// Part of MasterFSIContact,             Copyright Inria 2018
//===========================================================
/*!
 \author Jean-Frederic Gerbeau
 */
#include "multiContact.hpp"
#include <string>

char* c_string(const string& s){
  char* p = new char[s.length()+1];
  s.copy(p,string::npos);
  p[s.length()] = 0;
  return p;
}

MultiContact::MultiContact(const string& datafile):
#ifdef MSG_ZMQ
DataFile(datafile),_zmq_context(1)
#else
DataFile(datafile)
#endif
{
  
  
  size_t nb_coor(this->get_nb_coor());
  
  _masterSendInitialState=0;
  _masterRcvInitialState=0;
  _masterRcvFinalState=0;
  
  if(_initializationType) _masterSendInitialState=1;
  
  cout << "MultiContact: " << _nb_solid << " solids " << endl;
  _offset.resize(_nb_solid);
  _offset_state.resize(_nb_solid);
  _solid.resize(_nb_solid);
  _dualEnergy = new double[_nb_solid];
  if(_contact_algo==1) _simple_convex.resize( _nb_solid );
  int counter_convex(0);
  for(size_t  i=0;i<_nb_solid;i++)
  {
    counter_convex += _nb_contact_ref[i];
  }
  
  if(_contact_algo==2) _convex.resize( counter_convex*(counter_convex-1) );
  
  // resizing of _attach for attach constraint
  _nbatt=0;
  for(size_t k=0;k<_nb_attach;k++)
    _nbatt+=_nb_attach_ref[k];
  _attach.resize(_nbatt);
  
  _nb_linear_constraint = _nb_barycenter_constraint *nb_coor;
  _linear_constraint.resize(_nb_linear_constraint);
  _nb_point = 0;
  _nb_dof = 0;
  _nb_point_state = 0;
  _nb_dof_state = 0;
  _time = 0.;
  int nbcvx(0);
  _nbatt = 0;
  
#ifdef MSG_ZMQ
  _socket.resize(_nb_solid);
#endif
  
  for(size_t i=0;i<_nb_solid;i++){
    if(_verbose){
      cout << "Solid " << i << endl;
      cout << "  solver path = " << _solver_path[i] << endl;
      cout << "  mesh file = " << _mesh_dir + "/" + _mesh_file[i] << endl;
      cout << "  nb dof per point = " << _nb_dof_per_point[i] << endl;
    }
    _solid[i].init( nb_coor,i,_solver_path[i],_nb_dof_per_point[i],
                   _mesh_dir + "/" + _mesh_file[i],
                   _verbose,_contact_algo, _contact_sub_algo,
                   _nb_contact_ref[i],_contact_ref[i]);
    
    
    if(i==0) _offset[i] = 0;
    else _offset[i] = _offset[i-1] + _solid[i-1].nbPoint()*_solid[i-1].nbDofPerPoint();
    _nb_point += _solid[i].nbPoint();
    _nb_dof += _solid[i].nbPoint() * _solid[i].nbDofPerPoint();
    // Contact algo
    switch(_contact_algo){
      case 0:
      {
        cout << " no contact algorithm" << endl;
        break;
      }
        
      case 1:
      {
        _simple_convex[i].init(_solid[i],_gap,_wall_coef[0],_wall_coef[1],_wall_coef[2], _verbose);
        break;
      }
      case 2:
      {
        for(size_t j=0;j<=i;j++){  // allows auto-contact of (i,iref) with (i,jref) when iref <> jref
          for (size_t iref=0 ; iref<_solid[i].nbCtcRef() ; iref++) {
            for (size_t jref=0; jref<_solid[j].nbCtcRef() ; jref++) {
              if ((j==i) && (jref==iref)) break;
              _convex[nbcvx++].init(_solid[i], iref,_solid[j], jref, _gap,_verbose) ;
              _convex[nbcvx++].init(_solid[j], jref,_solid[i], iref, _gap,_verbose);
            }
          }
        }
        break;
      }
      default:
        cout << "multiContact.cpp: Unknown contact algorithm " << _contact_algo <<  endl;
    }
    // Attached points
    for(size_t k=0;k<_nb_attach;k++){
      if(_attach_solid[k] == (int)i){
        for(int j=0;j<_nb_attach_ref[k];j++){
          _attach[_nbatt] = new AttachConstraint(_solid[i],
                                                 _attach_coor[3*k+0],_attach_coor[3*k+1],
                                                 _attach_coor[3*k+2],_nb_coor,_attach_constraint_reference[k][j],_attach_dist[k][j],_init_stretch,_verbose);
          _nbatt++;
        }
      }
    }
    
    // Barycenter constraint
    
    for(size_t k=0;k<_nb_barycenter_constraint;k++){
      if(_barycenter_constraint_solid[k] == (int)i){
        for(size_t icoor=0;icoor<nb_coor;icoor++){
          _linear_constraint[nb_coor*k+icoor] =
          new LinearConstraint(_solid[i],
                               _barycenter_constraint_reference[k],
                               nb_coor,
                               icoor);
        }
      }
    }
  }
  
  if(_verbose) cout << "Number of convex sets = " << nbcvx << endl;
  _disp = new double[_nb_dof];
  
  /////////////////////////////////////////////////////
  // Added for aitken acceleration on convex iterations
  /////////////////////////////////////////////////////
  _disp_old = new double[_nb_dof];
  _disp_oldold = new double[_nb_dof];
  _disp_tilde = new double[_nb_dof];
  _disp_tildeold = new double[_nb_dof];
  /////////////////////////////////////////////////////
  
  _velo = new double[_nb_dof];
  _external_force = new double[_nb_dof];
  _total_force = new double[_nb_dof];
  _contact_force = new double[_nb_dof];

  for(size_t i=0;i<_nb_dof;i++){
    _disp[i] = 0.;
    _disp_old[i] = 0.;
    _disp_oldold[i] = 0.;
    _disp_tilde[i] = 0.;
    _disp_tildeold[i] = 0.;
    _velo[i] = 0.;
    _external_force[i] = 0.;
    _total_force[i] = 0.;
    _contact_force[i] = 0.;
  }
  for(size_t i=0;i<_nb_solid;i++){
    _solid[i].total_force = _total_force + _offset[i];
    _solid[i].external_force = _external_force + _offset[i];
    _solid[i].contact_force = _contact_force + _offset[i];
    _solid[i].disp = _disp + _offset[i];
    _solid[i].velo = _velo + _offset[i];
  }
  if(_verbose>1){
    for(size_t i=0;i<_nb_solid;i++){
      cout << _solid[i] << endl;
    }
  }
  
  if(_slave_mode){
    // Driven by MasterFSI
#if defined(MSG_PVM)
    char**  argv(NULL);
    int     argc = 0;
    msgAdmStruct = new MessageAdmStruct(argc,argv,_verbose_ms,_protocol,_unit); // seen from MasterFSI, this code is a struct
    _master_id = msgAdmStruct->masterId(); // msg id of MasterFSI
#endif
#if defined(MSG_ZMQ)
    char**  argv(NULL);
    int     argc = 0;
    msgAdmStruct = new MessageAdmStruct(argc,argv,_verbose_ms,_protocol,_unit,_my_socket_transport,_my_socket_address,_my_socket_port); // seen from MasterFSI, this code is a struct
#endif
  }
  //
  spawnStruct();
  firstContactWithStruct();
  
  //
  if(_slave_mode) sdToMasterFSI();
}

void MultiContact::spawnStruct(){
#ifdef MSG_PVM
  char**  args(NULL);
  int     flag = 0;
  int  ntask_struct=1;
#endif
  
  for(size_t i=0;i<_nb_solid;i++){
    if(_solid[i].useMSG()){
#ifdef MSG_PVM
      cout << "Pvm spawn structure #" << i << endl << _solid[i].solverName() << " on " <<
      HOSTNAME << "... " << flush;
      int info = pvm_spawn(c_string(_solid[i].solverName()),
                           args, flag,(char*)HOSTNAME,ntask_struct, &_solid[i].pvmId() );
      if(info) cout << "the structure task id is " << _solid[i].pvmId() << endl << flush;
      else{
        cerr << "cannot execute " << _solid[i].solverName() << " !\n";
        exit(1);
      }
#endif
#ifdef MSG_ZMQ
      _socket[i] = new zmq::socket_t(_zmq_context, ZMQ_PAIR);
      string socket_endpoint;
      if(_socket_port[i] != "")
        socket_endpoint = _socket_transport[i] + "://"+_socket_address[i]+":"+_socket_port[i];
      else
        socket_endpoint = _socket_transport[i] + "://"+_socket_address[i];
      cout << "[ZeroMQ] bind structure #" << i << " to " << socket_endpoint << endl;
      _socket[i]->bind(socket_endpoint.c_str());
#endif
    } else {
      cout << "Do nothing for structure #" << i << endl;
    }
  }
}

void MultiContact::firstContactWithStruct(){
  int nbstructnode,nbstructdofpernode;
  switch (_protocol) {
    case 1:case 11:
      //
      // Protocol 1 and 11
      //
      for(size_t i=0;i<_nb_solid;i++) {
        if(_solid[i].useMSG()) rvFromStruct(i);
      }
      break;
    case 2:
      //
      // Protocol 2
      //
      for(size_t i=0;i<_nb_solid;i++){
        if(_solid[i].useMSG()) sendInitializationToStruct(i);
      }
      if(_masterRcvInitialState){
        cout << "masterRcvInitialState not yet implemented" << endl;
        exit(1);
      }else{
        for(size_t i=0;i<_nb_solid;i++){
          if(_solid[i].useMSG()) {
            receiveInitializationFromStruct(nbstructnode,nbstructdofpernode,i);
            if(nbstructdofpernode != _solid[i].nbDofPerPoint()){
              cout << "Mismatch nbDofPerPoint" << endl;
              exit(1);
            }
            _solid[i].setNbPointState(nbstructnode);
          }
        }
      }
      for(size_t i=0;i<_nb_solid;i++){
        if(i==0) _offset_state[i] = 0;
        else _offset_state[i] = _offset_state[i-1] + _solid[i-1].nbPointState()*_solid[i-1].nbDofPerPoint();
        _nb_dof_state += _solid[i].nbPointState() * _solid[i].nbDofPerPoint();
        _nb_point_state += _solid[i].nbPointState();
      }
      _disp_state = new double[_nb_dof_state];
      _vel_state = new double[_nb_dof_state];
      _acc_state = new double[_nb_dof_state];
      
      if(_slave_mode){
        // ==== exchanges with masterFSI
        msgAdmStruct->receiveInitializationFromMaster();
        
        if ( msgAdmStruct->masterSendInitialState() != _masterSendInitialState){
          cout << "_masterSendInitialState should be the same in masterFSI and multiContact !" << endl;
          exit(1);
        }
        if(msgAdmStruct->masterRcvInitialState()){
          cout << "masterRcvInitialState() not yet implemented" << endl;
          exit(1);
        }else{
          // warning: we assume here that the number of dof per node is the same in all solid
          // so we can take the solid[0]
          msgAdmStruct->sendInitializationToMaster(_nb_point_state,_solid[0].nbDofPerPoint());
        }
        if(msgAdmStruct->masterSendInitialState()){
          int rcvnpts;
          int rcvncoor;
          msgAdmStruct->receiveStateFromMaster(rcvnpts,rcvncoor,_time,_disp_state,_vel_state,_acc_state);
          assert(_nb_point_state == rcvnpts);
          assert(_solid[0].nbDofPerPoint() == rcvncoor);
        }
        //===
      }
      
      if(_masterSendInitialState){
        // send the initial state to the structures
        for(size_t i=0;i<_nb_solid;i++){
          if(_solid[i].useMSG()) {
            sendStateToStruct(_solid[i].nbPointState(),_solid[i].nbDofPerPoint(), _time, _disp_state+_offset_state[i],_vel_state+_offset_state[i],i);
          }
        }
      }
      for(size_t i=0;i<_nb_solid;i++) {
        if(_solid[i].useMSG()) rvFromStruct(i);
      }
      
      break;
    default:
      cout << "MultiContact::firstContactWithStruct() Unknown protocol" << endl;
      exit(1);
      break;
  }
}

void MultiContact::sendInitializationToStruct(int i){
  if(_verbose_ms)  cout << "---> Master sends initialization to struct (msg 206)\n";
#ifdef MSG_PVM
  pvm_initsend(PvmDataDefault);
  pvm_pkint(&_protocol,1,1);
  pvm_pkint(&_unit,1,1);
  pvm_pkint(&_masterSendInitialState,1,1);
  pvm_pkint(&_masterRcvInitialState,1,1);
  pvm_pkint(&_masterRcvFinalState,1,1);
  pvm_send(_solid[i].pvmId(),206);
#endif
#ifdef MSG_ZMQ
  zmq::message_t msg1(1*sizeof(int));
  memcpy(msg1.data(),&_protocol,1*sizeof(int));
  _socket[i]->send(msg1);
  //
  zmq::message_t msg2(1*sizeof(int));
  memcpy(msg2.data(),&_unit,1*sizeof(int));
  _socket[i]->send(msg2);
  //
  zmq::message_t msg3(1*sizeof(int));
  memcpy(msg3.data(),&_masterSendInitialState,1*sizeof(int));
  _socket[i]->send(msg3);
  //
  zmq::message_t msg4(1*sizeof(int));
  memcpy(msg4.data(),&_masterRcvInitialState,1*sizeof(int));
  _socket[i]->send(msg4);
  //
  zmq::message_t msg5(1*sizeof(int));
  memcpy(msg5.data(),&_masterRcvFinalState,1*sizeof(int));
  _socket[i]->send(msg5);
#endif
  
  if(_verbose_ms>1) {
    cout << "\t masterSendInitialState =" << _masterSendInitialState << endl;
    cout << "\t masterRcvInitialState =" << _masterRcvInitialState << endl;
    cout << "\t masterRcvFinalState =" << _masterRcvFinalState << endl;
  }
}


void MultiContact::receiveInitializationFromStruct(int& nbNode,int& nbDofPerNode,int i){
  if(_verbose_ms)  cout << "---> Master receives initialization from struct (msg 207)\n";
#ifdef MSG_PVM
  pvm_recv(_solid[i].pvmId(),207);
  pvm_upkint(&nbNode,1,1);
  pvm_upkint(&nbDofPerNode,1,1);
#endif
#ifdef MSG_ZMQ
  zmq::message_t msg1;
  _socket[i]->recv(&msg1);
  memcpy(&nbNode,msg1.data(),1*sizeof(int));
  //
  zmq::message_t msg2;
  _socket[i]->recv(&msg2);
  memcpy(&nbDofPerNode,msg2.data(),1*sizeof(int));
#endif
  if(_verbose_ms){
    cout << "\t nbNode = " << nbNode << endl;
    cout << "\t nbDofPerNode = " << nbDofPerNode << endl;
  }
}

void MultiContact::sendStateToStruct(int nbNode,int nbDofPerNode,double time, double* disp,double* vel,int i){
  if(_verbose_ms)  cout << "---> Master send state to struct (msg 225)\n";
#ifdef MSG_PVM
  pvm_initsend(PvmDataDefault);
  pvm_pkint(&nbNode,1,1);
  pvm_pkint(&nbDofPerNode,1,1);
  pvm_pkdouble(&time,1,1);
  int dim = nbNode*nbDofPerNode;
  pvm_pkdouble(disp,dim,1);
  pvm_pkdouble(vel,dim,1);
  pvm_send(_solid[i].pvmId(),225);
#endif
#ifdef MSG_ZMQ
  zmq::message_t msg1(1*sizeof(int));
  memcpy(msg1.data(),&nbNode,1*sizeof(int));
  _socket[i]->send(msg1);
  //
  zmq::message_t msg2(1*sizeof(int));
  memcpy(msg2.data(),&nbDofPerNode,1*sizeof(int));
  _socket[i]->send(msg2);
  //
  zmq::message_t msg3(1*sizeof(double));
  memcpy(msg3.data(),&time,1*sizeof(double));
  _socket[i]->send(msg3);
  //
  int dim = nbNode*nbDofPerNode;
  //
  zmq::message_t msg4(dim*sizeof(double));
  memcpy(msg4.data(),disp,dim*sizeof(double));
  _socket[i]->send(msg4);
  //
  zmq::message_t msg5(dim*sizeof(double));
  memcpy(msg5.data(),vel,dim*sizeof(double));
  _socket[i]->send(msg5);
#endif
}

void MultiContact::solveSolid()
{
  /////
  
  //  for(size_t i=0;i<_nb_point;i++)
  //  for (size_t j=0; j<3; j++)
  //     if ( (i>1439) && (i<1476))  // ckt CRITICAL
  //	cout << "FORCE TO DYNA " << j << "  " << _solid[0].external_force[5 * i+j ] << endl;
  
  
  /////
  for(size_t i=0;i<_nb_solid;i++){
    if(_solid[i].useMSG()) sdToStruct(i);
  }
  for(size_t i=0;i<_nb_solid;i++){
    if(_solid[i].useMSG()) rvFromStruct(i);
  }
  if (_fsi_status!=2)  _fsi_status = 0;
}

void MultiContact::sendStop()
{
  for(size_t i=0;i<_nb_solid;i++){
    if(_solid[i].useMSG()) sdStopToStruct(i);
  }
}

double MultiContact::residual() const
{
  double r=0.;
  
  for (int i=0;i<_nbatt;i++){
    r += _attach[i]->residual();
  }
  
  cout<<"Residual for attachments = "<<r<<endl;
  
  for (int i=0;i<_nb_linear_constraint;i++){
    r += _linear_constraint[i]->residual();
  }
  
  return r;
}

double MultiContact::residual_contact() const
{
  double r=0.;
  switch(_contact_algo){
    case 0:
      break;
    case 1:
    {
      vector<SimpleConvex>::const_iterator i;
      for (i=_simple_convex.begin();i!=_simple_convex.end();i++){
        // The method below should be implemented and used in simpleConvex.cpp if one want to compute another residual for the accelerated Uzawa method with restart than the one used so far
        //if (_useAcceleratedUzawa == 1) {
        //  (*i).residual_acceleratedUzawa(step);
        //}
        //else {
        r += (*i).residual();
        //}
      }
      break;
    }
    case 2:
    {
      vector<Convex>::const_iterator i;
      for (i=_convex.begin();i!=_convex.end();i++){
        // The method below should be implemented and used in convex.cpp if one want to compute another residual for the accelerated Uzawa method with restart than the one used so far
        
        //if (_useAcceleratedUzawa == 1) {
        //  r += (*i).residual_acceleratedUzawa();
        //}
        //else {
        r += (*i).residual();
        //}
      }
      break;
    }
  }
  cout<<"Residual for contact = "<<r<<endl;
  return r;
}

double MultiContact::update()
{
  double r=0.;
  vector<Convex>::iterator i;
  for (i=_convex.begin();i!=_convex.end();i++){
    r += (*i).update();
  }
  return r;
}

void MultiContact::descent(const double& step, const bool & activate_aitken_uzawa)
{
  if(_nbatt!=0) cout<<"Attach constraint::descent"<<endl;
  for (int i=0;i<_nbatt;i++){
    _attach[i]->descent(step,_aitken_uzawa, activate_aitken_uzawa);
  }
  if(_fsi_status==2) {
    for (int i=0;i<_nb_linear_constraint;i++){
      _linear_constraint[i]->descentTG(step);}}
  else{
    for (int i=0;i<_nb_linear_constraint;i++){
      _linear_constraint[i]->descent(step);
    }
  }
}

void MultiContact::descent_contact(const double& step, const bool & activate_aitken_uzawa)
{
  switch(_contact_algo){
    case 1:
    {
      cout<<"Simple contact::descent"<<endl;
      vector<SimpleConvex>::iterator i;
      for (i=_simple_convex.begin();i!=_simple_convex.end();i++){
        
        if (_useAcceleratedUzawa == 1) {
          (*i).descent_acceleratedUzawa(step);
        }
        else {
          (*i).descent(step);
        }
        
      }
      break;
    }
    case 2:
    {
      cout<<"Multi body contact::descent"<<endl;
      vector<Convex>::iterator i;
      for (i=_convex.begin();i!=_convex.end();i++){
        
        if (_useAcceleratedUzawa == 1) {
          (*i).descent_acceleratedUzawa(step,_aitken_uzawa, activate_aitken_uzawa);
        }
        else {
          (*i).descent(step,_aitken_uzawa, activate_aitken_uzawa);
        }
        
      }
      break;
    }
  }
}

double  MultiContact::optimal_descent_step() {
  // Optimal descent step computation of dual fct for linear constraints
  // Independent of rk if B is constant like for the barycenter case
  
  double optimal_step;
  LinearConstraint *Lin;
  
  if ( (_descent_step < 0) || (_nb_linear_constraint == (int)(_nb_barycenter_constraint * _nb_coor))) {
    // We use h / dt≤ as descent step. In the barycenter case this formula is exact.
    // This is the case that we usually do since no informations are given on the structure matrix
    optimal_step = _solid[0].dx();
    for (size_t i = 1 ; i < _nb_solid; i++) {
      optimal_step  = min ( optimal_step ,  _solid[i].dx() );
    }
    optimal_step /= (_dt * _dt ) ;
    
  }
  else
  {
    
    // General case (B is not 1/Nb_node).
    // Step 1 : rk is known from linear_constraint.last_algebraic_residual
    // Step 2 : One can compute optimal descent coefficient approximation from
    //                      square(norm(rk)) *    h/  square (dt * norm(Bt_rk))
    // NB : If B is constant (baycenter case), we obtain with the following computation exactly the optimal_step computed above by h / dt≤
    
    double rk;            // Dual fct residual rk
    double rk_sqnorm;     // square of rk norm
    
    double Bt_rk ;        // B^t rk
    double Bt_rk_sqnorm;  // square of Bt_rk norm
    
    rk_sqnorm = 0;
    Bt_rk_sqnorm = 0;
    
    for (int i=0;i<_nb_linear_constraint;i++) {
      Lin  = _linear_constraint[i];
      rk  =  Lin->get_last_algebraic_residual();
      // cout << "New residual rk : " << rk << endl;
      rk_sqnorm  =   rk_sqnorm + rk * rk * Lin->get_solid()->dx();
      // cout << "@@@ rk_sqnorm for constraint " << i << "  :   " << rk*rk <<  endl;
      // cout << "@@@ dx for constraint " << i << "  :   " <<  Lin->get_solid()-> dx()  <<  endl;
      
      Bt_rk = 0;
      for(size_t n=0;n <  Lin-> get_nb_node() ;n++)
        for(size_t icoor=0;icoor<_nb_coor;icoor++)
          Bt_rk +=  rk  * Lin->get_coef(icoor , n);
      Bt_rk_sqnorm += Bt_rk * Bt_rk;
      // cout << "@@@ Bt_rk_sqnorm for constraint " << i <<  " : "  << Bt_rk * Bt_rk << endl;
    }
    
    // cout << "@@@ dt = " << _dt << endl;
    // cout << "@@@ rk_sqnorm  (dont dx)" << rk_sqnorm <<  endl;
    // cout << "@@@ Bt_sqnorm " << Bt_rk_sqnorm << endl;
    optimal_step  =  ( rk_sqnorm )  / ( _dt * _dt * Bt_rk_sqnorm ) ;
  }
  
  return  _omega_descent_step * optimal_step ;
  
}

bool MultiContact::admissibleUpdate()
{
  vector<Convex>::iterator i;
  _admissible=true;
  for (i=_convex.begin();i!=_convex.end();i++){
    _admissible = ( (*i).admissibleUpdate() and _admissible );
  }
  return _admissible;
}

void MultiContact::addConstraintForce() const
{
  switch(_contact_algo){
    case 1:
    {
      vector<SimpleConvex>::const_iterator i;
      for (i=_simple_convex.begin();i!=_simple_convex.end();i++){
        (*i).addContactForce();
      }
      break;
    }
    case 2:
    {
      vector<Convex>::const_iterator i;
      for (i=_convex.begin();i!=_convex.end();i++){
        (*i).addContactForce();
      }
      break;
    }
  }
  for (int i=0;i<_nbatt;i++){
    _attach[i]->addForce();
  }
  if(_fsi_status==2) {
    // This is used for the tangent problem, in the case that we use Newton or QN iterations
    // instead of fixed point iterations.
    for (int i=0;i<_nb_linear_constraint;i++){
      _linear_constraint[i]->addForceTG();}}
  else{
    for (int i=0;i<_nb_linear_constraint;i++){
      _linear_constraint[i]->addForce();
    }
  }
}

void MultiContact::newTimeStep()
{
  _time += _dt;
  if(_verbose){
    cout <<"======================================================================" << endl;
    cout <<" MultiContact: time = " << _time << endl;
    cout <<"======================================================================" << endl;
  }
  _fsi_status=1;
}


void MultiContact::sdToMasterFSI()
{
  
  /*
   if(msg_verbose){
   cout <<"\t disp: \n";
   for(int i=0;i<total_nb_points;i++)
   cout << disp[3*i+0] << " "<< disp[3*i+1] << " "<< disp[3*i+2] << " "
   << endl;
   cout << endl;
   cout <<"\t velo: \n";
   for(int i=0;i<total_nb_points;i++)
   cout << velo[3*i+0] << " "<< velo[3*i+1] << " "<< velo[3*i+2] << " "
   << endl;
   cout << endl;
   }
   */
  
  int nbdof = _nb_dof; // conversion size_t to int
  
  
  msgAdmStruct->sendInterfDispVelToMaster(nbdof,_disp,_velo);
  
}

void MultiContact::rvFromMasterFSI()
{
  int nbdof;
  msgAdmStruct->receiveInterfForceFromMaster(_fsi_status,_dt,nbdof,_external_force);
  
  if(nbdof != (int)_nb_dof){
    cout << "rvFromMasterFSI: fatal error : " << nbdof
    << " != " << _nb_dof << endl;
    exit(1);
  }
  if(_fsi_status==1) newTimeStep();
  
  /*
   if(msg_verbose){
   cout << "\t status: " << fsiStatus << endl;
   if(_fsi_status!=-1){
   cout << "\t dt=" << _dt << endl;
   cout << "\t force_fluid: \n";
   for(int i=0;i<total_nb_points;i++) cout << force_fluid[3*i+0] << " "
   << force_fluid[3*i+1] << " "
   << force_fluid[3*i+2] << " " << endl;
   cout << endl;
   }
   */
}


void MultiContact::sdToStruct(size_t istruct)
{
  Solid& phi = _solid[istruct];
  cout << "---> MultiContact sends to Struct #" << istruct << endl;
  int nbdof = phi.nbDofPerPoint() * phi.nbPoint();
  int nbdofperpt = phi.nbDofPerPoint();
  //
  //  if(_fsi_status==1) phi.plot_mesh();
  double* force = _total_force+_offset[istruct];
  double* ext_force = _external_force+_offset[istruct];
  double* ctc_force = _contact_force+_offset[istruct];

  if(_verbose>1){
    cout << "\t status : " << _fsi_status << endl;
    if(_fsi_status != -1){
      cout << "\t dt: " << _dt << endl;
      cout << "\t force struct (contact + fluid): \n";
      for(size_t j=0;j<phi.nbPoint();j++)
        cout << force[nbdofperpt*j+0] << " "
        << force[nbdofperpt*j+1] << " "
        << force[nbdofperpt*j+2] << endl;
      cout << "\t force contact: \n";
      for(size_t i=0;i<phi.nbPoint();i++){
        /*
        cout << force[nbdofperpt*i+0]-ext_force[nbdofperpt*i+0]  << " "
        << force[nbdofperpt*i+1]-ext_force[nbdofperpt*i+1] << " "
        << force[nbdofperpt*i+2]-ext_force[nbdofperpt*i+2]  << endl;
         */
        cout << ctc_force[nbdofperpt*i+0]  << " "
        << ctc_force[nbdofperpt*i+1] << " "
        << ctc_force[nbdofperpt*i+2]  << endl;
      }
      cout << endl;
    }
  }
#ifdef MSG_PVM
  pvm_initsend(PvmDataDefault);
  pvm_pkint(&_fsi_status,1,1);
  if(_fsi_status != -1){
    pvm_pkdouble(&_dt,1,1);
    pvm_pkint(&nbdof,1,1);
    if(_protocol==11){// send separately the external and the contact forces
      pvm_pkdouble(ext_force,nbdof,1);
      pvm_pkdouble(ctc_force,nbdof,1);
    }else{// send the total force
      pvm_pkdouble(force,nbdof,1);
    }
  }
  pvm_send(phi.pvmId(),130);
#endif
#ifdef MSG_ZMQ
  zmq::message_t msg1(1*sizeof(int));
  memcpy(msg1.data(),&_fsi_status,1*sizeof(int));
  _socket[istruct]->send(msg1);
  //
  if(_fsi_status != -1){
    zmq::message_t msg2(1*sizeof(double));
    memcpy(msg2.data(),&_dt,1*sizeof(double));
    _socket[istruct]->send(msg2);
    //
    zmq::message_t msg3(1*sizeof(int));
    memcpy(msg3.data(),&nbdof,1*sizeof(int));
    _socket[istruct]->send(msg3);
    //
    if(_protocol==11){// send separately the external and the contact forces
      zmq::message_t msg4(nbdof*sizeof(double));
      memcpy(msg4.data(),ext_force,nbdof*sizeof(double));
      _socket[istruct]->send(msg4);
      //
      zmq::message_t msg5(nbdof*sizeof(double));
      memcpy(msg5.data(),ctc_force,nbdof*sizeof(double));
      _socket[istruct]->send(msg5);
    }else{// send the total force
      zmq::message_t msg4(nbdof*sizeof(double));
      memcpy(msg4.data(),force,nbdof*sizeof(double));
      _socket[istruct]->send(msg4);
    }
    //
  }
#endif
  
  //void sendToStruct(force,_dt,_fsi_status,phi.pbmId(),?nbdof?);
}

void MultiContact::sdStopToStruct(size_t istruct)
{
  cout << "---> MultiContact sends stop to Struct #" << istruct << endl;
  if(_verbose>1){
    cout << "\t status : " << _fsi_status << endl;
  }
#ifdef MSG_PVM
  Solid& phi = _solid[istruct];
  pvm_initsend(PvmDataDefault);
  pvm_pkint(&_fsi_status,1,1);
  pvm_send(phi.pvmId(),130);
#endif
#ifdef MSG_ZMQ
  zmq::message_t msg1(1*sizeof(int));
  memcpy(msg1.data(),&_fsi_status,1*sizeof(int));
  _socket[istruct]->send(msg1);
  //
#endif
  //void sendToStruct(force,_dt,_fsi_status,phi.pbmId(),?nbdof?);
}

void MultiContact::rvFromStruct(size_t istruct)
{
  Solid& phi = _solid[istruct];
  int nbdofperpt = (int)phi.nbDofPerPoint();
  cout << "<--- MultiContact receives from Struct #" << istruct << endl;
  int nbdof;
  double* dispStruct = _disp + _offset[istruct];
  double* veloStruct = _velo + _offset[istruct];
  double* dualEnergy = &(_dualEnergy[istruct]);
  //void receiveFromStruct(double* dispStruct,double* veloStruct,int struct_id,int dim);
  //from here
  switch(_protocol){
    case 1: // standard protocol
    {
#ifdef MSG_PVM
      pvm_recv(phi.pvmId(),100);
      pvm_upkint(&nbdof,1,1);
      if( nbdof != nbdofperpt * (int) phi.nbPoint() ) {
        cout << "rvFromStruct: fatal error : " << nbdof << " != "
        << nbdofperpt * phi.nbPoint() << endl;
        exit(1);
      }
      pvm_upkdouble(dispStruct,nbdof,1);
      pvm_upkdouble(veloStruct,nbdof,1);
#endif
#ifdef MSG_ZMQ
      zmq::message_t msg1;
      _socket[istruct]->recv(&msg1);
      memcpy(&nbdof,msg1.data(),1*sizeof(int));
      if( nbdof != nbdofperpt * (int) phi.nbPoint() ) {
        cout << "rvFromStruct: fatal error : " << nbdof << " != "
        << nbdofperpt * phi.nbPoint() << endl;
        exit(1);
      }
      cout << "received dof" << endl;
      //
      zmq::message_t msg2;
      _socket[istruct]->recv(&msg2);
      memcpy(dispStruct,msg2.data(),nbdof*sizeof(double));
      cout << "received disp" << endl;
      //
      zmq::message_t msg3;
      _socket[istruct]->recv(&msg3);
      memcpy(veloStruct,msg3.data(),nbdof*sizeof(double));
      cout << "received velocity" << endl;
#endif
      break;
    }
    case 11: // like case 1, but receive also the value of the dual energy
    {
#ifdef MSG_PVM
      pvm_recv(phi.pvmId(),1100);
      pvm_upkint(&nbdof,1,1);
      if( nbdof != nbdofperpt * (int) phi.nbPoint() ) {
        cout << "rvFromStruct: fatal error : " << nbdof << " != "
        << nbdofperpt * phi.nbPoint() << endl;
        exit(1);
      }
      pvm_upkdouble(dispStruct,nbdof,1);
      pvm_upkdouble(veloStruct,nbdof,1);
      pvm_upkdouble(dualEnergy,1,1);
#endif
#ifdef MSG_ZMQ
      zmq::message_t msg1;
      _socket[istruct]->recv(&msg1);
      memcpy(&nbdof,msg1.data(),1*sizeof(int));
      if( nbdof != nbdofperpt * (int) phi.nbPoint() ) {
        cout << "rvFromStruct: fatal error : " << nbdof << " != "
        << nbdofperpt * phi.nbPoint() << endl;
        exit(1);
      }
      //
      zmq::message_t msg2;
      _socket[istruct]->recv(&msg2);
      memcpy(dispStruct,msg2.data(),nbdof*sizeof(double));
      //
      zmq::message_t msg3;
      _socket[istruct]->recv(&msg3);
      memcpy(veloStruct,msg3.data(),nbdof*sizeof(double));
      //
      zmq::message_t msg4;
      _socket[istruct]->recv(&msg4);
      memcpy(dualEnergy,msg4.data(),1*sizeof(double));
#endif
      cout << "Energy received = " << _dualEnergy[istruct] << endl;
      break;
    }
    default:
      cout << "Error in file: " << __FILE__ << " line: " << __LINE__ << " -> Unknown protocol" << endl;
      exit(1);
  }
  
  phi.applyDisplacement(dispStruct);
  if(_verbose>1){
    cout <<"\t dispStruct: \n";
    for(size_t j=0;j<phi.nbPoint();j++){
      cout << dispStruct[nbdofperpt*j+0]
      << " "<< dispStruct[nbdofperpt*j+1] << " "<< dispStruct[nbdofperpt*j+2] << " " << endl;
    }
    cout << endl;
    cout <<"\t veloStruct: \n";
    for(size_t j=0;j<phi.nbPoint();j++){
      cout.setf(ios_base::scientific);
      cout.precision(10);
      cout << veloStruct[nbdofperpt*j+0] << " "<< veloStruct[nbdofperpt*j+1] << " " << veloStruct[nbdofperpt*j+2] << " " << endl;
    }
    if(_protocol == 11) cout << "\t dualEnergy: " << _dualEnergy[istruct] << endl;
    cout << endl;
  }
}


void MultiContact::initForce() const
{
  //
  //cout << "@@@ Entering init force" << endl;
  //for(size_t i=0;i<_nb_dof / 3 ;i++) cout << "@@@ disp [ " << i << "] = " << _disp[i] << endl;
  //
  //cout << "total force in init force" << endl;
  for(size_t i=0;i<_nb_dof;i++){
    _total_force[i] = _external_force[i];
    _contact_force[i] = 0.0;
    //cout << i << "\t" << _total_force[i] << endl;
  }
}

void MultiContact::setExternalForce()
{
  for(size_t i=0;i<_nb_solid;i++){
    if(_verbose) cout << "setExternalForce on solid #" << i << endl;
    _solid[i].setExternalForce(_force[0],_force[1],_force[2],_time);
  }
}


void MultiContact::solveSimpleContact()
{
  int iter_lambda=0;
  double residu_lambda=2.*_tolerance;
  
  
  switch(_with_nlopt){
    case 0:
    {
      while( (residu_lambda > _tolerance) ){
        iter_lambda++;
        initForce();
        addConstraintForce();
        solveSolid();
        residu_lambda=residual_contact();
        if(_verbose>1) cout << "lambda " << iter_lambda << " residual = " << residu_lambda << endl;
        descent_contact(_descent_step); // call descent_contact or descent_contact_acceleratedUzawa of each simple convex problem based on the value of useAcceleratedUzawa
        if (_verbose) {
          if (_useAcceleratedUzawa == 1) {
            cout << "Accelerated Uzawa ends after " << iter_lambda << " iterations" << endl;
          }
          else {
            cout << "Uzawa ends after " << iter_lambda << " iterations" << endl;
          }
        }
      } // end of the while loop
      break;
    }
    case 1:
    {
      if(_protocol != 11){
        cout << "Error in " << __FILE__ << " line " << __LINE__ << ": nlopt needs the energy (protocol must be 11)" << endl;
        exit(1);
      }
      nlopt_optim();
      break;
    }
    default:
      cout << "Error in " << __FILE__ << " line " << __LINE__ << ": unknown optim algo, with_nlopt = " << _with_nlopt << endl;
      exit(1);
  }
  
}

void MultiContact::solveWithoutContact()

{
  // Here we don't have contact but we could have barycenter constraint
  double tolerance_lambda(_tolerance);
  // Here lambda represents the contact constraint
  double residu_lambda(2.*tolerance_lambda);
  int iter_lambda(0);     // decompte le nombre d'iteration d'Uzawa effectuÈes
  
  cout << "------------------------------------------"<<endl;
  cout << "Minimization without contact" << iter_lambda << endl;
  cout << "------------------------------------------"<<endl;
  
  //// Init of lambda = 0 for barycenter constraint
  /*
   size_t nb_coor(this->get_nb_coor());
   for(size_t i=0;i<_nb_solid;i++)
   for(size_t k=0;k<_nb_barycenter_constraint;k++)
   if(_barycenter_constraint_solid[k] == (int)i)
   for(size_t icoor=0;icoor<nb_coor;icoor++)
   _linear_constraint[nb_coor*k+icoor]->init();
   */
  ////
  
  while( (iter_lambda != _iter_lambda_max) and (residu_lambda > tolerance_lambda)){
    iter_lambda++;
    cout << "iter lambda = " << iter_lambda << endl;
    initForce();
    addConstraintForce();
    solveSolid();
    residu_lambda=residual();
    
    if(_verbose) cout<<"residual lambda = "<<residu_lambda<<endl;
    if (_descent_step < 0)
    {
      _descent_step = optimal_descent_step();
      if(_verbose > 1) cout << "step=" << _descent_step << endl;
    }
    
    //}
    
    _activate_aitken_uzawa= _aitken_uzawa && (iter_lambda> 2);
    
    if(residu_lambda > tolerance_lambda) descent(_descent_step,_activate_aitken_uzawa);
  }
}


void MultiContact::solveMultiContact()
{
  double residu_domaine;
  double residu_attach;
  double residu_contact;
  double residu_lambda;
  int iter_convex(0);
  residu_domaine = 2.*_tolerance_domain;
  
  while ( ( iter_convex < _iter_convex_max ) and (residu_domaine > _tolerance_domain) ){ // loop on the domain residual
    
    iter_convex++;
    cout << "--------------------------------------------------"<<endl;
    cout << "Minimisation on convex " << iter_convex << endl;
    int iter_lambda(0);     // counter for uzawa iterations
    int iter_attach(0);     // counter for uzawa iterations in attach
    int iter_contact(0);     // counter for uzawa iterations in attach
    bool admissible(true);  // THE INTIAL DEFORMATION IS ADMISSIBLE
    
    residu_lambda=2.*_tolerance;
    
    set_field_old(_disp_old,_disp_oldold);
    set_field_old(_disp,_disp_old);
    
    switch(_with_nlopt){
      case 0:
      {
        while( ((iter_lambda < _iter_lambda_max) and (residu_lambda > _tolerance)) or (!admissible)){ // loop on the contact residual and admissible status
          iter_lambda++;
          cout << "iter lambda = " << iter_lambda << endl;
          initForce();
          addConstraintForce();
          solveSolid();
          
          residu_attach=residual();
          residu_contact=residual_contact();
          residu_lambda=residu_attach+residu_contact;
          if(_verbose) cout << "residual lambda = "<<residu_lambda<<endl;
          // Verify if the deformation is still admissible: no intersections
          admissible=admissibleUpdate();
          //if(!_aitken_uzawa)
          //{
          if (_descent_step < 0) {
            _descent_step = optimal_descent_step();
            if(_verbose > 1) cout << "step = " << _descent_step << endl;
          }
          
          if(residu_attach > _tolerance/2)
          {
            iter_attach++;
            _activate_aitken_uzawa = _aitken_uzawa && (iter_attach> 2);
            descent(_descent_step,_activate_aitken_uzawa);
          }
          
          if((residu_contact > _tolerance/2)  or  (!admissible)) // if the residual is under the tolerance but the status is still unadmissible, keep looping
          {
            iter_contact++;
            _activate_aitken_uzawa_contact = _aitken_uzawa && (iter_contact> 2);
            descent_contact(_descent_step,_activate_aitken_uzawa_contact);
          }
        } // end of the inner while loop on contact and attach constraints
        
        if (_verbose) {
          if (_useAcceleratedUzawa == 1) {
            cout << "Accelerated Uzawa ends after " << iter_lambda << " iterations" << endl;
          }
          else {
            cout << "Uzawa ends after " << iter_lambda << " iterations" << endl;
          }
        }
        break;
      }//case 1
        
      case 1:
      {
		if(_protocol != 11){
			cout << "Error in " << __FILE__ << " line " << __LINE__ << ": nlopt needs the energy (protocol must be 11)" << endl;
			exit(1);
		}
        nlopt_optim();
        break;
      }
        
      default:
        cout << "Error in " << __FILE__ << " line " << __LINE__ << ": unknown protocol " << _protocol << endl;
        exit(1);
    }
    
    set_field_old(_disp_tilde,_disp_tildeold);
    set_field_old(_disp,_disp_tilde);
    
    residu_domaine=update();
    
    if ( _aitken_convex!=0 && iter_convex>2) relaxation_conv();
    
    if(_verbose) cout << "residual domain = " << residu_domaine << endl;
  } // end of the outter while loop on the convex set
}

void MultiContact::relaxation_conv()
{
  double r;
  
  if(_verbose) cout<<"Applying Aitken relaxation to convex iterations"<<endl;
  double sum1=0.;
  double sum2=0.;
  
  
  for(size_t i=0; i<_nb_dof; i++)
  {
    sum1+=(_disp_old[i] - _disp_oldold[i])*(_disp_old[i] - _disp_tilde[i] - _disp_oldold[i] + _disp_tildeold[i]);
    sum2+=(_disp_old[i] - _disp_tilde[i] - _disp_oldold[i] + _disp_tildeold[i])*(_disp_old[i] - _disp_tilde[i] - _disp_oldold[i] + _disp_tildeold[i]);
  }
  
  double  omega0 = sum1/sum2;
  cout<<"omega_aitken = "<< omega0<<endl;
  
  for (size_t i = 0; i < _nb_dof ; i++) _disp [i] =omega0*_disp_tilde[i]+(1.0-omega0)*_disp_old[i];
  
  for(size_t istruct=0;istruct<_nb_solid;istruct++)
  {
    Solid& phi = _solid[istruct];
    double* dispStruct = _disp + _offset[istruct];
    phi.applyDisplacement(dispStruct);
  }
  
  r=update();
  
}

void MultiContact::solveStructure()
{
  initForce();
  solveSolid();
}

void MultiContact::postProcessing(const size_t& iter)
{
  static int ipost=0;
  if( ! (iter%_post_period)){
    _post_time_list.push_back(_time);
    char numiter[5];
    char numsolid[3];
    string filename;
    string gnuplot_command;
    char name[256];
    bool dim2 = (_solid[0].dim() == 2);
    
    sprintf(numiter,"%04d",ipost);
    if(_gnuplot){
      if(_verbose) cout << "Postprocessing gnuplot in " << _post_dir << endl;
      for(size_t i=0;i<_nb_solid;i++){
        sprintf(numsolid,"%lu",i);
        filename=_post_dir+"/solid"+numsolid+"."+numiter;
        _solid[i].save(filename,_gnuplot_ref);
      }
      gnuplot_command=_post_dir+"/gp";
      ofstream gnuplot_file(gnuplot_command.c_str());
      gnuplot_file << "!echo Modify gp to activate png terminal"<<endl;
      gnuplot_file << "#set terminal png"<<endl;
      gnuplot_file << "set size ratio -1" << endl;
      gnuplot_file << "#unset border" << endl;
      gnuplot_file << "#unset xtics" <<endl;
      gnuplot_file << "#unset ytics" <<endl;
      //   gnuplot_file << "set xrange [-1:1]" << endl;
      //gnuplot_file << "set yrange[-1:1]" << endl;
      
      for(size_t it=0;it<_post_time_list.size();it++){
        sprintf(name,"set title 'INRIA-REO: time %f'",_post_time_list[it]);
        gnuplot_file <<name<<endl;
        if(_png_output)
        {
          sprintf(name,"set output 'time_%f.png'",_post_time_list[it]);
          gnuplot_file <<name<<endl;
        }
        if (dim2) gnuplot_file << "plot \\" << endl; else gnuplot_file << "splot \\" << endl;
        for(size_t i=0;i<_nb_solid;i++){
          sprintf(name," 'solid%lu.%04lu' notitle with linespoints 3",i,it);
          gnuplot_file << name;
          if(i<_nb_solid-1) gnuplot_file << ",\\";
          gnuplot_file << endl;
        }
        gnuplot_file<<"pause 0.1"<<endl;
      }
    }
    ipost++;
  }
}


void MultiContact::set_field_old(double * field , double * field_old) {
  for (size_t i = 0; i < _nb_dof; i++) field_old[i] = field[i];
}

////////////////////modified for nlopt/////////////////


void MultiContact::nlopt_optim()
{
  counter_nlopt = 0;
  size_of_lambda = 0;
  
  switch(_contact_algo){
    case 1:
    {
      size_of_lambda = _nb_point;
      break;
    }
    case 2:
    {
      for(size_t i=0;i<_nb_solid;i++){
        for(size_t j=0;j<=i;j++){  // allows auto-contact of (i,iref) with (i,jref) when iref <> jref
          for (size_t iref=0 ; iref<_solid[i].nbCtcRef() ; iref++) {
            for (size_t jref=0; jref<_solid[j].nbCtcRef() ; jref++) {
              if ((j==i) && (jref==iref)) break;
              size_of_lambda = 1 * 3 * _solid[i].nbCtcPt(iref) * _solid[j].nbCtcElt(jref);
              size_of_lambda += 2 * 2 * _solid[i].nbSubCtcElt(iref) * _solid[j].nbSubCtcElt(jref);
              size_of_lambda += 1 * 3 * _solid[j].nbCtcPt(jref) * _solid[i].nbCtcElt(iref);
              size_of_lambda += 2 * 2 * _solid[j].nbSubCtcElt(jref) * _solid[i].nbSubCtcElt(iref);
            }
          }
        }
      }
      cout << "size of lambda = " << size_of_lambda << endl;
      break;
    }
    default:
      cout << "Error in file: " << __FILE__ << " line: " << __LINE__ << " -> Unknown contact algorithm" << endl;
      exit(1);
  }
  
  //nlopt::opt opt(nlopt::LD_SLSQP, size_of_lambda);
  nlopt::opt opt(nlopt::LD_MMA, size_of_lambda);

  //std::cout << "size_of_lambda = " << size_of_lambda << std::endl;
  
  opt.set_max_objective(nlopt_wrapper, this);

  vector<double> lb(size_of_lambda,0.);

  opt.set_lower_bounds(lb);

  opt.set_ftol_rel(_tolerance_nlopt); // tolerance based on the relative value achieved by the function to optimize
  //opt.set_xtol_abs(1e-1);

  double maxf;
  vector<double> x(size_of_lambda);
  for(size_t i=0; i<size_of_lambda; i++)
  {
    x[i] = 0.;
  }
  
  cout << "### Solve with NLOPT ###" << endl;
  nlopt::result result = opt.optimize(x, maxf);
  cout << "   *** maxf = " << maxf << endl;
  cout << "No. of evaluations done by nlopt = " << counter_nlopt << endl;
}

double MultiContact::myvfunc(const vector<double> &x, vector<double> &grad)
{
  counter_nlopt++; 
  xToLambda(x); // update values of lagrangian multiplier p0 (put new values of x into p0 = lambda = mu)
  initForce();  
  addConstraintForce(); 
  solveSolid();
  double correction = correct_dual_energy(); 
  double function = 0.;
  for(size_t i=0; i<_nb_solid; i++){
    function += _dualEnergy[i];
  }
  function += correction;
  grad = gradient_dual_energy(); 
  cout << "Evaluation " << counter_nlopt <<"   *** function = " << function << endl;
  //cout << "   *** x = " << x[_nb_point-1] << endl;
  //cout << "   *** grad = " << grad[_nb_point-1] << endl;
  return function;
}


double nlopt_wrapper(const vector<double> &x, vector<double> &grad, void *my_func_data)
{
  MultiContact* obj = reinterpret_cast<MultiContact*>(my_func_data);;
  return (obj->myvfunc(x, grad));
}


double MultiContact::correct_dual_energy()
{
  double total_correction = 0.;
  switch(_contact_algo){
    case 1:
    {
      vector<SimpleConvex>::iterator i;
      for (i=_simple_convex.begin();i!=_simple_convex.end();i++){
        total_correction = total_correction + (*i).corr_dual_energy();
      }
      break;
    }
    case 2:
    {
      vector<Convex>::iterator i;
      for (i=_convex.begin();i!=_convex.end();i++){
        total_correction += (*i).corr_dual_energy();
      }
      break;
    }
    default:
      cout << "Error in file: " << __FILE__ << " line: " << __LINE__ << " -> Unknown contact algorithm" << endl;
      exit(1);
  }
  return total_correction;
}


vector<double> MultiContact::gradient_dual_energy()
{
  vector<double> gradient;
  vector<double> grad;
  int count = 0;
  switch(_contact_algo){
    case 1:
    {
      gradient.resize(_nb_point);
      for(size_t i=0;i<_nb_solid;i++){
        grad.resize(_solid[i].nbPoint());
        grad = _simple_convex[i].grad_dual_energy();
        for(int j=0; j<_solid[i].nbPoint(); j++){
          gradient[count++] = grad[j];
        }
      }
      if(count != _nb_point){
        cout << "gradient not correctly defined" << endl;
      }
      break;
    }
    case 2:
    {
      gradient.resize(size_of_lambda);
      vector<Convex>::iterator i;
      for (i=_convex.begin();i!=_convex.end();i++){
        count = ((*i).grad_dual_energy(gradient, count));
        //cout << "next convex set" << endl;
      }
      
      if( count != size_of_lambda)
        cout << count << " != " << size_of_lambda << ": Error assigning value of x to lambda" << endl;
      
      break;
    }
    default:
      cout << "Error in file: " << __FILE__ << " line: " << __LINE__ << " -> Unknown contact algorithm" << endl;
      exit(1);
  }
  return gradient;
}


void MultiContact::xToLambda(vector<double> x)
{
  int count = 0;
  
  switch(_contact_algo){
    case 1:
    {
      for(size_t i=0;i<_nb_solid;i++){
        if (i==0){
          count = 0;
        }
        else{
          count = count + _solid[i-1].nbPoint();
        }
        _simple_convex[i]._xToLambda(x,count);
      }
      break;
    }
    case 2:
    {
      vector<Convex>::iterator i;
      for (i=_convex.begin();i!=_convex.end();i++){
        count = ((*i)._xToLambda(x, count));
      }
      
      if( count != size_of_lambda)
        cout << count << " != " << size_of_lambda << ": Error assigning value of x to lambda" << endl;
      
      break;
    }
    default:
      cout << "Error in file: " << __FILE__ << " line: " << __LINE__ << " -> Unknown contact algorithm" << endl;
      exit(1);
  }
}


void MultiContact::writeMeanForces()
{
  /*
   static bool first_time=true;
   cout << "entered write" << endl;
   Solid& phi = _solid[0];
   int nbdof = phi.nbDofPerPoint() * phi.nbPoint();
   int nbdofperpt = phi.nbDofPerPoint();
   
   ofstream f1;
   if(first_time) f1.open("contact_force.dat");
   else f1.open("contact_force.dat",ios::app);
   f1 << _time << "\t" << (_total_force[nbdofperpt*(phi.nbPoint()-1)+1]-_external_force[nbdofperpt*(phi.nbPoint()-1)+1]) << endl;
   f1.close();
   first_time=false;
   */
  static bool first_time=true;
  for(size_t i=0;i<_solid.size();i++){
    Solid& phi = _solid[i];
    ofstream f1;
    stringstream filename;
    filename << "mean_forces_" << i << ".dat";
    if(first_time){
      f1.open(filename.str().c_str());
      f1 << "#   Time         Contact x       Contact y       Contact z       External x      External y       External z           Tip x         Tip y       Tip z" << endl;
    }
    else f1.open(filename.str().c_str(),ios::app);
    double average_x,average_y,average_z;
    double external_x,external_y,external_z;
    phi.meanForces(average_x,average_y,average_z,external_x,external_y,external_z);
    const Point& tip = phi(phi.nbPoint()-1);
    f1 << std::scientific << _time << " \t";
    f1 << std::scientific << average_x << " \t";
    f1 << std::scientific << average_y << " \t";
    f1 << std::scientific << average_z << " \t";
    f1 << std::scientific << external_x << " \t";
    f1 << std::scientific << external_y << " \t";
    f1 << std::scientific << external_z <<  " \t";
    f1 << std::scientific << tip(0) << " \t";
    f1 << std::scientific << tip(1) <<  " \t";
    f1 << std::scientific << tip(2) << endl;

    f1.close();
  }
  first_time=false;
}

