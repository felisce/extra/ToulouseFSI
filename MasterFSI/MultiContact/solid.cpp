//===========================================================
//                    MultiContact
//
// Part of MasterFSIContact,             Copyright Inria 2018
//===========================================================
/*!
 \author Jean-Frederic Gerbeau
 */
#include "solid.hpp"
#include <fstream>
#include <string>
#include <vector>
#include <iomanip>
#include "mesh.hpp"

ostream& operator <<(ostream& c,const Solid& s)
{
  c << "Solid  : " << endl;
  c << " " << "nb_point = " << s._nb_point << endl;
  c << " " << "nb_edge = " << s._nb_edge << endl;
  if (s.dim() == 3) {
    c << " " << "nb Contact Element (Faces) = " << s._nb_ctc_elt << endl;
    c << " " << "nb Sub Contact Element (Edges) = " << s._nb_sub_ctc_elt << endl;
  }
  if (s.dim() == 2)  c << " " << "nb Contact Element (Edges) = " << s._nb_ctc_elt << endl;
  c << " " << "nb Contact Points  = " << s._nb_ctc_pt << endl;
  if(s._verbose>0){
    for(size_t i=0;i<s._nb_point;i++){
      c << " " << s._point[i] << endl;
    }
    
    for(size_t i=0;i<s._nb_edge;i++){
      c << " " << s._edge[i] << endl;
    }
    
    for(size_t i=0;i<s._nb_ctc_elt;i++){
      c << " " << s._ctc_elt[i] << endl;
    }
    
    for(size_t i=0;i<s._nb_sub_ctc_elt;i++){
      c << " " << s._sub_ctc_elt[i] << endl;
    }
    c << "Contact force :" << endl;
    for(size_t i=0;i<s._nb_point;i++){
      c << i << "->"
      /*
       << s.total_force[ 3*i+0] -  s.external_force[ 3*i+0] << ","
       << s.total_force[ 3*i+1] -  s.external_force[ 3*i+1] << ","
       << s.total_force[ 3*i+2] -  s.external_force[ 3*i+2] << endl;
       */
      << s.contact_force[ 3*i+0] << ","
      << s.contact_force[ 3*i+1]<< ","
      << s.contact_force[ 3*i+2] << endl;
    }
  }
  return c;
}

void Solid::init(const size_t &nbcoor, size_t id,const string& solver_name,
                 size_t nbdof,const string& mesh_file,
                 int verbose,   int &contact_algo, int &contact_sub_algo,
                 size_t  &nb_contact_ref , size_t * contact_ref)
{
  _nbcoor = nbcoor;
  _id=id;
  _nb_dof_per_point = nbdof;
  _solver_name=solver_name;
  if( (_solver_name == "nil") || (_solver_name == "Nil") || (_solver_name == "NIL") )
    _use_msg = false;
  else _use_msg = true;
  _verbose=verbose;
  _contact_algo = contact_algo;
  _contact_sub_algo = contact_sub_algo;
  _nb_contact_ref = nb_contact_ref;
  _contact_ref = contact_ref;
  
  //======================================================================
  // read a mesh file (adapted from LiFE-V)
  string line;
  // double x, y, z;
  size_t p1,p2,p3,p4, ibc;
  size_t nVe( 0 ), nBVe( 0 ), nBFa( 0 );
  size_t numStoredFaces(0);
  size_t nVo( 0 ), nBEd( 0 );
  size_t i;
  R3 vec_temp;
  bool quadrilaterals(0);
  vector<int>  face2Node[4];
  vector<size_t> face2CtcRef; // = 0         if the face is NOT in the contact reference list
  // = (ref + 1) otherwise
  vector<size_t> pt2CtcRef;   // = 0         if the point does not belong to an element in the contact reference list
  // = (ref + 1) otherwise
  vector<size_t> index_CTC_ELT[3]; // index_CTC_ELT[k][j] is the current position to add a new contact element of type k and reference j
  bool fatal_error = false;
  
  // open stream to read header
  ifstream hstream( mesh_file.c_str() );
  if (verbose){
    cout<<"Reading mesh file "<<mesh_file<< endl;
  }
  
  if ( hstream.fail() ){
    cerr << " Error: File " << mesh_file
    << " not found or locked"
    << endl;
    abort();
  }
  if ( ! readINRIAMeshFileHead( hstream, nVe, nBVe, nBFa, nBEd, nVo,
                               numStoredFaces) ){
    cerr << " Error While reading INRIA mesh file headers" << endl;
    ABORT() ;
  }
  hstream.close();
  if (numStoredFaces) _dim = 3; else _dim = 2;
  //Reopen the stream: I know it is stupid but this is how it goes
  ifstream mystream( mesh_file.c_str() );
  if ( mystream.fail() ){
    cerr << " Error : File " << mesh_file
    << " not found or locked" << endl;
    abort();
  }
  // Euler formulas to get number of faces and number of edges (no hole !)
  
  _nb_point = nVe;
  _point = new Point[_nb_point] ;
  _ref_point = new Point[_nb_point];
  _nb_edge = nBEd;
  _edge = new Edge[_nb_edge];
  
  for (size_t i = 0; i < 3 ; i++) {
    _nb_CTC_ELT[i].resize(_nb_contact_ref);
    for (size_t j = 0 ; j < _nb_contact_ref ; j++) _nb_CTC_ELT[i][j] = 0;
  }
  
  if (_contact_algo == 2) {
    for (i = 0;i < 4; i++) face2Node[i].resize(numStoredFaces);
    face2CtcRef.resize(numStoredFaces);
    for (i = 0; i < numStoredFaces; i++) face2CtcRef[i] = 0;
    pt2CtcRef.resize(_nb_point);
    for (i = 0; i< _nb_point; i++) pt2CtcRef[i] = 0;
  }
  
  if(verbose){
    cout << "Reading " << _nb_point << " points, " << _nb_edge << " edges." << endl;
  }
  
  while ( next_good_line( mystream, line ).good() ){
    if ( line.find( "Vertices" ) != string::npos ){
      nextIntINRIAMeshField( line.substr( line.find_last_of( "s" ) + 1 ),mystream );
      for ( i = 0;i < nVe;i++ ){
        mystream >> vec_temp[0] >> vec_temp[1] >> vec_temp[2] >> ibc;
        _point[i].init(i,vec_temp,ibc);
        _ref_point[i].init(i,vec_temp,ibc);
        if(verbose>1) cout << _point[i] << endl;
      }
    }
    
    if ( line.find( "Triangles" ) != string::npos ) {
      cout << "Reading Triangles " << endl;
      nextIntINRIAMeshField( line.substr( line.find_last_of( "s" ) + 1 ), mystream );
      for ( i = 0;i <numStoredFaces;i++ ){
        mystream >> p1 >> p2 >> p3 >> ibc;
        if (contact_algo == 2) {
          for (size_t j = 0; j < _nb_contact_ref ; j++) {
            if (ibc == _contact_ref[j]) {
              _nb_CTC_ELT[CTCELT][j] += 1;
              face2CtcRef[i] = j+1;
              face2Node[0][i] = p1-1; face2Node[1][i] = p2-1; face2Node[2][i] = p3-1;
              if (_contact_sub_algo ==2) { // 3D Only : contact edges (sub ctc elt) are built from faces
                if (!pt2CtcRef[p1-1] || !pt2CtcRef[p2-1]) _nb_CTC_ELT[SUBCTCELT][j]++;
                if (!pt2CtcRef[p2-1] || !pt2CtcRef[p3-1]) _nb_CTC_ELT[SUBCTCELT][j]++;
                //if (!pt2CtcRef[p3-1] || !pt2CtcRef[p3-1]) _nb_CTC_ELT[SUBCTCELT][j]++; @@@ Bug ? jfg 5 juil 2017
                if (!pt2CtcRef[p3-1] || !pt2CtcRef[p1-1]) _nb_CTC_ELT[SUBCTCELT][j]++; // fixed,  jfg 5 juil 2017
              }
              
              if (!pt2CtcRef[p1-1]) { pt2CtcRef[p1-1] = j+1;  _nb_CTC_ELT[POINTELT][j]++; }
              else if ( (pt2CtcRef[p1-1] != j+1) && (verbose)) {
                cout << "Intersection Point " << p1 << " is a potential contact point with both references " <<
                _contact_ref[pt2CtcRef[p1-1]-1] << " and " << _contact_ref[j] << endl;
                fatal_error = true;
              }
              
              if (!pt2CtcRef[p2-1]) { pt2CtcRef[p2-1] = j+1;  _nb_CTC_ELT[POINTELT][j]++; }
              else if ( (pt2CtcRef[p2-1] != j+1) && (verbose)) {
                cout << "Intersection Point " << p2 << " is a potential contact point with both references " <<
                _contact_ref[pt2CtcRef[p2-1]-1] << " and " << _contact_ref[j] << endl;
                fatal_error = true;
              }
              
              if (!pt2CtcRef[p3-1]) { pt2CtcRef[p3-1] = j+1;  _nb_CTC_ELT[POINTELT][j]++; }
              else if ( (pt2CtcRef[p3-1] != j+1) && (verbose)) {
                cout << "Intersection Point " << p3 << " is a potential contact point with both references " <<
                _contact_ref[pt2CtcRef[p3-1]-1] << " and " << _contact_ref[j] << endl;
                fatal_error = true;
              }
            }
          }
        }
      }
      if (fatal_error) {
        cout << "---- Fatal error in Solid::init : one should check intersection points listed above" << endl;
        exit(1);
      }
      quadrilaterals=false;
      
    }
    
    if ( line.find( "Quadrilaterals" ) != string::npos ) {
      cout << "Reading Quadrilaterals " << endl;
      nextIntINRIAMeshField( line.substr( line.find_last_of( "s" ) + 1 ), mystream );
      for ( i = 0;i <numStoredFaces;i++ ){
        mystream >> p1 >> p2 >> p3 >> p4 >> ibc;
        if (contact_algo == 2) {
          for (size_t j = 0; j < _nb_contact_ref ; j++) {
            if (ibc == _contact_ref[j]) {
              _nb_CTC_ELT[CTCELT][j] += 2;
              face2CtcRef[i] = j+1;
              face2Node[0][i] = p1-1; face2Node[1][i] = p2-1; face2Node[2][i] = p3-1; face2Node[3][i] = p4-1;
              if (_contact_sub_algo ==2) { // 3D Only : contact edges (sub ctc elt) are built from faces
                if (!pt2CtcRef[p1-1] || !pt2CtcRef[p2-1]) _nb_CTC_ELT[SUBCTCELT][j]++;
                if (!pt2CtcRef[p2-1] || !pt2CtcRef[p3-1]) _nb_CTC_ELT[SUBCTCELT][j]++;
                if (!pt2CtcRef[p3-1] || !pt2CtcRef[p4-1]) _nb_CTC_ELT[SUBCTCELT][j]++;
                if (!pt2CtcRef[p4-1] || !pt2CtcRef[p1-1]) _nb_CTC_ELT[SUBCTCELT][j]++;
              }
              
              if (!pt2CtcRef[p1-1]) { pt2CtcRef[p1-1] = j+1;  _nb_CTC_ELT[POINTELT][j]++; }
              else if ( (pt2CtcRef[p1-1] != j+1) && (verbose)) {
                cout << "Intersection Point " << p1 << " is a potential contact point with both references " <<
                _contact_ref[pt2CtcRef[p1-1]-1] << " and " << _contact_ref[j] << endl;
                fatal_error = true;
              }
              
              if (!pt2CtcRef[p2-1]) { pt2CtcRef[p2-1] = j+1;  _nb_CTC_ELT[POINTELT][j]++; }
              else if ( (pt2CtcRef[p2-1] != j+1) && (verbose)) {
                cout << "Intersection Point " << p2 << " is a potential contact point with both references " <<
                _contact_ref[pt2CtcRef[p2-1]-1] << " and " << _contact_ref[j] << endl;
                fatal_error = true;
              }
              
              if (!pt2CtcRef[p3-1]) { pt2CtcRef[p3-1] = j+1;  _nb_CTC_ELT[POINTELT][j]++; }
              else if ( (pt2CtcRef[p3-1] != j+1) && (verbose)) {
                cout << "Intersection Point " << p3 << " is a potential contact point with both references " <<
                _contact_ref[pt2CtcRef[p3-1]-1] << " and " << _contact_ref[j] << endl;
                fatal_error = true;
              }
              
              if (!pt2CtcRef[p4-1]) { pt2CtcRef[p4-1] = j+1;  _nb_CTC_ELT[POINTELT][j]++; }
              else if ( (pt2CtcRef[p4-1] != j+1) && (verbose)) {
                cout << "Intersection Point " << p4 << " is a potential contact point with both references " <<
                _contact_ref[pt2CtcRef[p4-1]-1] << " and " << _contact_ref[j] << endl;
                fatal_error = true;
              }
            }
          }
        }
      }
      if (fatal_error) {
        cout << "---- Fatal error in Solid::init : one should check intersection points listed above" << endl;
        exit(1);
      }
      quadrilaterals=true;
    }
    
    
    
    if ( line.find( "Edges" ) != string::npos ){
      cout << "Reading Edges " << endl;
      nextIntINRIAMeshField( line.substr( line.find_last_of( "s" ) + 1 ), mystream );
      for ( i = 0;i < nBEd;i++ ){
        mystream >> p1 >> p2 >> ibc;
        _edge[i].init(i,_point[p1-1],_point[p2-1],ibc);
        if(verbose>1) cout << _edge[i] << endl;
        if (_contact_algo == 2) {
          if (_dim == 2) {
            for (size_t j = 0; j < _nb_contact_ref ; j++) {
              if (_edge[i].marker() == _contact_ref[j]) {
                _nb_CTC_ELT[CTCELT][j]++;
                if (! pt2CtcRef[_edge[i].pt1().id()]) {  pt2CtcRef[_edge[i].pt1().id()] = j+1; _nb_CTC_ELT[POINTELT][j]++; }
                if (! pt2CtcRef[_edge[i].pt2().id()]) {  pt2CtcRef[_edge[i].pt2().id()] = j+1; _nb_CTC_ELT[POINTELT][j]++; }
              }
            }
          }
        }
      }
    }
  }
  
  // Building of Contact elements (faces in 3d, edges in 2d) and sub contact elements (edges in 3d)
  _nb_ctc_elt = 0;
  _nb_sub_ctc_elt = 0;
  _nb_ctc_pt = 0;
  
  for (size_t k = 0 ; k < 3 ; k++) {
    _offset_CTC_ELT[k].resize(_nb_contact_ref+1);
    _offset_CTC_ELT[k][0] = 0;
    index_CTC_ELT[k].resize(_nb_contact_ref);
  }
  
  for (size_t j = 0 ; j < _nb_contact_ref ; j++) {
    _nb_ctc_elt     += _nb_CTC_ELT[CTCELT]   [j];
    _nb_sub_ctc_elt += _nb_CTC_ELT[SUBCTCELT][j];
    _nb_ctc_pt      += _nb_CTC_ELT[POINTELT] [j];
    
    for (size_t k = 0 ; k < 3 ; k++) {
      index_CTC_ELT[k][j] = _offset_CTC_ELT[k][j]; // index_CTC_ELT[k][j] is the current position to add a new contact element of type k and reference j
      _offset_CTC_ELT[k][j+1] = _offset_CTC_ELT[k][j] + _nb_CTC_ELT[k][j];
      // cout << "@@@ Offset " << k << " " << j+1 << " : " << _offset_CTC_ELT[k][j+1] << endl; // CRITICAL DEBUG
    }
  }
  
  if (verbose) {
    cout << "Current solid - Total number of contact elements : " << _nb_ctc_elt << endl;
    cout << "Current solid - Total number of sub-contact elements : " << _nb_sub_ctc_elt << endl;
    cout << "Current solid - Total number of contact points : " << _nb_ctc_pt << endl;
    
    for (size_t j = 0 ; j < _nb_contact_ref ; j++) {
      cout << "Current solid - Nb of contact     elts of reference " <<  _contact_ref[j] << " : " << _nb_CTC_ELT[CTCELT]   [j] <<  endl;
      cout << "Current solid - Nb of sub-contact elts of reference " <<  _contact_ref[j] << " : " << _nb_CTC_ELT[SUBCTCELT][j] <<  endl;
      cout << "Current solid - Nb of pt-contact  elts of reference " <<  _contact_ref[j] << " : " << _nb_CTC_ELT[POINTELT] [j] <<  endl;
    }
  }
  
  if (_contact_algo == 2) {
    
    // cout << _nb_ctc_elt << endl;
    if (_nb_ctc_elt > 0) {
      _ctc_elt = new ContactElement[_nb_ctc_elt];
      _CTC_ELT[CTCELT] = _ctc_elt;
    }
    // cout << _CTC_ELT[CTCELT] << endl;
    
    if (_nb_sub_ctc_elt > 0) { // Dim 3 only
      _sub_ctc_elt = new ContactElement[_nb_sub_ctc_elt];
      _CTC_ELT[SUBCTCELT] = _sub_ctc_elt;
    }
    
    if (_nb_ctc_pt > 0) {
      _ctc_pt = new ContactElement[_nb_ctc_pt];
      _CTC_ELT[POINTELT] = _ctc_pt;
    }
    
    if (_dim == 3) {
      if (_nb_ctc_elt > 0) { // 3D : Faces potentially in contact with nodes (quadrilaterals are splitted in triangles)
        size_t j = 0;
        for ( i = 0 ;i < numStoredFaces;i++ ){
          if (face2CtcRef[i]) {
            ibc = face2CtcRef[i] - 1;
            j = index_CTC_ELT[CTCELT][ibc];
            _ctc_elt[j].init  (j  ,_point[face2Node[0][i]],_point[face2Node[1][i]],_point[face2Node[2][i]],ibc);
            if(quadrilaterals) _ctc_elt[j+1].init(j+1,_point[face2Node[2][i]],_point[face2Node[3][i]],_point[face2Node[0][i]],ibc);
            if(verbose>1){
              cout << "Contact Elt (Faces) init -   " << endl;
              cout << "   " << _ctc_elt[j] << endl;
              if(quadrilaterals) cout << "   " << _ctc_elt[j+1] << endl;
            }
            index_CTC_ELT[CTCELT][ibc] += 1;
            if(quadrilaterals) index_CTC_ELT[CTCELT][ibc] += 1;
          }
        }
      }
      
      size_t nbPointsTriOrQuad;
      if(quadrilaterals) nbPointsTriOrQuad=3;
      else nbPointsTriOrQuad=2;
      
      if (_nb_sub_ctc_elt > 0) { // 3D : Edges potentially in contact with other edges //** 2
        size_t j = 0;
        size_t k,l;
        for (i = 0; i< _nb_point; i++) pt2CtcRef[i] = 0;
        for (i = 0;i < numStoredFaces;i++ ){
          if (face2CtcRef[i]) {
            ibc = face2CtcRef[i] - 1;
            for (k = 0 ; k<=nbPointsTriOrQuad ; k++) {
              l = (k+1)*(k<nbPointsTriOrQuad);
              if (!pt2CtcRef[face2Node[k][i]] || !pt2CtcRef[face2Node[l][i]]) {
                j = index_CTC_ELT[SUBCTCELT][ibc];
                _sub_ctc_elt[j].init(j, _point[face2Node[k][i]],_point[face2Node[l][i]],ibc);
                if(verbose>1) cout << "Sub Contact Elt (Edges) init - " << _sub_ctc_elt[j] << endl;
                index_CTC_ELT[SUBCTCELT][ibc]++;
              }
            }
            
            if (!pt2CtcRef[face2Node[0][i]]) pt2CtcRef[face2Node[0][i]] = ibc+1;
            if (!pt2CtcRef[face2Node[1][i]]) pt2CtcRef[face2Node[1][i]] = ibc+1;
            if (!pt2CtcRef[face2Node[2][i]]) pt2CtcRef[face2Node[2][i]] = ibc+1;
            if(quadrilaterals) if (!pt2CtcRef[face2Node[3][i]]) pt2CtcRef[face2Node[3][i]] = ibc+1;
          }
        }
      }
    }
    else {
      if (_nb_ctc_elt > 0) { // 2D : Edges potentially in contact with nodes
        size_t l;
        for ( i = 0 ; i < nBEd ; i++ ){
          ibc = _edge[i].marker();
          for (size_t k = 0 ; k < _nb_contact_ref ; k++) {
            if (ibc == _contact_ref[k]) {
              l = index_CTC_ELT[CTCELT][k];
              _ctc_elt[l].init(l,_point[_edge[i].pt1().id()],_point[_edge[i].pt2().id()],k);
              if(verbose>1) cout << "Contact Elt (Edges) init - " << _ctc_elt[l] << endl;
              index_CTC_ELT[CTCELT][k]++;
            }
          }
        }
      }
    }
    
    if (_nb_ctc_pt > 0) { // 2D & 3D : points potentially in contact with faces (3D) or edges (2D)
      size_t j;
      for (i = 0 ; i < _nb_point ; i++) {
        if (pt2CtcRef[i]) {
          ibc = pt2CtcRef[i] - 1;
          j = index_CTC_ELT[POINTELT][ibc];
          _ctc_pt[j].init(j ,_point[i],ibc);
          if (verbose > 1) cout << "Contact Point - init - " << _ctc_pt[j] << endl;
          index_CTC_ELT[POINTELT][ibc]++;
        }
      }
    }
  }
  
  //======================================================================
  //dx_init(_contact_algo);
  
}





void Solid::dx_init(int contact_algo)  {
  switch(contact_algo){
    case 0:
    {
      _L = meas(); // initial length
      _dx = _L / _nb_edge; // non-periodic case !
      
      break;
    }
    case 1:  case 3:
      
    {
      _L = meas(); // initial length
      _dx = _L / _nb_sub_ctc_elt; // non-periodic case !
      
      break;
    }
    case 2:
    {
      //_L = meas(); // initial length
      
      //_dx = _L / _nb_ctc_elt; // non-periodic case ! //ATTENTION FOR 2D ONLY
      
      // ATTENTION !!! APPROXIMATED WAY TO COMPUTE AVERAGE DX !!! M.A. 19/01/09
      size_t index(0);
      size_t typelt;
      double  dist;
      R3 temp1,temp2;
      
      _dx_ctc = new double [_nb_contact_ref];
      _L = 0;
      
      if (_dim == 3)
        typelt = SUBCTCELT;
      else
        typelt = CTCELT;
      
      for (size_t j = 0; j < _nb_contact_ref ; j++) {
        _dx_ctc[j] = 0;
        index = 0;
        for (size_t k = 0; k < _nb_CTC_ELT[typelt][j] ; k++) {
          for (size_t l = 0; l <  _dim-1 ; l++) {
            temp1 =  ((get_ctc_elt(j,typelt,k)).get_point(l));
            temp2 =  ((get_ctc_elt(j,typelt,k)).get_point(l+1));
            dist = 0;
            for (int dim = 0; dim < (int)_dim; dim++) {
              dist += (temp1[dim] - temp2[dim])*(temp1[dim] - temp2[dim]);
            }
            _dx_ctc[j] += sqrt(dist);
            _L += _dx_ctc[j];
            index++;
          }
        }
        
        _dx_ctc[j] = _dx_ctc[j] / index;
        
        if (_verbose) {
          cout << "Average dx-contact of current solid for reference " << j << " : " << _dx_ctc[j] << endl;
        }
        
        if (j==0)
          _dx = _dx_ctc[j];
        else
          _dx = min(_dx, _dx_ctc[j]);
      }
      /*
       if(_nb_edge !=0)
       {
       _L = meas(); // initial length
       _dx = _L / _nb_edge; // non-periodic case !
       }
       */
      break;
    }
      
    default:
      cout << "solid.cpp: Unknown contact algorithm " << _contact_algo <<  endl;
  }
  
  if(_verbose){
    cout << "Initial length = " << _L << endl;
    cout << "dx = " << _dx << endl;
  }
  
}

void Solid::setExternalForce(const double& fx,const double& fy,const double& fz,const double& )
{
  
  if (_dim == 3){
    cout << "Error in " << __FILE__ << " line " << __LINE__ << ": integration of an external force in 3D not yet implemented" << endl;
    exit(1);
  }

  R3 seg1, seg2;
  double dx1,dx2;
  seg1 =_point[1] - _point[0];
  dx1 = sqrt(seg1.norm()); // strange but norm is actually the norm squared
  external_force[ _nb_dof_per_point * 0 + 0 ] = fx*dx1/2.;
  external_force[ _nb_dof_per_point * 0 + 1 ] = fy*dx1/2.;
  external_force[ _nb_dof_per_point * 0 + 2 ] = fz*dx1/2.;
  for(size_t i=1;i<_nb_point-1;i++){
    seg1 =_point[i] - _point[i-1];
    dx1 = sqrt(seg1.norm()); // strange but norm is actually the norm squared
    seg2 =_point[i+1] - _point[i];
    dx2  = sqrt(seg2.norm()); // strange but norm is actually the norm squared
    external_force[ _nb_dof_per_point * i + 0 ] = fx*(dx1/2+dx2/2);
    external_force[ _nb_dof_per_point * i + 1 ] = fy*(dx1/2+dx2/2);
    external_force[ _nb_dof_per_point * i + 2 ] = fz*(dx1/2+dx2/2);
  }
  seg2 =_point[_nb_point-1] - _point[_nb_point-2];
  dx2  = sqrt(seg2.norm()); // strange but norm is actually the norm squared
  external_force[ _nb_dof_per_point * (_nb_point-1) + 0 ] = fx*dx2/2;
  external_force[ _nb_dof_per_point * (_nb_point-1) + 1 ] = fy*dx2/2;
  external_force[ _nb_dof_per_point * (_nb_point-1) + 2 ] = fz*dx2/2;
}

void Solid::meanForces(double& mean_contact_x, double& mean_contact_y, double& mean_contact_z,
                       double& mean_external_x, double& mean_external_y, double& mean_external_z){
  mean_contact_x = mean_contact_y = mean_contact_z = 0.;
  mean_external_x = mean_external_y = mean_external_z = 0.;

  for(size_t i=0;i<_nb_point;i++){
    /*
    mean_contact_x += total_force[ _nb_dof_per_point * i + 0 ] - external_force[ _nb_dof_per_point * i + 0 ];
    mean_contact_y += total_force[ _nb_dof_per_point * i + 1 ] - external_force[ _nb_dof_per_point * i + 1 ];
    mean_contact_z += total_force[ _nb_dof_per_point * i + 2 ] - external_force[ _nb_dof_per_point * i + 2 ];
     */
    mean_contact_x += contact_force[ _nb_dof_per_point * i + 0 ];
    mean_contact_y += contact_force[ _nb_dof_per_point * i + 1 ];
    mean_contact_z += contact_force[ _nb_dof_per_point * i + 2 ];
    mean_external_x += external_force[ _nb_dof_per_point * i + 0 ];
    mean_external_y += external_force[ _nb_dof_per_point * i + 1 ];
    mean_external_z += external_force[ _nb_dof_per_point * i + 2 ];
  }

  double measure = meas();
  mean_contact_x /= measure;
  mean_contact_y /= measure;
  mean_contact_z /= measure;
  mean_external_x /= measure;
  mean_external_y /= measure;
  mean_external_z /= measure;
}

double Solid::meas() const
{
  if (_dim == 3){
    cout << "Error in " << __FILE__ << " line " << __LINE__ << ": measure in 3D not yet implemented" << endl;
    exit(1);
  }
  double r=0.;
  for(size_t i=0;i<_nb_edge;i++) r += _edge[i].meas();
  return r;
}

void Solid::save(const string& filename, const size_t &ref) const
{
  ofstream f(filename.c_str());
  f.setf(ios_base::scientific);
  f.precision(10);
  bool save_ref_only = (ref > 0); // ref = 0 if all points should be saved.
  
  for(size_t i = 0; i < _nb_edge ; i++) {
    if (save_ref_only) if  (( (*this)( _edge[i].pt1().id() ).marker() != ref ) ||
                            ( (*this)( _edge[i].pt2().id() ).marker() != ref ) ) continue;
    f << _edge[i].pt1()[0]  << "  " <<  _edge[i].pt1()[1] << " " <<  _edge[i].pt1()[2] << endl;
    f << _edge[i].pt2()[0]  << "  " <<  _edge[i].pt2()[1] << " " <<  _edge[i].pt2()[2] << endl;
    f << endl; f << endl;
  }
  
  
  /* Following version in case where edges would not be available
   
   if ( (dim() == 2) || ( (dim() == 3) && (_nb_ctc_elt == 0)))  {
   if ( ref)  { // ref = 0 if all points should be saved.
   for(size_t i=0;i<_nb_point;i++)
   if ( (*this)(i).marker() == ref )
   f << (_point[i])[0]  << "  " << (_point[i])[1] << " " << (_point[i])[2] << endl;
   }
   else for(size_t i=0;i<_nb_point;i++)
   f << (_point[i])[0]  << "  " << (_point[i])[1] << " " << (_point[i])[2] << endl;
   }
   else {
   if (ref) {
   for (size_t i = 0; i < _nb_ctc_elt ; i++) {
   for (size_t k = 0 ; k < 3 ; k ++)
   if  ( _ctc_elt[i].get_point(k).marker() == ref)
   f << (_ctc_elt[i].get_point(k))[0] << " " << (_ctc_elt[i].get_point(k))[1] << " " << (_ctc_elt[i].get_point(k))[2] << endl;
   f << endl;
   f << endl;
   }
   }
   else {
   for (size_t i = 0; i < _nb_ctc_elt ; i++) {
   for (size_t k = 0 ; k < 3 ; k ++)
   f << (_ctc_elt[i].get_point(k))[0] << " " << (_ctc_elt[i].get_point(k))[1] << " " << (_ctc_elt[i].get_point(k))[2] << endl;
   f << endl;
   f << endl;
   }
   }
   }
   */
}

void Solid::applyDisplacement(double* disp, const double & coef)
{
  //  cout << "Entering applyDisplacement " << endl;
  for(size_t i=0;i<_nb_point;i++){
    for(int j=0; j<3;j++)
      _point[i].set_coor( _ref_point[i][j] + coef * disp[ _nb_dof_per_point*i+j],j);
    //    cout << "REF POINT [ " << i << " ] = " << _ref_point[i] << endl;
  }
}


void Solid::compact_mesh(const size_t &nbcoor, size_t id,const string& solver_name,size_t nbdof,const string& mesh_file, int verbose,   int &contact_algo, int &contact_sub_algo, int& contact_element_ref)
{
  //======================================================================
  // read a mesh file (adapted from LiFE-V)
  string line;
  // double x, y, z;
  size_t p1, p2,p3,p4, ibc;
  size_t nVe( 0 ), nBVe( 0 ), nBFa( 0 ), nVeNew( 0 );
  size_t numStoredFaces(0) , numVolumes (0) ;
  size_t nVo( 0 ), nBEd( 0 );
  size_t i;
  R3 vec_temp;
  vector<bool> Used;
  vector<size_t> tab,elt2node1, elt2node2, elt2node3, elt2node4;
  vector<double> coorX, coorY, coorZ;
  
  // --- STEP 1 : Identify the nodes that don't belong to any element : Used[n] = false
  //              Build the connectivity  new_node = tab[old_node]
  
  // open stream to read header
  ifstream hstream( mesh_file.c_str() );
  if (verbose){
    cout<<"Reading mesh file "<< mesh_file << endl;
  }
  
  if ( hstream.fail() ){
    cerr << " Error: File " << mesh_file
    << " not found or locked"
    << endl;
    abort();
  }
  if ( ! readINRIAMeshFileHead( hstream, nVe, nBVe, nBFa, nBEd, nVo,
                               numStoredFaces) ){
    cerr << " Error While reading INRIA mesh file headers" << endl;
    ABORT() ;
  }
  hstream.close();
  //Reopen the stream: I know it is stupid but this is how it goes
  ifstream mystream( mesh_file.c_str() );
  if ( mystream.fail() ){
    cerr << " Error : File " << mesh_file
    << " not found or locked" << endl;
    abort();
  }
  
  Used.resize(nVe);
  tab.resize(nVe);
  coorX.resize(nVe);
  coorY.resize(nVe);
  coorZ.resize(nVe);
  
  while ( next_good_line( mystream, line ).good() ){
    if ( line.find( "Vertices" ) != string::npos ){
      nextIntINRIAMeshField( line.substr( line.find_last_of( "s" ) + 1 ),mystream );
      for ( i = 0;i < nVe;i++ ){
        Used[i] = false;
        mystream >> vec_temp[0] >> vec_temp[1] >> vec_temp[2] >> ibc;
        coorX[i] = vec_temp[0];
        coorY[i] = vec_temp[1];
        coorZ[i] = vec_temp[2];
      }
    }
    
    if ( line.find( "Tetrahedra" ) != string::npos )
    {
      numVolumes = nextIntINRIAMeshField( line.substr( line.find_last_of( "a" ) + 1 ), mystream );
      elt2node1.resize(numVolumes);
      elt2node2.resize(numVolumes);
      elt2node3.resize(numVolumes);
      elt2node4.resize(numVolumes);
      
      for ( i = 0;i < numVolumes;i++ )
      {
        mystream >> p1 >> p2 >> p3 >> p4 >> ibc;
        elt2node1[i] = p1 -1;
        elt2node2[i] = p2 -1;
        elt2node3[i] = p3 -1;
        elt2node4[i] = p4 -1;
        
        
        Used[p1-1] = true;
        Used[p2-1] = true;
        Used[p3-1] = true;
        Used[p4-1] = true;
        
      }
    }
  }
  
  for (i = 0 ; i < nVe ; i++)
    if (Used[i]) tab[i] = nVeNew++;
  
  // --- STEP 2 : write the nodes
  cout << "MeshVersionFormatted 1 " << endl;
  cout << "Dimension" << endl;
  cout << " 3" << endl;
  cout << "# Mesh generated by compactMesh " << endl;
  cout << endl;
  cout << "Vertices" << endl;
  cout << nVeNew << endl;
  
  size_t nVeProv (0) ;
  for (i = 0 ; i < nVe ; i++)
    if (Used[i])
      cout << 1+nVeProv++ << " " << coorX[i] << " " << coorY[i] << " " << coorZ[i]  << endl;
  
  
  // --- STEP 3 : write the elements
  cout << "Tetrahedra" << endl;
  cout << numVolumes << endl;
  for (i = 0 ; i < numVolumes ; i++)
    cout << i+1 << " " << tab[elt2node1[i]]+1 << " " << tab[elt2node2[i]]+1 << " " << tab[elt2node3[i]]+1 << " " << tab[elt2node4[i]]+1 << endl;
  
}



