//===========================================================
//                    MultiContact
//
// Part of MasterFSIContact,             Copyright Inria 2018
//===========================================================
/*!
 \author Jean-Frederic Gerbeau
 */
#ifndef _MESH_HPP_
#define _MESH_HPP_


#define ABORT() abort()

# define ASSERT0(X,A) if ( !(X) ) \
ERROR_MSG(A << endl << "Error in file" << __FILE__ << " line " << __LINE__) ;

# define ERROR_MSG(A)  \
do { cerr << endl << endl << A << endl << endl ; ABORT() ; } while (0)

# define ASSERT_PRE0(X,A) if ( !(X) ) \
ERROR_MSG(A << endl << "Precondition Error " << "in file " << __FILE__ \
<< " line " << __LINE__) ;

#include <string>
#include <iostream>
#include <fstream>
#include <stdlib.h>

using namespace std;

istream & eatline( istream & s );
istream & eat_comments(istream & s );
istream & next_good_line( istream & s, string & line );
int nextIntINRIAMeshField( string const & line, istream & mystream );
bool readINRIAMeshFileHead( ifstream & mystream,
                           size_t & numVertices,
                           size_t & numBVertices,
                           size_t & numBFaces,
                           size_t & numBEdges,
                           size_t & numVolumes,
                           size_t & numStoredFaces);
#endif
