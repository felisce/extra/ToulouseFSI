//===========================================================
//                    MultiContact
//
// Part of MasterFSIContact,             Copyright Inria 2018
//===========================================================
/*!
 \author Jean-Frederic Gerbeau
 */
#ifndef _CONTACT_ELEMENT_HPP_
#define _CONTACT_ELEMENT_HPP_

#include <iostream>
#include "point.hpp"

#define CTCELT    0
#define SUBCTCELT 1
#define POINTELT  2


using namespace std;

class ContactElement
{
  Point  *_points [3];
  int _id;
  int _marker;
public:
  ContactElement(){};
  void init(const int& id,Point& pt1,const int& marker);
  void init(const int& id,Point& pt1,Point& pt2,const int& marker);
  void init(const int& id,Point &pt1,Point& pt2,Point &pt3 , const int& marker);
  inline const Point  & get_point(size_t j) const { return  *_points[j];};
  inline const size_t & get_idnode(size_t j) const {return (*_points[j]).id();};
  inline double meas() {
    return euclidNorm(*_points[0] - *_points[1]);};
  
  friend ostream& operator <<(ostream& c,const ContactElement & ed);
};
#endif
