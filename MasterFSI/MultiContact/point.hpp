//===========================================================
//                    MultiContact
//
// Part of MasterFSIContact,             Copyright Inria 2018
//===========================================================
/*!
 \author Jean-Frederic Gerbeau
 */
#ifndef _POINT_HPP_
#define _POINT_HPP_

#include <iostream>
#include <vector>
#include "Rn.hpp"

using namespace std;


class Point:
public R3
{
  size_t _id;
  size_t _marker;
public:
  Point():R3(),_id(0),_marker(0){};
  Point(const int& id,const R3& coor0, const int& marker):
  R3(coor0),_id(id),_marker(marker){};
  void init(const size_t& id,const R3& coor0,const size_t& marker);
  inline const size_t& marker() const{return _marker;};
  inline const size_t& id() const{return _id;};
  
  friend ostream& operator <<(ostream& c,const Point& pt);
  
  inline void  set_point(const R3 & coor0){
    for(int i=0; i<this->size();i++) (*this)[i]=coor0[i];};
  
  inline void  set_coor(const double & val, const int  & i) {(*this)[i]=val;  };
  
};

inline const  R3   operator- (const Point & vec1,const Point & vec2) {
  R3 result;
  for(int i=0; i<result.size();i++) result[i]=vec1[i]-vec2[i];
  return result;
}


inline const  R3   operator+ (const Point & vec1,const Point & vec2) {
  R3 result;
  for(int i=0; i<result.size();i++) result[i]=vec1[i]+vec2[i];
  return result;
}

inline const  R3   operator- (const Point & vec1,const R3 & vec2) {
  R3 result;
  for(int i=0; i<result.size();i++) result[i]=vec1[i]-vec2[i];
  return result;
}

inline const  R3   operator- (const R3 & vec1, const Point & vec2) {
  R3 result;
  for(int i=0; i<result.size();i++) result[i]=vec1[i]-vec2[i];
  return result;
}

#endif
