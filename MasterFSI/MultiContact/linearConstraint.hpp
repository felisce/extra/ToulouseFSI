//===========================================================
//                    MultiContact
//
// Part of MasterFSIContact,             Copyright Inria 2018
//===========================================================
/*!
 \author Jean-Frederic Gerbeau
 */
#ifndef _LINEARCONSTRAINT_HPP_
#define _LINEARCONSTRAINT_HPP_

#include <vector>
#include <iostream>
#include "vecteur.hpp"
#include "solid.hpp"
#include "dataFile.hpp"

using namespace std;

class LinearConstraint
{
  // F(X) = 0
  // F_i(X) = \sum_{j=0;j<_nb_nodes} _coef[i][j] X_j \cdot e_i
  // F = (F_i)_{i=0,..,_nb_coor - 1}
  Solid* _phi;
  size_t _nb_node;
  vector<size_t> _node;
  size_t _nb_coor;
  double* _coef; // array _nb_coor x _nb_node
  double _p0;
  double _p1; // Lagrange multiplier for tg problem
  int _verbose;
  double _last_algebraic_residual;
  
public:
  LinearConstraint(Solid& phi,size_t ref,int nb_coor,int icoor);
  void addForce() const;
  void addForceTG() const;//force added on tg problem
  double residual();
  void descent(double const& step);
  void descentTG(double const& step);
  inline void init() {_p0 = 0;};
  
  inline const double & get_last_algebraic_residual() { return   _last_algebraic_residual;};
  inline const size_t  & get_nb_node() {return _nb_node;};
  inline const double & get_coef(int icoor,int node) {return _coef[icoor * _nb_node + node];};
  inline const Solid * get_solid() { return _phi;};
  //  inline double & get_lambda() { return _p0;};
};
#endif
