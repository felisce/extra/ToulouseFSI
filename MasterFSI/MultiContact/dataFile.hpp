//===========================================================
//                    MultiContact
//
// Part of MasterFSIContact,             Copyright Inria 2018
//===========================================================
/*!
 \author Jean-Frederic Gerbeau
 */
#ifndef _DATAFILE_H
#define _DATAFILE_H

#include <string>
#include <iostream>
#include "getPot.hpp"
#include "messageAdmStruct.hpp"
#include "messageAdmMasterFSI.hpp"


using namespace std;

class DataFile
{
protected:
  int _verbose;
  int _verbose_ms; // to be used also in MessageAdmStruct and MessageAdmMaster
  int _protocol; // to be used for MessageAdmStruct and MessageAdmMaster
  int _unit;     // to be used for MessageAdmStruct and MessageAdmMaster
  int _initializationType;
  size_t _nb_coor;
  int _contact_algo;
  int _with_nlopt; // 0: home-made Uzawa, 1: nlopt library
  int _contact_sub_algo;
  size_t _nb_solid;
  string _mesh_dir;
  vector<string> _solver_path;
  vector<string> _mesh_file;
  vector<string> _socket_transport; // for the solids
  vector<string> _socket_address;  // for the solids
  vector<string> _socket_port;  // for the solids
  string _my_socket_transport; // for multistruct (in slave mode)
  string _my_socket_address; // for multistruct (in slave mode)
  string _my_socket_port; // for multistruct (in slave mode)
  vector<int> _nb_dof_per_point;
  vector<size_t> _nb_contact_ref;
  vector<size_t *> _contact_ref;
  size_t _nb_attach;
  vector<int> _attach_solid;
  vector<int> _nb_attach_ref;
  bool _init_stretch;
  vector<double *> _attach_dist;
  vector<size_t *> _attach_constraint_reference;
  vector<double> _attach_coor;
  size_t _nb_barycenter_constraint;
  vector<int> _barycenter_constraint_solid;
  vector<int> _barycenter_constraint_reference;
  double _gap;
  int _slave_mode;
  double _tolerance;
  double _tolerance_domain;
  double _tolerance_nlopt;
  double _wall_coef[3];
  double _descent_step;
  double _omega_descent_step;
  int _iter_lambda_max;
  int _iter_convex_max;
  bool _aitken_uzawa;
  bool _aitken_convex;
  bool _useAcceleratedUzawa;
  size_t _max_iter;
  double _dt;
  double _force[3];
  string _post_dir;
  int _post_period;
  int _gnuplot;
  int _gnuplot_ref;
  int _ensight;
  bool _png_output;
public:
  DataFile(const string& data_file_name);
  inline const size_t& get_nb_coor(){return _nb_coor;};
  const size_t& maxIter(){return _max_iter;};
  const int& slaveMode(){return _slave_mode;};
  const double& dt() const{return _dt;};
  const int& contactAlgo() const{return _contact_algo;};
};
#endif
