//===========================================================
//                    MultiContact
//
// Part of MasterFSIContact,             Copyright Inria 2018
//===========================================================
/*!
 \author Jean-Frederic Gerbeau
 */
#ifndef _SIMPLECONVEX_HPP_
#define _SIMPLECONVEX_HPP_

#include <vector>
#include <iostream>
#include "vecteur.hpp"
#include "solid.hpp"
#include <nlopt.hpp>

using namespace std;

class SimpleConvex
{
  // Equation of the line : a x + b y + c = 0
  // Any point (x_P,y_P) of the solid is assumed
  // to be on the side of line such that a x_P + b y_P + c > 0
  // The coefficient are divided by sqrt(a*a + b*b),
  // thus dist(P,line) = a x_P + b y_P + c
  double _gap;
  bool _admissible;
  int _verbose;
  Solid* _phi;
  vector<double>* _p0; // lambda
  vector<double> _p00; // lambda_old
  vector<double> _lambda;
  vector<double> _lambda0;
  vector<double> _omega;
  
  double tau;
  double tau0;
  double _a;
  double _b;
  double _c;
  R3 _normal_wall;
  double _str;
  
public:
  SimpleConvex(){}
  void init(Solid& phi,const double& gap,const double& coef_a,
            const double& coef_b,const double& coef_c, const int &verbose);
  void setStructureEnergy(const double& StructureEnergy);
  // friend ostream& operator<<(ostream& c,const SimpleConvex& cvx);
  double residual() const;
  void descent(double const& step);
  //  double residual_acceleratedUzawa() const;
  void descent_acceleratedUzawa(double const& step);
  void addContactForce() const;
  //  double update();
  //bool admissibleUpdate();
  //////modified for nlopt///////
  double corr_dual_energy();
  vector<double> grad_dual_energy();
  void _xToLambda(vector<double> x, int);
};

double nlopt_wrapper(const vector<double> &x, vector<double> &grad, void *my_func_data);

typedef struct {
  double _c;  
  double _str;
  int n;  //no. of points
  double _gap;     
  vector<double> _ps;
  int nlopt_counter;
} my_function_data;

#endif




