//===========================================================
//                    MultiContact
//
// Part of MasterFSIContact,             Copyright Inria 2018
//===========================================================
/*!
 \author Jean-Frederic Gerbeau
 */
#include "vecteur.hpp"
#include "convex.hpp"
#include "util_geometrie.hpp"

void Convex::init(Solid& phi,const size_t & phiRef, Solid& psi,const size_t & psiRef, const double& gap,const int &verbose)
{
  _gap = gap;
  _verbose = verbose;
  _admissible = true;
  _phi = &phi;
  _phi_ref = phiRef;
  _psi = &psi;
  _psi_ref = psiRef;
  //    _dx = min ( phi.dx(phiRef) , psi.dx(psiRef));
  _dx = min ( phi.dx() , psi.dx());
  meas=_dx;
  
  if (_psi->dim() == 3) meas *= meas;
  
  if (_verbose) cout << "Convex (" << phiRef << "," << psiRef << ") : dx = " << _dx << endl;
  
  init_(CTCELT   ,1,3,phi.nbCtcPt(phiRef)    , psi.nbCtcElt(psiRef));
  init_(SUBCTCELT,2,2,phi.nbSubCtcElt(phiRef), psi.nbSubCtcElt(psiRef));
  
  update();
}

void Convex::init_(const size_t & typelt,const int & NI,const int & NJ,const int & imax,const int & jmax) {
  // In the following : typelt = 0 is the       contact : Point / ( Face in 3D, Edge in 2D) ; hence NI = 1, NJ = 3 or 2.
  //                    typelt = 1 is the (sub) contact : Edge (3D only) / Edge (3D only)   ; hence NI = NJ = 2.
  
  _THETA[typelt] = new matrice<R3>(imax,jmax);
  
  for (int ni = 0 ; ni < NI ; ni++) {
    for (int nj = 0 ; nj < NJ ; nj++) {
      
      // WARNING: should not be there any delete in the destructor class? Possibly memory leaks?
      
      _P[typelt][ni*NJ+nj] = new matrice<double>(imax,jmax);
      // next 4 for aitken iterations
      _Pold[typelt][ni*NJ+nj]=new matrice<double>(imax,jmax);
      _Ptilde[typelt][ni*NJ+nj]=new matrice<double>(imax,jmax);
      _Poldold[typelt][ni*NJ+nj]=new matrice<double>(imax,jmax);
      _Ptildeold[typelt][ni*NJ+nj]=new matrice<double>(imax,jmax);
      
      // the following matrices and vectors are used for the accelerated Uzawa method with restart
      _P0[typelt][ni*NJ+nj]=new matrice<double>(imax,jmax); 
      _lambda[typelt][ni*NJ+nj]=new matrice<double>(imax,jmax); 
      _lambda0[typelt][ni*NJ+nj]=new matrice<double>(imax,jmax);
      _omega[typelt][ni*NJ+nj]=new matrice<double>(imax,jmax);    
      
      _diff[typelt][ni*NJ+nj]=new matrice<double>(imax,jmax); 
      
      _sum[typelt][ni*NJ+nj]=new vector<double>(jmax);
      _tau[typelt][ni*NJ+nj]=new vector<double>(jmax);
      _tau0[typelt][ni*NJ+nj]=new vector<double>(jmax);
      
      // set initial values
      for(int i=0;i < imax ; i++) {
        for (int j=0;j < jmax  ; j++) {
          (*_P[typelt][ni*NJ+nj])(i,j)=0.;
          (*_Pold[typelt][ni*NJ+nj])(i,j)=0.;
          (*_Ptilde[typelt][ni*NJ+nj])(i,j)=0.;
          (*_Poldold[typelt][ni*NJ+nj])(i,j)=0.;
          (*_Ptildeold[typelt][ni*NJ+nj])(i,j)=0.;
          
          (*_lambda[typelt][ni*NJ+nj])(i,j)=0.;
          (*_lambda0[typelt][ni*NJ+nj])(i,j)=0.;
          (*_omega[typelt][ni*NJ+nj])(i,j)=0.;
          
          (*_diff[typelt][ni*NJ+nj])(i,j)=0.;
          
        }
      }
      
      for (int j=0;j < jmax  ; j++) {
        (*_sum[typelt][ni*NJ+nj])[j] = 0.; 
        (*_tau0[typelt][ni*NJ+nj])[j] = 1.0;               
      }
      
    }
  }
  
}

double Convex::update()
{
  
  if (_verbose > 1) cout << "Entering Convex::update" << endl;
  
  double r;
  
  
  r = update_(POINTELT ,CTCELT   ,1,3,_phi->nbCtcPt(_phi_ref)    ,_psi->nbCtcElt(_psi_ref));
  r+= update_(SUBCTCELT,SUBCTCELT,2,2,_phi->nbSubCtcElt(_phi_ref),_psi->nbSubCtcElt(_psi_ref));
  
  return r*meas;
}

double  Convex::update_(const size_t & typelt0,const size_t & typelt,const int & NI,const int & NJ,const int & imax,const int & jmax) {
  
  double r(0);
  bool Dim2;
  R3 thetaprec , temp ;
  Solid &phi = *_phi;
  Solid &psi = *_psi;
  
  for(int i=0;i<imax;i++){
    for(int j=0;j<jmax;j++){
      thetaprec = (*_THETA[typelt])(i,j);
      
      if (typelt == SUBCTCELT) {
        projectionUnitVectorSub(phi(_phi_ref,typelt0,0,i),phi(_phi_ref,typelt0,1,i),
                                psi(_psi_ref,typelt,0,j),psi(_psi_ref,typelt,1,j),
                                (*_THETA[typelt])(i,j));
      }
      else {
        Dim2 =(_psi->dim() == 2);
        projectionUnitVector(Dim2, phi(_phi_ref,typelt0,0,i),psi(_psi_ref,typelt,0,j),
                             psi(_psi_ref,typelt,1,j),psi(_psi_ref,typelt,2,j),
                             (*_THETA[typelt])(i,j));
      }
      
      for (int l = 0 ; l < 3; l++)  temp[l]  = (*_THETA[typelt])(i,j)[l] - thetaprec[l];
      for (int ni = 0 ; ni < NI; ni++)
        for (int nj = 0 ; nj < NJ; nj++)
          r += abs( (*_P[typelt][ni*NJ+nj])(i,j) *  ( temp , phi(_phi_ref,typelt0,ni,i)-psi(_psi_ref,typelt,nj,j)));
    }
  }
  return r;
}

double Convex::residual() const{
  if (_verbose > 1) cout << "Entering Convex::residual" << endl;
  
  double r;
  
  r  = residual_(POINTELT ,CTCELT   ,1,3,_phi->nbCtcPt(_phi_ref)    ,_psi->nbCtcElt(_psi_ref));
  r += residual_(SUBCTCELT,SUBCTCELT,2,2,_phi->nbSubCtcElt(_phi_ref),_psi->nbSubCtcElt(_psi_ref));
  
  return r*meas;
}


double Convex::residual_(const size_t & typelt0,const size_t & typelt,const int & NI,const int & NJ,const int & imax,const int & jmax) const
{
  double F , F_moins , F_plus, r;
  size_t  nbj;
  double  t, s;
  R3 temp;
  Solid &phi = *_phi;
  Solid &psi = *_psi;
  
  r = 0;
  for (int i=0;i<imax;i++){
    nbj=0;
    t=0;
    for (int j=0;j<jmax;j++){
      s = 0;
      for (int ni =0; ni <NI ; ni++) {
        for (int nj =0; nj <NJ ; nj++) {
          temp= phi(_phi_ref,typelt0,ni,i) - psi(_psi_ref,typelt,nj,j);
          F = ( (*_THETA[typelt])(i,j) , temp) - _gap;
          F_moins = -min(F,0);
          F_plus  = F + F_moins;
          
          s += F_moins + F_plus * double( (*_P[typelt][ni*NJ+nj])(i,j) > 0.);
        }
        t += s;   //original
        if (s!=0) nbj++;
      }                         //    r+=t/max(2,nbj);
      //t += s;    //to test residual calculation
    }                            //cout<<"r="<<r*_phi->dx()<<endl;
    r+=t;
  }
  return r;
}

/*
 double Convex::residual_acceleratedUzawa() const{
 if (_verbose > 1) cout << "Entering Convex::residual" << endl;
 
 double r;
 
 r  = residual_acceleratedUzawa(POINTELT ,CTCELT   ,1,3,_phi->nbCtcPt(_phi_ref)    ,_psi->nbCtcElt(_psi_ref));
 r += residual_acceleratedUzawa(SUBCTCELT,SUBCTCELT,2,2,_phi->nbSubCtcElt(_phi_ref),_psi->nbSubCtcElt(_psi_ref));
 
 return r*meas;
 }
 
 
 double Convex::residual_acceleratedUzawa_(const size_t & typelt0,const size_t & typelt,const int & NI,const int & NJ,const int & imax,const int & jmax) const
 {
 double F , F_moins , F_plus, r;
 size_t  nbj;
 double  t, s;
 R3 temp;
 Solid &phi = *_phi;
 Solid &psi = *_psi;
 
 // need to be implemented, see method "residual_acceleratedUzawa" from simpleConvex.cpp file for inspiration
 
 
 return r;
 }
 */

void Convex::descent(const double & step, const bool & aitken, const bool & activate_aitken )
{
  if (_verbose > 1) cout << "Entering Convex::descent" << endl;
  
  descent_(aitken,POINTELT ,CTCELT   ,1,3,_phi->nbCtcPt(_phi_ref)    ,_psi->nbCtcElt(_psi_ref),step);
  
  descent_(aitken,SUBCTCELT,SUBCTCELT,2,2,_phi->nbSubCtcElt(_phi_ref),_psi->nbSubCtcElt(_psi_ref),step);
  
  if(activate_aitken)
  {
    relaxation(CTCELT,1,3,_phi->nbCtcPt(_phi_ref) ,_psi->nbCtcElt(_psi_ref));
    relaxation(SUBCTCELT,2,2,_phi->nbSubCtcElt(_phi_ref),_psi->nbSubCtcElt(_psi_ref));
  }
  
}

void Convex::descent_(const bool & aitken, const size_t &  typelt0,const size_t & typelt,const int & NI,const int & NJ,const int & imax,const int & jmax, const double & step)  {
  
  R3 temp;
  size_t nbj;
  Solid &phi = *_phi;
  Solid &psi = *_psi;
  
  for (int i=0;i<imax;i++){
    /*
     On compte le nombre de segment avec lesquels le point i est en interaction
     On divise le pas de mise a jour du multiplicateur par le nombre obtenu,
     de sorte que le comportement soit independant du nombre de points de discretisation
     */
    
    nbj=0;
    for (int j=0;j<jmax;j++) {
      for (int ni =0; ni <NI ; ni++) {
        for (int nj =0; nj <NJ ; nj++) {
          temp= phi(_phi_ref,typelt0,ni,i) -psi(_psi_ref,typelt,nj,j);
          if ( ((*_THETA[typelt])(i,j),temp) < _gap)  nbj++;
        }
      }
    }
    
    nbj=max(2,(int)nbj);
    
    
    for (int j=0;j<jmax;j++) {               // old descent_(step/nbj,i,j);
      for (int ni = 0 ; ni < NI; ni++) {
        for (int nj = 0 ; nj < NJ; nj++) {
          temp=phi(_phi_ref,typelt0,ni,i)-psi(_psi_ref,typelt,nj,j);
          if(aitken)
          {
            
            (*_Poldold[typelt][ni*NJ+nj])(i,j) =(*_Pold[typelt][ni*NJ+nj])(i,j);
            (*_Pold[typelt][ni*NJ+nj])(i,j) =(*_P[typelt][ni*NJ+nj])(i,j);
            
            (*_P[typelt][ni*NJ+nj])(i,j) -= step*( ((*_THETA[typelt])(i,j), temp ) - _gap) ;
            (*_P[typelt][ni*NJ+nj])(i,j) = max( 0., (*_P[typelt][ni*NJ+nj])(i,j) );
            
            (*_Ptildeold[typelt][ni*NJ+nj])(i,j) =(*_Ptilde[typelt][ni*NJ+nj])(i,j);
            (*_Ptilde[typelt][ni*NJ+nj])(i,j) =(*_P[typelt][ni*NJ+nj])(i,j);
          }
          else
          {
            (*_P[typelt][ni*NJ+nj])(i,j) -= (step /nbj) * ( ((*_THETA[typelt])(i,j), temp ) - _gap) ;
            (*_P[typelt][ni*NJ+nj])(i,j) = max( 0., (*_P[typelt][ni*NJ+nj])(i,j) );
          }
          
        }
      }
    }
  }
}

void Convex::descent_acceleratedUzawa(const double & step, const bool & aitken, const bool & activate_aitken )
{
  if (_verbose > 1) {
    cout << "Entering Convex::descent_acceleratedUzawa" << endl;
  }
  
  descent_acceleratedUzawa_(aitken,POINTELT ,CTCELT   ,1,3,_phi->nbCtcPt(_phi_ref)    ,_psi->nbCtcElt(_psi_ref),step);  
  descent_acceleratedUzawa_(aitken,SUBCTCELT,SUBCTCELT,2,2,_phi->nbSubCtcElt(_phi_ref),_psi->nbSubCtcElt(_psi_ref),step);
  
  /*  if(activate_aitken) // we do not relax here as the accelerated Uzawa is already a kind of relaxation
   {
   relaxation(CTCELT,1,3,_phi->nbCtcPt(_phi_ref) ,_psi->nbCtcElt(_psi_ref));
   relaxation(SUBCTCELT,2,2,_phi->nbSubCtcElt(_phi_ref),_psi->nbSubCtcElt(_psi_ref));
   } */
  
}

void Convex::descent_acceleratedUzawa_(const bool & aitken, const size_t &  typelt0,const size_t & typelt,const int & NI,const int & NJ,const int & imax,const int & jmax, const double & step)  {
  
  R3 temp;
  size_t nbj;
  Solid &phi = *_phi;
  Solid &psi = *_psi;
    
  for (int i=0;i<imax;i++){
    /*
     On compte le nombre de segment avec lesquels le point i est en interaction
     On divise le pas de mise a jour du multiplicateur par le nombre obtenu,
     de sorte que le comportement soit independant du nombre de points de discretisation
     */
    
    nbj=0;
    for (int j=0;j<jmax;j++) {
      for (int ni =0; ni <NI ; ni++) {
        for (int nj =0; nj <NJ ; nj++) {
          temp= phi(_phi_ref,typelt0,ni,i) -psi(_psi_ref,typelt,nj,j);
          if ( ((*_THETA[typelt])(i,j),temp) < _gap)  nbj++;
        }
      }
    }
    
    nbj=max(2,(int)nbj);
    
    for (int j=0;j<jmax;j++) {               // old descent_(step/nbj,i,j);
      for (int ni = 0 ; ni < NI; ni++) {
        for (int nj = 0 ; nj < NJ; nj++) {
          temp=phi(_phi_ref,typelt0,ni,i)-psi(_psi_ref,typelt,nj,j);
          // implement accelerated Uzawa method with restart here
          //(*_P[typelt][ni*NJ+nj])(i,j) -= (step /nbj) * ( ((*_THETA[typelt])(i,j), temp ) - _gap) ;
          //(*_P[typelt][ni*NJ+nj])(i,j) = max( 0., (*_P[typelt][ni*NJ+nj])(i,j) );
          (*_omega[typelt][ni*NJ+nj])(i,j) = - ((*_THETA[typelt])(i,j), temp ) + _gap;  
          (*_lambda[typelt][ni*NJ+nj])(i,j) = max(0., (*_omega[typelt][ni*NJ+nj])(i,j) * (step/nbj) + (*_P[typelt][ni*NJ+nj])(i,j));
          (*_P0[typelt][ni*NJ+nj])(i,j) = (*_P[typelt][ni*NJ+nj])(i,j); // _P0 is used to compute the residual in the "residual_acceleratedUzawa" method
          
        }
      }
    }
  } // end of loop on i
  
  for (int ni = 0 ; ni < NI ; ni++) {
    for (int nj = 0 ; nj < NJ ; nj++) {
      for (int j = 0; j < jmax; ++j) {
        (*_tau[typelt][ni*NJ+nj])[j] = 0.5 * (1 + sqrt(1 + 4 * (*_tau0[typelt][ni*NJ+nj])[j] * (*_tau0[typelt][ni*NJ+nj])[j] ) ); 
        (*_sum[typelt][ni*NJ+nj])[j] = 0.; 
        
        for (int i = 0; i < imax; ++i) {
          (*_diff[typelt][ni*NJ+nj])(i,j) = (*_lambda[typelt][ni*NJ+nj])(i,j) - (*_lambda0[typelt][ni*NJ+nj])(i,j);
          (*_sum[typelt][ni*NJ+nj])[j] += (*_diff[typelt][ni*NJ+nj])(i,j) * (*_omega[typelt][ni*NJ+nj])(i,j);
        }
        
        if ((*_sum[typelt][ni*NJ+nj])[j] >= 0) { // the restart is then not needed
          //cout << "no restart" << endl;
          for(int i=0; i < imax ; i++) {
            (*_P[typelt][ni*NJ+nj])(i,j) = (*_lambda[typelt][ni*NJ+nj])(i,j) + (((*_tau0[typelt][ni*NJ+nj])[j]-1)/((*_tau[typelt][ni*NJ+nj])[j])) * ((*_lambda[typelt][ni*NJ+nj])(i,j) - (*_lambda0[typelt][ni*NJ+nj])(i,j));
          }
        }  
        else { // the gradient has been inversed, then the restart is needed
          //cout << "restart" << endl;
          for(int i=0; i < imax ; i++) {
            (*_P[typelt][ni*NJ+nj])(i,j) = (*_lambda[typelt][ni*NJ+nj])(i,j);
          }
          (*_tau[typelt][ni*NJ+nj])[j] = 1.0;
        }
        
        for (int i=0; i < imax  ; i++) {
          (*_lambda0[typelt][ni*NJ+nj])(i,j) = (*_lambda[typelt][ni*NJ+nj])(i,j);
        }
        
        (*_tau0[typelt][ni*NJ+nj])[j] = (*_tau[typelt][ni*NJ+nj])[j];
        
      }
    }
  }
  
}

void Convex::relaxation(const size_t & typelt,const int & NI,const int & NJ,const int & imax,const int & jmax)
{
  double sum1=0.;
  double sum2=0.;
  
  for (int i=0;i<imax;i++)
    for (int j=0;j<jmax;j++)
      for (int ni =0; ni <NI ; ni++)
        for (int nj =0; nj <NJ ; nj++)
        {
          sum1+=((*_Pold[typelt][ni*NJ+nj])(i,j)-(*_Poldold[typelt][ni*NJ+nj])(i,j))*((*_Pold[typelt][ni*NJ+nj])(i,j)-(*_Ptilde[typelt][ni*NJ+nj])(i,j)-(*_Poldold[typelt][ni*NJ+nj])(i,j)+(*_Ptildeold[typelt][ni*NJ+nj])(i,j));
          
          sum2+=((*_Pold[typelt][ni*NJ+nj])(i,j)-(*_Ptilde[typelt][ni*NJ+nj])(i,j)-(*_Poldold[typelt][ni*NJ+nj])(i,j)+(*_Ptildeold[typelt][ni*NJ+nj])(i,j))*((*_Pold[typelt][ni*NJ+nj])(i,j)-(*_Ptilde[typelt][ni*NJ+nj])(i,j)-(*_Poldold[typelt][ni*NJ+nj])(i,j)+(*_Ptildeold[typelt][ni*NJ+nj])(i,j));
        }
  if (sum2>0)
  {
    
    if (_verbose > 1) cout<<"Applying Aitken relaxation to Uzawa iterations for contact"<<endl;
    
    _omega0 = sum1/sum2;
    
    if(_verbose>1 )  cout<<"step aitken = "<< _omega0<<endl;
    for (int i=0;i<imax;i++)
    {
      for (int j=0;j<jmax;j++)
        for (int ni = 0 ; ni < NI; ni++)
          for (int nj = 0 ; nj < NJ; nj++)
            (*_P[typelt][ni*NJ+nj])(i,j)=max(0.,_omega0*((*_Ptilde[typelt][ni*NJ+nj])(i,j))+(1-_omega0)*((*_Pold[typelt][ni*NJ+nj])(i,j)));
    }
    
  }
  else
  {
    for (int i=0;i<imax;i++)
    {
      for (int j=0;j<jmax;j++)
        for (int ni = 0 ; ni < NI; ni++)
          for (int nj = 0 ; nj < NJ; nj++)
            (*_P[typelt][ni*NJ+nj])(i,j)=0.;
    }
    
  }
  
}


void Convex::addContactForce() const
{
  if (_verbose > 1)
  {
    cout << "Entering Convex::addContactForce" << endl;
    cout << "*** addContactForce: (phi,psi) = (" << _phi->id() << "," << _psi->id() << ")"<< endl;
  }
  
  addContactForce_(POINTELT ,CTCELT   ,1,3,_phi->nbCtcPt(_phi_ref)    ,_psi->nbCtcElt(_psi_ref));
  addContactForce_(SUBCTCELT,SUBCTCELT,2,2,_phi->nbSubCtcElt(_phi_ref),_psi->nbSubCtcElt(_psi_ref));
}

void Convex::addContactForce_(const size_t & typelt0,const size_t & typelt,const int & NI,const int & NJ,const int & imax, const int & jmax) const {
  //cout << "------------------------------------------------------------------------------" << endl;
  //cout << "TO BE DONE: the array contact_force has to be filled here ! jfg, June 13, 2018" << __FILE__ << endl;
  //cout << "------------------------------------------------------------------------------" << endl;
  //exit(1);
  
  R3 f[2][3]; 
  
  int phidof, psidof;
  phidof = _phi->nbDofPerPoint();
  psidof = _psi->nbDofPerPoint();
  
  for(int i=0;i<imax;i++){
    for (int j=0;j<jmax;j++){
      
      for (int ni=0;ni<NI;ni++)
        for (int nj=0;nj<NJ;nj++)
          for (int l =0 ; l < 3; l ++)
            f[ni][nj][l]= -  ((*_P[typelt][ni*NJ+nj])(i,j)) * meas * ((*_THETA[typelt])(i,j)[l]);
      
      for (int l = 0 ;l < 3 ; l++) {
        for (int ni=0;ni<NI;ni++) {
          for (int nj=0;nj<NJ;nj++) { 
			_phi->total_force[phidof*  _phi->get_ctc_elt(_phi_ref,typelt0,i).get_idnode(ni) + l] -= f[ni][nj][l];
		    _phi->contact_force[phidof*  _phi->get_ctc_elt(_phi_ref,typelt0,i).get_idnode(ni) + l] -= f[ni][nj][l];
		  }	
        }
        
        for (int nj=0;nj<NJ;nj++) {
          for (int ni=0;ni<NI;ni++) { 
			_psi->total_force[psidof*  _psi->get_ctc_elt(_psi_ref,typelt,j).get_idnode(nj) + l] += f[ni][nj][l];
			_psi->contact_force[psidof*  _psi->get_ctc_elt(_psi_ref,typelt,j).get_idnode(nj) + l] += f[ni][nj][l];
          }
        }
      }
    }
  }
}


void Convex::admissibleUpdate_(size_t typelt0, size_t typelt,int NI, int NJ,int imax, int jmax) {
  R3 temp;
  Solid &phi = *_phi;
  Solid &psi = *_psi;
  
  for (int i=0;(_admissible && i<imax);i++){
    for (int j=0;(_admissible && j<jmax);j++){
      for (int ni=0;ni<NI;ni++) {
        for (int nj=0;nj<NJ;nj++) {
          temp = phi(_phi_ref,typelt0,ni,i)-psi(_psi_ref,typelt,nj,j);
          if ( ((*_THETA[typelt])(i,j) , temp) < 0 )  {
            _admissible=false;
            if (_verbose) {
              if (typelt == SUBCTCELT) cout << "!!!!! EDGE " << i << " AND EDGE " << j << " NON ADMISSIBLE : " << ((*_THETA[typelt])(i,j) , temp) << endl; else cout << "!!!!! NODE " << i << " AND ELT " << j << " NON ADMISSIBLE : " << ((*_THETA[typelt])(i,j) , temp) << endl;
              cout << "Search of other non admissible elt skipped " << endl;
            }
            continue;
          }
        }
      }
    }
  }
}

bool Convex::admissibleUpdate()
{
  if (_verbose > 1) cout << "Entering Convex::admissibleUpdate" << endl;
  
  _admissible=true;
  
  admissibleUpdate_(  POINTELT ,CTCELT   ,1,3,_phi->nbCtcPt(_phi_ref)    ,_psi->nbCtcElt(_psi_ref));
  if (_admissible)
    admissibleUpdate_(SUBCTCELT,SUBCTCELT,2,2,_phi->nbSubCtcElt(_phi_ref),_psi->nbSubCtcElt(_psi_ref));
  
  return _admissible;
}

double Convex::inZone(bool Dim2,const Point & M, const Point & P0, const Point & P1, const Point & P2c) {
  // returns a negative value if point M is in the part of the space opposed to P0
  // respective to the plane perpendicular to triangle (P0,P1,P2) and containing edge (P1,P2).
  // Criteria is (P1P2 ^P100).(P1P2 ^P1M(i)) < 0
  
  R3 P12 , temp1, temp2;
  
  P12 = P2c - P1;
  xprod( P12 , P0 - P1 , temp1);
  xprod( P12 , M   - P1 , temp2);
  
  //  cout << "Temp 1 " << temp1 << endl;
  //cout << "Temp 2 " << temp2 << endl;
  //cout << "Prod scal " << (temp1 , temp2) << endl;
  
  return  ( temp1 , temp2 ) ;
};

void Convex:: unitVector(const R3  & vec , R3 & nvec)   // double &nx, double &ny, double &nz)
{
  double epsilon = 1E-10;
  double non_null;
  
  nvec = vec;
  non_null = euclidNorm(vec);
  if (non_null < epsilon) {
    cout << "---- Fatal Error in unitVector : trying to normalize vector " << vec << endl;
    exit(1);
  }
  else nvec /= non_null;
};


double Convex::projectNodeOnEdge(
                                 const Point &M,
                                 const Point & P0,
                                 const Point & P1,
                                 const R3 & P01,
                                 R3 & Proj)
{
  // Computes in 3D the projection Proj of M on P0P1 and return the square of the norm of (M - Proj)
  // P01 is P0 - P1
  
  
  double coef;
  R3 temp;
  
  coef = ( P01 , M - P1 ) / (P01, P01);
  if (coef > 1) coef = 1 ; if (coef < 0) coef = 0;
  Proj = P01; Proj*= coef;  Proj +=P1;
  
  temp = M - Proj;
  return (temp ,temp);
}

void Convex::projectNodeOnEdge(
                               const Point &M,
                               const Point & P0,
                               const Point & P1,
                               const R3 & P01,
                               R3 & Proj,
                               R3 &nVec)
{
  // Computes in 3D the projection Proj of M on P0P1, and the unit vector of (M - Proj)
  // P01 is P0 - P1
  
  
  double coef;
  R3 temp;
  
  coef = ( P01 , M - P1 ) / (P01, P01);
  if (coef > 1) coef = 1 ; if (coef < 0) coef = 0;
  Proj = P01; Proj*= coef;  Proj +=P1;
  
  temp = M - Proj;
  unitVector(temp , nVec);
}


/* CUT and PASTE this to test
 cout << "-------------------------------------------------------------" << endl;
 cout << "---- TEST OF THE 3D PROJECTION ALGORITHM    " << endl;
 Point M,P0,P1,P2;
 R3 nvec;
 P0[0] = 1; P0[1] = 0; P0[2] = 0;
 P1[0] = 2; P1[1] = 0; P1[2] = 0;
 P2[0] = 1; P2[1] = 1; P2[2] = 0;
 M[0] = 0 ; M[1] = 0; M[2] = 0;
 cout << "Point to project :" << M << endl;
 projectionUnitVector( 0,M , P0 , P1 , P2 , nvec);
 cout << "Normal :" << nvec << endl;
 cout << "-------------------------------------------------------------" << endl;
 exit(1);
 */

void Convex::projectionUnitVector(
                                  bool Dim2 ,
                                  const Point & M,
                                  const Point & P0, const Point & P1, const Point & P2,
                                  R3 & nvec) {
  // Compute the unit vector nvec  between a point M and its projection on P0P1P2
  // In 2D, P2 = P0 and is useless
  
  R3 temp , Proj;
  Point P2c ;
  
  
  
  if (Dim2) {
    P2c[0] = P1[0];
    P2c[1] = P1[1];
    P2c[2] = P1[2] + 1;
  }
  else P2c = P2;
  
  if (  inZone(Dim2,M,P1,P0,P2c) < 0)                           // Zone 1 : projection is on P0 P2
    projectNodeOnEdge( M , P0 , P2c , P0 - P2c , Proj , nvec);
  else if (inZone(Dim2,M,P0,P1,P2c) < 0)                        // Zone 2 : projection is on P1 P2
    projectNodeOnEdge( M , P1 , P2c , P1 - P2c , Proj , nvec);
  else if ( (!Dim2) && (inZone(Dim2,M,P2c,P1,P0) < 0))         // Zone 3 : projection is on P0 P1
    projectNodeOnEdge( M , P0 , P1 , P0 - P1 , Proj , nvec);
  else {                                                       // Zone 4 : projection is on triangle
    xprod( P1 - P0 , P2c - P0 , temp );
    unitVector(temp , nvec);
    if  (  ( M - P0 , nvec ) < 0 ) {
      for (int k = 0; k < 3; k++) nvec[k] = - nvec[k];
    }
  }
}


/* CUT AND PASTE TO TEST
 cout << "-------------------------------------------------------------" << endl;
 cout << "---- TEST OF THE 3D PROJECTION ALGORITHM (EDGE/EDGE)   " << endl;
 Point A0,A1,B0,B1;
 R3 nvec;
 A0[0] = 1; A0[1] = 0; A0[2] = 0;
 A1[0] = 2; A1[1] = 0; A1[2] = 0;
 B0[0] = 1.5; B0[1] = 2; B0[2] = 0;
 B1[0] = 2.5; B1[1] = 2; B1[2] = 0;
 cout << "Edge A :" << A0 << "---" << A1 << endl;
 cout << "Edge B :" << B0 << "---" << B1 << endl;
 projectionUnitVectorSub(A0,A1,B0,B1,nvec);
 cout << "Normal :" << nvec << endl;
 cout << "-------------------------------------------------------------" << endl;
 exit(1);
 */

void Convex::projectionUnitVectorSub(
                                     const Point & A0,
                                     const Point & A1,
                                     const Point & B0,
                                     const Point & B1,
                                     R3 & nvec) {
  // Computes normal from SubCtcElt b = (B0,B1)  to a = (A0,A1)
  
  int verboseloc = _verbose-3;
  // static bool verboseloc = false;
  
  double epsilon = 1E-10;
  R3 temp0, temp1, temp2;
  R3 A01, B01, ProjA[2], ProjB[2];;
  double alpha, beta, sqA01, sqB01, A01B01, sqA01B01, A01colinearToB01;
  double dA[2], dB[2];
  int i_min[2];
  bool intersectionIsOutside;
  
  
  A01 = A0 - A1;
  sqA01 = (A01 , A01);
  B01 = B0 - B1;
  sqB01 = (B01 , B01);
  A01B01 = (A01 , B01);
  sqA01B01 = A01B01 * A01B01;
  
  A01colinearToB01 = sqA01 * sqB01- sqA01B01;
  intersectionIsOutside = true;
  
  /*
   cout << "A0 " << A0 << endl;
   cout << "A1 " << A1 << endl;
   cout << "A01 " << A01 << endl;
   cout << "sqA01 " << sqA01 << endl;
   cout << "B0 " << B0 << endl;
   cout << "B1 " << B1 << endl;
   cout << "B01 " << B01 << endl;
   cout << "sqB01 " << sqB01 << endl;
   cout << "A01B01 " << A01B01 << endl;
   */
  
  if (verboseloc>1) cout << "A01colinearToB01 " << A01colinearToB01 << endl;
  if (abs(A01colinearToB01) > epsilon) {
    temp0 = B1 - A1;
    temp1 = B01; temp1*= A01B01;
    temp2 = A01; temp2*= sqB01; temp2-=temp1;
    alpha = ( temp0 , temp2);
    alpha /= A01colinearToB01;
    if (verboseloc>1) cout << "ALPHA " << alpha << endl;
    if ( (alpha >= 0) && (alpha <= 1) ) {
      temp1 = B01; temp1*= sqA01;
      temp2 = A01; temp2*= A01B01; temp2-= temp1;
      beta = (temp0 , temp2);
      beta /= A01colinearToB01;
      if (verboseloc>1) cout << "BETA " << beta << endl;
      if ( (beta >= 0) && (beta <= 1) ) {
        intersectionIsOutside = false;
        temp1 = A01; temp1*= alpha; temp1+=A1;
        temp2 = B01; temp2*= beta; temp2+=B1; temp1-=temp2;
        unitVector(temp1, nvec);
        if (verboseloc>1) cout << "NVEC " << nvec << endl;
      }
    }
  }
  
  if (intersectionIsOutside) {
    dA[0] = projectNodeOnEdge(A0 , B0, B1, B01, ProjA[0]);    
    dA[1] = projectNodeOnEdge(A1 , B0, B1, B01, ProjA[1]);
    dB[0] = projectNodeOnEdge(B0 , A0, A1, A01, ProjB[0]);
    dB[1] = projectNodeOnEdge(B1 , A0, A1, A01, ProjB[1]);
    if (verboseloc>1) cout << "Distances when intersection is outside" << dA[0] << " " << dA[1] << " " << dB[0] << " " << dB[1] << endl;
    i_min[0] = (dA[0] > dA[1]);
    i_min[1] = (dB[0] > dB[1]);
    
    if ( dA[i_min[0]] < dB[ i_min[1]] ) {
      if (i_min[0]) unitVector( A1 - ProjA[1] , nvec);
      else          unitVector( A0 - ProjA[0] , nvec);
    }
    else{
      if (i_min[1])  unitVector( ProjB[1]- B1 , nvec);
      else           unitVector( ProjB[0]- B0 , nvec);
    }
    if (verboseloc>1) cout << "NVEC  " << nvec << endl;
  }
  
}


ostream& operator<<(ostream& c,const Convex& cvx)
{
  c << "Convex : " << endl;
  c << "  gap = " << cvx._gap << endl;
  c << "  admissible = " << cvx._admissible << endl;
  c << "  " << *cvx._phi << endl;
  c << "  " << *cvx._psi << endl;
  return c;
}


int Convex::_xToLambda(vector<double> x, int iterator)
{
	for(int i=0;i<(_phi->nbCtcPt(_phi_ref));i++){
    for (int j=0;j<(_psi->nbCtcElt(_psi_ref));j++){
      for (int ni=0;ni<1;ni++)
        for (int nj=0;nj<3;nj++)
            ((*_P[CTCELT][ni*3+nj])(i,j)) = x[iterator++];
		}
	}
	
	for(int i=0;i<(_phi->nbSubCtcElt(_phi_ref));i++){
    for (int j=0;j<(_psi->nbSubCtcElt(_psi_ref));j++){
      for (int ni=0;ni<2;ni++)
        for (int nj=0;nj<2;nj++)
            ((*_P[SUBCTCELT][ni*2+nj])(i,j)) = x[iterator++];
		}
	}
	return iterator;
}


double Convex::corr_dual_energy()
{
  
  /*cout << "File : " << __FILE__ << endl;
  cout << "Line : " << __LINE__ << endl;
  cout << "------------------------------------------------------------------------------" << endl;
  cout << "TO BE DONE: the correction should now be \lamda * F !" << __FILE__ << endl;
  cout << "(see the same function in simpleConvex.cpp) jfg, June 22, 2018" << __FILE__ << endl;
  cout << "------------------------------------------------------------------------------" << endl;
  exit(1);*/

  Solid &phi = *_phi;
  Solid &psi = *_psi;
  double correction;
  R3 temp;
  
  correction = 0.;
  
  for (int i=0;i<(_phi->nbCtcPt(_phi_ref));i++){
    for (int j=0;j<(_psi->nbCtcElt(_psi_ref));j++){
      for (int ni =0; ni <1 ; ni++) {
        for (int nj =0; nj <3 ; nj++) {
          temp= phi(_phi_ref,POINTELT,ni,i) - psi(_psi_ref,CTCELT,nj,j);
          //correction += _gap * ( (*_P[CTCELT][ni*3+nj])(i,j)); 
          correction += ((*_P[CTCELT][ni*3+nj])(i,j)) *  (_gap - ( (*_THETA[CTCELT])(i,j) , temp) );
        }
      }        
    }          
  }
  
  for (int i=0;i<(_phi->nbSubCtcElt(_phi_ref));i++){
    for (int j=0;j<(_psi->nbSubCtcElt(_psi_ref));j++){
      for (int ni =0; ni <2 ; ni++) {
        for (int nj =0; nj <2 ; nj++) {
          temp= phi(_phi_ref,SUBCTCELT,ni,i) - psi(_psi_ref,SUBCTCELT,nj,j);
          //correction += _gap * ( (*_P[SUBCTCELT][ni*2+nj])(i,j)); 
          correction += ((*_P[SUBCTCELT][ni*2+nj])(i,j)) * (_gap - ( (*_THETA[SUBCTCELT])(i,j) , temp) ) ;
        }
      }        
    }          
  }
  
  /*
  temp= phi(_phi_ref,typelt0,ni,i) - psi(_psi_ref,typelt,nj,j);
  F = ( (*_THETA[typelt])(i,j) , temp) - _gap;
  F_moins = -min(F,0);
  F_plus  = F + F_moins;
          
  s += F_moins + F_plus * double( (*_P[typelt][ni*NJ+nj])(i,j) > 0.);
  */
  
  return correction;
   
}


int Convex::grad_dual_energy(vector<double> &grad, int count)
{
  Solid &phi = *_phi;
  Solid &psi = *_psi;
  int size;
  R3 temp;
  
  for (int i=0;i<(_phi->nbCtcPt(_phi_ref));i++){
    for (int j=0;j<(_psi->nbCtcElt(_psi_ref));j++){
      for (int ni =0; ni <1 ; ni++) {
        for (int nj =0; nj <3 ; nj++) {
          temp= phi(_phi_ref,POINTELT,ni,i) - psi(_psi_ref,CTCELT,nj,j);
          grad[count++] = ( _gap - ( (*_THETA[CTCELT])(i,j) , temp) );
        }
      }        
    }          
  }
  
  for (int i=0;i<(_phi->nbSubCtcElt(_phi_ref));i++){
    for (int j=0;j<(_psi->nbSubCtcElt(_psi_ref));j++){
      for (int ni =0; ni <2 ; ni++) {
        for (int nj =0; nj <2 ; nj++) {
          temp= phi(_phi_ref,SUBCTCELT,ni,i) - psi(_psi_ref,SUBCTCELT,nj,j);
          grad[count++] = ( _gap - ( (*_THETA[SUBCTCELT])(i,j) , temp) );
          /*if (grad[count] > 0){
				cout << "contact: gradient = " << grad[count] << " at " << count << endl;
				cout << "lag mult: " << (*_P[SUBCTCELT][ni*2+nj])(i,j) << endl;
		  }*/
        }
      }        
    }          
  }
  
  return count;
}
