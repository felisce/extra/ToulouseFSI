//===========================================================
//                    MultiContact
//
// Part of MasterFSIContact,             Copyright Inria 2018
//===========================================================
/*!
 \author Jean-Frederic Gerbeau
 */
#ifndef _VECTEUR_HPP_
#define _VECTEUR_HPP_
#include<fstream>
#include<cassert>
#include<cstddef>
#include<iostream>
#include<complex>
#include "Rn.hpp"

class Add;
/* Traits donnant le type du resultat d'une operation */
template <class Left,class Op,class Right> class Result;

/* Traits donnant le type du resultat d'une operation */
template <class Left,class Op,class Right> class Result;

/* fonction realisant une operation sur deux vecteurs */
template <class Op,class K,class T_Left,class T_Right>
struct resultOp{
  static K perform(const size_t i,const T_Left* Left_,const T_Right* Right_ )
  {return Op::apply((*Left_)[i],(*Right_)[i]);};
};

template <class Op,class K,class T_Right>
struct resultOp<Op,K,int,T_Right>{
  static K perform(const size_t i,const int* Left_,const T_Right* Right_ )
  {return Op::apply((*Left_),(*Right_)[i]);};};

template <class Op,class K,class T_Right>
struct resultOp<Op,K,double,T_Right>{
  static K perform(const size_t i,const double* Left_,const T_Right* Right_ )
  {return Op::apply((*Left_),(*Right_)[i]);};};

template <class Op,class K,class T_Right>
struct resultOp<Op,K,R3,T_Right>{
  static K perform(const size_t i,const R3* Left_,const T_Right* Right_ )
  {return Op::apply((*Left_),(*Right_)[i]);};};

struct Switch;

template <class K,class T_Right>
struct resultOp<Switch,K,int,T_Right>{
  static K perform(const size_t i,const int* Left_,const T_Right* Right_)
  { return (*Right_)[(i+(*Left_))%(*Right_).Ndl()];}; };


/* fonction realisant une operation sur deux  matrices */
template <class Op,class K,class T_Left,class T_Right>
struct resultOp_matrices{
  static K perform(const size_t i,const size_t j,const T_Left* Left_,const T_Right* Right_ )
  {return Op::apply((*Left_)(i,j),(*Right_)(i,j));};
};

template <class Op,class K,class T_Right>
struct resultOp_matrices<Op,K,int,T_Right>{
  static K perform(const size_t i,const size_t j,const int* Left_,const T_Right* Right_ )
  {return Op::apply((*Left_),(*Right_)(i,j));};};

template <class Op,class K,class T_Right>
struct resultOp_matrices<Op,K,double,T_Right>{
  static K perform(const size_t i,const size_t j,const double* Left_,const T_Right* Right_ )
  {return Op::apply((*Left_),(*Right_)(i,j));};};

template <class Op,class K,class T_Right>
struct resultOp_matrices<Op,K,R3,T_Right>{
  static K perform(const size_t i,const size_t j,const R3* Left_,const T_Right* Right_ )
  {return Op::apply((*Left_),(*Right_)(i,j));};};

struct Switch_j;
struct Switch_i;

template <class K,class T_Right>
struct resultOp_matrices<Switch_j,K,int,T_Right>{
  static K perform(const size_t i,const size_t j,const int* Left_,const T_Right* Right_)
  { return (*Right_)(i,(j+(*Left_))%(*Right_).Ndly());};};

template <class K,class T_Right>
struct resultOp_matrices<Switch_i,K,int,T_Right>{
  static K perform(const size_t i,const size_t j,const int* Left_,const T_Right* Right_)
  { return (*Right_)((i+(*Left_))%(*Right_).Ndlx(),j);}; };


/**************************/
/* La classe des vecteurs */
/**************************/
/* vecteur de type expression */
template <class T_Left,class Op=void*,class T_Right=void*>
class vecteur {
public:
  typedef typename Result<T_Left,Op,T_Right>::K K;
  
  const T_Left* Left_;
  const T_Right* Right_;
  
  vecteur<int,Switch,vecteur<T_Left,Op,T_Right> > decalage(const int& j)
  {return  vecteur<int,Switch,vecteur<T_Left,Op,T_Right> >(j,*this);};
  
  K  operator[](size_t i) const  {
    assert(i<Ndl());
    return resultOp<Op,K,T_Left,T_Right>::perform(i,Left_,Right_);};
  size_t Ndl()  const {return (*Right_).Ndl();}
  
  vecteur(const T_Left &Left,const T_Right &Right):Left_(&Left),Right_(&Right){};
};

/*"vrai" vecteur */
template <class T>
class vecteur<T,void*,void*> {
private:
  const size_t Ndl_;
  T* val_;
public:
  typedef T K;
  T* val(){return val_;};
  
  void sauve(char* nom_fichier){
    ofstream fichier(nom_fichier);fichier<<(*this);fichier.close();};
  size_t Ndl() const {return Ndl_;}
  T& operator[](size_t i){
    assert(i<Ndl_);    return val_[i];};
  const T operator[] (size_t i) const {assert(i<Ndl_); return val_[i];}
  //  vecteur<vecteur<T,void*,void*>,Switch,int> decalage(const int& j)
  
  vecteur<int,Switch,vecteur<T,void*,void*> > decalage(const int& j)
  {return  vecteur<int,Switch,vecteur<T,void*,void*> >(j,(*this));};
  
  //  {return vecteur<vecteur<T,void*,void*>,Switch,int>((*this),j);};
  
  template <class T_Left,class Op,class T_Right> vecteur<T>& operator=(const vecteur<T_Left,Op,T_Right>& List)
  { for (size_t i=0;i<Ndl_;i++) val_[i]=List[i];  return *this;};
  
  vecteur& operator=(const vecteur& v) { for (size_t i=0;i<Ndl_;i++) val_[i]=v[i];  return *this;};
  
  vecteur(const vecteur& v):val_(new(T[v.Ndl()])),Ndl_(v.Ndl()) {//cout<<"Attention, copie d'un vecteur !"<<endl;
    for (size_t i=0;i<Ndl_;i++) val_[i]=v[i];};
  vecteur& operator=(T val){for (int i=0;i<Ndl_;i++) (*this)[i]=val; return *this;};
  //vecteur(size_t Ndl):Ndl_(Ndl),val_(new(T[Ndl])){};// jfg: warning: when type is in parentheses, array cannot have dynamic size
  vecteur(size_t Ndl):Ndl_(Ndl){val_=new T[Ndl];};
  ~vecteur(){delete[] val_;};
};

/* Affichage d'un vecteur */
template <class T_Left,class Op,class T_Right>
ostream &operator<<(ostream &out, const vecteur<T_Left,Op,T_Right> &a){
  for (int i=0;i<a.Ndl();i++)  out<<a[i]<<endl;
  return out;
};

/* Recuperation d'un vecteur */
template <class T_Left,class Op,class T_Right>
istream &operator>>(istream &input, vecteur<T_Left,Op,T_Right> &a){
  for (int i=0;i<a.Ndl();i++)  input>>a[i];
  return input;
};

/* Le resultat d'une operation appliquee a une contstante int, double, R3 */
template<class T_Left,class Op,class T_Right> class Result<int,Switch,vecteur<T_Left,Op,T_Right> > { public: typedef  typename vecteur<T_Left,Op,T_Right>::K K; };
template<class T_Left,class Op,class T_Right> class Result<int,Switch_i,vecteur<T_Left,Op,T_Right> > { public: typedef  typename vecteur<T_Left,Op,T_Right>::K K; };
template<class T_Left,class Op,class T_Right> class Result<int,Switch_j,vecteur<T_Left,Op,T_Right> > { public: typedef  typename vecteur<T_Left,Op,T_Right>::K K; };
// template<class Op,class T> class Result<double,Op,T > { public: typedef  typename T::K K; };
// template<class Op,class T> class Result<R3,Op,T > { public: typedef  typename T::K K; };


/********************************************/
/* Les operations definies sur les vecteurs */
/********************************************/
/* L'addition */
class Add
{public:
  template<class T,class S>
  static const T apply(const T &d1,const S &d2)
  {
    return d1+d2;
  };
  
};
/* Les traits de l'addition */
template<class S,class T_Left2,class Op2,class T_Right2>
class Result<S,Add,vecteur<T_Left2,Op2,T_Right2> >
{
public:
  typedef typename vecteur<T_Left2,Op2,T_Right2>::K K;
};
template<class T,class S>
class Result<S,Add,vecteur<T> >
{
public:
  typedef T K;
};
/* L'operateur + */
template<class S,class T_Left2,class Op2,class T_Right2>
vecteur<S,Add,vecteur<T_Left2,Op2,T_Right2> >
operator+(const S &Left,const vecteur<T_Left2,Op2,T_Right2> &Right){
  return vecteur<S,Add,vecteur<T_Left2,Op2,T_Right2> >(Left,Right);
};

/* La soustraction */
class Minus
{public:
  template<class T,class S>
  static const T apply(const T &d1,const S &d2)
  {
    return d1-d2;
  };
  
};
/* Les traits de la soustraction */
template<class S,class T_Left2,class Op2,class T_Right2>
class Result<S,Minus,vecteur<T_Left2,Op2,T_Right2> >
{
public:
  typedef typename vecteur<T_Left2,Op2,T_Right2>::K K;
};
template<class T,class S>
class Result<S,Minus,vecteur<T> >
{
public:
  typedef T K;
};
/* L'operateur - */
template<class S,class T_Left2,class Op2,class T_Right2>
vecteur<S,Minus,vecteur<T_Left2,Op2,T_Right2> >
operator-(const S &Left,const vecteur<T_Left2,Op2,T_Right2> &Right){
  return vecteur<S,Minus,vecteur<T_Left2,Op2,T_Right2> >(Left,Right);
  
};

/* Le produit */
class Prod
{public:
  template<class T,class S>
  static const S apply(const T &d1,const S &d2)
  {
    return d1*d2;
  };
  
};
/* Les traits du produit */
template<class S,class T_Left2,class Op2,class T_Right2>
class Result<S,Prod,vecteur<T_Left2,Op2,T_Right2> >
{
public:
  typedef typename vecteur<T_Left2,Op2,T_Right2>::K K;
};
template<class T,class S>
class Result<S,Prod,vecteur<T> >
{
public:
  typedef T K;
};
/* L'operateur * */
template<class S,class T_Left2,class Op2,class T_Right2>
vecteur<S,Prod,vecteur<T_Left2,Op2,T_Right2> >
operator*(const S &Left,const vecteur<T_Left2,Op2,T_Right2> &Right){
  return vecteur<S,Prod,vecteur<T_Left2,Op2,T_Right2> >(Left,Right);
  
  // template<class T_Left,class T_Right>
  // vecteur<T_Left,Prod,T_Right> operator*(const T_Left &Left,const T_Right &Right){
  //   return vecteur<T_Left,Prod,T_Right>(Left,Right);
};


// struct Switch_j; //pour le decalage des matrice suivant les colonnes

// template <class K,class T_Right>
// struct resultOp<Switch_j,K,int,T_Right>{
// static K perform(const size_t i,const int* Left_,const T_Right* Right_)
// { return (*Right_)[(i+(*Left_))%(*Right_).Ndl()];}; };


/****************/
/* Les matrices */
/****************/
/* Une classe de matrices*/
template <class T_Left,class Op=void*,class T_Right=void*>
class matrice:public vecteur<T_Left,Op,T_Right> {
public:
  typedef typename vecteur<T_Left,Op,T_Right>::K K;
  
  const size_t& Ndlx() const {return (*(vecteur<T_Left,Op,T_Right>::Right_)).Ndlx();}
  const size_t& Ndly()const {return (*(vecteur<T_Left,Op,T_Right>::Right_)).Ndly();}
  const K  operator()(size_t i,size_t j) const  {
    assert(i<Ndlx());assert(j<Ndly());
    return (*this)[i*Ndly()+j];}
  
  K operator[](size_t n) const {
    return resultOp<Op,K,T_Left,T_Right>::perform(n,vecteur<T_Left,Op,T_Right>::Left_,vecteur<T_Left,Op,T_Right>::Right_);
  };
  
  matrice<int,Switch_i,matrice<T_Left,Op,T_Right> > di(const int& j)
  {return  matrice<int,Switch_i,matrice<T_Left,Op,T_Right> >(j,*this);};
  
  matrice<int,Switch_j,matrice<T_Left,Op,T_Right> > dj(const int& j)
  {return  matrice<int,Switch_j,matrice<T_Left,Op,T_Right> >(j,*this);};
  
  
  matrice(const T_Left &Left,const T_Right &Right):vecteur<T_Left,Op,T_Right>(Left,Right){};
  
  void sauve(char* nom_fichier){
    ofstream fichier(nom_fichier);fichier<<(*this);fichier.close();};
};

/* vrai matrices */
template <class T>
class matrice<T,void*,void*> :public vecteur<T,void*,void*> {
private:
  const size_t Ndlx_;
  const size_t Ndly_;
public:
  const size_t& Ndlx() const {return Ndlx_;}
  const size_t& Ndly()const {return Ndly_;}
  T& operator()(int i,int j){return (*this)[i*Ndly_+j];};
  const T operator() (int i,int j) const {return (*this)[i*Ndly_+j];}
  
  matrice<int,Switch_i,matrice<T,void*,void*> > di(const int& j)
  {return  matrice<int,Switch_i,matrice<T,void*,void*> >(j,(*this));};
  
  matrice<int,Switch_j,matrice<T,void*,void*> > dj(const int& j)
  {return  matrice<int,Switch_j,matrice<T,void*,void*> >(j,(*this));};
  
  
  
  template <class T_Left,class Op,class T_Right> matrice<T>& operator=(matrice<T_Left,Op,T_Right> List)
  {for (size_t i=0;i<(*this).Ndlx();i++)
    for(size_t j=0;j<(*this).Ndly();j++) (*this)(i,j)=List(i,j); return *this;};
  matrice& operator=(T val){for (int i=0;i<(*this).Ndl();i++) (*this)[i]=val; return *this;};
  matrice& operator=(const matrice& v) {
    for (size_t i=0;i<(*this).Ndlx();i++)
      for(size_t j=0;j<(*this).Ndly();j++) (*this)(i,j)=v(i,j); return *this;};
  
  
  matrice(size_t Ndlx,size_t Ndly):vecteur<T>(Ndlx*Ndly),Ndlx_(Ndlx),Ndly_(Ndly){};
  void sauve(char* nom_fichier){
    ofstream fichier(nom_fichier);fichier<<(*this);fichier.close();};
};

/* Resultats  des operations sur les matrices */
template<class S,class Op,class T_Left,class Op2,class T_Right>
class Result<S,Op,matrice<T_Left,Op2,T_Right> >
{
public:
  typedef typename Result<S,Op,vecteur<T_Left,Op2,T_Right> >::K K;
};

/* L'operateur + pour les matrice */
template<class S,class T_Left2,class Op2,class T_Right2>
matrice<S,Add,matrice<T_Left2,Op2,T_Right2> >
operator+(const S &Left,const matrice<T_Left2,Op2,T_Right2> &Right){
  return
  matrice<S,Add,matrice<T_Left2,Op2,T_Right2> >(Left,Right);
};

/* L'operateur - pour les matrice */
template<class S,class T_Left2,class Op2,class T_Right2>
matrice<S,Minus,matrice<T_Left2,Op2,T_Right2> >
operator-(const S &Left,const matrice<T_Left2,Op2,T_Right2> &Right){
  return
  matrice<S,Minus,matrice<T_Left2,Op2,T_Right2> >(Left,Right);
};

/* L'operateur * pour les matrice */
template<class S,class T_Left2,class Op2,class T_Right2>
matrice<S,Prod,matrice<T_Left2,Op2,T_Right2> >
operator*(const S &Left,const matrice<T_Left2,Op2,T_Right2> &Right){
  return
  matrice<S,Prod,matrice<T_Left2,Op2,T_Right2> >(Left,Right);
};


template <class T_Left,class Op,class T_Right>
ostream &operator<<(ostream &out, const matrice<T_Left,Op,T_Right> &a){
  for (size_t i=0;i<a.Ndlx();i++){
    for (size_t j=0;j<a.Ndly();j++ ) out<<a(i,j)<<" ";
    cout<<endl;}
  return out;
};

double ps(const R3& A,const R3& B);
double ps(const vecteur<R3>& A,const vecteur<R3>& B);
double min(double a,double const &b);
double ps(const vecteur<double>& a,const vecteur<double>& b);


// int main()
// {
//   vecteur<double> u(10);
//   for (size_t i=0;i<u.Ndl();i++){u[i]=i;};
//   cout<<(u.decalage(1)).decalage(1)<<endl;
//   vecteur<double> v(10);
//   v=2.;
//   vecteur<double> w(10);
//   w=(5.+u)-4.*v;
//   //cout<<"u="<<u<<endl;
//   //cout<<"v="<<v<<endl;
//   //cout<<"w=5.+u-4.*v="<<w<<endl;
//   matrice<double> A(3,6);
//   matrice<double> B(3,6);
//   A=1.;
//   B=3.;
//   matrice<double> C(3,6);
//   for (size_t i=0;i<C.Ndlx();i++) for (size_t j=0;j<C.Ndly();j++) C(i,j)=10.*i+j;
//   cout<<"C="<<endl<<C<<endl;
//   matrice<double> D(3,6);
//   D=C.dj(1);
//   cout<<"D="<<endl<<D<<endl;
//   matrice<double> E(3,6);
//   E=D.di(1);
//   cout<<"E="<<endl<<E<<endl;
//   cout<<"E.di="<<endl<<(D.di(1)).di(1)<<endl;
//   matrice<double> F(3,6);
//   F=E.di(1);
//   cout<<"F="<<endl<<F<<endl;
//   cout<<"F+3.*D="<<F+3.*D<<endl;
// };
#endif
