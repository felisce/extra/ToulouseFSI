//===========================================================
//                    MultiContact
//
// Part of MasterFSIContact,             Copyright Inria 2018
//===========================================================
/*!
 \author Jean-Frederic Gerbeau
 */
#include "mesh.hpp"


istream & eatline( istream & s )    // eat a whole line from istream
{
  while ( s.get() != '\n' && s . good() )
  {}
  return s ;
}

istream & eat_comments( istream & s )    //eat lines starting with '!%#;$'
{
  char c = 'a';
  s.get( c ) ;
  while ( c == '!' ||
         c == '%' ||
         c == '#' ||
         c == ';' ||
         c == '$' )
  {
    s >> eatline ;
    s.get( c ) ;
  }
  return s.putback( c ) ;
}

istream & next_good_line( istream & s, string & line )
{
  s >> eat_comments;
  getline( s, line );
  return s;
}

int nextIntINRIAMeshField( string const & line, istream & mystream )
{
  // first control if line has something.
  // If so use atoi (the version from util_string.h) to extract
  // the integer. Otherwise get if from the input stream
  for ( string::const_iterator is = line.begin(); is != line.end(); ++is )
    if ( *is != ' ' )
      return atoi( line.c_str() );
  int dummy;
  mystream >> dummy;
  return dummy;
}

//! It Reads all basic info from INRIA MESH file so as to be able to
// properly dimension all arrays
bool
readINRIAMeshFileHead( ifstream & mystream,
                      size_t & numVertices,
                      size_t & numBVertices,
                      size_t & numBFaces,
                      size_t & numBEdges,
                      size_t & numVolumes,
                      size_t & numStoredFaces)
{
  unsigned done = 0;
  string line;
  double x, y, z;
  size_t p1, p2, p3, p4, p5, p6, p7, p8;
  size_t i, ibc;
  
  int idummy;
  
  size_t numReadFaces = 0;
  numStoredFaces    = 0;
  
  //shape = NONE;
  //streampos start=mystream.tellg();
  
  while ( next_good_line( mystream, line ).good() )
  {
    
    if ( line.find( "MeshVersionFormatted" ) != string:: npos )
    {
      idummy = nextIntINRIAMeshField( line.substr( line.find_last_of( "d" ) + 1 ), mystream );
      ASSERT_PRE0( idummy == 1, "I can read only formatted INRIA Mesh files, sorry" );
    }
    
    if ( line.find( "Dimension" ) != string:: npos )
    {
      idummy = nextIntINRIAMeshField( line.substr( line.find_last_of( "n" ) + 1 ), mystream );
      ASSERT_PRE0( idummy == 3, "I can read only 3D INRIA Mesh files, sorry" );
    }
    
    // I assume that internal vertices have their Ref value set to 0 (not clear from medit manual)
    if ( line.find( "Vertices" ) != string::npos )
    {
      numVertices = nextIntINRIAMeshField( line.substr( line.find_last_of( "s" ) + 1 ), mystream );
      done++;
      numBVertices = 0;
      
      for ( i = 0;i < numVertices;++i )
      {
        mystream >> x >> y >> z >> ibc;
      }
    }
    
    if ( line.find( "Triangles" ) != string::npos )
    {
      numReadFaces=nextIntINRIAMeshField( line.substr( line.find_last_of( "s" ) + 1 ), mystream );
      done++;
      numBFaces = 0;
      
      for ( size_t k = 0; k < numReadFaces; k++ )
      {
        mystream >> p1 >> p2 >> p3 >> ibc;
      }
      numStoredFaces=numReadFaces;
    }
    
    
    if ( line.find( "Quadrilaterals" ) != string::npos )
    {
      numReadFaces= nextIntINRIAMeshField( line.substr( line.find_last_of( "s" ) + 1 ), mystream );
      done++;
      numBFaces = 0;
      for ( size_t k = 0;k < numReadFaces;k++ )
      {
        mystream >> p1 >> p2 >> p3 >> p4 >> ibc;
      }
      numStoredFaces=numReadFaces;
    }
    // To cope with a mistake in INRIA Mesh files
    if ( line.find( "Tetrahedra" ) != string::npos )
    {
      numVolumes = nextIntINRIAMeshField( line.substr( line.find_last_of( "a" ) + 1 ), mystream );
      done++;
      for ( i = 0;i < numVolumes;i++ )
      {
        mystream >> p1 >> p2 >> p3 >> p4 >> ibc;
      }
    }
    
    if ( line.find( "Hexahedra" ) != string::npos )
    {
      numVolumes = nextIntINRIAMeshField( line.substr( line.find_last_of( "a" ) + 1 ), mystream );
      done++;
      for ( i = 0;i < numVolumes;i++ )
      {
        mystream >> p1 >> p2 >> p3 >> p4 >> p5 >> p6 >> p7 >> p8 >> ibc;
      }
    }
    if ( line.find( "Edges" ) != string::npos )
    {
      numBEdges = nextIntINRIAMeshField( line.substr( line.find_last_of( "s" ) + 1 ), mystream );
      done++;
      for ( i = 0;i < numBEdges;i++ )
      {
        mystream >> p1 >> p2 >> ibc;
      }
    }
  }
  return true ;
}
