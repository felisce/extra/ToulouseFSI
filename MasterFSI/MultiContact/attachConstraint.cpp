//===========================================================
//                    MultiContact
//
// Part of MasterFSIContact,             Copyright Inria 2018
//===========================================================
/*!
 \author Jean-Frederic Gerbeau
 */

#include "attachConstraint.hpp"

AttachConstraint::AttachConstraint(Solid& phi,double Ox,double Oy,double Oz, const int & nb_coor, const size_t & ref, const double &  dist, const bool & init_stretch, const int & verbose ):
_phi( &phi),_nb_coor(nb_coor),_nb_node(0)
{
  _attach_coor[0]=Ox;
  _attach_coor[1]=Oy;
  _attach_coor[2]=Oz;
  
  cout << "AttachConstraint::AttachConstraint() on solid #" << _phi->id() << ", on reference" << ref << endl;
  
  for(size_t i=0;i<phi.nbPoint();i++){
    if( phi(i).marker() == ref )
      _nb_node++;
  }
  cout<<"_nb_node = "<<_nb_node<<endl;
  
  _node = new int[_nb_node];
  _dist0 = new double[_nb_node];
  _dist = new double[_nb_node];
  _p0 = new double[_nb_node];
  _p0old = new double[_nb_node];
  _p0oldold = new double[_nb_node];
  _p0tilde = new double[_nb_node];
  _p0tildeold = new double[_nb_node];
  _nb_node=0;
  _verbose = verbose;
  
  for(size_t i=0;i<phi.nbPoint();i++){
    if( phi(i).marker() == ref )
    {
      _node[_nb_node]=(int)i; // store the node number
      cout<<"node = "<<i<<endl;
      _p0[_nb_node]=0.; //set to 0 the lagrangian multipliers;
      _p0old[_nb_node]=0.; //set to 0 the lagrangian multipliers;
      _p0oldold[_nb_node]=0.; //set to 0 the lagrangian multipliers;
      _p0tilde[_nb_node]=0.; //set to 0 the lagrangian multipliers;
      _p0tildeold[_nb_node]=0.; //set to 0 the lagrangian multipliers;
      //evaluate _dist0 for every node
      _dist0[_nb_node]=0;
      for(int j=0;j<_nb_coor;j++)
        _dist0[_nb_node]+= (phi(i)[j] - _attach_coor[j])*(phi(i)[j] - _attach_coor[j]);
      _dist0[_nb_node]=sqrt(_dist0[_nb_node]);
      cout<<"dist0 = "<<_dist0[_nb_node]<<endl;
      _dist[_nb_node]= dist*_dist0[_nb_node]*init_stretch + dist*(1-init_stretch);
      //_dist[_nb_node]= dist + _dist0[_nb_node]*init_stretch;
      //if init_stretch=0 dist is _dist, else dist is a stretch
      //  cout<<"_nb_node = "<<_nb_node<<endl;
      cout<<"dist = "<<_dist[_nb_node]<<endl;
      _nb_node++;
    }
  }
  /*
   switch(init_stretch)
   {
   case(0):
   break;
   case(1):
   for(int i=0; i<_nb_node; i++)
   _dist[i]+=_dist0;
   break;
   case(2):
   // if we want to implement the law...
   break;
   }
   */
}


void AttachConstraint::addForce() const
{
  if(_verbose>1) cout << "AttachConstraint::addForce()" << endl;
  Solid& phi = *_phi;
  int inod(0);
  double force(0);
  size_t phidof = phi.nbDofPerPoint();
  
  for(int i=0; i<_nb_node;i++){
    inod = _node[i];
    for(int j=0;j<_nb_coor;j++)
    {
      force =  -2. * _p0[i] * ( phi(inod)[j] - _attach_coor[j] );
      //cout << "Node " << inod <<", attach_force ["<<j <<"] = " << force << endl ;
      phi.total_force[phidof*inod + j] += force;
    }
    //  for(int j=0;j<_nb_coor;j++)
    //  {
    //cout << "Node " << inod <<", attach_force ["<<j <<"] = " <<phi.total_force[phidof*inod + j] << endl ;
    //}
  }
}


double AttachConstraint::residual() const
{
  if(_verbose>1) cout << "AttachConstraint::residual()" << endl;
  Solid& phi = *_phi;
  double r(0);
  int inod(0);
  double g,g_minus,g_plus;
  double dist_square_OM(0);
  
  
  for(int i=0; i<_nb_node;i++){
    inod = _node[i];
    dist_square_OM=0;
    for(int j=0;j<_nb_coor;j++)
    {
      dist_square_OM += (phi(inod)[j] - _attach_coor[j])*(phi(inod)[j] - _attach_coor[j]);
    }
    g = _dist[i]*_dist[i] - dist_square_OM;
    g_minus = - min(g,0);
    g_plus = g + g_minus;
    r += g_minus + g_plus * double( _p0[i] >0.);
  }
  return r;
}


void AttachConstraint::descent(const double & step, const bool & aitken, const bool & activate_aitken )
{
  
  if(_verbose>1) cout << "Entering AttachConstraint::descent" << endl;
  
  Solid& phi = *_phi;
  int inod;
  double g;
  int nbj;
  double dist_square_OM(0);
  
  /*
   On compte le nombre de segment avec lesquels le point i est en interaction
   On divise le pas de mise a jour du multiplicateur par le nombre obtenu,
   de sorte que le comportement soit independant du nombre de points de discretisation
   */
  
  nbj=0;
  for(int i=0; i<_nb_node;i++){
    inod = _node[i];
    dist_square_OM=0;
    for(int j=0;j<_nb_coor;j++)
    {
      dist_square_OM += (phi(inod)[j] - _attach_coor[j])*(phi(inod)[j] - _attach_coor[j]);
    }
    g = _dist[i]*_dist[i] - dist_square_OM;
    if(g<0) nbj++;
  }
  
  nbj=max(1,nbj);
  
  
  
  for(int i=0; i<_nb_node;i++){
    inod = _node[i];
    dist_square_OM=0;
    for(int j=0;j<_nb_coor;j++)
    {
      dist_square_OM += (phi(inod)[j] - _attach_coor[j])*(phi(inod)[j] - _attach_coor[j]);
    }
    g = _dist[i]*_dist[i] - dist_square_OM;
    if(aitken)
    {
      _p0oldold[i] =_p0old[i];
      _p0old[i] =_p0[i];
      
      _p0[i] -= (step/nbj) * g;
      _p0[i] = max( 0., _p0[i] );
      
      _p0tildeold[i] =_p0tilde[i];
      _p0tilde[i] =_p0[i];
      
    }
    else
    {
      _p0[i] -= (step/nbj) * g;
      _p0[i] = max( 0., _p0[i] );
    }
  }
  
  if(activate_aitken)
    relaxation();
}


void  AttachConstraint::relaxation()
{
  double sum1=0.;
  double sum2=0.;
  
  
  for(int i=0; i<_nb_node;i++)
  {
    sum1+=(_p0old[i]-_p0oldold[i])*(_p0old[i]-_p0tilde[i]-_p0oldold[i]+_p0tildeold[i]);
    
    sum2+=(_p0old[i]-_p0tilde[i]-_p0oldold[i]+_p0tildeold[i])*(_p0old[i]-_p0tilde[i]-_p0oldold[i]+_p0tildeold[i]);
  }
  if (sum2>0)
  {
    
    if(_verbose>1) cout<<"Applying Aitken relaxation to Uzawa iterations"<<endl;
    
    _omega0 = sum1/sum2;
    
    if(_verbose>1) cout<<"step aitken = "<< _omega0<<endl;
    for(int i=0; i<_nb_node;i++)
      _p0[i]=max(0.,_omega0*_p0tilde[i]+(1-_omega0)*(_p0old[i]));
  }
  else
  {
    for(int i=0; i<_nb_node;i++) _p0[i]=0;
  }
}


/*
 void AttachConstraint::descent(double const& step)
 {
 cout << "AttachConstraint::descent() step = " << step  << endl;
 Solid& phi = *_phi;
 int inod;
 double g;
 double dist_square_OM(0);
 
 for(int i=0; i<_nb_node;i++){
 inod = _node[i];
 dist_square_OM=0;
 for(int j=0;j<_nb_coor;j++)
 {
 dist_square_OM += (phi(inod)[j] - _attach_coor[j])*(phi(inod)[j] - _attach_coor[j]);
 }
 g = _dist[i]*_dist[i] - dist_square_OM;
 _p0[i] -= step * g;
 _p0[i] = max( 0., _p0[i] );
 }
 }
 */


