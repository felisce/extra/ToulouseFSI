//===========================================================
//                    MultiContact
//
// Part of MasterFSIContact,             Copyright Inria 2018
//===========================================================
/*!
 \author Jean-Frederic Gerbeau
 */
#include "simpleConvex.hpp"

void SimpleConvex::init(Solid& phi,const double& gap,const double& coef_a,
                        const double& coef_b,const double& coef_c, const int &verbose)
{
  _gap = gap;
  _admissible = true;
  _verbose = verbose;
  _phi = &phi;
  double n = sqrt( coef_a*coef_a + coef_b*coef_b);
  _a = coef_a/n;
  _b = coef_b/n;
  _c = coef_c/n;
  _normal_wall[0] = _a;
  _normal_wall[1] = _b;
  // dist(P,wall) = _normal_wall . OP + _c
  _p0 = new vector<double>(phi.nbPoint()); // FUITE DE MEMOIRE? AU SECOURS
  _p00.resize(phi.nbPoint());
  _lambda.resize(phi.nbPoint());
  _lambda0.resize(phi.nbPoint());
  _omega.resize(phi.nbPoint());
  tau0 = 1;
  _str = 0;
  
  for(size_t i=0;i<phi.nbPoint();i++){
    (*_p0)[i]=0.;
    (_lambda)[i]=0.;
    (_lambda0)[i]=0.;
    (_omega)[i]=0.;
    
  }
  cout << "SimpleConvex::init:  normal_wall = " << _normal_wall[0] << "," << _normal_wall[1] << endl;
}

double SimpleConvex::residual() const
{
  cout << "SimpleConvex::residual()" << endl;
  Solid& phi = *_phi;
  double r=0.;
  double g,g_minus,g_plus;
  for(size_t i=0;i<phi.nbPoint();i++){
    g = ps( _normal_wall, phi(i) ) + _c - _gap; // no contact: g>0, contact: g<= 0
    g_minus = - min(g,0); // no contact: g_minus = 0, contact: g_minus = abs(g)
    g_plus = g + g_minus; // no contact: g_plus = g, contact: g_plus = 0
    r += g_minus + g_plus * double( (*_p0)[i] >0.); // no contact: p0[i] = 0, contact: p0[i] > 0
    // no contact: r = 0, contact: r = abs(g)
  }
  
  return r;
}

/*double SimpleConvex::residual_acceleratedUzawa() const
 {
 cout << "SimpleConvex::residual_acceleratedUzawa()" << endl;
 Solid& phi = *_phi;
 double r=0.;
 for(size_t i=0;i<phi.nbPoint();i++){
 r+= (_p00[i]- _lambda[i]) * (_p00[i]- _lambda[i]);
 }
 
 return sqrt(r);
 }
 */

void SimpleConvex::descent(double const& step)
{
  cout << "SimpleConvex::descent()" << endl;
  Solid& phi = *_phi;
  for (size_t i=0;i<phi.nbPoint();i++){
    // cout <<  "grad " << i << " = " << ps( _normal_wall, phi(i) ) + _c - _gap << endl;
    (*_p0)[i] += step * ( -ps( _normal_wall, phi(i) ) - _c + _gap );
    (*_p0)[i] = max( 0., (*_p0)[i] );
  }
}

void SimpleConvex::descent_acceleratedUzawa(double const& step)
{
  cout << "SimpleConvex::descent_acceleratedUzawa()" << endl;
  Solid& phi = *_phi;
  
  for (size_t i=0;i<phi.nbPoint();i++){
    _omega[i] =  - ps( _normal_wall, phi(i) ) - _c + _gap ;
    _lambda[i] = max( 0., _omega[i]*step + (*_p0)[i] );
    _p00[i] = (*_p0)[i]; // _p00 is used to compute the residual in the "residual_acceleratedUzawa" method
  }
  
  tau = 0.5*(1 + sqrt(1+4*tau0*tau0));
  
  vector<double> diff(phi.nbPoint());
  for (size_t i=0;i<diff.size();i++) {
    diff[i] = _lambda[i] - _lambda0[i];
  }
  
  double sum=0.;
  for (size_t i=0;i<diff.size();i++){
    sum += diff[i]*_omega[i];
  }
  
  if( sum >= 0 ) {
    for (size_t i=0;i<diff.size();i++) {
      (*_p0)[i] = _lambda[i] + ((tau0-1)/(tau))*(_lambda[i] - _lambda0[i]);
    }
  }
  else {
    for (size_t i=0;i<diff.size();i++) {
      (*_p0)[i] = _lambda[i];
    }
    tau = 1;
  }
  
  // save old values
  for (size_t i=0;i<_lambda.size();i++){
    _lambda0[i] = _lambda[i];
  }
  tau0 = tau;
  
}


void SimpleConvex::addContactForce() const
{
  cout << "SimpleConvex::addContactForce()" << endl;
  R3 f0;
  Solid& phi = *_phi;
  int phidof = phi.nbDofPerPoint();
  for(size_t i=0;i<phi.nbPoint();i++){
    f0[0]= (*_p0)[i] * _normal_wall[0];
    f0[1]= (*_p0)[i] * _normal_wall[1];
    if(_verbose > 0) {
       //cout << "contact_force = " << f0 << endl;
    }
    phi.total_force[phidof*i + 0] += f0[0];
    phi.total_force[phidof*i + 1] += f0[1];
    phi.contact_force[phidof*i + 0] = f0[0];
    phi.contact_force[phidof*i + 1] = f0[1];
  }
}


double SimpleConvex::corr_dual_energy()
{
   double correction = 0.;
   Solid& phi = *_phi;
   for (size_t i=0;i<phi.nbPoint();i++){
      //correction = correction + (*_p0)[i] * (_gap - _c);
     correction += (*_p0)[i] * (_gap - ps( _normal_wall, phi(i) ) - _c); 
  }
  return correction;
}


vector<double> SimpleConvex::grad_dual_energy()
{
  Solid& phi = *_phi;
  vector<double> grad(phi.nbPoint());
  for (size_t i=0;i<phi.nbPoint();i++){
    grad[i] =  _gap - ps( _normal_wall, phi(i) ) - _c ;
  }
  return grad;
}


void SimpleConvex::_xToLambda(vector<double> x, int iterator)
{
	Solid& phi = *_phi;
	for(size_t i=0;i<phi.nbPoint();i++){
        (*_p0)[i] = x[iterator++];
    }
}

