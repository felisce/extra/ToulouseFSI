//===========================================================
//                    MultiContact
//
// Part of MasterFSIContact,             Copyright Inria 2018
//===========================================================
/*!
 \author Jean-Frederic Gerbeau
 */
#include <iostream>
#include <vector>
#include "dataFile.hpp"

using namespace std;

DataFile::DataFile(const string& data_file_name): //constructor
_nb_coor(3)
{
  GetPot dfile(data_file_name.c_str());
  //dfile("nb_coordinates",3);
  _nb_solid = dfile("solids/nb_solid",1);
  _mesh_dir = dfile("solids/mesh_dir","./");
  _solver_path.resize(_nb_solid);
  _mesh_file.resize(_nb_solid);
  _nb_dof_per_point.resize(_nb_solid);
  _nb_contact_ref.resize(_nb_solid);
  _contact_ref.resize(_nb_solid);
  _socket_transport.resize(_nb_solid);
  _socket_address.resize(_nb_solid);
  _socket_port.resize(_nb_solid);
  
  int k = 0;
  for(int i=0;i<_nb_solid;i++){
    _solver_path[i] = dfile("solids/solver_path","nil",i);
    _mesh_file[i] = dfile("solids/mesh_file","nil",i);
    _socket_transport[i] = dfile("solids/socket_transport","tcp",i);
    _socket_address[i] = dfile("solids/socket_address","127.0.0.1",i);
    _socket_port[i] = dfile("solids/socket_port","5556",i); // warning: each solid must have a different port
    _nb_dof_per_point[i] = dfile("solids/nb_dof_per_point",3,i);
    _nb_contact_ref[i] =   dfile("contact/nb_contact_ref",0,i);
    
    _contact_ref[i] = new size_t[_nb_contact_ref[i]];
    for (size_t j = 0 ; j < _nb_contact_ref[i] ; j++) {
      _contact_ref[i][j] = dfile("contact/contact_ref",0,k);
      k++;
    }
  }
  
  _with_nlopt = dfile("contact/with_nlopt",0);
  _contact_algo = dfile("contact/contact_algo",0);
  _contact_sub_algo = dfile("contact/contact_sub_algo",2);
  
  
  
  _gap = dfile("contact/gap",.01);
  
  _wall_coef[0] = dfile("contact/wall_coef",0.,0);
  _wall_coef[1] = dfile("contact/wall_coef",-1.,1);
  _wall_coef[2] = dfile("contact/wall_coef",1.,2);
  
  _tolerance = dfile("solver/tolerance",.001);
  _tolerance_domain = dfile("solver/tolerance_domain",.001);
  _tolerance_nlopt = dfile("solver/tolerance_nlopt",1e-9);
  _descent_step = dfile("solver/descent_step",-1.);
  _omega_descent_step = dfile("solver/omega_descent_step", (double)  (_descent_step < 0));
  if (_descent_step < 0 ) { // automatic computation of the descent step
    if (_omega_descent_step <= 0 ) _omega_descent_step = 1.; // to prevent a negative value of omega in the data file
  }
  else
    _omega_descent_step = 0.; // to prevent a non null value of omega from the data File in the manual case, as omega is used later to check the manual/auto mode.
  _iter_lambda_max = dfile("solver/iter_uzawa_max",100);
  _iter_convex_max = dfile("solver/iter_convex_max",100);
  
  _aitken_uzawa = dfile("solver/aitken_uzawa",0);
  _aitken_convex = dfile("solver/aitken_convex",0);
  _useAcceleratedUzawa = dfile("solver/useAcceleratedUzawa",0);
  
  _nb_attach = dfile("solids/nb_attach",0);
  _attach_solid.resize(_nb_attach);
  _nb_attach_ref.resize(_nb_attach);
  _init_stretch = dfile("solids/init_stretch",0);
  _attach_dist.resize(_nb_attach);
  _attach_constraint_reference.resize(_nb_attach);
  
  k=0;
  
  _attach_coor.resize(3*_nb_attach);
  for(int i=0;i<_nb_attach;i++){
    _attach_solid[i] = dfile("solids/attach_solid",0,i);
    
    _nb_attach_ref[i] = dfile("solids/nb_attach_ref",0,i);
    
    _attach_coor[3*i+0] = dfile("solids/attach_coor",0.,3*i+0);
    _attach_coor[3*i+1] = dfile("solids/attach_coor",0.,3*i+1);
    _attach_coor[3*i+2] = dfile("solids/attach_coor",0.,3*i+2);
    
    _attach_dist[i] = new double[_nb_attach_ref[i]];
    _attach_constraint_reference[i] = new size_t[_nb_attach_ref[i]];
    for (int j = 0 ; j < _nb_attach_ref[i] ; j++) {
      _attach_dist[i][j] = dfile("solids/attach_dist",0.,k);
      _attach_constraint_reference[i][j] = dfile("solids/attach_ref",0,k);
      k++;
    }
  }
  
  _nb_barycenter_constraint = dfile("solids/nb_barycenter_constraint",0);
  _barycenter_constraint_solid.resize(_nb_barycenter_constraint);
  _barycenter_constraint_reference.resize(_nb_barycenter_constraint);
  for(int i=0;i<_nb_barycenter_constraint;i++){
    _barycenter_constraint_solid[i] = dfile("solids/barycenter_constraint_solid",0,i);
    _barycenter_constraint_reference[i] = dfile("solids/barycenter_constraint_reference",0,i);
  }
  _slave_mode = dfile("master_slave/slave_mode",1);
  _protocol = dfile("master_slave/protocol",1);
  _my_socket_transport = dfile("master_slave/my_socket_transport","tcp");
  _my_socket_address = dfile("master_slave/my_socket_address","127.0.0.1");
  _my_socket_port = dfile("master_slave/my_socket_port","5555");
  
  _unit = dfile("master_slave/unit",1);
  _initializationType = dfile("master_slave/initializationType",0);
  _dt = dfile("master_slave/dt",.01);
  _force[0] = dfile("master_slave/force",0.,0);
  _force[1] = dfile("master_slave/force",0.,1);
  _force[2] = dfile("master_slave/force",0.,2);
  _max_iter = dfile("master_slave/max_iter",10);
  _verbose_ms = dfile("master_slave/verbose",1);
  
  _post_dir = dfile("post/post_dir","./Post");
  _gnuplot = dfile("post/gnuplot",1);
  _png_output = dfile("post/png_output",0);
  _ensight = dfile("post/ensight",0);
  _post_period = dfile("post/post_period",1);
  _verbose = dfile("post/verbose",1);
  if(_verbose){
    cout << "Data file:" << endl;
    dfile.print();
  }
  vector<string> ufos;
  ufos = dfile.unidentified_variables(52,
                                      // "nb_coordinates",
                                      "solids/nb_solid",
                                      "solids/mesh_dir",
                                      "solids/solver_path",
                                      "solids/mesh_file",
                                      "solids/socket_port",
                                      "solids/socket_address",
                                      "solids/socket_transport",
                                      "solids/nb_dof_per_point",
                                      "solids/nb_attach",
                                      "solids/attach_solid",
                                      "solids/nb_attach_ref",
                                      "solids/attach_ref",
                                      "solids/init_stretch",
                                      "solids/attach_dist",
                                      "solids/attach_coor",
                                      "solids/nb_barycenter_constraint",
                                      "solids/barycenter_constraint_solid",
                                      "solids/barycenter_constraint_reference",
                                      "contact/contact_algo",
                                      "contact/with_nlopt",
                                      "contact/contact_sub_algo",
                                      "contact/nb_contact_ref" ,
                                      "contact/contact_ref",
                                      "contact/gap",
                                      "contact/wall_coef",
                                      "solver/tolerance",
                                      "solver/tolerance_domain",
                                      "solver/tolerance_nlopt",
                                      "solver/descent_step",
                                      "solver/omega_descent_step",
                                      "solver/iter_uzawa_max",
                                      "solver/iter_convex_max",
                                      "solver/aitken_uzawa",
                                      "solver/aitken_convex",
                                      "solver/useAcceleratedUzawa",
                                      "master_slave/slave_mode",
                                      "master_slave/protocol",
                                      "master_slave/unit",
                                      "master_slave/initializationType",
                                      "master_slave/dt",
                                      "master_slave/force",
                                      "master_slave/max_iter",
                                      "master_slave/verbose",
                                      "master_slave/my_socket_port",
                                      "master_slave/my_socket_address",
                                      "master_slave/my_socket_transport",
                                      "post/post_dir",
                                      "post/png_output",
                                      "post/gnuplot",
                                      "post/ensight",
                                      "post/post_period",
                                      "post/verbose"
                                      );
  if(ufos.size()){
    cerr << "Incorrect datafile:" << endl;
    cerr << "    The following entries are unknown:" << endl;
    for(vector<string>::const_iterator it = ufos.begin(); it != ufos.end();it++){
      cerr << "      " << *it << endl;
    }
    exit(1);
  }
  if(_verbose){
    cout << "\nCUT & PASTE ->.............................................\n";
    cout << "# -*- getpot -*-" << endl;
    cout << "#----------------------------------------------" << endl;
    cout << "#      MultiContact data file " << endl;
    cout << "#----------------------------------------------" << endl;
    cout << endl;
    //  cout << "nb_coordinates = " << _nb_coor << " # number of coordinates" << endl;
    cout << "[solids]" << endl;
    cout << "nb_solid = " << _nb_solid << " # number of solids" << endl;
    cout << "mesh_dir = " << _mesh_dir << " # directory of the meshes (absolute path)" << endl;
    cout << "solver_path = '" << _solver_path[0];
    for(size_t i=1;i<_nb_solid;i++){
      cout << endl << _solver_path[i];
    }
    cout << " ' # absolute path to the solid solvers" << endl;
    cout << "mesh_file = '" << _mesh_file[0];
    for(size_t i=1;i<_nb_solid;i++){
      cout << endl << _mesh_file[i];
    }
    cout << " ' # solid mesh files" << endl;
    
    cout << "socket_transport = '" << _socket_transport[0];
    for(size_t i=1;i<_nb_solid;i++){
      cout << endl << _socket_transport[i];
    }
    cout << " ' # solid socket transports (useful for ZMQ only)" << endl;
    
    cout << "socket_address = '" << _socket_address[0];
    for(size_t i=1;i<_nb_solid;i++){
      cout << endl << _socket_address[i];
    }
    cout << " ' # solid socket address (useful for ZMQ only)" << endl;
    
    cout << "socket_port = '" << _socket_port[0];
    for(size_t i=1;i<_nb_solid;i++){
      cout << endl << _socket_port[i];
    }
    cout << " ' # solid socket ports (useful for ZMQ only)" << endl;
    
    cout << "nb_dof_per_point = '" << _nb_dof_per_point[0];
    for(size_t i=1;i<_nb_solid;i++){
      cout << endl << _nb_dof_per_point[i];
    }
    cout << " ' # number of dof per solid nodes (typically:3, shell:5)" << endl;
    cout << endl;
    //attach constraint
    cout << "nb_attach = " << _nb_attach << " # number of attached points" << endl;
    cout << "attach_solid = '";
    for(size_t i=0;i<_nb_attach;i++){
      cout << _attach_solid[i] << " ";
    }
    cout << " ' # number (zero-based) of the attached solid" << endl;
    cout << "nb_attach_ref = '";
    for(size_t i=0;i<_nb_attach;i++){
      cout << _nb_attach_ref[i] << " ";
    }
    cout << " ' # number of references of the attached nodes per attached point" << endl;
    cout << "attach_ref = '";
    for(size_t i=0;i<_nb_attach;i++){
      for(int j=0;j<_nb_attach_ref[i];j++){
        cout << _attach_constraint_reference[i][j] << " ";
      }
    }
    cout << " ' # references of the attached nodes" << endl;
    cout << "init_stretch = " << _init_stretch << " # 0:the lentgth is given, 1: the stretch is given  " << endl;
    cout << "attach_dist = '";
    for(size_t i=0;i<_nb_attach;i++){
      for(int j=0;j<_nb_attach_ref[i];j++){
        cout << _attach_dist[i][j] << " ";
      }
    }
    cout << " ' # admissible distance (if init_stretch=0) or stretch of the attached nodes" << endl;
    cout << "attach_coor = '";
    for(size_t i=0;i<_nb_attach;i++){
      cout << _attach_coor[3*i+0] << " "
      << _attach_coor[3*i+1] << " "
      << _attach_coor[3*i+2] << " ";
    }
    cout << " ' # x_i,y_i,z_i of the fixed attached point" << endl;
    //end of attach constraint
    
    cout << "nb_barycenter_constraint = " << _nb_barycenter_constraint << " # barycenter constraints" << endl;
    cout << "barycenter_constraint_solid = '";
    for(size_t i=0;i<_nb_barycenter_constraint;i++){
      cout << _barycenter_constraint_solid[i] << " ";
    }
    cout << " ' # number (zero-based) of the solids" << endl;
    cout << "barycenter_constraint_reference = '";
    for(size_t i=0;i<_nb_barycenter_constraint;i++){
      cout << _barycenter_constraint_reference[i] << " ";
    }
    cout << " ' # nodes reference" << endl;
    cout<< endl;
    
    cout << "[contact]" << endl;
    cout << "with_nlopt = " << _with_nlopt
    << " # 0:home-made Uzawa, 1:nlopt" << endl;
    cout << "contact_algo = " << _contact_algo
    << " # 0:no contact, 1:simple wall, 2:Olivier Pantz" << endl;
    cout << "contact_sub_algo = " << _contact_sub_algo
    << " # 1:node/face only , 2:node / face + edge / edge (IF contact_algo = 2 only)" << endl;
    
    cout << "nb_contact_ref = '" << _nb_contact_ref[0];
    for(size_t i=1;i<_nb_solid;i++){
      cout << endl << _nb_contact_ref[i];
    }
    cout << " ' # number of contact references per solid " << endl;
    
    cout << "contact_ref = '" << _contact_ref[0][0];
    for (size_t i = 0; i < _nb_solid ; i++) {
      for (size_t j = (i==0) ; j < _nb_contact_ref[i] ; j++) {
        cout << endl << _contact_ref[i][j];
      }
    }
    cout << " ' # contact references  " << endl;
    
    cout << "gap = " << _gap << " # minimal distance between 2 solids" << endl;
    cout << "wall_coef = '" << _wall_coef[0] << " " << _wall_coef[1] << " "
    <<_wall_coef[2] << " ' # coef a,b,c of the wall equation ax+by+c" << endl;
    cout << endl;
    cout << "[solver]" << endl;
    cout << "tolerance = " << _tolerance
    << " # tolerance for the lagrange multiplier (force)" << endl;
    cout << "tolerance_domain = " << _tolerance_domain
    << " # tolerance for the domain (for contact algo 2)" << endl;
    cout << "tolerance_nlopt = " << _tolerance_nlopt
    << " #tolerance based on the relative value achieved by the function to optimize (for with_nlopt = 1)" << endl;
    cout << "descent_step = " << _descent_step
    << " # If < 0 : automatic computation. If > 0 gives Uzawa manual descent coefficient" << endl;
    cout << "omega_descent_step = " << _omega_descent_step
    << " # STEP will be omega * automatic_descent step (IF descent_step < 0 only)" << endl;
    cout << "iter_uzawa_max = " << _iter_lambda_max
    << " # maximum number of uzawa iterations (default 100)" << endl;
    cout << " iter_convex_max = " << _iter_convex_max
    << "# maximum number of convex iterations (default 100)" << endl;
    cout << "aitken_uzawa = " << _aitken_uzawa
    << " # STEP will be automatically chosen with aitken method (bool 0-1). If 1, previous choice for descent-step aren't considered" << endl;
    cout << "aitken_convex = " << _aitken_convex
    << " # If 1, use Aitken method to accelerate fixed point iterations for convexes (bool 0-1)" << endl;
    cout << "useAcceleratedUzawa = " << _useAcceleratedUzawa
    << " # If 0 use classical uzawa algorithm, if 1 use accelerated Uzawa method with restart (bool 0-1). If set to 1, aitken_uzawa will be ignored" << endl;
    cout << endl;
    cout << "[master_slave]" << endl;
    cout << "slave_mode = " << _slave_mode << " # 0: master, 1: slave" << endl;
    cout << "protocol = " << _protocol << " # 1: original version, 2: restart version, 3: estimation (not yet implemented), 11: like 1 plus a scalar" << endl;
    
    cout << "my_socket_transport = '" << _my_socket_transport;
    cout << " ' # multistruct socket transports (useful for ZMQ only, in slave mode)" << endl;
    
    cout << "my_socket_address = '" << _my_socket_address;
    cout << " ' # multistruct socket address (useful for ZMQ only, in slave mode)" << endl;
    
    cout << "my_socket_port = '" << _my_socket_port;
    cout << " ' # multistruct socket ports (useful for ZMQ only, in slave mode)" << endl;
    
    cout << "unit = " << _unit << " # parameter for master" << endl;
    cout << "initializationType = " << _initializationType << " # parameter for initialization 0 or 1" << endl;
    cout << "dt = " << _dt
    << " # time step (slave_mode is false only)" << endl;
    cout << "force = '" << _force[0] << " " << _force[1] << " "
    <<_force[2] << " ' # external force (slave_mode is false only)" << endl;
    cout << "max_iter = " << _max_iter
    << " # maximum number of time iteration (slave_mode is false only)" << endl;
    cout << "verbose = " << _verbose_ms << " # verbose for message administrator" << endl;
    cout << endl;
    cout << "[post]" << endl;
    cout << "post_dir = " << _post_dir << " # directory for postprocessing " << endl;
    cout << "gnuplot = " << _gnuplot << " # 0 or 1 ( = - Ref to show only points of reference Ref)" << endl;  
    _gnuplot_ref = (_gnuplot < 0) * ( - _gnuplot ) ; // CKT VERSIONPERSO
    _gnuplot = (  _gnuplot  != 0);                                         // CKT VERSIONPERSO
    cout << "png_output = " << _png_output << " # 0 or 1 ( = to pring a png image of the gnuplot output)" << endl;  
    
    cout << "ensight = " << _ensight << " # 0 or 1 " << endl;
    cout << "post_period = " << _post_period << " # period for postprocessing " << endl;
    cout << "verbose = " << _verbose
    << " # 0:silent, 1:default, >1:debug" << endl;
    cout << "..............................................<- CUT & PASTE \n" << endl;
  }
}
