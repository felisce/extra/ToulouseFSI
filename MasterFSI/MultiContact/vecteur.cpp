//===========================================================
//                    MultiContact
//
// Part of MasterFSIContact,             Copyright Inria 2018
//===========================================================
/*!
 \author Jean-Frederic Gerbeau
 */
///#include "Rn.hpp"
#include "vecteur.hpp"


double ps(const R3& A,const R3& B){
  double result(0.);
  result= ( A , B);
  return result;
};

double ps(const vecteur<R3>& A,const vecteur<R3>& B){
  double val(0.);
  assert(A.Ndl()==B.Ndl());
  for(size_t i=0;i<A.Ndl();i++)
    val+= (A[i]  , B[i] );
  return val;
};


double min(double a,double const &b)
{
  if (a>b) {a=b;};
  return a;
};

// Le produit scalaire L2 //
double ps(const vecteur<double>& a,const vecteur<double>& b){
  double val=0;
  assert(a.Ndl()==b.Ndl());
  for (size_t i=0;i<a.Ndl();i++) val+=a[i]*b[i];
  return val;
};
