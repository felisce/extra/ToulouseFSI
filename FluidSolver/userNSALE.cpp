/*============================================================================= 
 This file is part of the finite element code FELiScE.
 Finite Element for Life Sciences and Engineering
 Copyright (C) 2009, INRIA
 
 Authors: 
 Julien Castelneau, Dominique Chapelle, Miguel Fernandez, 
 Jeremie Foulon, Jean-Frederic Gerbeau, 
 Vincent Martin, Philippe Moireau, Marina Vidrascu 
 
 FELiScE is free software; you can redistribute it and/or modify it under
 the terms of the GNU Lesser General Public License as published by the Free
 Software Foundation; either version 2.1 of the License, or (at your option)
 any later version.
 
 FELiScE is distributed in the hope that it will be useful, but WITHOUT ANY
 WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
 more details.
 
 You should have received a copy of the GNU Lesser General Public License
 along with FELiScE. If not, see http://www.gnu.org/licenses/.
 =============================================================================*/
// SVN Version $Id

/*!  
 \file UserNSALE.cpp
 \authors M.A. Fernandez & M. Landajuela
 \date June 2013
 \brief UserNSALE class. 
 */

#include <userNSALE.hpp>
#include "FiniteElement/elementMatrix.hpp"
#include <DegreeOfFreedom/boundaryCondition.hpp>
using namespace std;
namespace felisce
{   
  
  UserNSALE::UserNSALE():
    LinearProblemNS()
  {
    m_linearizedFlag = false;
  }
  
  UserNSALE::~UserNSALE()
  {}

  void UserNSALE::userFinalizeEssBCTransient()
  { 
    if  ( (m_fstransient->iteration > 0) and (not FelisceParam::instance().hasImmersedData)  ) {
      if  (FelisceParam::instance().fsiRNscheme != -1000 ) { // robin schemes 
      	this->rhs().axpby(-1,1,this->externalVec(2)); // fluid stress at the previous time step 
      }
      else 
	{
    	//===============getListDofOfBC===============//
    	BoundaryCondition& BC = *m_boundaryConditionList[0]; 
    	int iUnknown = BC.getUnknown();
    	int idVar = m_listUnknown.idVariable(iUnknown);
	const int idMsh = m_listVariable[idVar].idMesh();
    	felInt idEltInSupportLocal = 0;
    	felInt idEltInSupportGlobal = 0;
    	felInt idDof;
    	double aux;
    	BC.ValueBCInSupportDof().clear();  
    	for (unsigned iSupportDofBC = 0; iSupportDofBC < BC.idEltAndIdSupport().size(); iSupportDofBC++)
    	  {
    	    idEltInSupportLocal = BC.idEltAndIdSupport()[iSupportDofBC].first;
    	    ISLocalToGlobalMappingApply(m_mappingElem[idMsh],1,&idEltInSupportLocal,&idEltInSupportGlobal);
    	    for( auto it_comp = BC.getComp().begin(); it_comp != BC.getComp().end(); it_comp++)
    	      {
    		dof().loc2glob(idEltInSupportGlobal, BC.idEltAndIdSupport()[iSupportDofBC].second, idVar, *it_comp, idDof);
    		AOApplicationToPetsc(m_ao,1,&idDof);
    		this->externalVec(1).getValues(1, &idDof, &aux);
    		BC.ValueBCInSupportDof().push_back(aux);
    	      }
    	  }	
	}
    }
    // cavity with flexible bottom (shell)
    if ( (FelisceParam::instance().testCase == 2) || (FelisceParam::instance().testCase == 20)  )  {
      if ( this->dimension() == 3) {
	if ( m_linearizedFlag )
	  m_boundaryConditionList[4]->setValueT(this,m_velCavityZero,m_fstransient->time);
	else
	  m_boundaryConditionList[4]->setValueT(this,m_velCavity,m_fstransient->time);
      }
      else {
	if ( m_linearizedFlag )
	  m_boundaryConditionList[3]->setValueT(this,m_velCavityZero,m_fstransient->time);
	else
	  m_boundaryConditionList[3]->setValueT(this,m_velCavity,m_fstransient->time);
      }
    } 
  }
  
  
  void UserNSALE::userElementComputeNaturalBoundaryCondition(const std::vector<Point*>& elemPoint, const std::vector<felInt>& elemIdPoint, felInt& iel, int label)
  {
    
    
    (void) elemPoint;
    (void) elemIdPoint;
    (void) iel;
    (void) label;


    //std::cout << FelisceParam::instance().testCase <<std::endl;
    
    CurvilinearFiniteElement& curvFeVel = *m_listCurvilinearFiniteElement[m_iVelocity];
    curvFeVel.updateMeasNormalQuadPt(0, elemPoint);
	
	 
    m_elemFieldInterfaceVel.initialize(DOF_FIELD,curvFeVel,this->dimension());
    m_elemFieldPreviousVel.initialize(DOF_FIELD,curvFeVel, this->dimension());
    
    // Backflow stabilization 
    if (FelisceParam::instance().addedBoundaryFlag == 2){
      double coef = FelisceParam::instance().stabilizationCoef * FelisceParam::instance().density;
      for(int ilab=0; ilab < FelisceParam::instance().stabilizationLabel.size(); ilab++){
        if (label == FelisceParam::instance().stabilizationLabel[ilab]){
          m_elemFieldPreviousVel.setValue(m_solExtrapol, curvFeVel, iel, m_iVelocity, m_ao, dof());      
          m_elementMatBD[0]->f_dot_n_phi_i_phi_j(-coef, curvFeVel , m_elemFieldPreviousVel, 0, 0, dimension(), FelisceParam::instance().addedBoundaryFlag);
        }
      }
    }

  
    // pressure wave within an straight elastic tube 
    if ( (FelisceParam::instance().testCase == 1) || (FelisceParam::instance().testCase == 10)  ) {   
      if ( (FelisceParam::instance().fsiRNscheme == 1) and (this->dimension() == 3) ) {
	// Dirichlet BC on the ringds, Robin-Neumann 
	std::vector<int> ids {  10460, 10459, 10458, 10457, 10570, 10571, 10572, 10573, 10574, 10575, 10576,
	    10624, 10623, 10622, 10621, 10620, 10619, 10515, 10514, 10516, 10517, 10518, 10519, 10520, 10464,
	    10463, 10462, 10461};
	std::vector<int> idss {120, 64, 63, 62, 61, 60, 59, 58, 57, 170, 171, 172, 173, 174, 175, 176, 224,
	    223, 222, 221, 220, 219, 114, 115, 116, 117, 118, 119 };
	if (label == 3 ) {
	  for (int i=0; i < 3; ++i) {
	    auto it = find (ids.begin(),ids.end(), elemIdPoint[i]+1);
	    if (it != ids.end()) {
	      for (int ic=0; ic<3;++ic)
		m_elementMatBD[0]->mat()(i+3*ic,i+3*ic) = 1e30; 
	    }	  
	  }
	}
	else if (label == 2 ) {
	  for (int i=0; i < 3; ++i) {
	    auto it = find (idss.begin(),idss.end(), elemIdPoint[i]+1);
	    if (it != idss.end()) {
	      for (int ic=0; ic<3;++ic)
		m_elementMatBD[0]->mat()(i+3*ic,i+3*ic) = 1e30;
	    }	  
	  }
	}
      }
      if ( (FelisceParam::instance().fsiRNscheme == 4) and (this->dimension() == 3) ) {
	// Dirichlet BC on the ringds, Robin-Neumann 
	std::vector<int> idss {118, 120, 122, 124, 126, 128, 240, 238, 236, 234, 232, 229, 228, 440, 438, 444, 442, 446, 448, 352, 350, 348, 346, 344, 342, 340, 114, 115};
	std::vector<int> ids {5371, 5372, 5373, 5374, 5375, 5376, 5424, 5423, 5422, 5421, 5420, 5419, 5315, 5314, 5316, 5317, 5319, 5318, 5320, 5264, 5263, 5262, 5261, 5260, 5259, 5257, 5258, 5370};
	if (label == 3 ) { 
	  for (int i=0; i < 3; ++i) {
	    auto it = find (ids.begin(),ids.end(), elemIdPoint[i]+1);
	    if (it != ids.end()) {
	      for (int ic=0; ic<3;++ic)
		m_elementMatBD[0]->mat()(i+3*ic,i+3*ic) = 1e30; 
	    }	  
	  } 
	}
	else if (label == 2 ) {
	  for (int i=0; i < 3; ++i) {
	    auto it = find (idss.begin(),idss.end(), elemIdPoint[i]+1);
	    if (it != idss.end()) {
	      for (int ic=0; ic<3;++ic)
		m_elementMatBD[0]->mat()(i+3*ic,i+3*ic) = 1e30;
	    }	  
	  }
	}
      }
      
      m_Pt.clear();
      if ( ((label == 4) && (this->dimension() == 2)) || (  (label == 2) && (this->dimension() == 3)   )) {
        if ( m_linearizedFlag )
          m_Pt.push_back(0.);
        else
          m_Pt.push_back(m_inPress(m_fstransient->time));
	
        m_elemFieldNeumannNormal[0].setValue(m_Pt);
      }

      // Robin-Neumann 
      if ( ( FelisceParam::instance().fsiRNscheme != -1000 ) and ( FelisceParam::instance().fsiRNscheme != 4 ) and 
	   ( ( ( label == 3 ) and (this->dimension() == 2 ) ) or
	     ( ( label == 1 ) and (this->dimension() == 3 ) ) ) ) 
        {
          m_elemFieldInterfaceVel.setValue(this->externalVec(1), curvFeVel, iel, m_iVelocity, m_ao, dof());   
          double rho     = 1.2;
          double epsilon = 0.1;
          double tau     = m_fstransient->timeStep;
          double a = rho*epsilon/tau ;
          m_elementMatBD[0]->phi_i_phi_j(a,curvFeVel,0,0, this->dimension()); //(rho*epsilon/tau)*int_Sigma[u*v] 
          this->elemVecBD()->source(a,curvFeVel,m_elemFieldInterfaceVel,0, this->dimension()); //(rho*epsilon/tau)*int_Sigma[\dotd^{k-1}*v]  	     
        }
    }
    
    // cavity with flexible bottom (shell) -- Robin-Neuman scheme 
    if ( (FelisceParam::instance().testCase == 2) and ( FelisceParam::instance().fsiRNscheme != -1000 ) ) {
      if ( label == 1 ) {
    	m_elemFieldInterfaceVel.setValue(this->externalVec(1), curvFeVel, iel, m_iVelocity, m_ao, dof());	
    	double rho     = 250;
    	double epsilon = 0.002;
    	double tau     = m_fstransient->timeStep;
    	double a = FelisceParam::instance().gammaRN*rho*epsilon/tau;

    	m_elementMatBD[0]->phi_i_phi_j(a,curvFeVel,0,0, this->dimension()); //(rho*epsilon/tau)*int_Sigma[u*v] 
    	this->elemVecBD()->source(a,curvFeVel,m_elemFieldInterfaceVel,0, this->dimension()); //(rho*epsilon/tau)*int_Sigma[\dotd^{k-1}*v]     
      }  
    }

    // 20 : cavity with flexible bottom (2d) -- Robin-Robin scheme
    // 10 : pressure wave 3d
    if ( (FelisceParam::instance().testCase == 20) || (FelisceParam::instance().testCase == 10)   ) {
      if ( label == 1 ) {
	m_elemFieldInterfaceVel.setValue(this->externalVec(1), curvFeVel, iel, m_iVelocity, m_ao, dof());
    	double a = FelisceParam::instance().gammaRN;
	 
    	m_elementMatBD[0]->phi_i_phi_j(a,curvFeVel,0,0, this->dimension()); //gamma*int_Sigma[u^n*v]
    	this->elemVecBD()->source(a,curvFeVel,m_elemFieldInterfaceVel,0, this->dimension()); //\gamma*int_Sigma[\dotd^n*v]
      }  
    }
    
    // FSI with RIS valves -- Robin-Neuman scheme 
    if ( (FelisceParam::instance().testCase == 100) and ( FelisceParam::instance().fsiRNscheme != -1000 ) ) {
      if ( label == 2 ) {
	m_elemFieldInterfaceVel.setValue(this->externalVec(1), curvFeVel, iel, m_iVelocity, m_ao, dof());	
	double rho     = 1.2;
	double epsilon = 0.2;
	double tau     = m_fstransient->timeStep;
	double a = FelisceParam::instance().gammaRN*rho*epsilon/tau;
	
	m_elementMatBD[0]->phi_i_phi_j(a,curvFeVel,0,0, this->dimension()); //(rho*epsilon/tau)*int_Sigma[u*v] 
	this->elemVecBD()->source(a,curvFeVel,m_elemFieldInterfaceVel,0, this->dimension()); //(rho*epsilon/tau)*int_Sigma[\dotd^{k-1}*v]     
      }
    }

    // FSI closed valve -- ALE or FD 
    if ( FelisceParam::instance().testCase == 100000 ) {
      if ( (FelisceParam::instance().fsiRNscheme != -1000) and FelisceParam::instance().useALEformulation )  {
	if ( label == 8 ) { 
	  m_elemFieldInterfaceVel.setValue(this->externalVec(1), curvFeVel, iel, m_iVelocity, m_ao, dof());	
	  double rho     = 100.;
	  double epsilon = 0.0212;
	  double tau     = m_fstransient->timeStep;
	  double a = FelisceParam::instance().gammaRN*rho*epsilon/tau;
	  
	  m_elementMatBD[0]->phi_i_phi_j(a,curvFeVel,0,0, this->dimension()); //(rho*epsilon/tau)*int_Sigma[u*v] 
	  this->elemVecBD()->source(a,curvFeVel,m_elemFieldInterfaceVel,0, this->dimension()); //(rho*epsilon/tau)*int_Sigma[\dotd^{k-1}*v]     
	}
      }

      m_Pt.clear();
      if ( label == 4 ) {
        if ( m_linearizedFlag )
          m_Pt.push_back(0.);
        else
          m_Pt.push_back( -3.0e5*std::atan(10.0*m_fstransient->time) );
        m_elemFieldNeumannNormal[0].setValue(m_Pt);
      }
    }
    
  }

  void UserNSALE::userElementComputeBD(const std::vector<Point*>& elemPoint, const std::vector<felInt>& elemIdPoint,felInt& iel) {

    CurvilinearFiniteElement& curvFeVel = *m_listCurvilinearFiniteElement[m_iVelocity];
    curvFeVel.updateMeasNormalQuadPt(0, elemPoint);

    std::cout <<  "iel = " << iel << std::endl;
    
    m_elemFieldInterfaceVel.initialize(DOF_FIELD,curvFeVel,this->dimension());
    m_elemFieldPreviousVel.initialize(DOF_FIELD,curvFeVel, this->dimension());

    // 20 : cavity with flexible bottom (2d) -- Robin-Robin scheme
    // 10 : pressure wave3D 
    if ( (FelisceParam::instance().testCase == 20) || (FelisceParam::instance().testCase == 10)  ) {
      if ( m_currentLabel == 1 ) {
	// consistent relative flow stabilization
	m_elemFieldInterfaceVel.setValue(this->externalVec(1), curvFeVel, iel, m_iVelocity, m_ao, dof()); // vel^n = w^n 
	m_elemFieldPreviousVel.setValue(m_solExtrapol, curvFeVel, iel, m_iVelocity, m_ao, dof()); // u^{n-1} 
	m_elemFieldPreviousVel.val -= m_elemFieldInterfaceVel.val; // u^{n-1} - w^n 
	const double coef = 0.5 * FelisceParam::instance().density;	
	m_elementMatBD[0]->f_dot_n_phi_i_phi_j(-coef, curvFeVel , m_elemFieldPreviousVel, 0, 0, dimension());
      }  
    }
    
  }
  

  double inPressure::operator() (double t) const
  { 
    double pi = acos(-1.);
    double Tstar = 0.005;
    double Pmax  = 2.0E4;
    double val = 0.; 
    if ( t < Tstar )
      val=  -0.5*Pmax*(1-cos(2.*t*pi/Tstar));
    return val;
  }


  double velCavity::operator() (double t) const
  { 
    double pi = acos(-1.);
    return  1-cos(0.4*pi*t);
  }
  
  double velCavityZero::operator() (double t) const
  { 
    return  0.;
  }
  
}
