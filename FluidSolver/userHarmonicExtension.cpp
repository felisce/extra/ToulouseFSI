/*============================================================================= 
  This file is part of the finite element code FELiScE.
  Finite Element for Life Sciences and Engineering
  Copyright (C) 2009, INRIA
 
  Authors: 
  Julien Castelneau, Dominique Chapelle, Miguel Fernandez, 
  Jeremie Foulon, Jean-Frederic Gerbeau, 
  Vincent Martin, Philippe Moireau, Marina Vidrascu
 
  FELiScE is free software; you can redistribute it and/or modify it under
  the terms of the GNU Lesser General Public License as published by the Free
  Software Foundation; either version 2.1 of the License, or (at your option)
  any later version.
 
  FELiScE is distributed in the hope that it will be useful, but WITHOUT ANY
  WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
  more details.
 
  You should have received a copy of the GNU Lesser General Public License
  along with FELiScE. If not, see http://www.gnu.org/licenses/.
  =============================================================================*/
// SVN Version $Id

  /*!
    \file linearProblemPoisson.hpp
    \authors M.A. Fernández and M. Landajuela
    \date May 2013
    \brief Matrix and rhs for the Poisson solver.
  */

#include <userHarmonicExtension.hpp>
#include <DegreeOfFreedom/boundaryCondition.hpp>

using namespace std;
namespace felisce
{
  
  UserHarmonicExtension::UserHarmonicExtension():
    LinearProblemHarmonicExtension()
  {}
  
  UserHarmonicExtension::~UserHarmonicExtension()
  {}
  
  void UserHarmonicExtension::userFinalizeEssBCTransient()
  {
      // Boilevin-Kayl 11/2018: the following will need to be modified when the FD-FSI will be REALLY coupled with a fluid boudary solved with ALE
      if ( (m_fstransient->iteration > 0) and (not FelisceParam::instance().hasImmersedData) )
      { 
      
        //===============getListDofOfBC===============//
        BoundaryCondition& BC = *m_boundaryConditionList[0]; 
        int iUnknown = BC.getUnknown();
        int idVar = m_listUnknown.idVariable(iUnknown);
	const int idMsh = m_listVariable[idVar].idMesh();
      
        felInt idEltInSupportLocal = 0;
        felInt idEltInSupportGlobal = 0;
        felInt idDof;
        double aux;
        BC.ValueBCInSupportDof().clear();
        for (unsigned iSupportDofBC = 0; iSupportDofBC < BC.idEltAndIdSupport().size(); iSupportDofBC++)
	  {
	    idEltInSupportLocal = BC.idEltAndIdSupport()[iSupportDofBC].first;
	    ISLocalToGlobalMappingApply(m_mappingElem[idMsh],1,&idEltInSupportLocal,&idEltInSupportGlobal);
	    for( auto it_comp = BC.getComp().begin(); it_comp != BC.getComp().end(); it_comp++)
	      {
	        dof().loc2glob(idEltInSupportGlobal, BC.idEltAndIdSupport()[iSupportDofBC].second, idVar, *it_comp, idDof);
	        AOApplicationToPetsc(m_ao,1,&idDof);
	        this->externalVec(0).getValues(1, &idDof, &aux);
	        BC.ValueBCInSupportDof().push_back(aux);	      
	      }
	  }
      }
    }  
}

