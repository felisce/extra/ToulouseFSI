/*============================================================================= 
 This file is part of the finite element code FELiScE.
 Finite Element for Life Sciences and Engineering
 Copyright (C) 2009, INRIA
 
 Authors: 
 Julien Castelneau, Dominique Chapelle, Miguel Fernandez, 
 Jeremie Foulon, Jean-Frederic Gerbeau, 
 Vincent Martin, Philippe Moireau, Marina Vidrascu
 
 FELiScE is free software; you can redistribute it and/or modify it under
 the terms of the GNU Lesser General Public License as published by the Free
 Software Foundation; either version 2.1 of the License, or (at your option)
 any later version.
 
 FELiScE is distributed in the hope that it will be useful, but WITHOUT ANY
 WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
 more details.
 
 You should have received a copy of the GNU Lesser General Public License
 along with FELiScE. If not, see http://www.gnu.org/licenses/.
 =============================================================================*/
// SVN Version $Id

/*!
 \file UserNSALE.hpp
 \authors M.A. Fernández
 \date Feb 2014
 \brief UserNSALE class
 */


#ifndef _UserNSALE_HPP
#define _UserNSALE_HPP
#include <Solver/linearProblemNS.hpp>
#include <math.h>
#include <Core/felisceParam.hpp>


#define PI acos(-1.0)

using namespace std;

namespace felisce
{

  class inPressure
  {
  public:
    double operator() (double t) const;
  };


  class velCavity
  {
  public:
    double operator() (double t) const;
  };

  class velCavityZero
  {
  public:
    double operator() (double t) const;
  };

  class UserNSALE:
    public LinearProblemNS
  {
  public:
    UserNSALE(); 
    ~UserNSALE();
    
    void userFinalizeEssBCTransient() override;
  
    void userElementComputeNaturalBoundaryCondition(const std::vector<Point*>& elemPoint, const std::vector<felInt>& elemIdPoint, felInt& iel, int label) override;

    void userElementComputeBD(const std::vector<Point*>& elemPoint, const std::vector<felInt>& elemIdPoint,felInt& iel) override;
    
    double evaluateMean(PetscVector &vec);
    
  protected:

    ElementField m_elemFieldInterfaceVel;
    ElementField m_elemFieldPreviousVel;
    
    vector<double> m_Pt;    
    inPressure m_inPress;
    velCavity m_velCavity;
    velCavityZero m_velCavityZero;
  };

}

#endif
