/*============================================================================= 
 This file is part of the finite element code FELiScE.
 Finite Element for Life Sciences and Engineering
 Copyright (C) 2009, INRIA
 
 Authors: 
 Julien Castelneau, Dominique Chapelle, Miguel Fernandez, 
 Jeremie Foulon, Jean-Frederic Gerbeau, 
 Vincent Martin, Philippe Moireau, Marina Vidrascu
 
 FELiScE is free software; you can redistribute it and/or modify it under
 the terms of the GNU Lesser General Public License as published by the Free
 Software Foundation; either version 2.1 of the License, or (at your option)
 any later version.
 
 FELiScE is distributed in the hope that it will be useful, but WITHOUT ANY
 WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
 more details.
 
 You should have received a copy of the GNU Lesser General Public License
 along with FELiScE. If not, see http://www.gnu.org/licenses/.
 =============================================================================*/
// SVN Version $Id

/*!
 \file UserHarmonicExtension.hpp
 \authors M.A. Fernández
 \date Feb 2014
 \brief UserHarmonicExtension class
 */


#ifndef _UserHarmonicExtension_HPP
#define _UserHarmonicExtension_HPP
#include <Solver/linearProblemHarmonicExtension.hpp>
using namespace std;

namespace felisce
{
  class UserHarmonicExtension:
    public LinearProblemHarmonicExtension
  {
  public:
    UserHarmonicExtension(); 
    ~UserHarmonicExtension();
    void userFinalizeEssBCTransient();
  };

}

#endif
