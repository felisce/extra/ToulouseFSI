#include "userCurvedBeam.hpp"
#include "FiniteElement/elementField.hpp"
#include "FiniteElement/elementVector.hpp"
#include "FiniteElement/elementMatrix.hpp"

using namespace std;
namespace felisce {

  UserCurvedBeam::UserCurvedBeam():
    LinearProblemCurvedBeam()
  {  }

  UserCurvedBeam::~UserCurvedBeam()
  {  }
    
  void UserCurvedBeam::userElementInitBD() 
  {

    m_elemField.initialize(DOF_FIELD,*m_feDisp,m_listVariable[m_iDisp].numComponent());
    m_elemField_disp.initialize(DOF_FIELD,*m_feDisp,m_listVariable[m_iDisp].numComponent());
    m_elemField_dispo.initialize(DOF_FIELD,*m_feDisp,m_listVariable[m_iDisp].numComponent());
    m_elemField_velfo.initialize(DOF_FIELD,*m_feDisp,m_listVariable[m_iDisp].numComponent());
    
    // FSI with RIS valves : solid volume force 
    if (FelisceParam::instance().testCase == 100) {
      double xpi = std::acos(-1.0);
      // here volumic force is definied
      UBlasMatrix& val= this->m_elemField.get_val();
      val.clear();
      for (size_t j=0;  j < val.size2(); ++j) 
	val(1,j)=100.0*std::sin(4 * xpi * m_fstransient->time);
    }  
  }
  
  void UserCurvedBeam::userElementComputeBD(const std::vector<Point*>& elemPoint, const std::vector<felInt>& elemIdPoint,felInt& iel)
  {
    (void) elemPoint;
    (void) elemIdPoint;
    (void) iel;
    
    m_feDisp->updateMeasQuadPt(0, elemPoint);
    
    // FSI with RIS valves : solid volume force 
    if (FelisceParam::instance().testCase == 100) {
      this->m_elementVectorBD[0]->source(1.,*m_feDisp,this->m_elemField,0,2);
    }

    // cavity with flexible bottom RR
    if (FelisceParam::instance().testCase == 20) {
      
      const double gamma = FelisceParam::instance().gammaRN;
      const double tau =  1./m_fstransient->timeStep;
      
      this->m_elementMatBD[0]->phi_i_phi_j(gamma*tau,*m_feDisp,0,0,2);

      m_elemField_disp.setValue( this->seqEvaluationState(), *m_feDisp, iel, m_iDisp, m_ao, dof());
      m_elemField_dispo.setValue(this->externalVec(1),       *m_feDisp, iel, m_iDisp, m_ao, dof());
      m_elemField_velfo.setValue(this->externalVec(4),       *m_feDisp, iel, m_iDisp, m_ao, dof());
      m_elemField.val = tau*(m_elemField_disp.val - m_elemField_dispo.val);
      m_elemField.val -= m_elemField_velfo.val;
      this->m_elementVectorBD[0]->source(gamma,*m_feDisp,this->m_elemField,0,2);
    }
  }
}
