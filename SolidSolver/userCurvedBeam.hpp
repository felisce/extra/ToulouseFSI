#ifndef _UserCURVEDBEAM_HPP
#define _UserCURVEDBEAM_HPP
#include <Solver/linearProblemCurvedBeam.hpp>
#include <Core/felisceParam.hpp>
using namespace std;

namespace felisce {

  class UserCurvedBeam:
    public LinearProblemCurvedBeam {
  public:
    UserCurvedBeam();
    ~UserCurvedBeam();
    
    void userElementInitBD() override;
    void userElementComputeBD(const std::vector<Point*>& elemPoint, const std::vector<felInt>& elemIdPoint,felInt& iel) override;
  private:
    ElementField m_elemField;
    ElementField m_elemField_disp;
    ElementField m_elemField_dispo;
    ElementField m_elemField_velfo;
  };
}


#endif
