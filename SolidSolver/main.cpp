#include "Model/curvedBeamModel.hpp"
#include "userCurvedBeam.hpp"
#include <Core/solidToMaster.hpp>
using namespace felisce;


int main(const int argc, const char ** argv)
{
  // read command line and data file
  const std::size_t instanceIndex = 0; // It is already 0 by default, this is an example
  auto opt = FelisceParam::instance(instanceIndex).initialize(argc, argv);
  FelisceTransient fstransient;
  
  // copy the data file in the Post-processing folder
  felisce::filesystemUtil::copyFileSomewhereElse(opt.dataFileName(), FelisceParam::instance().resultDir + "/dataFile");
  
  // create model and solver
  CurvedBeamModel model;
  model.initializeModel(opt,fstransient);
  model.initializeLinearProblem( new UserCurvedBeam(), true); 
 int nbinter = model.linearProblem(0)->supportDofUnknown(0).listNode().size();
  int nbCoor = 2;
  int nbDOF = nbCoor*nbinter;

  std::vector<double> lumpM, dispS, veloS, dispSo, forceF, forceC;
  lumpM.resize(nbDOF,0.);
  dispS.resize(nbDOF,0.);
  veloS.resize(nbDOF,0.);
  dispSo.resize(nbDOF,0.);
  forceF.resize(nbDOF,0.);
  forceC.resize(nbDOF,0.);
  

  SolidToMaster solidToMaster(-1,  nbinter, nbCoor, lumpM, dispS, veloS, forceF, forceC);
  
  if ( FelisceParam::instance().evalEnergy == true )
    solidToMaster.setEnergy(model.linearProblem(0)->energy());
  
   
  int rank;
  MPI_Comm_rank(MpiInfo::petscComm(), &rank);
  if (rank == 0) { // only proc 0 communicates with master
    solidToMaster.setMaster();
    //solidToMaster.sdToMasterFSI(99);
    //if ( ! FelisceParam::instance().testCase == 20 ) {
    if ( FelisceParam::instance().evalEnergy == true )
      solidToMaster.sdToMasterFSI(1100);
    else
      solidToMaster.sdToMasterFSI(100);
    //}
    //else
    //  solidToMaster.rvFromMasterFSI(20);
  }
  
  
  
  std::vector<PetscInt> ao_force(nbDOF);
  // sequential global numbering on the interface
  for (int i=0; i<nbinter; ++i) {
    for (int j=0; j<nbCoor; ++j) 
      ao_force[nbCoor*i+j]= nbCoor*i+j;
  }
  
  // from sequatial mesh global numberings  to petsc global numbering   
  AOApplicationToPetsc(model.linearProblem(0)->ao(),nbCoor*nbinter,ao_force.data());


  PetscVector forceFluid, forceContact;
  forceFluid.duplicateFrom(model.linearProblem(0)->rhs());  
  forceFluid.zeroEntries();
  forceContact.duplicateFrom(model.linearProblem(0)->rhs());  
  forceContact.zeroEntries();
  
  model.linearProblem(0)->pushBackExternalVec(forceFluid); // fluid force  [3] 
  if ( (FelisceParam::instance().evalEnergy == true) || (FelisceParam::instance().testCase == 20) )
    model.linearProblem(0)->pushBackExternalVec(forceContact);  // fluid velocity [4] 


  //UserCurvedBeam& lpb = *static_cast<UserCurvedBeam*>(model.linearProblem(0));
  
  int status;
   //Loop time
  do {
    
    if ( rank == 0 ) {
      if ( (FelisceParam::instance().evalEnergy == true) || (FelisceParam::instance().testCase == 20) )
	solidToMaster.rvFromMasterFSI(1100);
      else 
	solidToMaster.rvFromMasterFSI();
      status = solidToMaster.status();
      model.fstransient()->timeStep = solidToMaster.timeStep();
    }
    MPI_Bcast(&status,1, MPI_INT, 0, MpiInfo::petscComm());
    MPI_Bcast(&model.fstransient()->timeStep, 1, MPI_DOUBLE, 0, MpiInfo::petscComm());
    MPI_Bcast(forceF.data(), nbCoor*nbinter, MPI_DOUBLE, 0, MpiInfo::petscComm());
    
    if ( (FelisceParam::instance().evalEnergy == true) || (FelisceParam::instance().testCase == 20) )
      MPI_Bcast(forceC.data(), nbCoor*nbinter, MPI_DOUBLE, 0, MpiInfo::petscComm());
    
    // inserting interface values into petsc vector with petsc numbering 
    forceFluid.setValues(nbCoor*nbinter,ao_force.data(),forceF.data(), INSERT_VALUES);
    forceFluid.assembly();

    if ( (FelisceParam::instance().evalEnergy == true) || (FelisceParam::instance().testCase == 20)  ) {
      forceContact.setValues(nbCoor*nbinter,ao_force.data(),forceC.data(), INSERT_VALUES);
      forceContact.assembly();
    }

    std::vector<int> label(1);
    label[0]=1;
    std::vector<double> flow(1);
    std::vector<double> measure(1);
    switch ( status ) {
      
    case -1: // Normal exit
      std::cout << "Normal solid exit\n" << std::flush;
      model.writeSolution(); 
      exit(1);
      break;
    case 1: // New time step
      for (int i=0; i < nbDOF; ++i) {
	dispSo[i] = dispS[i];
	//std::cout << dispSo[i] << std::endl;
      }
      model.forward();
      //lpb.meshLocal().moveMesh(dispSo,1.0);
      //std::cout << " ########### MEAN 1 = " << lpb.evaluateMean() << std::endl;
      //lpb.meshLocal().moveMesh(dispSo,0.0);
      break;
    case 0:
      // Inner iteration
      model.solve();
      //lpb.meshLocal().moveMesh(dispSo,1.0);
      //std::cout << " ########### MEAN 0 = " << lpb.evaluateMean() << std::endl;
      //lpb.meshLocal().moveMesh(dispSo,0.0);
      break;
    case 2: 
      // Inner tangent iteration 
      model.solveLinearized();
      //lpb.meshLocal().moveMesh(dispSo,1.0);
      //std::cout << " ########### MEAN 2 = " << lpb.evaluateMeanL() << std::endl ;
      //lpb.meshLocal().moveMesh(dispSo,0.0);
      break;
    default:
      std::cout << " This status number is not allowed.\n" << std::endl << std::flush;
      exit(1);	
    }
     
    if ( rank == 0 ) { 
      model.linearProblem(0)->sequentialSolution().getValues(nbCoor*nbinter, ao_force.data(), dispS.data());
      if ( status == 2 )
	for (int i=0; i < nbDOF; ++i) {
	  veloS[i] = dispS[i]/model.fstransient()->timeStep;
	}
      else {
	for (int i=0; i < nbDOF; ++i) 
	  veloS[i] = (dispS[i] - dispSo[i])/model.fstransient()->timeStep;
      }
      
      if ( FelisceParam::instance().evalEnergy == true )
	solidToMaster.sdToMasterFSI(1100);
      else
	solidToMaster.sdToMasterFSI(100);
    }
  }
  while ( 1 );  // close infinite do
}	




