//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:     LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    B. Fabreges, M. A. Fernandez, F.M. Gerosa
//

/*!
 \file  userFluid.cpp
 \authors B. Fabreges, M. A. Fernandez, F.M. Gerosa
 \date  04/2018
 \brief File where is implemented UserFluid class
 */

// System includes

// External includes

// Project includes
#include "userFluid.hpp"
#include "DegreeOfFreedom/boundaryCondition.hpp"
#include "FiniteElement/elementVector.hpp"

namespace felisce
{
  UserFluid::UserFluid():
    LinearProblemNitscheXFEM()
  {
  }
  
  UserFluid::~UserFluid()
  {}

  void UserFluid::userFinalizeEssBCTransient()
  {

    switch (FelisceParam::instance().testCase) {
      case 3 :
        if ( m_linearizedFlag )
          m_boundaryConditionList[1]->setValueVec(this, m_velZero, m_fstransient->time);
        else
          m_boundaryConditionList[1]->setValueVec(this, m_velVesicle, m_fstransient->time);
        break;

      case 4 :
        if ( m_linearizedFlag )
          m_boundaryConditionList[0]->setValueVec(this, m_velZero, m_fstransient->time);
        else
          m_boundaryConditionList[0]->setValueVec(this, m_velBall3D, m_fstransient->time);
        break;

      case 4000 :
        if ( m_linearizedFlag )
          m_boundaryConditionList[0]->setValueVec(this, m_velZero, m_fstransient->time);
        else 
          m_boundaryConditionList[0]->setValueVec(this, m_valveProfile, m_fstransient->time);
        break;

      // Open valve
      case 5000 :
        if ( m_linearizedFlag )
          m_boundaryConditionList[0]->setValueVec(this, m_velZero, m_fstransient->time);
        else
          m_boundaryConditionList[0]->setValueVec(this, m_valveProfile, m_fstransient->time);
        break;

      default :
        break;
    }
  }

  void UserFluid::userElementComputeNaturalBoundaryCondition(const std::vector<Point*>& /*elemPoint*/, const std::vector<felInt>& elemIdPoint, felInt& iel, int /*label*/)
  {

    // Stabilisation of backflow
    if (FelisceParam::instance().addedBoundaryFlag == 2){
      auto stabilizationLabel = FelisceParam::instance().stabilizationLabel;
      if ( std::find(stabilizationLabel.begin(), stabilizationLabel.end(), m_currentLabel) != stabilizationLabel.end() ){
        const double coef = FelisceParam::instance().stabilizationCoef * FelisceParam::instance().density;
        
        m_elemFieldPreviousVel.initialize(DOF_FIELD, *m_curvFeVel, m_velocity->numComponent());
        m_elemFieldPreviousVel.setValue(m_seqVelExtrapol[0], *m_curvFeVel, iel, m_iVelocity, m_aoOld[0], m_dofOld[0]); 
        m_elementMatBD[0]->f_dot_n_phi_i_phi_j(-coef, *m_curvFeVel, m_elemFieldPreviousVel, m_velBlock, m_velBlock, m_velocity->numComponent(), FelisceParam::instance().addedBoundaryFlag);
      }
    }

    switch (FelisceParam::instance().testCase) {
      case 1000 :
        for(size_t i=0; i < m_boundaryConditionList.numNeumannNormalBoundaryCondition(); ++i){
          m_BC = m_boundaryConditionList.NeumannNormal(i);
          if (m_BC->typeValueBC() == FunctionT){
            for (auto it_labelNumber = m_BC->listLabel().begin();
              it_labelNumber != m_BC->listLabel().end(); it_labelNumber++){
              if(*it_labelNumber == m_currentLabel){
                m_Pt.clear();
                m_Pt.push_back(m_pressureInlet(m_fstransient->time));
                m_elemFieldNeumannNormal[i].setValue(m_Pt);
              }
            }
          }
        }
        break;

      case 1001 :
        for(size_t i=0; i < m_boundaryConditionList.numNeumannNormalBoundaryCondition(); ++i){
          m_BC = m_boundaryConditionList.NeumannNormal(i);
          if (m_BC->typeValueBC() == FunctionT){
            for (auto it_labelNumber = m_BC->listLabel().begin();
              it_labelNumber != m_BC->listLabel().end(); it_labelNumber++){
              if(*it_labelNumber == m_currentLabel){
                m_Pt.clear();
                m_Pt.push_back(m_pressureInletD(m_fstransient->time));
                m_elemFieldNeumannNormal[i].setValue(m_Pt);
              }
            }
          }
        }
        break;

      // Aortic valve
      case 1002 :
        for(size_t i=0; i < m_boundaryConditionList.numNeumannNormalBoundaryCondition(); ++i){
          m_BC = m_boundaryConditionList.NeumannNormal(i);
          if (m_BC->typeValueBC() == FunctionT){
            for (auto it_labelNumber = m_BC->listLabel().begin();
              it_labelNumber != m_BC->listLabel().end(); it_labelNumber++){
              if(*it_labelNumber == m_currentLabel){
                m_Pt.clear();
                m_Pt.push_back(m_pressureInletAorticValve(m_fstransient->time));
                m_elemFieldNeumannNormal[i].setValue(m_Pt);
              }
            }
          }
        }
        break;

      case 2000 :
        for(size_t i=0; i < m_boundaryConditionList.numNeumannNormalBoundaryCondition(); ++i){
          m_BC = m_boundaryConditionList.NeumannNormal(i);
          if (m_BC->typeValueBC() == FunctionT){
            for (auto it_labelNumber = m_BC->listLabel().begin();
              it_labelNumber != m_BC->listLabel().end(); it_labelNumber++){
              if(*it_labelNumber == m_currentLabel){
                m_Pt.clear(); 
                m_Pt.push_back(m_inPressFDNSwallValves(m_fstransient->time));
                m_elemFieldNeumannNormal[i].setValue(m_Pt);
              }
            }
          }
        }
        break;

      // Closed valve
      case 3000 :
        for(size_t i=0; i < m_boundaryConditionList.numNeumannNormalBoundaryCondition(); ++i){
          m_BC = m_boundaryConditionList.NeumannNormal(i);
          if (m_BC->typeValueBC() == FunctionT){
            for (auto it_labelNumber = m_BC->listLabel().begin();
              it_labelNumber != m_BC->listLabel().end(); it_labelNumber++){
              if(*it_labelNumber == m_currentLabel){
                m_Pt.clear();
                if ( m_linearizedFlag )
                  m_Pt.push_back(m_closedValvePressureZero(m_fstransient->time));
                else
                  m_Pt.push_back(m_closedValvePressure(m_fstransient->time));
                m_elemFieldNeumannNormal[i].setValue(m_Pt);
              }
            }
          }
        }
        break;

      default :
        break;
    }
  }

  double UserFluid::mean( std::vector<double>& mean_vec)
  {
    const auto& r_par = FelisceParam::instance();
    if ( r_par.meanLabel.size() == 0 )
      return 0.;

    ElementType eltType;              //geometric element type in the mesh.
    int numPointPerElt = 0;           //number of points per geometric element.
    int currentLabel = 0;             //number of label domain.
    felInt numEltPerLabel = 0;        //number of element for one label and one eltType.
    double tmp, value = 0;

    // fill vector
    std::fill(mean_vec.begin(), mean_vec.end(), 0.);

    // use to define a "global" numbering of element in the mesh.
    felInt numElement[ meshLocal()->m_numTypesOfElement ];
    for (int ityp=0; ityp< meshLocal()->m_numTypesOfElement; ityp++ ) {
      eltType = (ElementType)ityp;
      numElement[eltType] = 0;
    }

    // contains points of the current element.
    std::vector<Point*> elemPoint;
    std::vector<Point*> facePoint;
    
    // contains ids of point of the current element.
    std::vector<felInt> elemIdPoint;

    // contains the ids of the support elements of a mesh element
    std::vector<felInt> vectorIdSupport;

    // use to get element number in support dof mesh ordering.
    ElementField m_elemU;

    //Assembly loop.
    //First loop on geometric type.
    const std::vector<ElementType>& bagElementTypeDomainBoundary = meshLocal()->bagElementTypeDomainBoundary();
    for (std::size_t i = 0; i < bagElementTypeDomainBoundary.size(); ++i) {
      eltType =  bagElementTypeDomainBoundary[i];

      defineCurvilinearFiniteElement(eltType);
      m_curvFeVel = m_listCurvilinearFiniteElement[m_iVelocity];

      m_elemU.initialize(QUAD_POINT_FIELD, *m_curvFeVel, m_listVariable[m_iVelocity].numComponent());
    
      // Resize array.
      numPointPerElt = meshLocal()->m_numPointsPerElt[eltType];
      elemPoint.resize(numPointPerElt, nullptr);
      elemIdPoint.resize(numPointPerElt, 0);
      
      // Second loop on region of the mesh.
      for(auto itRef = meshLocal()->intRefToBegEndMaps[eltType].begin(); itRef != meshLocal()->intRefToBegEndMaps[eltType].end(); itRef++) {
        currentLabel = itRef->first;
        numEltPerLabel = itRef->second.second;

        if ( r_par.meanLabel.end() == find(r_par.meanLabel.begin(), r_par.meanLabel.end(), currentLabel) ){
          numElement[eltType] += numEltPerLabel; 
          continue;
        }

        //Third loop on element in the region with the type: eltType. ("real" loop on elements).
        tmp = 0;
        for (felInt iel = 0; iel < numEltPerLabel; iel++) {
          std::vector<felInt> vectorIdSupport;

          // get the global id of the element
          felInt ielGeoGlobal, ielGeo = 0;
          meshLocal()->getIdElemFromTypeElemAndIdByType(eltType, numElement[eltType], ielGeo);
          ISLocalToGlobalMappingApply(mappingElem(), 1, &ielGeo, &ielGeoGlobal);

          // return each id of point of the element and coordinate in two arrays: elemPoint and elemIdPoint.
          setElemPoint(eltType, numElement[eltType], elemPoint, elemIdPoint, vectorIdSupport);

          // Fourth loop over all the support elements
          for (std::size_t it1 = 0; it1 < vectorIdSupport.size(); it1++) {
            // get the id of the support
            felInt ielSupportDof1 = vectorIdSupport[it1];

            // Update velocity
            m_elemU.setValue(this->sequentialSolution(), *m_curvFeVel, ielSupportDof1, m_iVelocity, m_ao, dof());
            UBlasMatrix& val = m_elemU.get_val();

            // get the side of the sub elements
            std::vector<sideOfInterface> bndElemSide;
            m_duplicateSupportElements->getSubBndEltSide(ielGeoGlobal, bndElemSide);

            // check if the face belongs to the intersected mesh
            if( bndElemSide.size() > 0 ) {

              if ( m_curvFeVel->hasOriginalQuadPoint() )
                m_curvFeVel->updateMeasNormalContra(0, elemPoint);
              else
                m_curvFeVel->updateBasisAndNormalContra(0, elemPoint);

              // detect current side
              felInt ielSupport;
              m_supportDofUnknown[0].getIdElementSupport(ielGeoGlobal, ielSupport);
              const sideOfInterface side = ielSupport == ielSupportDof1 ? LEFT : RIGHT;

              // get sub-faces coordinates
              std::vector< std::vector< Point > > bndElemCrd;
              m_duplicateSupportElements->getSubBndElt(ielGeoGlobal, bndElemCrd);

              // loop over the sub-faces
              for(std::size_t iSubElt = 0; iSubElt < bndElemSide.size(); ++iSubElt) {

                // check if sub-face is the good side
                if ( bndElemSide[iSubElt] != side )
                  continue;

                // get sub-face coordinates
                for ( std::size_t iPt = 0; iPt < m_numVerPerSolidElt; ++iPt)
                  *m_subItfPts[iPt] = bndElemCrd[iSubElt][iPt];

                // update finite elements
                m_curvFeVel->updateSubElementMeasNormal(0, elemPoint, m_subItfPts);

                for (int iq = 0; iq < m_curvFeVel->numQuadraturePoint(); ++iq) 
                  for (int ic = 0; ic < m_curvFeVel->numCoor(); ++ic)
                    tmp += val(ic,iq) * m_curvFeVel->weightMeas(iq) * m_curvFeVel->normal[iq](ic);
              }
            } else {

              if ( m_curvFeVel->hasOriginalQuadPoint() )
                m_curvFeVel->updateMeasNormalContra(0, elemPoint);
              else
                m_curvFeVel->updateBasisAndNormalContra(0, elemPoint);

              for (int iq = 0; iq < m_curvFeVel->numQuadraturePoint(); ++iq) 
                for (int ic = 0; ic < m_curvFeVel->numCoor(); ++ic)
                  tmp += val(ic,iq) * m_curvFeVel->weightMeas(iq) * m_curvFeVel->normal[iq](ic);
            }
          }

          numElement[eltType]++;
        }
     
        value += tmp;

        mean_vec[currentLabel] = tmp;
      }
    }

    return value;
  }
}
