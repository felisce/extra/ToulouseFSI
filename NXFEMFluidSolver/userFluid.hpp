//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:     LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    B. Fabreges, M. A. Fernandez, F.M. Gerosa
//

/*!
 \file    userFluid.hpp
 \authors M. Fernández & F.M. Gerosa
 \date    04/2018
 \brief   File where is define user specificities to solve a fluid problem with Nitsche-XFEM
 */

#ifndef _UserFluid_HPP
#define _UserFluid_HPP

// System includes
#include <math.h>

// External includes

// Project includes
#include "Core/felisceParam.hpp"
#include "Solver/linearProblemNitscheXFEM.hpp"  

#define PI acos(-1.0)

namespace felisce
{ 
  class ClosedValvePressure 
  {
    public:
      double operator() (double t) const
      {
        return -3.0e5*std::atan(10.0*t); 
      }
  };

  class ClosedValvePressureZero 
  {
    public:
      double operator() (double t) const
      {
        return 0.0;
      }
  };

  class PressureInlet 
  {
    public:
      double operator() (double t) const
      {
        return -100 * sin(2. * PI * t);
      }
  }; 

  class PressureInletAorticValve 
  {
    public:
      double operator() (double t) const
      {
        return - 1333.224 * ( 80. + 40. * sin(4.2 * PI * t) ); // maximum at t = 0.12
      }
  }; 

  class VelocityInletAorticValve 
  {
    public:
      double operator() (int iComp, double x, double /*y*/, double z, double t) const
      {
        if(iComp == 1)
          return  70 * sin(4.2 * PI * t) * (1 - std::pow(0.0091 - x, 2) /*- std::pow(-0.6330 - y, 2)*/ - std::pow(0.0692 - z, 2));
        else
          return 0.;
      }
  };

  class PressureInletD 
  {
    public:
      double operator() (double t) const
      {
        return -5*t; 
      }
  };

  class inPressureFDNSwallValves
  {
    public:
      double operator() (double t) const
      { 
        const double T = 1.0;         // total period
        const double T_1 = 0.7;       // period for positive pressure
        const double amplitude = 200;
        double result;

        while (t > T) {
          t = t - T;
        }

        if ( t < T_1 )
          result = + tanh(100.*t) * amplitude;
        else
          result = - amplitude;

        return result;
      }
  };

  class ValveProfile 
  {
    public:
      double operator() (int iComp, double /*x*/, double y, double /*z*/, double t) const
      {
        if(iComp == 0){
          return 5 * (sin(2 * PI * t) +1.1) * y * (1.61 - y);
        }
        else{
          return 0.;
        }
      }
  };

  class velVesicle 
  {
  public:
    double operator() (int iComp, double x, double /*y*/, double z, double t) const
    {
      // if(iComp == 0){
      //   if(x<= 0.5){
      //     if (z<= 0.5)
      //       return tanh(100*x*z);
      //     else
      //       return -tanh(100*x*(z-1));
      //   }
      //   else {
      //     if (z<= 0.5)
      //       return -tanh(100*(x-1)*z);
      //     else
      //       return tanh(100*(x-1)*(z-1));
      //   }
      // }
      // else{
      //   return 0.;
      // }
      if(iComp == 0){
        return 1.;
        // if(x<= 0.5)
        //   return tanh(100*x);
        // else 
        //   return -tanh(-100*(1-x));
      }
      else{
        return 0.;
      }
    }
  };

  class velZero
  {
    public:
      double operator() (int /*iComp*/, double /*x*/, double /*y*/, double /*z*/, double /*t*/)  const 
      {
        return 0.0;
      }
  };

  class velBall3D
  {
    public:
      double operator() (int iComp, double /*x*/, double y, double /*z*/, double /*t*/) const
      {
        if(iComp == 0){
          return y;
        }
        else{
          return 0.;
        }
      }
  };  
  
  class UserFluid:
    public LinearProblemNitscheXFEM
  {
    public:
      UserFluid(); 
      ~UserFluid();

      void userElementComputeNaturalBoundaryCondition(const std::vector<Point*>& elemPoint, const std::vector<felInt>& elemIdPoint, felInt& iel, int label) override;
      void userFinalizeEssBCTransient() override;

      double mean(std::vector<double>& mean_vec);
        
    private:
      BoundaryCondition*  m_BC;
      std::vector<double> m_Pt;

      PressureInlet  m_pressureInlet;
      PressureInletD m_pressureInletD;

      PressureInletAorticValve m_pressureInletAorticValve;
      VelocityInletAorticValve m_velocityInletAorticValve;

      inPressureFDNSwallValves m_inPressFDNSwallValves;
      
      ValveProfile m_valveProfile;
      velZero      m_velZero;
      velVesicle   m_velVesicle;
      velBall3D    m_velBall3D;
      
      ClosedValvePressure     m_closedValvePressure;
      ClosedValvePressureZero m_closedValvePressureZero;

      ElementField m_elemFieldPreviousVel;
      ElementField m_normalField;
  };
}


#endif
