//   ______ ______ _      _  _____      ______
//  |  ____|  ____| |    (_)/ ____|    |  ____|
//  | |__  | |__  | |     _| (___   ___| |__
//  |  __| |  __| | |    | |\___ \ / __|  __|
//  | |    | |____| |____| |____) | (__| |____
//  |_|    |______|______|_|_____/ \___|______|
//  Finite Elements for Life Sciences and Engineering
//
//  License:     LGL2.1 License
//  FELiScE default license: LICENSE in root folder
//
//  Main authors:    M. Fernandez & F.M. Gerosa
//

#include "userFluid.hpp"
#include "Core/felisceTransient.hpp"
#include "Core/felisceParam.hpp"
#include "Core/fluidToMaster.hpp"
#include "Model/NitscheXFEMModel.hpp"

using namespace felisce;

int main(const int argc, const char** argv)
{
  // read command line and data file
  const std::size_t instanceIndex = 0; // It is already 0 by default, this is an example
  auto& r_par = FelisceParam::instance(instanceIndex);
  auto opt = r_par.initialize(argc, argv);
  FelisceTransient fstransient;

  // copy the data file in the Post-processing folder
  filesystemUtil::copyFileSomewhereElse(opt.dataFileName(), FelisceParam::instance().resultDir + "/dataFile");
 
  // create model and solver
  NitscheXFEMModel model;
  model.initializeModel(opt,fstransient);
  std::vector<LinearProblem*> linearPb; 
  linearPb.push_back(new UserFluid()); 
  model.initializeLinearProblem(linearPb); 

  std::vector<int> nodesSigma; 
  std::vector<double> disp_master, disp;
  std::vector<double> velo_master, vel;
  std::vector<double> lumpM_master;
  std::vector<double> residual/*, res*/;
  std::vector<double> veloF;

  int nbCoor = model.linearProblem(0)->dimension();

  FluidToMaster fluidToMaster(-1, nbCoor, nodesSigma, disp_master, velo_master, lumpM_master, residual, veloF);

  int rank, nbinter, info, status, hasLumpedMass;
  MPI_Comm_rank(MpiInfo::petscComm(), &rank);
  if (rank == 0)  {
    fluidToMaster.setMaster();
    fluidToMaster.rvFromMasterFSI(140);
    fluidToMaster.sdInterfCoorToMasterFSI();
    hasLumpedMass = fluidToMaster.hasLumpedMass();
    nbinter = fluidToMaster.nbNodes();
  }      

  info = MPI_Bcast(&nbinter,      1, MPI_INT, 0, MpiInfo::petscComm());
  info = MPI_Bcast(&hasLumpedMass,1, MPI_INT, 0, MpiInfo::petscComm());
  
  disp.resize(nbCoor*nbinter,0);
  vel.resize(nbCoor*nbinter, 0);
  // res.resize(nbCoor*nbinter, 0);  
  if (rank != 0 ) {
    disp_master.resize(nbCoor*nbinter);
    lumpM_master.resize(nbCoor*nbinter);
    residual.resize(nbCoor*nbinter);
  }
  // std::vector<PetscInt> ao_disp(nbCoor*nbinter), ao_velo(nbCoor*nbinter);

  // PetscVector dispFluid, veloFluid, resFluid;
  
  LinearProblemNitscheXFEM* lpb = static_cast<LinearProblemNitscheXFEM*>(model.linearProblem(0));

  model.setInterfaceExchangedData(disp.data());
  lpb->setInterfaceExchangedData(vel.data(),residual.data());

  UserFluid* ptr = static_cast<UserFluid*>(model.linearProblem(0));
  double mean_val = 0;
  auto   mean_siz = std::max_element(r_par.meanLabel.begin(), r_par.meanLabel.end());
  std::vector<double> mean_vec( ( mean_siz == r_par.meanLabel.end() ? 0 : *mean_siz ) +1 );


  // Loop time
  do {

    if ( rank == 0 ) {
      fluidToMaster.rvFromMasterFSI(110);
      status =  fluidToMaster.status();
      model.fstransient()->timeStep = fluidToMaster.timeStep();
      vel = velo_master;
      disp = disp_master;
    }
    info = MPI_Bcast(&status,                        1, MPI_INT,    0, MpiInfo::petscComm());
    info = MPI_Bcast(&model.fstransient()->timeStep, 1, MPI_DOUBLE, 0, MpiInfo::petscComm());
    info = MPI_Bcast(disp.data(), nbCoor*nbinter, MPI_DOUBLE, 0, MpiInfo::petscComm());
    info = MPI_Bcast(vel.data(),  nbCoor*nbinter, MPI_DOUBLE, 0, MpiInfo::petscComm());
    // info = MPI_Bcast(res.data(),  nbCoor*nbinter, MPI_DOUBLE, 0, MpiInfo::petscComm());

    switch ( status  ) {     

      case -1: // Normal exit
        //model.writeSolution();   
        std::cout << "Normal fluid exit\n" << std::flush;
        model.writeSolutionAndMeshes();
        if( FelisceParam::verbose() > 4 )
          lpb->printStructStress(model.fstransient()->iteration, fluidToMaster.timeStep());
        exit(1);
        break;

      case 1: // New time step
        // moving mesh and solving fluid
        model.forward();

        mean_val = ptr->mean(mean_vec); 

        MPI_Allreduce(MPI_IN_PLACE, mean_vec.data(), mean_vec.size(), MPI_DOUBLE, MPI_SUM, MpiInfo::petscComm());
        MPI_Allreduce(MPI_IN_PLACE, &mean_val, 1, MPI_DOUBLE, MPI_SUM, MpiInfo::petscComm());
        
        if ( rank == 0 ) {

          for (auto it : FelisceParam::instance().meanLabel )
            std::cout << "Mean flux on label " << std::scientific << it << " is " << mean_vec[it] << std::endl;

          std::cout << "Mean flux = " << std::scientific << mean_val << " (iteration " << model.fstransient()->iteration << " ) ";
          if ( std::abs(mean_val) > 1e-8 ) std::cout << " WARNING!!! ";
          std::cout << std::endl;
        }
        break;

      case 0: // Inner fluid solver evaluation
        model.solveLinearized( false );
        break;

      case 2: // Inner fluid solver evaluation
        model.solveLinearized( true );
        break;
        
      default:
        std::cout << " This status number is not allowed.\n" << std::endl << std::flush;
        exit(1);	
    }    
    
    if ( rank == 0 ) {  
      // sending fluid force to master     
      std::cout << " Fluid is ready to send master " << std::endl;
      fluidToMaster.sdToMasterFSI();
    }
  } while ( 1 );  // close infinite do
}

